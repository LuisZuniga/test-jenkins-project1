<?php

class Lote {

    private $idlote;
    private $numeroLote;

    function __construct($idlote, $numeroLote) {
        $this->idlote = $idlote;
        $this->numeroLote =  $numeroLote;   
    }

    function getIdlote() {
        return $this->idlote;
    }

    function getNumeroLote() {
        return $this->numeroLote;
    }

    function setIdlote($idlote) {
        $this->idlote = $idlote;
    }

    function setNumeroLote($numeroLote) {
        $this->numeroLote = $numeroLote;
    }
}