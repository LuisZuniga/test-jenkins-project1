<?php

class Seccion {

    private $numeroseccion;
    private $numerolote;
    private $planta;
    private $plantaActual;
    private $cantidad;
    private $fechaSiembra;
    private $fechaExtraccion;

    function __construct(){
		$params = func_get_args();
		$num_params = func_num_args();
		$funcion_constructor ='__construct'.$num_params;
		if (method_exists($this,$funcion_constructor)) {
			call_user_func_array(array($this,$funcion_constructor),$params);
		}
    }

    function __construct6($planta,$numeroseccion,$numerolote,$cantidad,$fechaSiembra,$fechaExtraccion) {
        $this->numeroseccion =  $numeroseccion;
        $this->numerolote = $numerolote;
        $this->planta = $planta;
        $this->cantidad = $cantidad;
        $this->fechaSiembra = $fechaSiembra;
        $this->fechaExtraccion = $fechaExtraccion;       
    }

    function __construct7($planta,$plantaActual,$numeroseccion,$numerolote,$cantidad,$fechaSiembra,$fechaExtraccion) {
        $this->numeroseccion =  $numeroseccion;
        $this->numerolote = $numerolote;
        $this->planta = $planta;
        $this->plantaActual = $plantaActual;
        $this->cantidad = $cantidad;
        $this->fechaSiembra = $fechaSiembra;
        $this->fechaExtraccion = $fechaExtraccion;       
    }

    function getNumeroseccion() {
        return $this->numeroseccion;
    }

    function getNumerolote() {
        return $this->numerolote;
    }

    function getPlanta() {
        return $this->planta;
    }

    function getPlantaActual() {
        return $this->plantaActual;
    }

    function getCantidad() {
        return $this->cantidad;
    }

    function getFechaSiembra() {
        return $this->fechaSiembra;
    }

    function getFechaExtraccion() {
        return $this->fechaExtraccion;
    }

    function setNumeroseccion($numeroseccion) {
        $this->numeroseccion = $numeroseccion;
    }

    function setNumerolote($numerolote) {
        $this->numerolote = $numerolote;
    }

    function setPlanta($planta) {
        $this->planta = $planta;
    }

    function setPlantaActual($plantaActual) {
        $this->plantaActual = $plantaActual;
    }

    function setCantidad($cantidad) {
        $this->cantidad = $cantidad;
    }

    function setFechaSiembra($fechaSiembra) {
        $this->fechaSiembra = $fechaSiembra;
    }

    function setFechaExtraccion($fechaExtraccion) {
        $this->fechaExtraccion = $fechaExtraccion;
    }


}
