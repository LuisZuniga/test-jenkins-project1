<?php

class Acodo {
    private $idAcodo;
    private $idLote;
    private $idSeccion;
    private $idPlanta;
    private $cantidad;
    private $fecha;
    private $nombrePlanta;
    
    
    function __construct()
	{
		//obtengo un array con los parámetros enviados a la función
		$params = func_get_args();
		//saco el número de parámetros que estoy recibiendo
		$num_params = func_num_args();
		//cada constructor de un número dado de parámtros tendrá un nombre de
		//función
		//atendiendo al siguiente modelo __construct1() __construct2()...
		$funcion_constructor ='__construct'.$num_params;
		//compruebo si hay un constructor con ese número de parámetros
		if (method_exists($this,$funcion_constructor)) {
			//si existía esa función, la invoco, reenviando los parámetros que recibí en el constructor original
			call_user_func_array(array($this,$funcion_constructor),$params);
		}
	}
	//ahora declaro una serie de métodos constructores que aceptan diversos números de
	//parámetros
    function __construct6($idAcodo, $idLote, $idSeccion, $idPlanta, $cantidad, $fecha) {
        $this->idAcodo = $idAcodo;
        $this->idLote = $idLote;
        $this->idSeccion = $idSeccion;
        $this->idPlanta = $idPlanta;
        $this->cantidad = $cantidad;
        $this->fecha = $fecha;
    }

    function __construct7($idAcodo, $idLote, $idSeccion, $idPlanta, $cantidad, $fecha, $nombrePlanta) {
        $this->idAcodo = $idAcodo;
        $this->idLote = $idLote;
        $this->idSeccion = $idSeccion;
        $this->idPlanta = $idPlanta;
        $this->cantidad = $cantidad;
        $this->fecha = $fecha;
        $this->nombrePlanta = $nombrePlanta;
    }
    
    function setIdAcodo($idAcodo) {
        $this->idAcodo = $idAcodo;
    }

    function setIdLote($idLote) {
        $this->idLote = $idLote;
    }

    function setIdSeccion($idSeccion) {
        $this->idSeccion = $idSeccion;
    }

    function setIdPlanta($idPlanta) {
        $this->idPlanta = $idPlanta;
    }

    function setCantidad($cantidad) {
        $this->cantidad = $cantidad;
    }

    function setFecha($fecha) {
        $this->fecha = $fecha;
    }

    function setNombrePlanta($nombrePlanta) {
        $this->nombrePlanta = $nombrePlanta;
    }

    function getIdAcodo() {
        return $this->idAcodo;
    }

    function getIdLote() {
        return $this->idLote;
    }

    function getIdSeccion() {
        return $this->idSeccion;
    }

    function getIdPlanta() {
        return $this->idPlanta;
    }

    function getCantidad() {
        return $this->cantidad;
    }

    function getFecha() {
        return $this->fecha;
    }

    function getNombrePlanta() {
        return $this->nombrePlanta;
    }

}
