<?php

class Quimico {
    private $idQuimico;
    private $nombre;
    private $idTipoQuimico;
    private $tipoQuimico;
    private $toxicidad;
    private $cantidad;
    private $precio;

    function __construct(){
		$params = func_get_args();
		$num_params = func_num_args();
		$funcion_constructor ='__construct'.$num_params;
		if (method_exists($this,$funcion_constructor)) {
			call_user_func_array(array($this,$funcion_constructor),$params);
		}
    }

    function __construct4($idQuimico, $nombre, $cantidad, $precio) {
        $this->idQuimico = $idQuimico;
        $this->nombre = $nombre;
        $this->cantidad = $cantidad;
        $this->precio = $precio;
    }

    function __construct6($idQuimico,$nombre,$idTipoQuimico,$toxicidad,$cantidad,$precio) {
        $this->idQuimico = $idQuimico;
        $this->nombre = $nombre;
        $this->idTipoQuimico = $idTipoQuimico;
        $this->toxicidad = $toxicidad;
        $this->cantidad = $cantidad;
        $this->precio = $precio;
    }
    
    function __construct7($idQuimico,$nombre,$idTipoQuimico,$tipoQuimico,$toxicidad,$cantidad,$precio) {
        $this->idQuimico = $idQuimico;
        $this->nombre = $nombre;
        $this->idTipoQuimico = $idTipoQuimico;
        $this->tipoQuimico = $tipoQuimico;
        $this->toxicidad = $toxicidad;
        $this->cantidad = $cantidad;
        $this->precio = $precio;
    }
    
    function getIdQuimico() {
        return $this->idQuimico;
    }

    function getNombre() {
        return $this->nombre;
    }

    function getIdTipoQuimico() {
        return $this->idTipoQuimico;
    }

    function getTipoQuimico() {
        return $this->tipoQuimico;
    }

    function getToxicidad() {
        return $this->toxicidad;
    }

    function getCantidad() {
        return $this->cantidad;
    }

    function getPrecio() {
        return $this->precio;
    }

    function setIdQuimico($idQuimico) {
        $this->idQuimico = $idQuimico;
    }

    function setNombre($nombre) {
        $this->nombre = $nombre;
    }

    function setIdTipoQuimico($idTipoQuimico) {
        $this->idTipoQuimico = $idTipoQuimico;
    }

    function setTipoQuimico($tipoQuimico) {
        $this->tipoQuimico = $tipoQuimico;
    }

    function setToxicidad($toxicidad) {
        $this->toxicidad = $toxicidad;
    }

    function setCantidad($cantidad) {
        $this->cantidad = $cantidad;
    }

    function setPrecio($precio) {
        $this->precio = $precio;
    }


    
}
