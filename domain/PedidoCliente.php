<?php

class PedidoCliente
{

    public $idPedidoCliente;
    public $idPedido;
    public $idCliente;
    public $nombreCliente;
    public $cedulaCliente;
    // Vector de plantas del pedido de este cliente.
    public $pedidoClientePlanta;

    function __construct()
    {
        $params = func_get_args();
        $num_params = func_num_args();
        $funcion_constructor = '__construct' . $num_params;
        if (method_exists($this, $funcion_constructor)) {
            call_user_func_array(array($this, $funcion_constructor), $params);
        }
    }

    function __construct4($idPedido, $idPedidoCliente, $idCliente, $pedidoClientePlanta)
    {
        $this->idPedido = $idPedido;
        $this->idPedidoCliente = $idPedidoCliente;
        $this->idCliente = $idCliente;
        $this->pedidoClientePlanta = $pedidoClientePlanta;
    }

    function __construct3($idPedidoCliente, $nombreCliente, $cedulaCliente)
    {
        $this->idPedidoCliente = $idPedidoCliente;
        $this->nombreCliente = $nombreCliente;
        $this->cedulaCliente = $cedulaCliente;
    }


    function setIdPedido($idPedido)
    {
        $this->idPedido = $idPedido;
    }

    function setIdPedidoCliente($idPedidoCliente)
    {
        $this->idPedidoCliente = $idPedidoCliente;
    }

    function setIdCliente($idCliente)
    {
        $this->idCliente = $idCliente;
    }

    function setPedidoClientePlanta($pedidoClientePlanta)
    {
        $this->pedidoClientePlanta = $pedidoClientePlanta;
    }

    function setCedulaCliente($cedulaCliente)
    {
        $this->cedulaCliente = $cedulaCliente;
    }

    function setNombreCliente($nombreCliente)
    {
        $this->nombreCliente = $nombreCliente;
    }

    function getIdPedido()
    {
        return $this->idPedido;
    }

    function getIdPedidoCliente()
    {
        return $this->idPedidoCliente;
    }

    function getIdCliente()
    {
        return $this->idCliente;
    }

    function getPedidoClientePlanta()
    {
        return $this->pedidoClientePlanta;
    }

    function getCedulaCliente()
    {
        return $this->cedulaCliente;
    }

    function getNombreCliente()
    {
        return $this->nombreCliente;
    }
}
