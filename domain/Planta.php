<?php

Class Planta{

    private $idPlanta;
    private $nombreComun;
    private $nombreCientifico;
    private $precioUnitario;
    private $cantidad;

    function __construct($idPlanta, $nombreComun, $nombreCientifico, $cantidad, $precioUnitario){
        $this->idPlanta = $idPlanta;
        $this->nombreComun = $nombreComun;
        $this->nombreCientifico = $nombreCientifico;
        $this->precioUnitario = $precioUnitario;
        $this->cantidad = $cantidad;
    }

    function setIdPlanta($idPlanta){
        $this->idPlanta = $idPlanta;
    }

    function getIdPlanta(){
        return $this->idPlanta;
    }

    function setNombreComun($nombreComun){
        $this->nombreComun = $nombreComun;
    }

    function getNombreComun(){
        return $this->nombreComun;
    }

    function setNombreCientifico($nombreCientifico){
        $this->nombreCientifico = $nombreCientifico;
    }

    function getNombreCientifico(){
        return $this->nombreCientifico;
    }

    function setCantidad($cantidad){
        $this->cantidad = $cantidad;
    }

    function getCantidad(){
        return $this->cantidad;
    }

    function setPrecioUnitario($precioUnitario){
        $this->precioUnitario = $precioUnitario;
    }

    function getPrecioUnitario(){
        return $this->precioUnitario;
    }

}