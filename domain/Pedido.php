<?php

class Pedido {
    private $idPedido;
    private $montoTotal;
    private $fechaEnvio;
    private $pedidoCliente;
    
    
    function __construct($idPedido, $montoTotal, $fechaEnvio, $pedidoCliente) {
        $this->idPedido = $idPedido;
        $this->montoTotal = $montoTotal;
        $this->fechaEnvio = $fechaEnvio;
        $this->pedidoCliente = $pedidoCliente;
    }

    
    function setIdPedido($idPedido) {
        $this->idPedido = $idPedido;
    }

    function setMontoTotal($montoTotal) {
        $this->montoTotal = $montoTotal;
    }

    function setFechaEnvio($fechaEnvio) {
        $this->fechaEnvio = $fechaEnvio;
    }

    function setPedidoCliente($pedidoCliente) {
        $this->pedidoCliente = $pedidoCliente;
    }

    function getIdPedido() {
        return $this->idPedido;
    }

    function getMontoTotal() {
        return $this->montoTotal;
    }

    function getFechaEnvio() {
        return $this->fechaEnvio;
    }

    function getPedidoCliente() {
        return $this->pedidoCliente;
    }


}
