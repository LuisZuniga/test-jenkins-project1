<?php

class Fumigacion {
    private $idFumigacion;
    private $dosis;
    private $fechaFumigacion;
    private $idLote;
    private $idQuimico;
    private $nombreQuimico;
    private $idSeccion;
    private $idColaborador;
    private $nombreColaborador;

    function __construct(){
		$params = func_get_args();
		$num_params = func_num_args();
		$funcion_constructor ='__construct'.$num_params;
		if (method_exists($this,$funcion_constructor)) {
			call_user_func_array(array($this,$funcion_constructor),$params);
		}
    }

    function __construct7($idFumigacion, $idSeccion, $idLote, $idQuimico, $idColaborador, $dosis, $fechaFumigacion) {
        $this->idFumigacion = $idFumigacion;
        $this->idSeccion = $idSeccion;
        $this->idLote = $idLote;
        $this->idQuimico = $idQuimico;
        $this->idColaborador = $idColaborador;
        $this->dosis = $dosis;
        $this->fechaFumigacion = $fechaFumigacion;
    }
    
    function __construct9($idFumigacion, $idSeccion, $idLote, $idQuimico, $nombreQuimico, $idColaborador, $nombreColaborador, $dosis, $fechaFumigacion) {
        $this->idFumigacion = $idFumigacion;
        $this->idSeccion = $idSeccion;
        $this->idLote = $idLote;
        $this->idQuimico = $idQuimico;
        $this->nombreQuimico = $nombreQuimico;
        $this->idColaborador = $idColaborador;
        $this->nombreColaborador = $nombreColaborador;
        $this->dosis = $dosis;
        $this->fechaFumigacion = $fechaFumigacion;
    }

    
    function setIdFumigacion($idFumigacion) {
        $this->idFumigacion = $idFumigacion;
    }

    function setDosis($dosis) {
        $this->dosis = $dosis;
    }

    function setFechaFumigacion($fechaFumigacion) {
        $this->fechaFumigacion = $fechaFumigacion;
    }

    function setIdLote($idLote) {
        $this->idLote = $idLote;
    }

    function setIdSeccion($idSeccion) {
        $this->idSeccion = $idSeccion;
    }

    function setIdQuimico($idQuimico) {
        $this->idQuimico = $idQuimico;
    }

    function setNombreQuimico($nombreQuimico) {
        $this->nombreQuimico = $nombreQuimico;
    }

    function setIdColaborador($idColaborador) {
        $this->idColaborador = $idColaborador;
    }

    function setNombreColaborador($nombreColaborador) {
        $this->nombreColaborador = $nombreColaborador;
    }

    function getIdFumigacion() {
        return $this->idFumigacion;
    }

    function getDosis() {
        return $this->dosis;
    }

    function getFechaFumigacion() {
        return $this->fechaFumigacion;
    }

    function getIdLote() {
        return $this->idLote;
    }

    function getIdQuimico() {
        return $this->idQuimico;
    }

    function getNombreQuimico() {
        return $this->nombreQuimico;
    }

    function getIdSeccion() {
        return $this->idSeccion;
    }

    function getIdColaborador() {
        return $this->idColaborador;
    }

    function getNombreColaborador() {
        return $this->nombreColaborador;
    }
}
