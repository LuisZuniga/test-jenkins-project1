<?php

class PedidoClientePlanta {

    public $idPedidoClientePlanta;
    public $idPlanta;
    public $idClientePedido;
    public $cantidad;
    public $nombrePlanta;
    public $precio;

    function __construct(){
		$params = func_get_args();
		$num_params = func_num_args();
		$funcion_constructor ='__construct'.$num_params;
		if (method_exists($this,$funcion_constructor)) {
			call_user_func_array(array($this,$funcion_constructor),$params);
		}
    }

    function __construct3($nombrePlanta, $precio, $cantidad) {
        $this->nombrePlanta = $nombrePlanta;
        $this->precio = $precio;
        $this->cantidad = $cantidad;
    }
    
    function __construct4($idPlanta, $idPedidoClientePlanta, $idClientePedido, $cantidad) {
        $this->idPlanta = $idPlanta;
        $this->idPedidoClientePlanta = $idPedidoClientePlanta;
        $this->idClientePedido = $idClientePedido;
        $this->cantidad = $cantidad;
    }

    function __construct5($idPlanta, $idPedidoClientePlanta, $idClientePedido, $cantidad, $nombrePlanta) {
        $this->idPlanta = $idPlanta;
        $this->idPedidoClientePlanta = $idPedidoClientePlanta;
        $this->idClientePedido = $idClientePedido;
        $this->cantidad = $cantidad;
        $this->nombrePlanta = $nombrePlanta;
    }

    
    function setIdPlanta($idPlanta) {
        $this->idPlanta = $idPlanta;
    }

    function setIdPedidoClientePlanta($idPedidoClientePlanta) {
        $this->idPedidoClientePlanta = $idPedidoClientePlanta;
    }

    function setIdClientePedido($idClientePedido) {
        $this->idClientePedido = $idClientePedido;
    }

    function setCantidad($cantidad) {
        $this->cantidad = $cantidad;
    }

    function setNombrePlanta($nombrePlanta) {
        $this->nombrePlanta = $nombrePlanta;
    }

    function setPrecio($precio) {
        $this->precio = $precio;
    }

    function getIdPlanta() {
        return $this->idPlanta;
    }

    function getIdPedidoClientePlanta() {
        return $this->idPedidoClientePlanta;
    }

    function getIdClientePedido() {
        return $this->idClientePedido;
    }

    function getCantidad() {
        return $this->cantidad;
    }

    function getNombrePlanta() {
        return $this->nombrePlanta;
    }

    function getPrecio() {
        return $this->precio;
    }
}

