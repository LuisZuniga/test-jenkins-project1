<?php

class Colaborador {
    private $idColaborador;
    private $cedula;
    private $nombre;
    private $apellido1;
    private $apellido2;
    private $direccion;
    private $telefono;

    function __construct(){
		$params = func_get_args();
		$num_params = func_num_args();
		$funcion_constructor ='__construct'.$num_params;
		if (method_exists($this,$funcion_constructor)) {
			call_user_func_array(array($this,$funcion_constructor),$params);
		}
    }

    function __construct6($cedula, $nombre, $apellido1, $apellido2, $direccion, $telefono) {
        $this->cedula = $cedula;
        $this->nombre = $nombre;
        $this->apellido1 = $apellido1;
        $this->apellido2 = $apellido2;
        $this->direccion = $direccion;
        $this->telefono = $telefono;
    }
    
    function __construct7($idColaborador, $cedula, $nombre, $apellido1, $apellido2, $direccion, $telefono) {
        $this->idColaborador = $idColaborador;
        $this->cedula = $cedula;
        $this->nombre = $nombre;
        $this->apellido1 = $apellido1;
        $this->apellido2 = $apellido2;
        $this->direccion = $direccion;
        $this->telefono = $telefono;
    }

    function getIdColaborador() {
        return $this->idColaborador;
    }

    function getCedula() {
        return $this->cedula;
    }

    function getNombre() {
        return $this->nombre;
    }

    function getApellido1() {
        return $this->apellido1;
    }

    function getApellido2() {
        return $this->apellido2;
    }

    function getDireccion() {
        return $this->direccion;
    }

    function getTelefono() {
        return $this->telefono;
    }

    function setIdColaborador($idColaborador) {
        $this->idColaborador = $idColaborador;
    }
    
    function setCedula($cedula) {
        $this->cedula = $cedula;
    }

    function setNombre($nombre) {
        $this->nombre = $nombre;
    }

    function setApellido1($apellido1) {
        $this->apellido1 = $apellido1;
    }

    function setApellido2($apellido2) {
        $this->apellido2 = $apellido2;
    }

    function setDireccion($direccion) {
        $this->direccion = $direccion;
    }

    function setTelefono($telefono) {
        $this->telefono = $telefono;
    }


}
