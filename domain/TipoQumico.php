<?php

class TipoQuimico {

 
    private $idTipoQuimico;
    private $nombre;

    function __construct($idTipoQuimico,$nombre) {
        $this->idTipoQuimico = $idTipoQuimico;
        $this->nombre = $nombre;
    }

    function getIdTipoQuimico() {
        return $this->idTipoQuimico;
    }

    function getNombre() {
        return $this->nombre;
    }

    function setIdTipoQuimico($idTipoQuimico) {
        $this->idTipoQuimico = $idTipoQuimico;
    }

    function setNombre($nombre) {
        $this->nombre = $nombre;
    }
}
