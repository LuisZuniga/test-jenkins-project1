<?php

class Cliente {
    private $idCliente;
    private $cedula;
    private $nombre;
    private $apellido1;
    private $apellido2;
    private $direccion;
    private $telefono;
    
    
    function __construct($idCliente, $cedula, $nombre, $apellido1, $apellido2, $direccion, $telefono) {
        $this->idCliente = $idCliente;
        $this->cedula = $cedula;
        $this->nombre = $nombre;
        $this->apellido1 = $apellido1;
        $this->apellido2 = $apellido2;
        $this->direccion = $direccion;
        $this->telefono = $telefono;
    }

    
    function setIdCliente($idCliente) {
        $this->idCliente = $idCliente;
    }

    function setCedula($cedula) {
        $this->cedula = $cedula;
    }

    function setNombre($nombre) {
        $this->nombre = $nombre;
    }

    function setApellido1($apellido1) {
        $this->apellido1 = $apellido1;
    }

    function setApellido2($apellido2) {
        $this->apellido2 = $apellido2;
    }

    function setDireccion($direccion) {
        $this->direccion = $direccion;
    }

    function setTelefono($telefono) {
        $this->telefono = $telefono;
    }

    function getIdCliente() {
        return $this->idCliente;
    }

    function getCedula() {
        return $this->cedula;
    }

    function getNombre() {
        return $this->nombre;
    }

    function getApellido1() {
        return $this->apellido1;
    }

    function getApellido2() {
        return $this->apellido2;
    }

    function getDireccion() {
        return $this->direccion;
    }

    function getTelefono() {
        return $this->telefono;
    }


}
