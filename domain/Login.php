<?php

    class Login{

        private $idUsuario;
        private $username;
        private $password;
        private $email;
        private $notificacion;

        function __construct($idUsuario, $username,$password, $email, $notificacion){
            $this->username = $username;
            $this->password = $password;
        }

        function setIdUsuario($idUsuario) {
            $this->idUsuario = $idUsuario;
        }

        function getIdUsuario() {
            return $this->idUsuario;
        }
        
        function setUsername($username){
            $this->username = $username;
        }

        function getUsername(){
            return $this->username;
        }

        function setPassword($password){
            $this->password = $password;
        }

        function getPassword(){
            return $this->password;
        }

        function setEmail($email) {
            $this->email = $email;
        }

        function getEmail() {
            return $this->email;
        }

        function setNotificacion($notificacion) {
            $this->notificacion = $notificacion;
        }
    }