<?php

class Suministro {

    private $idSuministro;
    private $tipoSuministro;
    private $nombre;
    private $cantidad;

    function __construct(){
		$params = func_get_args();
		$num_params = func_num_args();
		$funcion_constructor ='__construct'.$num_params;
		if (method_exists($this,$funcion_constructor)) {
			call_user_func_array(array($this,$funcion_constructor),$params);
		}
    }

    function __construct3($idSuministro,$nombre, $cantidad) {
        $this->idSuministro = $idSuministro;
        $this->nombre = $nombre;       
        $this->cantidad = $cantidad;
    }

    function __construct4($idSuministro, $tipoSuministro, $nombre, $cantidad) {
        $this->idSuministro = $idSuministro;
        $this->tipoSuministro =  $tipoSuministro;
        $this->nombre = $nombre;       
        $this->cantidad = $cantidad;
    }

    function getIdSuministro() {
        return $this->idSuministro;
    }

    function getNombre() {
        return $this->nombre;
    }

    function getTipoSuministro() {
        return $this->tipoSuministro;
    }

    function getCantidad() {
        return $this->cantidad;
    }

    function setIdSuministro($idSuministro) {
        $this->idSuministro = $idSuministro;
    }

    function setNombre($nombre) {
        $this->nombre = $nombre;
    }

    function setTipoSuministro($tipoSuministro) {
        $this->tipoSuministro = $tipoSuministro;
    }

    function setCantidad($cantidad) {
        $this->cantidad = $cantidad;
    }
}
