<?php

include_once 'Data.php';
include '../domain/Quimico.php';

class QuimicoData extends Data
{

    public function mostrarTBQuimico()
    {
        $conn = $this->getConexion();
        $quimicos = [];
        if ($conn != null) {
            $query = "CALL sp_mostrar_quimicos()";
            $statement = $conn->prepare($query);
            $statement->execute();
            while ($row = $statement->fetch(PDO::FETCH_BOTH)) {
                $quimicoActual = new Quimico($row[0], $row[1], $row[2], $row[3], $row[4], $row[5], $row[6]);
                array_push($quimicos, $quimicoActual);
            }
            $conn = null;
        }
        return $quimicos;
    }

    public function insertarTBQuimico($quimico)
    {
        $conn = $this->getConexion();
        $result = 0;
        if ($conn != null) {
            $query = "CALL sp_insertar_quimico(?,?,?,?,?)";
            $statement = $conn->prepare($query);
            $quimico = [
                $quimico->getNombre(),
                $quimico->getIdTipoQuimico(),
                $quimico->getToxicidad(),
                $quimico->getCantidad(),
                $quimico->getPrecio()
            ];
            $result = $statement->execute($quimico);
            $conn = null;
        }
        return $result;
    }

    public function actualizarTBQuimico($quimico)
    {
        $conn = $this->getConexion();
        $result = 0;
        if ($conn != null) {
            $query = "CALL sp_modificar_quimico(?,?,?,?,?,?)";
            $statement = $conn->prepare($query);
            $quimico = [
                $quimico->getIdQuimico(),
                $quimico->getNombre(),
                $quimico->getIdTipoQuimico(),
                $quimico->getToxicidad(),
                $quimico->getCantidad(),
                $quimico->getPrecio()
            ];
            $result = $statement->execute($quimico);
            $conn = null;
        }
        return $result;
    }

    public function eliminarTBQuimico($idQuimico)
    {
        $conn = $this->getConexion();
        $result = 0;
        if ($conn != null) {
            $query = "CALL sp_eliminar_quimico(?)";
            $statement = $conn->prepare($query);
            $result = $statement->execute([$idQuimico]);
            $conn = null;
        }
        return $result;
    }

    public function mostrarQuimicosAgotados()
    {
        $conn = $this->getConexion();
        $quimicos = [];
        if ($conn != null) {
            $query = "CALL sp_mostrar_quimicos_agotados()";
            $statement = $conn->prepare($query);
            $statement->execute();
            while ($row = $statement->fetch(PDO::FETCH_BOTH)) {
                $quimicoActual = new Quimico($row[0], $row[1], $row[2], $row[3]);
                array_push($quimicos, $quimicoActual);
            }
            $conn = null;
        }
        return $quimicos;
    }

    // Me devuelve la cantidad de la tabla quimico
    public function getCantidadQuimico($idQuimico)
    {
        $conn = $this->getConexion();
        $cantidad = 0;
        if ($conn != null) {
            $query = "CALL sp_get_cantidad_quimico(?)";
            $statement = $conn->prepare($query);
            $statement->execute([$idQuimico]);
            if ($row = $statement->fetch(PDO::FETCH_BOTH)) {
                $cantidad = $row[0];
            }
            $conn = null;
        }
        return $cantidad;
    }

    public function actualizarCantidad($idQuimico, $cantidad)
    {
        $conn = $this->getConexion();
        $result = 0;
        if ($conn != null) {
            $query = "CALL sp_modificar_cantidad_quimico(?,?)";
            $statement = $conn->prepare($query);
            $datos = [
                $idQuimico,
                $cantidad
            ];
            $result = $statement->execute($datos);
            $conn = null;
        }
        return $result;
    }


    /////////////////////////////////////////////////////////////


    public function buscarTBQuimico($nombre)
    {
        $conn = $this->getConexion();
        $quimico = null;
        if ($conn != null) {
            $query = "SELECT * FROM tbquimico WhERE nombre = ? AND estado = 1 LIMIT 1;";
            $statement = $conn->prepare($query);
            $statement->execute([$nombre]);
            if ($row = $statement->fetch(PDO::FETCH_BOTH)) {
                $quimico = new Quimico($row[0], $row[1], $row[2], $row[3]);
            }
            $conn = null;
        }
        return $quimico;
    }

    public function getLastId()
    {
        $conn = $this->getConexion();
        $idQuimico = 0;
        if ($conn != null) {
            $query = "SELECT MAX(idquimico) AS idquimico FROM tbquimico;";
            $statement = $conn->prepare($query);
            $statement->execute();
            if ($row = $statement->fetch(PDO::FETCH_BOTH)) {
                $idQuimico = $row[0];
            }
            $conn = null;
        }
        return $idQuimico;
    }

    public function getLastIdTQ()
    {
        $conn = $this->getConexion();
        $id = 0;
        if ($conn != null) {
            $query = "SELECT MAX(idtipoquimico) AS idtipoquimico FROM tbtipoquimico;";
            $statement = $conn->prepare($query);
            $statement->execute();
            if ($row = $statement->fetch(PDO::FETCH_BOTH)) {
                $id = $row[0];
            }
            $conn = null;
        }
        return $id;
    }


    /*public function insertarTBQuimico($quimico) {
        $conn = mysqli_connect($this->server, $this->user, $this->password, $this->db);
        $conn->set_charset('utf8');
        $query = "CALL sp_insertar_quimico(?,?,?)";
        $statement = $conn->prepare($query);
        $statement->bind_param("sii",$nombre,$cantidad,$precio);
        $nombre = $quimico->getNombre();
        $cantidad = $quimico->getCantidad();
        $precio = $quimico->getPrecio();
        $result = $statement->execute();
        $statement->close();
        mysqli_close($conn);
        return $result;
    }*/

    /*public function actualizarTBQuimico($quimico)
    {
        $conn = mysqli_connect($this->server, $this->user, $this->password, $this->db);
        $conn->set_charset('utf8');
        $query = "CALL sp_modificar_quimico(?,?,?,?)";
        $statement = $conn->prepare($query);
        $statement->bind_param("isii", $idQuimico, $nombre, $cantidad, $precio);
        $idQuimico = $quimico->getIdQuimico();
        $nombre = $quimico->getNombre();
        $cantidad = $quimico->getCantidad();
        $precio = $quimico->getPrecio();
        $result = $statement->execute();
        $statement->close();
        mysqli_close($conn);
        return $result;
    }*/

    /*public function eliminarTBQuimico($idQuimico)
    {
        $conn = mysqli_connect($this->server, $this->user, $this->password, $this->db);
        $conn->set_charset('utf8');
        $query = "CALL sp_eliminar_quimico(?)";
        $statement = $conn->prepare($query);
        $statement->bind_param("i", $idQuimico);
        $result = $statement->execute();
        $statement->close();
        mysqli_close($conn);
        return $result;
    }*/

    /*public function mostrarTBQuimico()
    {
        $conn = mysqli_connect($this->server, $this->user, $this->password, $this->db);
        $conn->set_charset('utf8');

        /*$query = "CALL sp_mostrar_quimicos()";
        $statement = $conn->prepare($query);
        $statement->execute();
        $result = $statement->get_result();
        $statement->close();
        $query = "SELECT * FROM tbquimico;";
        $result = mysqli_query($conn, $query);
        mysqli_close($conn);
        $quimicos = [];
        while ($row = mysqli_fetch_array($result)) {
            $quimicoActual = new Quimico($row[0], $row[1], $row[2], $row[3]);
            array_push($quimicos, $quimicoActual);
        }
        return $quimicos;
    }*/

    /*public function mostrarQuimicosAgotados()
    {
        $conn = mysqli_connect($this->server, $this->user, $this->password, $this->db);
        $conn->set_charset('utf8');
        /*$query = "CALL sp_mostrar_quimicos_agotados()";
        $statement = $conn->prepare($query);
        $statement->execute();
        $result = $statement->get_result();
        $statement->close();
        $query = "SELECT * FROM tbquimico;";
        $result = mysqli_query($conn, $query);
        mysqli_close($conn);
        $quimicos = [];
        while ($row = mysqli_fetch_array($result)) {
            $quimicoActual = new Quimico($row[0], $row[1], $row[2], $row[3]);
            array_push($quimicos, $quimicoActual);
        }
        return $quimicos;
    }*/

    /*public function buscarTBQuimico($nombre, $inicio, $limite)
    {
        $conn = mysqli_connect($this->server, $this->user, $this->password, $this->db);
        $conn->set_charset('utf8');

        $querySelect = "SELECT * FROM tbquimico WhERE nombre LIKE CONCAT('%','" . $nombre . "','%') OR cantidad LIKE CONCAT('%','" . $nombre . "','%')  OR precio LIKE CONCAT('%','" . $nombre . "','%') ORDER BY nombre Limit $inicio,$limite;";
        $result = mysqli_query($conn, $querySelect);
        mysqli_close($conn);
        $quimicos = [];
        while ($row = mysqli_fetch_array($result)) {
            $quimicoActual = new Quimico($row['idquimico'], $row['nombre'], $row['cantidad'], $row['precio']);
            array_push($quimicos, $quimicoActual);
        }
        return $quimicos;
    }*/

    /*public function getTotalRegistros()
    {
        $conn = mysqli_connect($this->server, $this->user, $this->password, $this->db);
        $conn->set_charset('utf8');
        $query = "SELECT COUNT(*) as cantregistros FROM tbquimico;";
        $result = mysqli_query($conn, $query);
        mysqli_close($conn);
        $cantidadRegistros = 0;
        if ($row = mysqli_fetch_array($result)) {
            $cantidadRegistros = $row['cantregistros'];
        }
        return $cantidadRegistros;
    }

    public function getTotalRegistrosBuscados($cadena)
    {
        $conn = mysqli_connect($this->server, $this->user, $this->password, $this->db);
        $conn->set_charset('utf8');
        $query = "SELECT COUNT(*) as cantregistros FROM tbquimico WHERE nombre like concat('%','" . $cadena . "','%');";
        $result = mysqli_query($conn, $query);
        mysqli_close($conn);
        $cantidadRegistros = 0;
        if ($row = mysqli_fetch_array($result)) {
            $cantidadRegistros = $row['cantregistros'];
        }
        return $cantidadRegistros;
    }

    public function mostrarRegistrosPaginados($inicio, $limite)
    {
        $conn = mysqli_connect($this->server, $this->user, $this->password, $this->db);
        $conn->set_charset('utf8');
        $query = "SELECT * FROM tbquimico ORDER BY nombre Limit $inicio,$limite;";
        $result = mysqli_query($conn, $query);
        mysqli_close($conn);
        $quimicos = [];
        while ($row = mysqli_fetch_array($result)) {
            $quimicoActual = new Quimico($row['idquimico'], $row['nombre'], $row['cantidad'], $row['precio']);
            array_push($quimicos, $quimicoActual);
        }
        return $quimicos;
    }*/

    /*public function getLastId()
    {
        $conn = mysqli_connect($this->server, $this->user, $this->password, $this->db);
        $conn->set_charset('utf8');

        $querySelect = "SELECT MAX(idquimico) AS idquimico FROM tbquimico;";
        $result = mysqli_query($conn, $querySelect);
        mysqli_close($conn);
        $idquimico = 0;
        if ($row = mysqli_fetch_array($result)) {
            $idquimico = $row['idquimico'];
        }
        return $idquimico;
    }*/

    // Me devuelve la cantidad de la tabla quimico
    /*public function getCantidadQuimico($idQuimico)
    {
        $conn = mysqli_connect($this->server, $this->user, $this->password, $this->db);
        $conn->set_charset('utf8');
        $query = "SELECT cantidad FROM tbquimico WHERE idquimico = " . $idQuimico . ";";
        $result = mysqli_query($conn, $query);
        mysqli_close($conn);
        $cantidad = 0;
        if ($row = mysqli_fetch_array($result)) {
            $cantidad = $row['cantidad'];
        }
        return $cantidad;
    }

    public function actualizarCantidad($idQuimico, $cantidad)
    {
        $conn = mysqli_connect($this->server, $this->user, $this->password, $this->db);
        $conn->set_charset('utf8');
        $query = "CALL sp_modificar_cantidad_quimico(?,?)";
        $statement = $conn->prepare($query);
        $statement->bind_param("ii", $idQuimico, $cantidad);
        $result = $statement->execute();
        $statement->close();
        mysqli_close($conn);
        return $result;
    }*/
}
