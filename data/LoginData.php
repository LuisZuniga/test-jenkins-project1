<?php

include_once 'Data.php';
include '../domain/Login.php';

session_start();

class LoginData extends Data
{

    public function iniciarSession($user)
    {
        $conn = $this->getConexion();
        $query = "CALL sp_login(?);";
        $statement = $conn->prepare($query);
        $statement->execute([$user->getUsername()]);
        $rows = $statement->rowCount();
        if ($rows > 0) {
            while ($row = $statement->fetch(PDO::FETCH_BOTH)) {
                if ($row['pass'] == $user->getPassword()) {
                    $_SESSION['idusuario'] = $row['idusuario'];
                    $_SESSION['role'] = $row['role'];
                    $_SESSION['username'] = $row['username'];
                    $_SESSION['notificacion'] = $row['notificacion'];
                    return "../index.php";
                }
            }
            $conn = null;
            return $rows;
        } else {
            $conn = null;
            return $rows;
        }
    }

    public function actualizarNombreUsuario($username)
    {
        $conn = $this->getConexion();
        $result = 0;
        if ($conn != null) {
            $query = "CALL sp_modificar_username(?,?)";
            $statement = $conn->prepare($query);
            $data = [
                $username,
                $_SESSION['idusuario']
            ];
            $result = $statement->execute($data);
            $conn = null;
        }
        return $result;
    }

    // current pass and new pass
    public function actualizarContrasena($cPass, $nPass)
    {
        $conn = $this->getConexion();
        $result = 0;
        if ($conn != null) {
            //$query = "UPDATE tbusuario SET pass = ? WHERE idusuario = ?";
            $query = "CALL 	sp_cambiar_pswd(?,?,?)";
            $res = 10;
            $statement = $conn->prepare($query);
            $data = [
                $_SESSION['idusuario'],
                $cPass,
                $nPass,
            ];
            $statement->execute($data);
            $result = $statement->fetch()[0];
            $conn = null;
        }
        return $result;
    }

    public function actualizarEmail($email)
    {
        $conn = $this->getConexion();
        $result = 0;
        if ($conn != null) {
            $query = "CALL sp_modificar_email(?,?);";
            $statement = $conn->prepare($query);
            $data = [
                $email,
                $_SESSION['idusuario']
            ];
            $result = $statement->execute($data);
            $conn = null;
        }
        return $result;
    }

    public function actualizarNotificacion($notificacion)
    {
        $conn = $this->getConexion();
        $result = 0;
        if ($conn != null) {
            $query = "CALL sp_modificar_notificacion(?,?)";
            $statement = $conn->prepare($query);
            $data = [
                $notificacion,
                $_SESSION['idusuario']
            ];
            $result = $statement->execute($data);
            $conn = null;
        }
        return $result;
    }

    /* Cambiar a sp con el getresult */

    public function getTBUsuario()
    {
        $conn = $this->getConexion();
        $result = [];
        if ($conn != null) {
            $query = "CALL sp_mostrar_usuario(?)";
            $statement = $conn->prepare($query);
            $statement->execute([$_SESSION['idusuario']]);
            if ($row = $statement->fetch(PDO::FETCH_BOTH)) {
                $_SESSION['idusuario'] = $row['idusuario'];
                $_SESSION['role'] = $row['role'];
                $_SESSION['username'] = $row['username'];
                $_SESSION['notificacion'] = $row['notificacion'];
                $result = [
                    $row[0],
                    $row[1],
                    $row[2],
                    $row[4],
                    $row[5]
                ];
            }
        }
        return $result;
    }

    public function getPass($mail){
        $conn = $this->getConexion();
        $result = '';
        if($conn != null){
            $query = 'CALL sp_get_pass(?)';
            $statement = $conn->prepare($query);
            $statement->execute([$mail]);
            if($row = $statement->fetch(PDO::FETCH_BOTH)){
                $result = $row[0];
            }
        }
        return $result;
    }


}
