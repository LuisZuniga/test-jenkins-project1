<?php

include_once 'Data.php';
include '../domain/Suministro.php';

class SuministroData extends Data
{
    public function mostrarTBSuministro()
    {
        $conn = $this->getConexion();
        $suministros = [];
        if ($conn != null) {
            $query = "CALL sp_mostrar_suministros()";
            $statement = $conn->prepare($query);
            $statement->execute();
            while ($row = $statement->fetch(PDO::FETCH_BOTH)) {
                $suministro = new Suministro($row[0], $row[1], $row[2], $row[3]);
                array_push($suministros, $suministro);
            }
        }
        return $suministros; //retorna el registro
    }
    /*
    public function mostrarTBTipoSuministro()
    {
        $conn = $this->getConexion();
        $tipossuministro = [];
        if($conn != null){
            $query = "CALL sp_mostrar_tiposuministro()";
            $statement = $conn->prepare($query);
            $statement->execute();
            while($row = $statement->fetch(PDO::FETCH_BOTH)){
                $tiposuministro = new TipoSuministro($row[0],$row[1]);
                array_push($tipossuministro,$tiposuministro);
            }
        }
        return $tipossuministro; //retorna el registro
    }
    */

    public function mostrarSuministrosAgotados()
    {
        $conn = $this->getConexion();
        $suministros = [];
        if($conn != null){
            $query = "CALL sp_mostrar_suministros_agotados()";
            $statement = $conn->prepare($query);
            $statement->execute();
            while ($row = $statement->fetch(PDO::FETCH_BOTH)) {
                $suministro = new Suministro($row[0], $row[1], $row[2], $row[3]);
                array_push($suministros, $suministro);
            }
        }
        return $suministros; //retorna los registros
    }

    public function insertarTBSuministro($suministro)
    {
        $conn = $this->getConexion();
        $result = 0;
        if ($conn != null) {
            $query = "CALL sp_insertar_suministro(?,?)";
            $statement = $conn->prepare($query);
            $data = [
                $suministro->getNombre(),
                $suministro->getCantidad()
            ];
            $result = $statement->execute($data);
            $conn = null;
        }
        return $result;
    }

    public function eliminarTBSuministro($idSuministro)
    {
        $conn = $this->getConexion();
        $result = 0;
        if ($conn != null) {
            $query = "CALL sp_eliminar_suministro(?)";
            $statement = $conn->prepare($query);
            $result = $statement->execute([$idSuministro]);
            $conn = null;
        }
        return $result;
    }

    public function actualizarTBSuministro($suministro)
    {
        $conn = $this->getConexion();
        $result = 0;
        if ($conn  != null) {
            $query =  "CALL sp_modificar_suministro(?,?,?);";
            $statement = $conn->prepare($query);
            $data = [
                $suministro->getIdSuministro(),
                $suministro->getNombre(),
                $suministro->getCantidad(),
            ];
            $result = $statement->execute($data);
            $conn = null;
        }
        return $result;
    }

    //--------------------------------------------------------------------------------------
    //Insertar en la tabla suministros.

    /*public function insertarTBSuministro($suministros){
        $conn = mysqli_connect($this->server, $this->user, $this->password, $this->db);
        $conn->set_charset('utf8');

        $queryInsert = "INSERT INTO tbsuministro VALUES(".$suministros->getIdSuministro().", 
            ".$suministros->getTipoSuministro().", 
            '".$suministros->getNombre()."', 
            ".$suministros->getCantidad().");";
        $result = mysqli_query($conn,$queryInsert);
        mysqli_close($conn);
        return $result;
    }*/

    /*public function insertarTBSuministro($suministro){
        $conn = mysqli_connect($this->server, $this->user, $this->password, $this->db);
        $conn->set_charset('utf8');

        $query = "CALL sp_insertar_suministro(?,?,?)";
        $statement = $conn->prepare($query);
        $statement->bind_param("iss",$tiposuministro,$nombre,$cantidad);
        $tiposuministro = $suministro->getTipoSuministro();
        $nombre = $suministro->getNombre();
        $cantidad = $suministro->getCantidad();
        $result = $statement->execute();
        $statement->close();
        mysqli_close($conn);
        return $result;
    }*/

    //--------------------------------------------------------------------------------------   
    //Eliminar de la tabla suministros.
    /*public function eliminarTBSuministro($idSuministro) {
        $conn = mysqli_connect($this->server, $this->user, $this->password, $this->db);
        $conn->set_charset('utf8');

        $queryUpdate = "DELETE FROM tbsuministro WHERE idsuministro='" . $idSuministro . "';";
        $result = mysqli_query($conn, $queryUpdate);
        mysqli_close($conn);

        return $result;
     }*/


    /*public function eliminarTBSuministro($idSuministro)
    {
        $conn = mysqli_connect($this->server, $this->user, $this->password, $this->db);
        $conn->set_charset('utf8');

        $query = "CALL sp_eliminar_suministro(?)";
        $statement = $conn->prepare($query);
        $statement->bind_param("i", $idSuministro);
        $result = $statement->execute();
        $statement->close();
        mysqli_close($conn);
        return $result;
    }*/
    //--------------------------------------------------------------------------------------
    //Actualizar en la tabla suministros.
    /*public function actualizarTBSuministro($suministros){
        $conn = mysqli_connect($this->server, $this->user, $this->password, $this->db);
        $conn->set_charset('utf8');

        $queryUpdate = "UPDATE tbsuministro SET nombre='".$suministros->getNombre(). 
        "', 
        tiposuministro=" . $suministros->getTipoSuministro().", 
        cantidad=".  $suministros->getCantidad().
        " WHERE idsuministro=" . $suministros->getIdSuministro().";";
        $result = mysqli_query($conn, $queryUpdate);
        mysqli_close($conn);
        return $result;
    }*/

    /*public function actualizarTBSuministro($suministro)
    {
        $conn = mysqli_connect($this->server, $this->user, $this->password, $this->db);
        $conn->set_charset('utf8');

        $query =  "CALL sp_modificar_suministro(?,?,?,?);";
        $statement = $conn->prepare($query);
        $statement->bind_param("iiss", $idSuministro, $tiposuministro, $nombre, $cantidad);
        $idSuministro = $suministro->getIdSuministro();
        $tiposuministro = $suministro->getTipoSuministro();
        $nombre = $suministro->getNombre();
        $cantidad = $suministro->getCantidad();
        $result = $statement->execute();
        $statement->close();
        mysqli_close($conn);
        return $result;
    }*/

    //--------------------------------------------------------------------------------------
    //Mostrar datos de la tabla suministros.
    /*public function mostrarTBSuministro()
    {
        $conn = mysqli_connect($this->server, $this->user, $this->password, $this->db);
        $conn->set_charset('utf8');

        /*$query = "CALL sp_mostrar_suministros()";
        $statement = $conn->prepare($query);
        $statement->execute();
        $result = $statement->get_result();
        $statement->close();
        $query = "SELECT * FROM tbsuministro;";
        $result = mysqli_query($conn, $query);
        mysqli_close($conn);
        $lista = [];
        while ($row = mysqli_fetch_array($result)) {
            $suministro = new suministro($row[0], $row[1], $row[2], $row[3]);
            array_push($lista, $suministro);
        }
        return $lista; //retorna el registro
}*/
    //--------------------------------------------------------------------------------------
    //Mostrar todos los datos del combo.
    /*public function mostrarTBTipoSuministro()
    {
        $conn = mysqli_connect($this->server, $this->user, $this->password, $this->db);
        $conn->set_charset('utf8');

        /*$query = "CALL sp_mostrar_tiposuministro()";
        $statement = $conn->prepare($query);
        $statement->execute();
        $result = $statement->get_result();
        $statement->close();
        $query = "SELECT * FROM tbtiposuministro;";
        $result = mysqli_query($conn, $query);
        mysqli_close($conn);
        $lista = [];
        while ($row = mysqli_fetch_array($result)) {
            $tiposuministro = new tiposuministro($row[0], $row[1]);
            array_push($lista, $tiposuministro);
        }
        return $lista; //retorna el registro
    }*/
    //--------------------------------------------------------------------------------------
    //Mostrar datos del combo en especifico.
    /*public function mostrarTBTipoSuministroEspecifico()
    {
        $conn = mysqli_connect($this->server, $this->user, $this->password, $this->db);
        $conn->set_charset('utf8');

        $querySelect = "SELECT T1.idtiposuministro,T1.nombre FROM tbtiposuministro T1 INNER JOIN tbsuministro T2 WHERE T2.tiposuministro = T1.idtiposuministro";
        $result = mysqli_query($conn, $querySelect);
        mysqli_close($conn);
        $lista = [];
        while ($row = mysqli_fetch_array($result)) {
            $tiposuministro = new tiposuministro($row['idtiposuministro'], $row['nombre']);
            array_push($lista, $tiposuministro);
        }
        return $lista; //retorna el registro
    }*/
    //--------------------------------------------------------------------------------------

    //Filtra Suministros por nombre.
    /*public function buscarSuministros($dato, $inicio, $limite)
    {
        $conn = mysqli_connect($this->server, $this->user, $this->password, $this->db);
        $conn->set_charset('utf8');

        $queryFiltrar = "SELECT * FROM tbsuministro WHERE nombre LIKE CONCAT('%','" . $dato . "','%') ORDER BY tiposuministro,nombre Limit $inicio,$limite;";
        $result = mysqli_query($conn, $queryFiltrar);
        mysqli_close($conn);
        $lista = [];
        while ($row = mysqli_fetch_array($result)) {
            $suministro = new suministro($row['idsuministro'], $row['tiposuministro'], $row['nombre'], $row['cantidad']);
            array_push($lista, $suministro);
        }
        return $lista; //retorna la consulta.
    }*/
    //------------------------------------------------------------------------------------


    /*public function getTotalRegistros()
    {
        $conn = mysqli_connect($this->server, $this->user, $this->password, $this->db);
        $conn->set_charset('utf8');
        $query = "SELECT COUNT(*) as cantregistros FROM tbsuministro;";
        $result = mysqli_query($conn, $query);
        mysqli_close($conn);
        $cantidadRegistros = 0;
        if ($row = mysqli_fetch_array($result)) {
            $cantidadRegistros = $row['cantregistros'];
        }
        return $cantidadRegistros;
    }*/

    /*public function getTotalRegistrosBuscados($cadena)
    {
        $conn = mysqli_connect($this->server, $this->user, $this->password, $this->db);
        $conn->set_charset('utf8');
        $query = "SELECT COUNT(*) as cantregistros FROM tbsuministro WHERE nombre like concat('%','" . $cadena . "','%');";
        $result = mysqli_query($conn, $query);
        mysqli_close($conn);
        $cantidadRegistros = 0;
        if ($row = mysqli_fetch_array($result)) {
            $cantidadRegistros = $row['cantregistros'];
        }
        return $cantidadRegistros;
    }*/

    /*public function mostrarRegistrosPaginados($inicio, $limite)
    {
        $conn = mysqli_connect($this->server, $this->user, $this->password, $this->db);
        $conn->set_charset('utf8');
        $query = "SELECT * FROM tbsuministro ORDER BY tiposuministro,nombre Limit $inicio,$limite;";
        $result = mysqli_query($conn, $query);
        mysqli_close($conn);
        $lista = [];
        while ($row = mysqli_fetch_array($result)) {
            $suministro = new suministro($row['idsuministro'], $row['tiposuministro'], $row['nombre'], $row['cantidad']);
            array_push($lista, $suministro);
        }
        return $lista; //retorna el registro
    }*/

    /** Mostrar los suministros que estan por agotarse */
    /*public function mostrarSuministrosAgotados()
    {
        $conn = mysqli_connect($this->server, $this->user, $this->password, $this->db);
        $conn->set_charset('utf8');
        $query = "SELECT * FROM tbsuministro WHERE cantidad <= 10 ORDER BY cantidad ASC";
        $result = mysqli_query($conn, $query);
        mysqli_close($conn);
        $lista = [];
        while ($row = mysqli_fetch_array($result)) {
            $suministro = new suministro($row['idsuministro'], $row['tiposuministro'], $row['nombre'], $row['cantidad']);
            array_push($lista, $suministro);
        }
        return $lista; //retorna los registros
    }*/
}
