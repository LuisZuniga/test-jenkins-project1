<?php

include_once 'Data.php';
include '../domain/Planta.php';

class PlantaData extends Data
{

    public function mostrarTBPlanta()
    {
        $conn = $this->getConexion();
        $plantas = [];
        if ($conn != null) {
            $query = "CALL sp_mostrar_plantas()";
            $statement = $conn->prepare($query);
            $statement->execute();
            while ($row = $statement->fetch(PDO::FETCH_BOTH)) {
                $plantaActual = new Planta($row[0], $row[1], $row[2], $row[3], $row[4]);
                array_push($plantas, $plantaActual);
            }
            $conn = null;
        }
        return $plantas;
    }


    public function insertarTBPlanta($planta)
    {
        $conn = $this->getConexion();
        $result = 0;
        if ($conn != null) {
            $query = "CALL  sp_insertar_planta(?,?,?,?)";
            $statement = $conn->prepare($query);
            $planta = [
                $planta->getNombreComun(),
                $planta->getNombreCientifico(),
                $planta->getCantidad(),
                $planta->getPrecioUnitario()
            ];
            $result = $statement->execute($planta);
            $conn = null;
        }
        return $result;
    }

    public function actualizarTBPlanta($planta)
    {

        $conn = $this->getConexion();
        $result = 0;
        if ($conn != null) {
            $query = "CALL sp_modificar_planta(?,?,?,?,?)";
            $statement = $conn->prepare($query);
            $planta = [
                $planta->getIdPlanta(),
                $planta->getNombreComun(),
                $planta->getNombreCientifico(),
                $planta->getCantidad(),
                $planta->getPrecioUnitario()
            ];
            $result = $statement->execute($planta);
            $conn = null;
        }
        return $result;
    }

    public function eliminarTBPlanta($idPlanta)
    {
        $conn = $this->getConexion();
        $result = 0;
        if ($conn != null) {
            $query = "CALL sp_eliminar_planta(?)";
            $statement = $conn->prepare($query);
            $planta = [
                $idPlanta
            ];
            $result = $statement->execute($planta);
            $conn = null;
        }
        return $result;
    }

    function setCantidadPlanta($idPlanta, $cantidad)
    {
        $conn = $this->getConexion();
        $result = 0;
        if ($conn != null) {
            $query = "CALL sp_set_cantidad_plantas(?,?)";
            $statement = $conn->prepare($query);
            $planta = [
                $idPlanta,
                $cantidad
            ];
            $result = $statement->execute($planta);
            $conn = null;
        }
        return $result;
    }

    function getCantidadPlanta($idPlanta)
    {
        $conn = $this->getConexion();
        $result = 0;
        if ($conn != null) {
            $query = "CALL sp_get_cantidad_plantas(?)";
            $statement = $conn->prepare($query);
            $planta = [
                $idPlanta
            ];
            $result = $statement->execute($planta);
            if ($row = $statement->fetch(PDO::FETCH_BOTH)) {
                $result = $row[0];
            }
        }
        return $result;
    }

    public function getNombreComun($idPlanta)
    {
        $conn = $this->getConexion();
        $nombrecomun = "";
        if ($conn != null) {
            $query = "SELECT nombrecomun FROM tbplanta WHERE idplanta = ?;";
            $statement = $conn->prepare($query);
            $statement->execute([$idPlanta]);
            if ($row = $statement->fetch(PDO::FETCH_BOTH)) {
                $nombrecomun = $row[0];
            }
            $conn = null;
        }
        return $nombrecomun;
    }

    public function getTotalPlantas(){
        $conn = $this->getConexion();
        $total = 0;
        if ($conn != null) {
            $query = "SELECT cantidad FROM tbplanta;";
            $statement = $conn->prepare($query);
            $statement->execute();
            while ($row = $statement->fetch(PDO::FETCH_BOTH)) {
                $total += $row[0];
            }
            $conn = null;
        }
        return $total;
    }


    // Obtenemos el precio unitario de la planta que estamos enviando por parametro.
    public function getPrecioUnitario($idPlanta)
    {
        $conn = $this->getConexion();
        $precioUnitario = 0;
        if($conn != null){
            $query = "SELECT preciounitario FROM tbplanta WHERE idplanta = ?;";
            $statement = $conn->prepare($query);
            $statement->execute([$idPlanta]);
            if ($row = $statement->fetch(PDO::FETCH_BOTH)) {
                $precioUnitario = $row[0];
            }
            $conn = null;
        }
        return $precioUnitario;
    }

    ////////// METOS DE TESTING. //////////////


    public function buscarPlanta($nombrecomun)
    {
        $conn = $this->getConexion();
        $planta = null;
        if ($conn != null) {
            $query = "SELECT * FROM tbplanta WHERE nombrecomun = ? AND estado = 1 LIMIT 1";
            $statement = $conn->prepare($query);
            $statement->execute([$nombrecomun]);
            if ($row = $statement->fetch(PDO::FETCH_BOTH)) {
                $planta = new Planta($row[0], $row[1], $row[2], $row[3], $row[4]);
            }
            $conn = null;
        }
        return $planta;
    }

    public function getLastId()
    {
        $conn = $this->getConexion();
        $idplanta = 0;
        if ($conn != null) {
            $query = "SELECT MAX(idplanta) AS idplanta FROM tbplanta;";
            $statement = $conn->prepare($query);
            $statement->execute();
            if ($row = $statement->fetch(PDO::FETCH_BOTH)) {
                $idplanta = $row[0];
            }
            $conn = null;
        }
        return $idplanta;
    }

    /*public function insertarTBPlanta($planta){
        $conn = mysqli_connect($this->server, $this->user, $this->password, $this->db);
        $conn->set_charset('utf8');
        $query = "CALL  sp_insertar_planta(?,?,?,?)";
        $statement = $conn->prepare($query);
        $statement->bind_param("ssid",$nombreComun,$nombreCientifico,$cantidad,$precioUnitario);
        //$idPlanta = 0;
        $nombreComun = $planta->getNombreComun();
        $nombreCientifico = $planta->getNombreCientifico();
        $cantidad = $planta->getCantidad();
        $precioUnitario = $planta->getPrecioUnitario();
        $result = $statement->execute();
        $statement->close();
        mysqli_close($conn);
        return $result;
    }*/

    /*public function eliminarTBPlanta($idPlanta)
    {
        $conn = mysqli_connect($this->server, $this->user, $this->password, $this->db);
        $conn->set_charset('utf8');

        $query = "CALL sp_eliminar_planta(?)";
        $statement = $conn->prepare($query);
        $statement->bind_param("i", $id);
        $id = $idPlanta;
        $result = $statement->execute();
        $statement->close();
        mysqli_close($conn);
        return $result;
    }*/

    /*public function actualizarTBPlanta($planta)
    {
        $conn = mysqli_connect($this->server, $this->user, $this->password, $this->db);
        $conn->set_charset('utf8');

        $query = "CALL sp_modificar_planta(?,?,?,?,?)";
        $statement = $conn->prepare($query);
        $statement->bind_param("issid", $idPlanta, $nombreComun, $nombreCientifico, $cantidad, $precioUnitario);
        $idPlanta = $planta->getIdPlanta();
        $nombreComun = $planta->getNombreComun();
        $nombreCientifico = $planta->getNombreCientifico();
        $cantidad = $planta->getCantidad();
        $precioUnitario = $planta->getPrecioUnitario();
        $result = $statement->execute();
        $statement->close();
        mysqli_close($conn);
        return $result;
    }*/

    /*public function mostrarTBPlanta(){
        $conn = mysqli_connect($this->server, $this->user, $this->password, $this->db);
        $conn->set_charset('utf8');

        /*$query = "CALL sp_mostrar_plantas()";
        $statement = $conn->prepare($query);
        $statement->execute();
        $result = $statement->get_result();
        $statement->close();*/
    /*$query = "SELECT * FROM tbplanta;";
        $result = mysqli_query($conn,$query);
        $plantas = [];
        while($row = mysqli_fetch_array($result)){
            $plantaActual = new Planta($row[0], $row[1], $row[2],$row[3], $row[4]);
            array_push($plantas, $plantaActual);
        }
        mysqli_close($conn);
        return $plantas;
    }*/




    /*public function mostrarTBPlanta(){
        try {
            $dsn  = 'mysql:host=' . $this->server . ';dbname=' . $this->db;
            $conn = new PDO($dsn,$this->user,$this->password,null);
            $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $query = "CALL sp_mostrar_plantas()";
            $statement = $conn->prepare($query);
            $statement->execute();
            $plantas = [];
            while ($row = $statement->fetch(PDO::FETCH_BOTH)) {
                $plantaActual = new Planta($row[0], $row[1], $row[2], $row[3], $row[4]);
                array_push($plantas, $plantaActual);
            }
            //mysqli_close($conn);
            $statement = null;
            return $plantas;
        } catch (PDOException $e) {
            die();
        }
    }*/

    /*function getCantidadPlanta($idPlanta){
        $conn = mysqli_connect($this->server, $this->user, $this->password, $this->db);
        $conn->set_charset('utf8');

        $query = "CALL sp_get_cantidad_plantas(?)";
        $statement = $conn->prepare($query);
        $statement->bind_param("i",$idPlanta);
        $statement->execute();
        $result = $statement->get_result();
        $cantidad = 0;
        if($row = mysqli_fetch_array($result)){
            $cantidad = $row['cantidad'];
        }
        mysqli_close($conn);
        return $cantidad;
    }*/

    /*function setCantidadPlanta($idPlanta,$cantidad){
        $conn = mysqli_connect($this->server, $this->user, $this->password, $this->db);
        $conn->set_charset('utf8');
        $query = "UPDATE tbplanta SET cantidad=".$cantidad." WHERE idplanta=".$idPlanta.";";
        $result = mysqli_query($conn, $query);
        mysqli_close($conn);
    }*/


    /*public function getNombreComun($idPlanta){
        $conn = mysqli_connect($this->server, $this->user, $this->password, $this->db);
        $conn->set_charset('utf8');

        $query = "CALL sp_get_nombre_comun(?)";
        $statement = $conn->prepare($query);
        $statement->bind_param("i",$idPlanta);
        $statement->execute();
        $result = $statement->get_result();

        $nombrecomun;
        if($row = mysqli_fetch_array($result)){
            $nombrecomun = $row['nombrecomun'];
        }
        mysqli_close($conn);        
        return $nombrecomun;

    }*/


    /*public function getLastId()
    {
        $conn = mysqli_connect($this->server, $this->user, $this->password, $this->db);
        $conn->set_charset('utf8');

        $querySelect = "SELECT MAX(idplanta) AS idplanta FROM tbplanta;";
        $result = mysqli_query($conn, $querySelect);
        mysqli_close($conn);
        $idplanta = 0;
        if ($row = mysqli_fetch_array($result)) {
            $idplanta = $row['idplanta'];
        }
        return $idplanta;
    }*/

    /*public function buscarPlantas($cadena)
    {
        $conn = mysqli_connect($this->server, $this->user, $this->password, $this->db);
        $conn->set_charset('utf8');

        $querySelect = "SELECT * FROM  tbplanta WHERE nombrecomun LIKE CONCAT('%','" . $cadena . "','%') OR nombrecientifico LIKE CONCAT('%','" . $cadena . "','%') OR cantidad LIKE CONCAT('%','" . $cadena . "','%') OR preciounitario LIKE CONCAT('%','" . $cadena . "','%') ORDER BY nombrecomun,nombrecientifico";
        $result = mysqli_query($conn, $querySelect);
        mysqli_close($conn);

        $plantas = [];
        while ($row = mysqli_fetch_array($result)) {
            $plantaActual = new Planta($row['idplanta'], $row['nombrecomun'], $row['nombrecientifico'], $row['cantidad'], $row['preciounitario']);
            array_push($plantas, $plantaActual);
        }
        return $plantas;
    }*/
}
