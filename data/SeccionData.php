<?php

include_once 'Data.php';
include '../domain/Seccion.php';
include '../domain/Lote.php';

class seccionData extends Data
{

    public function mostrarTBSeccion($lote, $seccion)
    {
        $conn = $this->getConexion();
        $resgistros = [];
        if ($conn != null) {
            $query = "CALL sp_mostrar_plantaseccion(?,?)";
            $statement = $conn->prepare($query);
            $data = [
                $seccion,
                $lote
            ];
            $statement->execute($data);
            while ($row = $statement->fetch(PDO::FETCH_BOTH)) {
                $seccion = new Seccion($row[0], $row[1], $row[2], $row[3], $row[4], $row[5]);
                array_push($resgistros, $seccion);
            }
        }
        return $resgistros;
    }

    function mostrarSecciones($fechaActual)
    {
        $conn = $this->getConexion();
        $registros = [];
        if ($conn != null) {
            $query = "CALL sp_get_cosecha(?)";
            $statement = $conn->prepare($query);
            $statement->execute([$fechaActual]);
            while ($row = $statement->fetch(PDO::FETCH_BOTH)) {
                $seccion = new Seccion($row[0], $row[1], $row[2], $row[3], $row[4], $row[5]);
                array_push($registros, $seccion);
            }
        }
        return $registros;
    }

    public function insertarTBSeccion($seccion)
    {
        $conn = $this->getConexion();
        $result = 0;
        if ($conn != null) {
            $query = "CALL sp_insertar_plantaseccion(?,?,?,?,?,?)";
            $statement = $conn->prepare($query);
            $data = [
                $seccion->getPlanta(),
                $seccion->getNumeroseccion(),
                $seccion->getNumerolote(),
                $seccion->getCantidad(),
                $seccion->getFechaSiembra(),
                $seccion->getFechaExtraccion()
            ];
            $result = $statement->execute($data);
            $conn = null;
        }
        return $result;
    }

    public function actualizarTBSeccion($seccion)
    {
        $conn = $this->getConexion();
        $result = 0;
        if ($conn != null) {
            $query = "CALL sp_modificar_plantaseccion(?,?,?,?,?,?,?)";
            $statement = $conn->prepare($query);
            $data = [
                $seccion->getCantidad(),
                $seccion->getFechaSiembra(),
                $seccion->getFechaExtraccion(),
                $seccion->getPlantaActual(),
                $seccion->getPlanta(),
                $seccion->getNumeroseccion(),
                $seccion->getNumerolote()
            ];
            $result = $statement->execute($data);
            $conn = null;
        }
        return $result;
    }

    public function eliminarTBSeccion($planta, $numeroseccion, $numerolote)
    {
        $conn = $this->getConexion();
        $result = 0;
        if ($conn != null) {
            $query = "CALL sp_eliminar_plantaseccion(?,?,?)";
            $statement = $conn->prepare($query);
            $data = [
                $numerolote,
                $numeroseccion,
                $planta
            ];
            $result = $statement->execute($data);
            $conn = null;
        }
        return $result;
    }

    function getPlantasPorLote($idLote)
    {
        $conn = $this->getConexion();
        $total = 0;
        if($conn != null){
            $query = "CALL sp_get_plantas_por_lote(?);";
            $statement = $conn->prepare($query);
            $statement->execute([$idLote]);
            while($row = $statement->fetch(PDO::FETCH_BOTH)){
                $total += $row[0];
            }
            $conn = null;
        }
        return $total;
    }

    function getPlantasSL($idPlanta, $idSeccion, $idLote)
    {
        $conn = $this->getConexion();
        $cantidad = 0;
        if($conn != null){
            $query = "CALL sp_get_plantasloteseccion(?,?,?)";
            $statement = $conn->prepare($query);
            $data = [
                $idPlanta,
                $idSeccion,
                $idLote
            ];
            $statement->execute($data);
            if($row = $statement->fetch(PDO::FETCH_BOTH)){
                $cantidad = $row[0];
            }
            $conn = null;
        }
        return $cantidad;
    }

    //--------------------------------------------------------------------------------------
    //Insertar en la tabla seccion.
    /*public function insertarTBSeccion($seccion){
        $conn = mysqli_connect($this->server, $this->user, $this->password, $this->db);
        $conn->set_charset('utf8');
        $queryInsert = "INSERT INTO tbseccion VALUES( 
            ".$seccion->getNumeroSeccion().", 
            ".$seccion->getNumerolote().");";
        $result = mysqli_query($conn,$queryInsert);
        mysqli_close($conn);
        return $result;
    }*/

    /*public function insertarTBSeccion($seccion){
        $conn = mysqli_connect($this->server, $this->user, $this->password, $this->db);
        $conn->set_charset('utf8');
        $queryInsert = "INSERT INTO tbplantaseccion VALUES(?,?,?,?,?,?)";
        $statement = $conn->prepare($queryInsert);
        $statement->bind_param("iiiiss",$planta,$numeroSeccion,$numeroLote,$cantidad,$fechaSiembra,$fechaExtraccion);
        $numeroLote = $seccion->getNumerolote();
        $numeroSeccion = $seccion->getNumeroseccion();
        $planta = $seccion->getPlanta();
        $cantidad = $seccion->getCantidad();
        $fechaSiembra = $seccion->getFechaSiembra();
        $fechaExtraccion = $seccion->getFechaExtraccion();
        $result = $statement->execute();
        $statement->close();
        mysqli_close($conn);
        return $result;
    }*/
    //--------------------------------------------------------------------------------------   
    //Eliminar de la tabla seccion.
    /*public function eliminarTBSeccion($numeroseccion,$numerolote) {
        $conn = mysqli_connect($this->server, $this->user, $this->password, $this->db);
        $conn->set_charset('utf8');

        $queryUpdate = "DELETE FROM tbseccion WHERE numeroseccion =" . $numeroseccion . " AND numerolote=".$numerolote.";";
        $result = mysqli_query($conn, $queryUpdate);
        mysqli_close($conn);

        return $result;
     }*/
    /*public function eliminarTBSeccion($planta, $numeroseccion, $numerolote)
    {
        $conn = mysqli_connect($this->server, $this->user, $this->password, $this->db);
        $conn->set_charset('utf8');
        $queryDelete = "DELETE FROM tbplantaseccion WHERE numerolote = ? AND numeroseccion = ? AND idplanta = ?";
        $statement = $conn->prepare($queryDelete);
        $statement->bind_param("iii", $numerolote, $numeroseccion, $planta);
        $planta = $planta;
        $numeroseccion = $numeroseccion;
        $numerolote = $numerolote;
        $result = $statement->execute();
        $statement->close();
        mysqli_close($conn);
        return $result;
    }*/
    //--------------------------------------------------------------------------------------
    //Actualizar en la tabla seccion.
    /*public function actualizarTBSeccion($seccion) {
        $conn = mysqli_connect($this->server, $this->user, $this->password, $this->db);
        $conn->set_charset('utf8');

        $queryUpdate = "UPDATE tbseccion SET numerolote=".  $seccion->getNumerolote().
        " WHERE numeroseccion=" . $seccion->getNumeroSeccion()." AND numerolote=".$seccion->getNumerolote().";";
        $result = mysqli_query($conn, $queryUpdate);
        mysqli_close($conn);
        return $result;
    }*/

    /*public function actualizarTBSeccion($seccion)
    {
        $conn = mysqli_connect($this->server, $this->user, $this->password, $this->db);
        $conn->set_charset('utf8');
        $queryUpdate = "UPDATE tbplantaseccion SET cantidadplantas=?, fechasiembra=?, fechaextraccion=? WHERE idplanta=? AND numeroseccion=? AND numerolote=?";
        $statement = $conn->prepare($queryUpdate);
        $statement->bind_param("issiii", $cantidad, $fechaSiembra, $fechaExtraccion, $planta, $numeroSeccion, $numeroLote);
        $numeroLote = $seccion->getNumerolote();
        $numeroSeccion = $seccion->getNumeroseccion();
        $planta = $seccion->getPlanta();
        $cantidad = $seccion->getCantidad();
        $fechaSiembra = $seccion->getFechaSiembra();
        $fechaExtraccion = $seccion->getFechaExtraccion();
        $result = $statement->execute();
        $statement->close();
        mysqli_close($conn);
        return $result;
    }*/

    //--------------------------------------------------------------------------------------
    //Mostrar datos de la tabla seccion.
    /*public function mostrarTBSeccion($lote, $seccion)
    {
        $conn = mysqli_connect($this->server, $this->user, $this->password, $this->db);
        $conn->set_charset('utf8');
        $querySelect = "SELECT t1.numerolote, t1.numeroseccion, t1.idplanta, t1.cantidadplantas, t1.fechasiembra, t1.fechaextraccion FROM
            tbplantaseccion AS t1 INNER JOIN tbplanta AS t2 ON t1.idplanta = t2.idplanta WHERE
            t1.numeroseccion = " . $seccion . " AND t1.numerolote = " . $lote . " ORDER BY t2.nombrecomun";
        $result = mysqli_query($conn, $querySelect);
        mysqli_close($conn);
        $lista = [];
        while ($row = mysqli_fetch_array($result)) {
            //$seccion = new Seccion($row['numeroseccion'], $row['numerolote']);
            $seccion = new Seccion($row['numeroseccion'], $row['numerolote'], $row['idplanta'], $row['cantidadplantas'], $row['fechasiembra'], $row['fechaextraccion']);
            array_push($lista, $seccion);
        }
        return $lista; //retorna el registro
    }*/
    //--------------------------------------------------------------------------------------


    //Mostrar todos los datos del combo.
    /*public function mostrarTBLote()
    {
        $conn = mysqli_connect($this->server, $this->user, $this->password, $this->db);
        $conn->set_charset('utf8');

        $querySelect = "SELECT * FROM tblote";
        $result = mysqli_query($conn, $querySelect);
        mysqli_close($conn);
        $lista = [];
        while ($row = mysqli_fetch_array($result)) {
            $lote = new Lote($row['idlote'], $row['numerolote']);
            array_push($lista, $lote);
        }
        return $lista; //retorna el registro
    }*/
    //--------------------------------------------------------------------------------------
    //Mostrar datos del combo en especifico.
    /*public function mostrarTBLoteEspecifico()
    {
        $conn = mysqli_connect($this->server, $this->user, $this->password, $this->db);
        $conn->set_charset('utf8');

        $querySelect = "SELECT T1.idlote,T1.numerolote FROM tblote T1 INNER JOIN tbseccion T2 WHERE T2.numerolote = T1.idlote";
        $result = mysqli_query($conn, $querySelect);
        mysqli_close($conn);
        $lista = [];
        while ($row = mysqli_fetch_array($result)) {
            $lote = new Lote($row['idlote'], $row['numerolote']);
            array_push($lista, $lote);
        }
        return $lista; //retorna el registro
    }*/
    //------------------------------------------------------------------------------------

    /*public function getTotalRegistros()
    {
        $conn = mysqli_connect($this->server, $this->user, $this->password, $this->db);
        $conn->set_charset('utf8');
        $query = "SELECT COUNT(*) as cantregistros FROM tbplantaseccion;";
        $result = mysqli_query($conn, $query);
        mysqli_close($conn);
        $cantidadRegistros = 0;
        if ($row = mysqli_fetch_array($result)) {
            $cantidadRegistros = $row['cantregistros'];
        }
        return $cantidadRegistros;
    }*/

    /*public function getTotalRegistrosBuscados($cadena)
    {
        $conn = mysqli_connect($this->server, $this->user, $this->password, $this->db);
        $conn->set_charset('utf8');
        $query = "SELECT COUNT(*) as cantregistros FROM tbplantaseccion WHERE numeroseccion like concat('%','" . $cadena . "','%');";
        $result = mysqli_query($conn, $query);
        mysqli_close($conn);
        $cantidadRegistros = 0;
        if ($row = mysqli_fetch_array($result)) {
            $cantidadRegistros = $row['cantregistros'];
        }
        return $cantidadRegistros;
    }*/

    /*public function mostrarSeccionPorLote($lote, $seccion)
    {
        $conn = mysqli_connect($this->server, $this->user, $this->password, $this->db);
        $conn->set_charset('utf8');
        $querySelect = "SELECT t1.numerolote, t1.numeroseccion, t2.idplanta, t1.cantidadplantas, t1.fechasiembra, t1.fechaextraccion FROM
        tbplantaseccion AS t1 INNER JOIN tbplanta AS t2 ON t1.idplanta = t2.idplanta WHERE
        t1.numeroseccion = " . $seccion . " AND t1.numerolote = " . $lote . " ORDER BY t2.nombrecomun";
        $result = mysqli_query($conn, $querySelect);
        mysqli_close($conn);
        $lista = [];
        while ($row = mysqli_fetch_array($result)) {
            $seccion = new Seccion($row['numeroseccion'], $row['numerolote'], $row['idplanta'], $row['cantidadplantas'], $row['fechasiembra'], $row['fechaextraccion']);
            array_push($lista, $seccion);
        }
        return $lista; //retorna el registro
    }*/

    /*public function mostrarRegistrosPaginados($inicio, $limite){
    $conn = mysqli_connect($this->server, $this->user, $this->password, $this->db);
    $conn->set_charset('utf8');
    //$query = "SELECT * FROM tbseccion Limit $inicio,$limite;";
    $query = "SELECT t1.numerolote, t1.numeroseccion, t2.nombrecomun, t1.cantidadplantas
        FROM tbplantaseccion AS t1 INNER JOIN tbplanta AS t2 ON t1.idplanta = t2.idplanta
        ORDER BY t1.numerolote, t1.numeroseccion";
    $result = mysqli_query($conn, $query);
    mysqli_close($conn);
    $lista = [];
    while ($row = mysqli_fetch_array($result)) {
        //$seccion = new Seccion($row['numeroseccion'], $row['numerolote']);
        $seccion = new Seccion($row['numeroseccion'],$row['numerolote'],$row['nombrecomun'],$row['cantidadplantas']);
        array_push($lista, $seccion);
    }
    return $lista;//retorna el registro
}*/

    /*public function buscarSeccion($cadena,$inicio, $limite){
    $conn = mysqli_connect($this->server, $this->user, $this->password, $this->db);
    $conn->set_charset('utf8');
    $query = "SELECT * FROM tbseccion WHERE numeroseccion =".$cadena." Limit $inicio,$limite;";
    $result = mysqli_query($conn, $query);
    mysqli_close($conn);
    $lista = [];
    while ($row = mysqli_fetch_array($result)) {
        $seccion = new Seccion($row['numeroseccion'], $row['numerolote']);
        array_push($lista, $seccion);
    }
    return $lista;//retorna el registro
}*/

    /*public function buscarSeccion($lote, $seccion, $cadena, $inicio, $limite)
    {
        $conn = mysqli_connect($this->server, $this->user, $this->password, $this->db);
        $conn->set_charset('utf8');
        $querySelect = "SELECT t1.numerolote, t1.numeroseccion, t2.idplanta, t1.cantidadplantas, t1.fechasiembra, t1.fechaextraccion FROM
        tbplantaseccion AS t1 INNER JOIN tbplanta AS t2 ON t1.idplanta = t2.idplanta WHERE
        t1.numeroseccion = " . $seccion . " AND t1.numerolote = " . $lote . " AND t2.nombrecomun LIKE CONCAT('%','" . $cadena . "','%') ";
        $result = mysqli_query($conn, $querySelect);
        mysqli_close($conn);
        $lista = [];
        while ($row = mysqli_fetch_array($result)) {
            $seccion = new Seccion($row['numeroseccion'], $row['numerolote'], $row['idplanta'], $row['cantidadplantas'], $row['fechasiembra'], $row['fechaextraccion']);
            array_push($lista, $seccion);
        }
        return $lista; //retorna el registro
    }*/

    /*function getPlantasSL($idPlanta, $idSeccion, $idLote)
    {
        $conn = mysqli_connect($this->server, $this->user, $this->password, $this->db);
        $conn->set_charset('utf8');
        $query = "SELECT cantidadplantas FROM tbplantaseccion WHERE idplanta=" . $idPlanta . " AND numeroseccion=" . $idSeccion . " AND numerolote=" . $idLote . ";";
        $result = mysqli_query($conn, $query);
        mysqli_close($conn);
        $cantidad = 0;
        if ($row = mysqli_fetch_array($result)) {
            $cantidad = $row['cantidadplantas'];
        }
        return $cantidad;
    }*/

    /*function getDiasRestantesParaCosechar($fechaActual, $fechaExtraccion)
    {
        $conn = mysqli_connect($this->server, $this->user, $this->password, $this->db);
        $conn->set_charset('utf8');
        $query = "SELECT DATEDIFF($fechaActual , $fechaExtraccion) AS dias;";
        $result = mysqli_query($conn, $query);
        mysqli_close($conn);
        $dias = 0;
        if ($row = mysqli_fetch_array($result)) {
            $dias = $row['dias'];
        }
        return $dias;
    }*/
    // metodo para el index.php
    /** Mostrar la tabla completa sin filtro*/

    /*function mostrarSecciones()
    {
        $conn = mysqli_connect($this->server, $this->user, $this->password, $this->db);
        $conn->set_charset('utf8');
        $query = "SELECT * FROM tbplantaseccion WHERE (SELECT DATEDIFF(fechaextraccion,fechasiembra) < 10);";
        $result = mysqli_query($conn, $query);
        mysqli_close($conn);
        $lista = [];
        while ($row = mysqli_fetch_array($result)) {
            $seccion = new Seccion($row['numeroseccion'], $row['numerolote'], $row['idplanta'], $row['cantidadplantas'], $row['fechasiembra'], $row['fechaextraccion']);
            array_push($lista, $seccion);
        }
        return $lista; //retorna el registro
    }*/
}
