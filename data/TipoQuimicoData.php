<?php

include_once 'Data.php';
include '../domain/TipoQumico.php';

class TipoQuimicoData extends Data
{

    public function insertarTBTipoQuimico($tipoQuimico)
    {
        $conn = $this->getConexion();
        $result = 0;
        if ($conn != null) {
            $query = "CALL sp_insertar_tipoquimico(?)";
            $statement = $conn->prepare($query);
            $result = $statement->execute([$tipoQuimico->getNombre()]);
            $conn = null;
        }
        return $result;
    }

    public function mostrarTBTipoQuimico()
    {
        $conn = $this->getConexion();
        $tiposquimico = [];
        if ($conn != null) {
            $query = "CALL sp_mostrar_tipoquimico()";
            $statement = $conn->prepare($query);
            $statement->execute();
            while ($row = $statement->fetch(PDO::FETCH_BOTH)) {
                $tipoquimicoA = new TipoQuimico($row[0], $row[1]);
                array_push($tiposquimico, $tipoquimicoA);
            }
        }
        return $tiposquimico;
    }

    public function modificarTBTipoQuimico($tipoQuimico){
        $conn = $this->getConexion();
        $result = 0;
        if ($conn != null) {
            $query = "CALL sp_modificar_tipoquimico(?,?)";
            $statement = $conn->prepare($query);
            $data = [
                $tipoQuimico->getNombre(),
                $tipoQuimico->getIdTipoQuimico()
            ];
            $result = $statement->execute($data);
            $conn = null;
        }
        return $result;
    }

    public function eliminarTBTipoQuimico($idTipoQuimico){
        $conn = $this->getConexion();
        $result = 0;
        if ($conn != null) {
            $query = "CALL sp_eliminar_tipoquimico(?)";
            $statement = $conn->prepare($query);
            $result = $statement->execute([$idTipoQuimico]);
            $conn = null;
        }
        return $result;
    }


    ////////////////////////////////////////////////////////////////////////////////////////////

    public function buscarTipoQuimico($id)
    {
        $conn = $this->getConexion();
        $tipoQuimico = null;
        if ($conn != null) {
            $query = "SELECT * FROM tbtipoquimico WHERE idtipoquimico = ? AND estado = 1 LIMIT 1";
            $statement = $conn->prepare($query);
            $statement->execute([$id]);
            if($row = $statement->fetch(PDO::FETCH_BOTH)) {
                $tipoQuimico = new TipoQuimico($row[0], $row[1]);
            }
            $conn = null;
        }
        return $tipoQuimico;
    }

    public function getLastId()
    {
        $conn = $this->getConexion();
        $id = 0;
        if ($conn != null) {
            $query = "SELECT MAX(idtipoquimico) AS idtipoquimico FROM tbtipoquimico;";
            $statement = $conn->prepare($query);
            $statement->execute();
            if ($row = $statement->fetch(PDO::FETCH_BOTH)) {
                $id = $row[0];
            }
            $conn = null;
        }
        return $id;
    }





}
