<?php

include_once 'Data.php';
include '../domain/Colaborador.php';

class ColaboradorData extends Data
{
    public function mostrarTBColaborador()
    {
        $conn = $this->getConexion();
        $colaboradores = [];
        if ($conn != null) {
            $query = "CALL sp_mostrar_colaboradores()";
            $statement = $conn->prepare($query);
            $statement->execute();
            while ($row = $statement->fetch(PDO::FETCH_BOTH)) {
                $colaboradorActual = new Colaborador($row[0], $row[1], $row[2], $row[3], $row[4], $row[5], $row[6]);
                array_push($colaboradores, $colaboradorActual);
            }
            $conn = null;
        }
        return $colaboradores;
    }

    public function insertarTBColaborador($colaborador)
    {
        $conn = $this->getConexion();
        $result = 0;
        if ($conn != null) {
            $query = "CALL sp_insertar_colaborador(?,?,?,?,?,?)";
            $statement = $conn->prepare($query);
            $colaborador = [
                $colaborador->getCedula(),
                $colaborador->getNombre(),
                $colaborador->getApellido1(),
                $colaborador->getApellido2(),
                $colaborador->getDireccion(),
                $colaborador->getTelefono()
            ];
            $result = $statement->execute($colaborador);
            $conn = null;
        }
        return $result;
    }

    public function actualizarTBColaborador($colaborador)
    {
        $conn = $this->getConexion();
        $result = 0;
        if ($conn != null) {
            $query = "CALL sp_modificar_colaborador(?,?,?,?,?,?,?)";
            $statement = $conn->prepare($query);
            $colaborador = [
                $colaborador->getCedula(),
                $colaborador->getNombre(),
                $colaborador->getApellido1(),
                $colaborador->getApellido2(),
                $colaborador->getDireccion(),
                $colaborador->getTelefono(),
                $colaborador->getIdColaborador()
            ];
            $result = $statement->execute($colaborador);
            $conn = null;
        }
        return $result;
    }

    public function eliminarTBColaborador($cedula)
    {
        $conn = $this->getConexion();
        $result = 0;
        if ($conn != null) {
            $query = "CALL sp_eliminar_colaborador(?)";
            $statement = $conn->prepare($query);
            $result = $statement->execute([$cedula]);
            $conn = null;
        }
        return $result;
    }


    ////// METODOS DE TESTING //////////////////////

    public function buscarColaborador($cedula)
    {
        $conn = $this->getConexion();
        $colaborador = null;
        if ($conn != null) {
            $query = "SELECT * FROM tbcolaborador WHERE cedula = ? AND estado = 1 LIMIT 1";
            $statement = $conn->prepare($query);
            $statement->execute([$cedula]);
            if($row = $statement->fetch(PDO::FETCH_BOTH)) {
                $colaborador = new Colaborador($row[0], $row[1], $row[2], $row[3], $row[4], $row[5],$row[6]);
            }
            $conn = null;
        }
        return $colaborador;
    }

    public function getLastId()
    {
        $conn = $this->getConexion();
        $id = 0;
        if ($conn != null) {
            $query = "SELECT MAX(idcolaborador) AS idcolaborador FROM tbcolaborador;";
            $statement = $conn->prepare($query);
            $statement->execute();
            if ($row = $statement->fetch(PDO::FETCH_BOTH)) {
                $id = $row[0];
            }
            $conn = null;
        }
        return $id;
    }



    // Insertar en la tabla de animal registration
    /*public function insertarTBColaborador($colaborador) {
        $conn = mysqli_connect($this->server, $this->user, $this->password, $this->db);
        $conn->set_charset('utf8');
        //INSERT INTO `tbcolaborador` (`colaboradorid`, `animalnumber`, `guidenumber`, `sellernumber`) VALUES ('30/08/2018-2', '2', '2', '2');
        $queryInsert = "INSERT INTO tbcolaborador VALUES (" . $colaborador->getCedula() . ",'" .
                $colaborador->getNombre() . "','" .
                $colaborador->getApellido1() . "','" .
                $colaborador->getApellido2() . "','" .
                $colaborador->getDireccion() . "'," .
                $colaborador->getTelefono() . ");";

        $result = mysqli_query($conn, $queryInsert);
        mysqli_close($conn);
        return $result;
    }*/

    /*public function insertarTBColaborador($colaborador) {
        $conn = mysqli_connect($this->server, $this->user, $this->password, $this->db);
        $conn->set_charset('utf8');

        $query = "CALL sp_insertar_colaborador(?,?,?,?,?,?)";
        $statement = $conn->prepare($query);
        $statement->bind_param("issssi",$cedula,$nombre,$apellido1,$apellido2,$direccion,$telefono);
        $cedula = $colaborador->getCedula();
        $nombre = $colaborador->getNombre();
        $apellido1 = $colaborador->getApellido1();
        $apellido2 = $colaborador->getApellido2();
        $direccion = $colaborador->getDireccion();
        $telefono = $colaborador->getTelefono();
        $result = $statement->execute();
        $statement->close();
        mysqli_close($conn);
        return $result;
    }*/

    //Actualizar en la tabla de animal registration
    /*public function actualizarTBColaborador($colaborador) {
        $conn = mysqli_connect($this->server, $this->user, $this->password, $this->db);
        $conn->set_charset('utf8');
        $queryUpdate = "UPDATE tbcolaborador SET nombre='" . $colaborador->getNombre() .
                "', apellido1='" . $colaborador->getApellido1() .
                "', apellido2='" . $colaborador->getApellido2() .
                "', direccion='" . $colaborador->getDireccion() .
                "', telefono=" . $colaborador->getTelefono() .
                " WHERE cedula=" . $colaborador->getCedula() . ";";

        $result = mysqli_query($conn, $queryUpdate);
        mysqli_close($conn);
        return $result;
    }*/

    /*public function actualizarTBColaborador($colaborador) {
        $conn = mysqli_connect($this->server, $this->user, $this->password, $this->db);
        $conn->set_charset('utf8');

        $query = "CALL sp_modificar_colaborador(?,?,?,?,?,?)";
        $statement = $conn->prepare($query);
        $statement->bind_param("issssi",$cedula,$nombre,$apellido1,$apellido2,$direccion,$telefono);
        $cedula = $colaborador->getCedula();
        $nombre = $colaborador->getNombre();
        $apellido1 = $colaborador->getApellido1();
        $apellido2 = $colaborador->getApellido2();
        $direccion = $colaborador->getDireccion();
        $telefono = $colaborador->getTelefono();
        $result = $statement->execute();
        $statement->close();
        mysqli_close($conn);
        return $result;
    }*/

    //Eliminar de la tabla de animal registration

    /*public function eliminarTBColaborador($colaboradorCedula) {
        $conn = mysqli_connect($this->server, $this->user, $this->password, $this->db);
        $conn->set_charset('utf8');

        $queryUpdate = "DELETE from tbcolaborador WHERE cedula='" . $colaboradorCedula . "';";
        $result = mysqli_query($conn, $queryUpdate);
        mysqli_close($conn);

        return $result;
    }*/

    /*public function eliminarTBColaborador($cedula) {
        $conn = mysqli_connect($this->server, $this->user, $this->password, $this->db);
        $conn->set_charset('utf8');

        $query = "CALL sp_eliminar_colaborador(?)";
        $statement = $conn->prepare($query);
        $statement->bind_param("i",$cedula);
        $result = $statement->execute();
        $statement->close();
        mysqli_close($conn);
        return $result;
    }*/

    //Mostrar datos de la tabla de animal registration
    /*public function mostrarTBColaborador() {
        $conn = mysqli_connect($this->server, $this->user, $this->password, $this->db);
        $conn->set_charset('utf8');

        $querySelect = "SELECT * FROM tbcolaborador;";
        $result = mysqli_query($conn, $querySelect);
        mysqli_close($conn);
        $lista = [];
        while ($row = mysqli_fetch_array($result)) {
            $colaboradorActual = new Colaborador($row['cedula'], $row['nombre'], $row['apellido1'], $row['apellido2'], $row['direccion'], $row['telefono']);
            array_push($lista, $colaboradorActual);
        }
        return $lista; //retorna el registro
    }*/

    /*public function mostrarTBColaborador()
    {
        $conn = mysqli_connect($this->server, $this->user, $this->password, $this->db);
        $conn->set_charset('utf8');

        /*$query = "CALL sp_mostrar_colaboradores()";
        $statement = $conn->prepare($query);
        $statement->execute();
        $result = $statement->get_result();
        $statement->close();
        $query = "SELECT * FROM tbcolaborador;";
        $result = mysqli_query($conn, $query);
        mysqli_close($conn);
        $colaboradores = [];
        while ($row = mysqli_fetch_array($result)) {
            $colaboradorActual = new Colaborador($row['cedula'], $row['nombre'], $row['apellido1'], $row['apellido2'], $row['direccion'], $row['telefono']);
            array_push($colaboradores, $colaboradorActual);
        }
        return $colaboradores;
    }*/

    public function filtradoColaborador($dato, $inicio, $limite)
    {
        $conn = mysqli_connect($this->server, $this->user, $this->password, $this->db);
        $conn->set_charset('utf8');

        $queryFiltrar = "SELECT * FROM tbcolaborador WHERE cedula LIKE CONCAT('%','" . $dato . "','%') OR nombre LIKE CONCAT('%','" . $dato . "','%') OR apellido1 LIKE CONCAT('%','" . $dato . "','%') OR apellido2 LIKE CONCAT('%','" . $dato . "','%') ORDER BY nombre,apellido1,apellido2 LIMIT $inicio,$limite;";
        $result = mysqli_query($conn, $queryFiltrar);
        mysqli_close($conn);
        $listaColaboradores = [];
        while ($row = mysqli_fetch_array($result)) {
            $colaboradorActual = new Colaborador($row['cedula'], $row['nombre'], $row['apellido1'], $row['apellido2'], $row['direccion'], $row['telefono']);
            array_push($listaColaboradores, $colaboradorActual);
        }
        return $listaColaboradores;
    }

    // Obtiene el total de rows que tiene la tabla con el dato que se desea filtrar
    /*public function getTotalColaboradoresBuscados($dato)
    {
        $conn = mysqli_connect($this->server, $this->user, $this->password, $this->db);
        $conn->set_charset('utf8');
        $query = "SELECT COUNT(*) as cantregistros FROM tbcolaborador WHERE cedula LIKE CONCAT('%','" . $dato . "','%') OR nombre LIKE CONCAT('%','" . $dato . "','%') OR apellido1 LIKE CONCAT('%','" . $dato . "','%') OR apellido2 LIKE CONCAT('%','" . $dato . "','%');";
        $result = mysqli_query($conn, $query);
        mysqli_close($conn);
        $cantidadRegistros = 0;
        if ($row = mysqli_fetch_array($result)) {
            $cantidadRegistros = $row['cantregistros'];
        }
        return $cantidadRegistros;
    }

    // Obtiene el total de colaboradores que hay en la tabla tbcolaborador
    public function getTotalColaboradores()
    {
        $conn = mysqli_connect($this->server, $this->user, $this->password, $this->db);
        $conn->set_charset('utf8');
        $query = "SELECT COUNT(*) AS cantregistros FROM tbcolaborador";
        $result = mysqli_query($conn, $query);
        mysqli_close($conn);
        $cantidadRegistros = 0;
        if ($row = mysqli_fetch_array($result)) {
            $cantidadRegistros = $row['cantregistros'];
        }
        return $cantidadRegistros;
    }*/
}
