<?php

include_once 'Data.php';
include '../domain/Cliente.php';

class ClienteData extends Data
{

    public function mostrarTBCliente()
    {
        $conn = $this->getConexion();
        $clientes = [];
        if ($conn != null) {
            $query = "CALL sp_mostrar_clientes()";
            $statement = $conn->prepare($query);
            $statement->execute();
            while($row = $statement->fetch(PDO::FETCH_BOTH)){
                $clienteActual = new Cliente($row[0],$row[1],$row[2],$row[3],$row[4],$row[5],$row[6]);
                array_push($clientes,$clienteActual);
            }
            $conn = null;
        }
        return $clientes;
    }

    public function insertarTBCliente($cliente)
    {
        $conn = $this->getConexion();
        $result = 0;
        if ($conn != null) {
            $query = "CALL sp_insertar_cliente(?,?,?,?,?,?)";
            $statement = $conn->prepare($query);
            $cliente = [
                $cliente->getCedula(),
                $cliente->getNombre(),
                $cliente->getApellido1(),
                $cliente->getApellido2(),
                $cliente->getDireccion(),
                $cliente->getTelefono()
            ];
            $result = $statement->execute($cliente);
            $conn = null;
        }
        return $result;
    }

    public function actualizarTBCliente($cliente)
    {
        $conn = $this->getConexion();
        $result = 0;
        if ($conn != null) {
            $query = "CALL sp_modificar_cliente(?,?,?,?,?,?,?)";
            $statement = $conn->prepare($query);
            $cliente = [
                $cliente->getIdCliente(),
                $cliente->getCedula(),
                $cliente->getNombre(),
                $cliente->getApellido1(),
                $cliente->getApellido2(),
                $cliente->getDireccion(),
                $cliente->getTelefono()
            ];
            $result = $statement->execute($cliente);
            $conn = null;
        }
        return $result;
    }

    public function eliminarTBCliente($idCliente)
    {
        $conn = $this->getConexion();
        $result = 0;
        if ($conn != null) {
            $query = "CALL sp_eliminar_cliente(?)";
            $statement = $conn->prepare($query);
            $cliente = [
                $idCliente
            ];
            $result = $statement->execute($cliente);
            $conn = null;
        }
        return $result;
    }

    ////// METODOS DE TESTING //////////////////////

    public function buscarTBCliente($cedula)
    {
        $conn = $this->getConexion();
        $cliente = null;
        if ($conn != null) {
            $query = "SELECT * FROM tbcliente WHERE cedula = ?  AND estado = 1 LIMIT 1";
            $statement = $conn->prepare($query);
            $statement->execute([$cedula]);
            if($row = $statement->fetch(PDO::FETCH_BOTH)){
                $cliente = new Cliente($row[0],$row[1],$row[2],$row[3],$row[4],$row[5],$row[6]);
            }
            $conn = null;
        }
        return $cliente;
    }

    public function getLastId()
    {
        $conn = $this->getConexion();
        $id = 0;
        if ($conn != null) {
            $query = "SELECT MAX(idcliente) AS idcliente FROM tbcliente;";
            $statement = $conn->prepare($query);
            $statement->execute();
            if ($row = $statement->fetch(PDO::FETCH_BOTH)) {
                $id = $row[0];
            }
            $conn = null;
        }
        return $id;
    }

    /*public function insertarTBCliente($cliente) {
        $conn = mysqli_connect($this->server, $this->user, $this->password, $this->db);
        $conn->set_charset('utf8');

        $cedula = $cliente->getCedula();
        $nombre = $cliente->getNombre();
        $apellido1 = $cliente->getApellido1();
        $apellido2 = $cliente->getApellido2();
        $direccion = $cliente->getDireccion();
        $telefono = $cliente->getTelefono();

        $preparedStatement = $conn->prepare("Insert INTO tbcliente VALUES(?,?,?,?,?,?)");
        $preparedStatement->bind_param("sssssi", $cedula, $nombre, $apellido1, $apellido2, $direccion, $telefono);
        $result = $preparedStatement->execute();


        $preparedStatement->close();
        mysqli_close($conn); // cerramos coneccion
        return $result;
    }*/

    /*public function insertarTBCliente($cliente) {
        $conn = mysqli_connect($this->server, $this->user, $this->password, $this->db);
        $conn->set_charset('utf8');

        $query = "CALL sp_insertar_cliente(?,?,?,?,?,?)";
        $statement = $conn->prepare($query);
        $statement->bind_param("sssssi",$cedula,$nombre,$apellido1,$apellido2,$direccion,$telefono);
        $cedula = $cliente->getCedula();
        $nombre = $cliente->getNombre();
        $apellido1 = $cliente->getApellido1();
        $apellido2 = $cliente->getApellido2();
        $direccion = $cliente->getDireccion();
        $telefono = $cliente->getTelefono();
        $result = $statement->execute();
        $statement->close();
        mysqli_close($conn);
        return $result;
    }*/

    /*public function actualizarTBCliente($cliente) {
        $conn = mysqli_connect($this->server, $this->user, $this->password, $this->db);
        $conn->set_charset('utf8');

        $query = "CALL sp_modificar_cliente(?,?,?,?,?,?)";
        $statement = $conn->prepare($query);
        $statement->bind_param("sssssi",$cedula,$nombre,$apellido1,$apellido2,$direccion,$telefono);
        $cedula = $cliente->getCedula();
        $nombre = $cliente->getNombre();
        $apellido1 = $cliente->getApellido1();
        $apellido2 = $cliente->getApellido2();
        $direccion = $cliente->getDireccion();
        $telefono = $cliente->getTelefono();
        $result = $statement->execute();
        $statement->close();
        mysqli_close($conn);
        return $result;        
    }*/


    /*public function eliminarTBCliente($idCliente) {
        $conn = mysqli_connect($this->server, $this->user, $this->password, $this->db);
        $conn->set_charset('utf8');

        $queryDelete = "DELETE FROM tbcliente WHERE cedula='" . $idCliente . "';";
        $result = mysqli_query($conn, $queryDelete);
        mysqli_close($conn);

        return $result;
    }*/

    /*public function eliminarTBCliente($idCliente) {
        $conn = mysqli_connect($this->server, $this->user, $this->password, $this->db);
        $conn->set_charset('utf8');

        $query = "CALL sp_eliminar_cliente(?)";
        $statement = $conn->prepare($query);
        $statement->bind_param("i",$idCliente);
        $result = $statement->execute();
        $statement->close();
        mysqli_close($conn);
        return $result;
    }*/

    /*public function mostrarTBCliente() {
        $conn = mysqli_connect($this->server, $this->user, $this->password, $this->db);
        $conn->set_charset('utf8');

        $querySelect = "SELECT * FROM tbcliente;";
        $result = mysqli_query($conn, $querySelect);
        mysqli_close($conn);
        $lista = [];
        while ($row = mysqli_fetch_array($result)) {
            $clienteActual = new Cliente($row['cedula'], $row['nombre'], $row['apellido1'], $row['apellido2'], $row['direccion'], $row['telefono']);
            array_push($lista, $clienteActual);
        }
        return $lista; //retorna el registro
    }*/

    /*public function mostrarTBCliente()
    {
        $conn = mysqli_connect($this->server, $this->user, $this->password, $this->db);
        $conn->set_charset('utf8');
        $query = "SELECT * FROM tbcliente;";
        $result = mysqli_query($conn, $query);
        mysqli_close($conn);
        $clientes = [];
        while ($row = mysqli_fetch_array($result)) {
            $clienteActual = new Cliente($row[0], $row[1], $row[2], $row[3], $row[4], $row[5]);
            array_push($clientes, $clienteActual);
        }
        return $clientes;
    }*/


    /*public function buscarTBCliente($cadena, $inicio, $limite)
    {
        $conn = mysqli_connect($this->server, $this->user, $this->password, $this->db);
        $conn->set_charset('utf8');

        $querySelect = "SELECT * FROM tbcliente WHERE cedula LIKE CONCAT('%','" . $cadena . "','%') OR nombre LIKE CONCAT('%','" . $cadena . "','%') OR apellido1 LIKE CONCAT('%','" . $cadena . "','%') OR apellido2 LIKE CONCAT('%','" . $cadena . "','%') OR direccion LIKE CONCAT('%','" . $cadena . "','%') OR telefono LIKE CONCAT('%','" . $cadena . "','%') ORDER BY nombre,apellido1,apellido2 LIMIT $inicio,$limite;";
        $result = mysqli_query($conn, $querySelect);
        mysqli_close($conn);
        $lista = [];
        while ($row = mysqli_fetch_array($result)) {
            $clienteActual = new Cliente($row['cedula'], $row['nombre'], $row['apellido1'], $row['apellido2'], $row['direccion'], $row['telefono']);
            array_push($lista, $clienteActual);
        }
        return $lista; //retorna el registro
    }*/




    /*public function actualizarTBCliente($cliente) {
        $conn = mysqli_connect($this->server, $this->user, $this->password, $this->db);
        $conn->set_charset('utf8');

        $cedula = $cliente->getCedula();
        $nombre = $cliente->getNombre();
        $apellido1 = $cliente->getApellido1();
        $apellido2 = $cliente->getApellido2();
        $direccion = $cliente->getDireccion();
        $telefono = $cliente->getTelefono();

        $preparedStatement = $conn->prepare("UPDATE tbcliente SET nombre=?,apellido1=?,apellido2=?,direccion=?, telefono=? WHERE cedula=?");
        $preparedStatement->bind_param("ssssis", $nombre, $apellido1, $apellido2, $direccion, $telefono, $cedula);
        $result = $preparedStatement->execute();


        $preparedStatement->close();
        mysqli_close($conn); // cerramos coneccion
        return $result;
    }*/


    /*public function getTotalRegistros()
    {
        $conn = mysqli_connect($this->server, $this->user, $this->password, $this->db);
        $conn->set_charset('utf8');
        $query = "SELECT COUNT(*) as cantregistros FROM tbcliente;";
        $result = mysqli_query($conn, $query);
        mysqli_close($conn);
        $cantidadRegistros = 0;
        if ($row = mysqli_fetch_array($result)) {
            $cantidadRegistros = $row['cantregistros'];
        }
        return $cantidadRegistros;
    }

    public function getTotalRegistrosBuscados($cadena)
    {
        $conn = mysqli_connect($this->server, $this->user, $this->password, $this->db);
        $conn->set_charset('utf8');
        $query = "SELECT COUNT(*) as cantregistros FROM tbcliente WHERE cedula LIKE CONCAT('%','" . $cadena . "','%') OR nombre LIKE CONCAT('%','" . $cadena . "','%') OR apellido1 LIKE CONCAT('%','" . $cadena . "','%') OR apellido2 LIKE CONCAT('%','" . $cadena . "','%') OR direccion LIKE CONCAT('%','" . $cadena . "','%') OR telefono LIKE CONCAT('%','" . $cadena . "','%');";
        $result = mysqli_query($conn, $query);
        mysqli_close($conn);
        $cantidadRegistros = 0;
        if ($row = mysqli_fetch_array($result)) {
            $cantidadRegistros = $row['cantregistros'];
        }
        return $cantidadRegistros;
    }

    public function mostrarRegistrosPaginados($inicio, $limite)
    {
        $conn = mysqli_connect($this->server, $this->user, $this->password, $this->db);
        $conn->set_charset('utf8');
        $query = "SELECT * FROM tbcliente ORDER BY nombre,apellido1,apellido2 Limit $inicio,$limite;";
        $result = mysqli_query($conn, $query);
        mysqli_close($conn);
        $lista = [];
        while ($row = mysqli_fetch_array($result)) {
            $clienteActual = new Cliente($row['cedula'], $row['nombre'], $row['apellido1'], $row['apellido2'], $row['direccion'], $row['telefono']);
            array_push($lista, $clienteActual);
        }
        return $lista;
    }*/
}
