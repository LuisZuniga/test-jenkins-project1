<?php

include_once 'Data.php';
include '../domain/Acodo.php';

class AcodoData extends Data
{

    public function mostrarTBAcodo($idLote, $idSeccion)
    {
        $conn = $this->getConexion();
        $acodos = [];
        if ($conn != null) {
            $query = "CALL sp_mostrar_acodo(?,?)";
            $statement = $conn->prepare($query);
            $data = [
                $idLote,
                $idSeccion
            ];
            $statement->execute($data);
            while ($row = $statement->fetch(PDO::FETCH_BOTH)) {
                $acodoActual = new Acodo($row[0], $row[1], $row[2], $row[3], $row[4], $row[5], $row[6]);
                array_push($acodos, $acodoActual);
            }
            $conn = null;
        }
        return $acodos;
    }


    public function insertarTBAcodo($acodo)
    {
        $conn = $this->getConexion();
        $result = 0;
        if ($conn != null) {
            $query = "CALL  sp_insertar_acodo(?,?,?,?,?)";
            $statement = $conn->prepare($query);
            $data = [
                $acodo->getIdLote(),
                $acodo->getIdSeccion(),
                $acodo->getIdPlanta(),
                $acodo->getCantidad(),
                $acodo->getFecha()
            ];
            $result = $statement->execute($data);
            $conn = null;
        }
        return $result;
    }

    public function modificarTBAcodo($acodo)
    {
        $conn = $this->getConexion();
        $result = 0;
        if ($conn != null) {
            $query = "CALL  sp_modificar_acodo(?,?,?,?)";
            $statement = $conn->prepare($query);
            $data = [
                $acodo->getIdAcodo(),
                $acodo->getIdPlanta(),
                $acodo->getCantidad(),
                $acodo->getFecha()
            ];
            $result = $statement->execute($data);
            $conn = null;
        }
        return $result;
    }

    public function eliminarTBAcodo($idAcodo)
    {
        $conn = $this->getConexion();
        $result = 0;
        if($conn != null){
            $query = "CALL  sp_eliminar_acodo(?)";
            $statement = $conn->prepare($query);
            $result = $statement->execute([$idAcodo]);
            $conn = null;
        }
        return $result;
    }

    /*public function insertarTBAcodo($acodo){
        $conn = mysqli_connect($this->server, $this->user, $this->password, $this->db);
        $conn->set_charset('utf8');
        $query = "CALL  sp_insertar_acodo(?,?,?,?,?)";
        $statement = $conn->prepare($query);
        $statement->bind_param("iiiis",$idLote,$idSeccion,$idPlanta,$cantidad, $fecha);
        $idLote = $acodo->getIdLote();
        $idSeccion = $acodo->getIdSeccion();
        $idPlanta = $acodo->getIdPlanta();
        $cantidad = $acodo->getCantidad();
        $fecha = $acodo->getFecha();
        $result = $statement->execute();
        $statement->close();
        mysqli_close($conn);
        return $result;
    }*/

    /*public function modificarTBAcodo($acodo)
    {
        $conn = mysqli_connect($this->server, $this->user, $this->password, $this->db);
        $conn->set_charset('utf8');
        $query = "CALL  sp_modificar_acodo(?,?,?,?)";
        $statement = $conn->prepare($query);
        $statement->bind_param("iiis", $idAcodo, $idPlanta, $cantidad, $fecha);
        $idAcodo = $acodo->getIdAcodo();
        $idPlanta = $acodo->getIdPlanta();
        $cantidad = $acodo->getCantidad();
        $fecha = $acodo->getFecha();
        $result = $statement->execute();
        $statement->close();
        mysqli_close($conn);
        return $result;
    }*/

    /*public function eliminarTBAcodo($idAcodo)
    {
        $conn = mysqli_connect($this->server, $this->user, $this->password, $this->db);
        $conn->set_charset('utf8');
        $query = "CALL  sp_eliminar_acodo(?)";
        $statement = $conn->prepare($query);
        $statement->bind_param("i", $idAcodo);
        $result = $statement->execute();
        $statement->close();
        mysqli_close($conn);
        return $result;
    }*/



    /*public function mostrarTBAcodo($idLote, $idSeccion) {
        $conn = mysqli_connect($this->server, $this->user, $this->password, $this->db);
        $conn->set_charset('utf8');
        $query = "SELECT tba.idacodo, tba.idlote, tba.idseccion, tba.idplanta, tba.fecha, tba.cantidad, tbp.nombrecomun FROM tbacodo AS tba INNER JOIN tbplanta AS tbp ON tba.idplanta = tbp.idplanta WHERE tba.idlote = " .$idLote. " AND tba.idseccion = ".$idSeccion.";";
        $result = mysqli_query($conn,$query);
        mysqli_close($conn);
        $acodos = [];
        while ($row = mysqli_fetch_array($result)) {
            $acodoActual = new Acodo($row['idacodo'], $row['idlote'], $row['idseccion'], $row['idplanta'], $row['cantidad'], $row['fecha'] , $row['nombrecomun']);
            array_push($acodos, $acodoActual);
        }
        return $acodos;
    }*/
}
