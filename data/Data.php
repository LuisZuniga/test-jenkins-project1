<?php

class Data {

    public $server;
    public $user;
    public $password;
    public $db;
    public $connection;
    public $isActive;

    /* constructor */

    public function __construct() {
        $hostName = gethostname();
        
        switch ($hostName) {
            case "DESKTOP-P3REO0S": //Chris's PC
                $this->isActive = false;
                $this->server = "127.0.0.1";
                $this->user = "root";
                $this->password = "";
                $this->db = "dbornexp";
                break;
            case "So-master": //Luis's PC LINUX
                $this->isActive = false;
                $this->server = "127.0.0.1";
                $this->user = "root";
                $this->password = "";
                $this->db = "dbornexp";
                break;
            case "DESKTOP-R5NP8HE": //Yeu's PC
                $this->isActive = false;
                $this->server = "127.0.0.1";
                $this->user = "root";
                $this->password = "";
                $this->db = "dbornexp";
                break;
            default: //Hosting
                $this->isActive = false;
                $this->server = "107.180.4.72";
                $this->user = "unagrupo3";
                $this->password = "kx%Jfse63Oi";
                $this->db = "unagrupo3"; 
                break;
        }
    }

    public function getConexion(){
        try{
            $dsn  = 'mysql:host=' . $this->server . ';dbname=' . $this->db;
            $conn = new PDO($dsn,$this->user,$this->password,null);
            $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            return $conn;
        }catch(PDOException $e){
            return null;
        }
    }

}

