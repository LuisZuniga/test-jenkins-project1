<?php

include_once 'Data.php';
include '../domain/Fumigacion.php';

class FumigacionData extends Data
{

    public function mostrarTBFumigacion($idSeccion, $idLote)
    {
        $conn = $this->getConexion();
        $fumigaciones = [];
        $query = "CALL sp_mostrar_fumigacion(?,?)";
        $statement = $conn->prepare($query);
        $data = [
            $idSeccion,
            $idLote
        ];
        $statement->execute($data);
        while ($row = $statement->fetch(PDO::FETCH_BOTH)) {
            $fumigacion = new Fumigacion($row[0], $row[1], $row[2], $row[3], $row[4], $row[5], $row[6], $row[7], $row[8]);
            array_push($fumigaciones, $fumigacion);
        }
        return $fumigaciones;
    }

    public function insertarTBFumigacion($fumigacion)
    {
        $conn = $this->getConexion();
        $result = 0;
        if ($conn != null) {
            $query = "CALL sp_insertar_fumigacion(?,?,?,?,?,?)";
            $statement = $conn->prepare($query);
            $data = [
                $fumigacion->getIdSeccion(),
                $fumigacion->getIdLote(),
                $fumigacion->getIdQuimico(),
                $fumigacion->getIdColaborador(),
                $fumigacion->getDosis(),
                $fumigacion->getFechaFumigacion()
            ];
            $result = $statement->execute($data);
            $conn = null;
        }
        return  $result;
    }

    public function modificarTBFumigacion($fumigacion)
    {
        $conn = $this->getConexion();
        $result = 0;
        if ($conn != null) {
            $query = "CALL sp_modificar_fumigacion(?,?,?,?,?)";
            $statement = $conn->prepare($query);
            $data = [
                $fumigacion->getIdFumigacion(),
                $fumigacion->getDosis(),
                $fumigacion->getIdQuimico(),
                $fumigacion->getIdColaborador(),
                $fumigacion->getFechaFumigacion()
            ];
            $result = $statement->execute($data);
            $conn = null;
        }
        return $result;
    }

    public function eliminarTBFumigacion($idFumigacion)
    {
        $conn = $this->getConexion();
        $result = 0;
        if ($conn != null) {
            $query = "CALL sp_eliminar_fumigacion(?)";
            $statement = $conn->prepare($query);
            $idFumigacion = $idFumigacion;
            $result = $statement->execute([$idFumigacion]);
            $conn = null;
        }
        return $result;
    }

    /*public function insertarTBFumigacion($fumigacion) {
        $conn = mysqli_connect($this->server, $this->user, $this->password, $this->db);
        $conn->set_charset('utf8');
        $query = "CALL sp_insertar_fumigacion(?,?,?,?,?,?)";
        $statement = $conn->prepare($query);
        $statement->bind_param("iiiiis",$idSeccion,$idLote, $idQuimico, $idColaborador, $dosis, $fechaFumigacion);
        $idSeccion = $fumigacion->getIdSeccion();
        $idLote = $fumigacion->getIdLote();
        $idQuimico = $fumigacion->getIdQuimico();
        $idColaborador = $fumigacion->getIdColaborador();
        $dosis = $fumigacion->getDosis();
        $fechaFumigacion = $fumigacion->getFechaFumigacion();
        $result = $statement->execute();
        $statement->close();
        mysqli_close($conn);
        return  $result;
    }*/

    /*public function modificarTBFumigacion($fumigacion) {
        $conn = mysqli_connect($this->server, $this->user, $this->password, $this->db);
        $conn->set_charset('utf8');
        $query = "CALL sp_modificar_fumigacion(?,?,?,?,?)";
        $statement = $conn->prepare($query);
        $statement->bind_param("iiiis",$idFumigacion,$dosis, $idQuimico, $idColaborador, $fechaFumigacion);
        $idFumigacion = $fumigacion->getIdFumigacion();
        $idQuimico = $fumigacion->getIdQuimico();
        $idColaborador = $fumigacion->getIdColaborador();
        $dosis = $fumigacion->getDosis();
        $fechaFumigacion = $fumigacion->getFechaFumigacion();
        $result = $statement->execute();
        $statement->close();
        mysqli_close($conn);
        return $result;
    }*/


    /*public function eliminarTBFumigacion($idFumigacion)
    {
        $conn = mysqli_connect($this->server, $this->user, $this->password, $this->db);
        $conn->set_charset('utf8');
        $query = "CALL sp_eliminar_fumigacion(?)";
        $statement = $conn->prepare($query);
        $statement->bind_param("i", $idFumigacion);
        $idFumigacion = $idFumigacion;
        $result = $statement->execute();
        $statement->close();
        mysqli_close($conn);
        return $result;
    }*/


    /*public function mostrarTBFumigacion($idSeccion, $idLote) {
        $conn = mysqli_connect($this->server, $this->user, $this->password, $this->db);
        $conn->set_charset('utf8');
        ////SELECT t1.idfumigacion, t1.idseccion, t1.idlote, t1.dosis, t1.idquimico, t1.fechafumigacion, t2.nombre FROM tbfumigacion AS t1 INNER JOIN tbquimico AS t2 ON t1.idquimico = t2.idquimico
        $query = "SELECT * FROM tbfumigacion WHERE tbfumigacion.idseccion = ".$idSeccion." AND tbfumigacion.idlote = ".$idLote.";";
        $result = mysqli_query($conn, $query);
        /*
        $query = "CALL sp_mostrar_fumigacion(?,?)";
        $statement = $conn->prepare($query);
        $statement->bind_param("ii",$idSeccion,$idLote);
        /*$result = $statement->execute();
        $result = $statement->get_result();
        $statement->close();
        mysqli_close($conn);
        $fumigaciones = [];
        while($row = mysqli_fetch_array($result)){
            $fumigacion = new Fumigacion($row[0],$row[1],$row[2],$row[3],$row[4],$row[5],$row[6]);
            array_push($fumigaciones,$fumigacion);
        }
        return $fumigaciones;
    }*/
}
