<?php

include '../data/TipoQuimicoData.php';

class TipoQuimicoBusiness{

    private $tipoQuimicoData;

    public function __construct(){
        $this->tipoQuimicoData = new TipoQuimicoData();
    }

    public function insertarTBTipoQuimico($tipoQuimico){
        return $this->tipoQuimicoData->insertarTBTipoQuimico($tipoQuimico);
    }

    public function mostrarTBTipoQuimico(){
        return $this->tipoQuimicoData->mostrarTBTipoQuimico();
    }

    public function modificarTBTipoQuimico($tipoQuimico){
        return $this->tipoQuimicoData->modificarTBTipoQuimico($tipoQuimico);
    }

    public function eliminarTBTipoQuimico($idTipoQuimico){
        return $this->tipoQuimicoData->eliminarTBTipoQuimico($idTipoQuimico);
    }

}