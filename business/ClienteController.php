<?php

    include '../business/ClienteBusiness.php';
    $clienteBusiness = new ClienteBusiness();
    $clientes = $clienteBusiness->mostrarTBCliente();

echo   '
    <div class="modal fade" id="modalAgregar" tabindex="-1" role="dialog" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-scrollable" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalScrollableTitle">Agregar Cliente</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form novalidate>
                    <div class="input-box">
                        <input novalidate type="text" required onkeyup="validarAgregarCliente(this)" class="cedula" name="cedula" id="cedula"/><label class="mensaje"></label>
                        <label>Cédula <span class="campo-obligatorio"> * </span></label>
                        <span id="tooltip-cedula" class="tooltip-text" ></span>
                    </div>

                    <div class="input-box">
                        <input type="text" required onkeyup="validarAgregarCliente(this)" class="nombre" name="nombre" id="nombre"/><label class="mensaje"></label>
                        <label>Nombre <span class="campo-obligatorio"> * </span></label>
                        <span id="tooltip-nombre" class="tooltip-text" ></span>
                    </div>
                    
                    <div class="input-box">
                        <input type="text" required onkeyup="validarAgregarCliente(this)" class="nombre" name="apellido1" id="apellido1"/><label class="mensaje"></label>
                        <label>Primer apellido </label>
                        <span id="tooltip-apellido1" class="tooltip-text" ></span>
                    </div>

                    <div class="input-box">
                        <input type="text" required onkeyup="validarAgregarCliente(this)" class="nombre" name="apellido2" id="apellido2"/><label class="mensaje"></label>
                        <label>Segundo apellido</label>
                        <span id="tooltip-apellido2" class="tooltip-text" ></span>
                    </div>

                    <div class="input-box">
                        <input type="text" required onkeyup="validarAgregarCliente(this)" name="direccion" id="direccion"/><label class="mensaje"></label>
                        <label>Dirección </label>
                        <span id="tooltip-direccion" class="tooltip-text" ></span>
                    </div>

                    <div class="input-box">
                        <input type="tel" required onkeyup="validarAgregarCliente(this)" name="telefono" id="telefono"/><label class="mensaje"></label>
                        <label>Teléfono <span class="campo-obligatorio"> * </span></label>
                        <span id="tooltip-telefono" class="tooltip-text" ></span>
                    </div>   
                </form>
                
            </div>
            <div class="modal-footer">
                <input type="button" class="boton boton-success" value="Agregar" name="insert" id="insert" onclick="insertar()">
                <input type="button" class="boton boton-cancelar" value="Cancelar" name="clear" id="clear" onclick="limpiar()">
            </div>
            </div>
        </div>
    </div>



    <div class="modal fade" id="modalModificar" tabindex="-1" role="dialog" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalScrollableTitle">Modificar Cliente</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form novalidate>
                    <input type="hidden" id="idCliente">

                    <div class="input-box">
                        <input type="text" required onkeyup="validarActualizarCliente(this)" class="cedula" name="nCedula" id="nCedula"/><label class="mensaje"></label>
                        <label>Cédula <span class="campo-obligatorio"> * </span></label>
                        <span id="tooltip-nCedula" class="tooltip-text" ></span>
                    </div>

                    <div class="input-box">
                        <input type="text" required onkeyup="validarActualizarCliente(this)" class="nombre" name="nNombre" id="nNombre"/><label class="mensaje"></label>
                        <label>Nombre <span class="campo-obligatorio"> * </span></label>
                        <span id="tooltip-nNombre" class="tooltip-text" ></span>
                    </div>

                    <div class="input-box">
                        <input type="text" required onkeyup="validarActualizarCliente(this)" class="nombre" name="nApellido1" id="nApellido1"/><label class="mensaje"></label>
                        <label>Primer apellido </label>
                        <span id="tooltip-nApellido1" class="tooltip-text" ></span>
                    </div>

                    <div class="input-box">
                        <input type="text" required onkeyup="validarActualizarCliente(this)"  class="nombre" name="nApellido2" id="nApellido2"/><label class="mensaje"></label>
                        <label>Segundo apellido</label>
                        <span id="tooltip-nApellido2" class="tooltip-text" ></span>
                    </div>

                    <div class="input-box">
                        <input type="text" required onkeyup="validarActualizarCliente(this)"  name="nDireccion" id="nDireccion"/><label class="mensaje"></label>
                        <label>Dirección </label>
                        <span id="tooltip-nDireccion" class="tooltip-text" ></span>
                    </div>

                    <div class="input-box">
                        <input type="tel" required onkeyup="validarActualizarCliente(this)"name="nTelefono" id="nTelefono"/><label class="mensaje"></label>
                        <label>Teléfono <span class="campo-obligatorio"> * </span></label>
                        <span id="tooltip-nTelefono" class="tooltip-text" ></span>
                    </div>  
                </form> 
            </div>
            <div class="modal-footer">
            <input type="button" class="boton boton-primario" value="Confirmar" name="update" id="update" onclick="actualizar()">
            <input type="button" class="boton boton-cancelar" value="Cancelar" name="clear" id="clear" onclick="limpiarActualizar()">
            </div>
        </div>
        </div>
    </div>';

        echo '<table id="tabla_clientes" class="table-boxes display" cellspacing="0" cellpadding="0">
            <thead>
                <tr id ="cabeceraTabla">
                    <th hidden></th>
                    <th>Cédula</th>
                    <th>Nombre</th>
                    <th>Primer apellido</th>
                    <th>Segundo apellido</th>
                    <th>Dirección</th>
                    <th>Teléfono</th>
                    <th class="botones no-sort"></th>
                    <th class="botones no-sort"></th>
                </tr>
            </thead>
            <tbody>';

            foreach ($clientes as $clienteActual) {
                echo '<tr>';
                echo '<td hidden>' . $clienteActual->getIdCliente() . '</td>';
                echo '<td>' . $clienteActual->getCedula() . '</td>';
                echo '<td>' . $clienteActual->getNombre() . '</td>';
                echo '<td>' . $clienteActual->getApellido1() . '</td>';
                echo '<td>' . $clienteActual->getApellido2() . '</td>';
                echo '<td>' . $clienteActual->getDireccion() . '</td>';
                echo '<td>' . $clienteActual->getTelefono() . '</td>';
                echo '<td><button class="boton boton-primario tool" id="Actualizar" title="Editar" onclick="levantarActualizar(this)"><i class="fa fa-edit" aria-hidden="true"></i></button></td>';
                echo '<td><button class="boton boton-cancelar tool" id="Eliminar" title="Eliminar" onclick="levantarEliminar('.$clienteActual->getIdCliente().')"><i class="fa fa-trash" aria-hidden="true"></i></button></td>';
                echo '</tr>';
            }
            echo '
            </tbody>
            
        </table>';