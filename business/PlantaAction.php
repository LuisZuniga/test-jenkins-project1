<?php

include './PlantaBusiness.php';

if(isset($_POST['update'])){
    if(isset($_POST['idPlanta']) && isset($_POST['nombreComun']) && isset($_POST['nombreCientifico']) && isset($_POST['cantidad']) && isset($_POST['precioUnitario'])){
        $idPlanta = $_POST['idPlanta'];
        $nombreComun = $_POST['nombreComun'];
        $nombreCientifico = $_POST['nombreCientifico'];
        $precioUnitario = str_replace("$ ","",$_POST['precioUnitario']);
        $cantidad = $_POST['cantidad'];
        $plantaBusiness = new PlantaBusiness();
        $planta = new Planta($idPlanta, $nombreComun, $nombreCientifico,$cantidad, $precioUnitario);
        $result = $plantaBusiness->actualizarTBPlanta($planta);
        echo $result;
    }else{
        echo '0';
    }
}else if(isset($_POST['delete'])){
    if(isset($_POST['idPlanta'])){
        $idPlanta = $_POST['idPlanta'];
        $plantaBusiness = new PlantaBusiness();            
        $result = $plantaBusiness->eliminarTBPlanta($idPlanta);
        echo $result;
    }else{
        echo '0';
    }
}else if(isset($_POST['create'])){
    if(isset($_POST['nombreComun']) && isset($_POST['nombreCientifico']) && isset($_POST['precioUnitario'])){
        $nombreComun = $_POST['nombreComun'];
        $nombreCientifico = $_POST['nombreCientifico'];
        $precioUnitario = $_POST['precioUnitario'];
        $plantaBusiness = new PlantaBusiness();
        $planta = new Planta(0, $nombreComun, $nombreCientifico, 0, $precioUnitario);
        $result = $plantaBusiness->insertarTBPlanta($planta);
        echo $result;
    }else{
        echo '0';
    }
}else if(isset($_POST['total'])){
    $plantaBusiness = new PlantaBusiness();
    echo $plantaBusiness->getTotalPlantas();
}