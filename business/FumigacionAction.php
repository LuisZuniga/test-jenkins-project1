<?php

include './FumigacionBusiness.php';
include './QuimicoBusiness.php';

if(isset($_POST['create'])){
    if(isset($_POST['idSeccion']) && isset($_POST['idLote']) &&  isset($_POST['colaboradorEncargado']) && isset($_POST['dosis'])
    && isset($_POST['fechaFumigacion']) && isset($_POST['idQuimico'])){

        $idSeccion = $_POST['idSeccion'];
        $idLote = $_POST['idLote'];
        $idQuimico = $_POST['idQuimico'];
        $idColaborador= $_POST['colaboradorEncargado'];
        $dosis = $_POST['dosis'];
        $fechaFumigacion = $_POST['fechaFumigacion'];

        $quimicioBusiness = new QuimicoBusiness();
        $cantidadStock = $quimicioBusiness->getCantidadQuimico($idQuimico); // Me devuelve la cantidad de quimico que hay en el stock.
        
        if ($dosis <= $cantidadStock) {// Me compara si la dosis que desea aplicar es menor que la del stock.
            
            $fumigacionBusiness = new FumigacionBusiness();
            $fumigacion = new Fumigacion(0, $idSeccion, $idLote, $idQuimico, $idColaborador, $dosis, $fechaFumigacion);
            $result = $fumigacionBusiness->insertarTBFumigacion($fumigacion);
            if ($result == "1") {
                $cantidadRestante = $cantidadStock - $dosis;
                $result = $quimicioBusiness->actualizarCantidad($idQuimico, $cantidadRestante); //Actualiza la cantidad del stock, restandole la cantidad de dosis aplicada.
            }
            echo $result;
        } else {
            echo '2';
        }
    }else{
        echo '0';
    }
} else if(isset($_POST['update'])) {
    if (isset($_POST['idFumigacion']) && isset($_POST['dosis']) && isset($_POST['colaboradorEncargado']) && isset($_POST['idQuimico']) && isset($_POST['fechaFumigacion'])) {
        
        $idFumigacion = $_POST['idFumigacion'];
        $idQuimico = $_POST['idQuimico'];
        $idColaborador= $_POST['colaboradorEncargado'];
        $dosis = $_POST['dosis'];
        $fechaFumigacion = $_POST['fechaFumigacion'];

        $fumigacionBusiness = new FumigacionBusiness();
        $fumigacion = new Fumigacion($idFumigacion, 0, 0, $idQuimico, $idColaborador, $dosis, $fechaFumigacion);
        $result = $fumigacionBusiness->modificarTBFumigacion($fumigacion);

        echo $result;
    } else {
        echo '0';
    }
    
} else if(isset($_POST['delete'])) {
    if (isset($_POST['idFumigacion'])) {
        $idFumigacion = $_POST['idFumigacion'];
        $fumigacionBusiness = new FumigacionBusiness();
        $result = $fumigacionBusiness->eliminarTBFumigacion($idFumigacion);
        
        echo $result;
    } else {
        echo '0';
    }
    
}
