<?php

include './ClienteBusiness.php';

if (isset($_POST['Insertar'])) {
    if (isset($_POST['cedula']) && isset($_POST['nombre']) && isset($_POST['apellido1']) && isset($_POST['apellido2']) && isset($_POST['direccion']) && isset($_POST['telefono'])) {
        $cedula = $_POST['cedula'];
        $nombre = $_POST['nombre'];
        $apellido1 = $_POST['apellido1'];
        $apellido2 = $_POST['apellido2'];
        $direccion = $_POST['direccion'];
        $telefono = $_POST['telefono'];
        $clienteBusiness = new ClienteBusiness();
        $cliente = new Cliente(0,$cedula, $nombre, $apellido1, $apellido2, $direccion, $telefono);
        $result = $clienteBusiness->insertarTBCliente($cliente);
        echo $result;
    } else {
        echo '0';
    }
} else if (isset($_POST['Eliminar'])) {
    $idCliente = $_POST['idCliente'];
    if (strlen($idCliente) > 0) {
        $clienteBusiness = new ClienteBusiness();
        $result = $clienteBusiness->eliminarTBCliente($idCliente);
        echo $result;
    } else {
        echo '0';
    }
    
} else if (isset($_POST['Actualizar'])) {
    if (isset($_POST['cedula']) && isset($_POST['nombre']) && isset($_POST['apellido1']) && isset($_POST['apellido2']) && isset($_POST['direccion']) && isset($_POST['telefono'])) {
        $idCliente = $_POST['idCliente'];
        $cedula = $_POST['cedula'];
        $nombre = $_POST['nombre'];
        $apellido1 = $_POST['apellido1'];
        $apellido2 = $_POST['apellido2'];
        $direccion = $_POST['direccion'];
        $telefono = $_POST['telefono'];
        $clienteBusiness = new ClienteBusiness();
        $cliente = new Cliente($idCliente,$cedula, $nombre, $apellido1, $apellido2, $direccion, $telefono);
        $result = $clienteBusiness->actualizarTBCliente($cliente);
        echo $result;
    } else {
        echo '0';
    }
}


