<?php

include '../business/AcodoBusiness.php';
include '../business/PlantaBusiness.php';

   
    $lote = $_POST['lote'];
    $seccion = $_POST['seccion'];

    $AcodoBusiness = new AcodoBusiness();
    $acodos = $AcodoBusiness->mostrarTBAcodo($lote, $seccion);
    $PlantaBusiness = new PlantaBusiness();
    $plantas = $PlantaBusiness->mostrarTBPlanta();
   

    echo '<div class="modal fade" id="modalAgregar" tabindex="-1" role="dialog" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalScrollableTitle">Agregar Acodo</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form novalidate>

                            <input type="hidden" id="lote" value="'.$lote.'"/>
                            <input type="hidden" id="seccion" value="'.$seccion.'"/>

                            <div class="input-box">
                                <select required onchange="validarAgregarAcodo(this)" name="planta" id="planta">';                    
                                    foreach($plantas as $plantaActual){
    echo                            '<option value="'.$plantaActual->getIdPLanta().'">'.$plantaActual->getNombreComun().'</option>';
                                    }
            
    echo                        '</select>
                                <label>Seleccione una planta <span class="campo-obligatorio"> * </span></label>
                                <span id="tooltip-planta" class="tooltip-text" data-tooltip="Solo numeros"></span>
                            </div>
        
                            <div class="input-box">
                                <input required onkeyup="validarAgregarAcodo(this)" type="text"  class="nombre" name="cantidad" id="cantidad">
                                <label>Cantidad <span class="campo-obligatorio"> * </span></label>
                                <span id="tooltip-cantidad" class="tooltip-text" data-tooltip="Solo numeros"></span>
                            </div>

                            <div class="input-box">
                                <input type="date"  onchange="validarAgregarAcodo(this)" name="fecha" id="fecha">
                                <label>Selecione la fecha <span class="campo-obligatorio"> * </span></label>
                                <span id="tooltip-fecha" class="tooltip-text" data-tooltip="Solo numeros"></span>
                            </div>
                        </form>
                    </div>
                        <div class="modal-footer">
                            <input type="button" class="boton boton-success" value="Agregar" name="insert" id="insert" onclick="insertar()">
                            <input type="button" class="boton boton-cancelar" value="Descartar" name="clear" id="clear" onclick="limpiar()">
                        </div>
                    </div>
                </div>
            </div>';

    echo '<div class="modal fade" id="modalModificar" tabindex="-1" role="dialog" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalScrollableTitle">Modificar Acodo</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form novalidate>

                            <input type="hidden" id="nLote" value="'.$lote.'"/>
                            <input type="hidden" id="nSeccion" value="'.$seccion.'"/>
                            <input type="hidden" id="nAcodo" value=""/>

                            <div class="input-box">
                                <select onchange="validarActualizarAcodo(this)" name="nPlanta" id="nPlanta">';                    
                                    foreach($plantas as $plantaActual){
                                echo'<option value="'.$plantaActual->getIdPLanta().'">'.$plantaActual->getNombreComun().'</option>';
                                    }
                            
                            echo'</select>
                                <label>Seleccione una planta <span class="campo-obligatorio"> * </span></label>
                                <span id="tooltip-nPlanta" class="tooltip-text" data-tooltip="Solo numeros"></span>
                            </div>
                    
                            <div class="input-box">
                                <input  onkeyup="validarActualizarAcodo(this)" type="text"  class="nombre" name="nCantidad" id="nCantidad">
                                <label>Cantidad <span class="campo-obligatorio"> * </span></label>
                                <span id="tooltip-nCantidad" class="tooltip-text" data-tooltip="Solo numeros"></span>
                            </div>


                            <div class="input-box">
                                <input type="date"  onchange="validarActualizarAcodo(this)" name="nFecha" id="nFecha">
                                <label>Selecione la fecha <span class="campo-obligatorio"> * </span></label>
                                <span id="tooltip-nFecha" class="tooltip-text" data-tooltip="Solo numeros"></span>
                            </div>
                        </form>
                    </div>
                        <div class="modal-footer">
                            <input type="button" class="boton boton-primario" value="Actualizar" name="update" id="update" onclick="actualizar()">
                            <input type="button" class="boton boton-cancelar" value="Descartar" name="clear" id="clear" onclick="limpiarActualizar()">
                        </div>
                    </div>
                </div>
            </div>';

    
    echo '<table id="tabla_acodos" class="table-boxes display" cellpadding="0" cellspacing="0" >
            <thead>
                <tr>
                    <th hidden></th>
                    <th hidden></th>
                    <th># Sección</th>
                    <th># Lote</th>
                    <th>Planta</th>
                    <th>Fecha de Acodo</th>
                    <th>Cantidad</th>       
                    <th class="botones no-sort"></th>
                    <th class="botones no-sort"></th>
                </tr>
            </thead>
            <tbody>
        ';

        
        foreach ($acodos as $acodoActual) {
            echo '<form method="post" enctype="multipart/form-data" action="../business/FumigacionAction.php">';
            echo '<tr>';
            echo '<td hidden>' . $acodoActual->getIdAcodo() . '</td>';
            echo '<td hidden>' . $acodoActual->getIdPLanta() . '</td>';
            echo '<td>' . $acodoActual->getIdSeccion() . '</td>';
            echo '<td>' . $acodoActual->getIdLote() . '</td>';            
            echo '<td>' . $acodoActual->getNombrePlanta() . '</td>';
            echo '<td>' . $acodoActual->getCantidad() . '</td>';
            echo '<td>' . $acodoActual->getFecha() . '</td>';
            echo '<td><button class="boton boton-primario tool" title="Editar" id="Actualizar" onclick="levantarActualizar(this)"><i class="fa fa-edit" aria-hidden="true"></i></button></td>';
            echo '<td><button class="boton boton-cancelar tool" title="Borrar" id="Eliminar" onclick="levantarEliminar('.$acodoActual->getIdAcodo().')"><i class="fa fa-trash" aria-hidden="true"></i></button></td>';
            echo '</tr>';
            echo '</form>';
        }

        echo '
        
            </tbody>
        </table>';