<?php

include '../data/SuministroData.php';

class SuministroBusiness {
    
   private $suministroData; 
     
   public function __construct(){
        $this->suministroData = new SuministroData();
    }
    
    public function insertarTBSuministro($suministros){
        return $this->suministroData->insertarTBSuministro($suministros);
    }

    public function eliminarTBSuministro($suministroId){
        return $this->suministroData->eliminarTBSuministro($suministroId);
    }

    public function actualizarTBSuministro($suministros){
        return $this->suministroData->actualizarTBSuministro($suministros);
    }

    public function mostrarTBSuministro(){
        return $this->suministroData->mostrarTBSuministro();
    }

    /*
    public function mostrarTBTipoSuministro(){
        return $this->suministroData->mostrarTBTipoSuministro();
    }
    */

    public function buscarSuministros($dato, $inicio, $limite){
        return $this->suministroData->buscarSuministros($dato, $inicio, $limite);
    }

    /*
    public function mostrarTBTipoSuministroEspecifico(){
        return $this->suministroData->mostrarTBTipoSuministroEspecifico();
    }
    */
    
    public function consultarTipoFinca($filtro){
        return $this->suministroData->consultarTipoFinca($filtro);
    }

    public function getTotalRegistros(){
        return $this->suministroData->getTotalRegistros();
    }

    public function mostrarRegistrosPaginados($inicio, $limite){
        return $this->suministroData->mostrarRegistrosPaginados($inicio, $limite);
    }

    public function getTotalRegistrosBuscados($cadena){
        return $this->suministroData->getTotalRegistrosBuscados($cadena);
    }

    public function mostrarSuministrosAgotados(){
        return $this->suministroData->mostrarSuministrosAgotados();
    }
}
