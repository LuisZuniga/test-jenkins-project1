<?php
include '../data/FumigacionData.php';

class FumigacionBusiness {
    
    private $fumigacionData; 
     
   public function __construct(){
        $this->fumigacionData = new FumigacionData();
    }
    
    public function insertarTBFumigacion($fumigacion) {
        return $this->fumigacionData->insertarTBFumigacion($fumigacion);
    }

    public function modificarTBFumigacion($fumigacion) {
        return $this->fumigacionData->modificarTBFumigacion($fumigacion);
    }

    public function mostrarTBFumigacion($idSeccion, $idLote) {
        return $this->fumigacionData->mostrarTBFumigacion($idSeccion, $idLote);
    }

    public function eliminarTBFumigacion($idFumigacion) {
        return $this->fumigacionData->eliminarTBFumigacion($idFumigacion);
    }
    
}
