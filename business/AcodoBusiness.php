<?php

include '../data/AcodoData.php';

class AcodoBusiness {

    private $acodoData;

    public function __construct() {
        $this->acodoData = new AcodoData();
    }

    public function insertarTBAcodo($acodo) {
        return $this->acodoData->insertarTBAcodo($acodo);
    }
    
    public function modificarTBAcodo($acodo) {
        return $this->acodoData->modificarTBAcodo($acodo);
    }

    public function mostrarTBAcodo($idLote, $idSeccion){
        return $this->acodoData->mostrarTBAcodo($idLote, $idSeccion);
    }
    
    public function eliminarTBAcodo($idAcodo){
        return $this->acodoData->eliminarTBAcodo($idAcodo);
    }
}