<?php

include './ColaboradorBusiness.php';
$accion = ($_POST['accion']);

if($accion == "Actualizar"){
    if(isset($_POST['idColaborador']) && isset($_POST['cedula']) && isset($_POST['nombre']) &&  isset($_POST['apellido1']) && isset($_POST['apellido2'])
    && isset($_POST['direccion']) && isset($_POST['telefono'])){

        $idColaborador = $_POST['idColaborador'];
        $cedula = $_POST['cedula'];
        $nombre = $_POST['nombre'];
        $apellido1 = $_POST['apellido1'];
        $apellido2 = $_POST['apellido2'];
        $direccion = $_POST['direccion'];
        $telefono = $_POST['telefono'];
        $colaboradorBusiness = new ColaboradorBusiness();
        $colaborador = new Colaborador($idColaborador,$cedula, $nombre, $apellido1, $apellido2, $direccion, $telefono);
        $result = $colaboradorBusiness->actualizarTBColaborador($colaborador);
        echo $result;           
    }else{
        echo '0';
    }
}else if($accion == "Eliminar"){
    if(isset($_POST['cedula'])){
        $cedula = $_POST['cedula'];
        $colaboradorBusiness = new ColaboradorBusiness();
        $result = $colaboradorBusiness->eliminarTBColaborador($cedula);
        echo $result;         
    }else{
        echo '0';
    }
}else if($accion == "Insertar"){
    if(isset($_POST['cedula']) && isset($_POST['nombre']) &&  isset($_POST['apellido1']) && isset($_POST['apellido2'])
    && isset($_POST['direccion']) && isset($_POST['telefono'])){

        $cedula = $_POST['cedula'];
        $nombre = $_POST['nombre'];
        $apellido1= $_POST['apellido1'];
        $apellido2 = $_POST['apellido2'];
        $direccion = $_POST['direccion'];
        $telefono = $_POST['telefono'];
        $colaboradorBusiness = new ColaboradorBusiness();
        $Colaborador = new Colaborador($cedula, $nombre, $apellido1, $apellido2, $direccion, $telefono);
        $result = $colaboradorBusiness->insertarTBColaborador($Colaborador);

        echo $result;
    }else{
        echo '0';
    }
}
