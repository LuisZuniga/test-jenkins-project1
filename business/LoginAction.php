<?php
    include './LoginBusiness.php';

    if(isset($_POST['login'])){
        $loginBusiness = new LoginBusiness();
        $username =  $_POST['username'];
        $password = $_POST['password'];
        $user = new Login(null, $username,$password,0,0);
        echo $loginBusiness->iniciarSession($user);
    }else if(isset($_POST['changename'])){
        $loginBusiness = new LoginBusiness();
        $username = $_POST['username'];
        echo $loginBusiness->actualizarNombreUsuario($username);
    }else if(isset($_POST['changepass'])){
        $loginBusiness = new LoginBusiness();
        $cpass = $_POST['cpass']; //current pass
        $npass = $_POST['npass']; //new pass
        echo $loginBusiness->actualizarContrasena($cpass, $npass);
    }else if(isset($_POST['changemail'])){
        $loginBusiness = new LoginBusiness();
        $email = $_POST['email'];
        echo $loginBusiness->actualizarEmail($email);
    }else if(isset($_POST['changenotification'])){
        $loginBusiness = new LoginBusiness();
        $notification = $_POST['notification'];
        echo $loginBusiness->actualizarNotificacion($notification);
    }else if(isset($_POST['getUsuario'])){
        $loginBusiness = new LoginBusiness();
        header('Content-Type: application/json');
        echo json_encode($loginBusiness->getTBUsuario());
    } else if(isset($_POST['getNotificationType'])) {
        echo $_SESSION['notificacion'];
    }