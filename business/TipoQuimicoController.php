<?php
include '../business/TipoQuimicoBusiness.php';

//-----------------Instancias---------------------

$TipoQuimicoBusiness = new TipoQuimicoBusiness();
$tiposQuimico = $TipoQuimicoBusiness->mostrarTBTipoQuimico();


//----------------------FORM AGREGAR------------
echo '  <div class="modal fade" id="modalAgregar" tabindex="-1" role="dialog" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalScrollableTitle">Agregar Tipo de Químico</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form novalidate>
                        <div class="input-box">
                            <input onkeyup="validarAgregarTipoQuimico(this)" required type="text" class="nombre" name="nombre" id="nombre"/>
                            <label>Nombre del tipo químico</label>
                            <span id="tooltip-nombre" class="tooltip-text" ></span>
                        </div>
                    </form>
                </div>
                    <div class="modal-footer">
                        <input type="button" class="boton boton-success" value="Agregar" name="insert" id="insert" onclick="insertar()">
                        <input type="button" class="boton boton-cancelar" value="Descartar" name="clear" id="clear" onclick="limpiar()">
                    </div>
                </div>
            </div>
        </div>

        <!-- Formulario de actualizar tipo de químico !-->
        <div class="modal fade" id="modalModificar" tabindex="-1" role="dialog" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalScrollableTitle">Modificar Tipo de Químico</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form novalidate>
                            <input type="hidden" id="nIdTipoQuimico">

                            <div class="input-box">
                                <input type="text" required onkeyup="validarActualizarTipoQuimico(this)" class="nombre" name="nNombre" id="nNombre"/><label class="mensaje"></label>
                                <label>Nombre del tipo de químico</label>
                                <span id="tooltip-nNombre" class="tooltip-text" ></span>
                            </div>
                        </form>
                    </div>
                <div class="modal-footer">
                    <input type="button" class="boton boton-primario" value="Confirmar" name="update" id="update" onclick="actualizarTipoQuimico()"/>
                    <input type="button" class="boton boton-cancelar" value="Descartar" name="clear" id="clear" onclick="limpiar()"/>
                </div>
            </div>
        </div>
    </div>';

    echo '<table id="tabla_tipoQuimicos" class="table-boxes display" cellspacing="0" cellpadding="0">
                <thead>
                    <tr>
                        <th>Tipo Quimico</th>
                        <th>Nombre</th>
                        <th class="botones no-sort"></th>
                        <th class="botones no-sort"></th>
                    </tr> 
                </thead> 
                <tbody>';
    foreach ($tiposQuimico as $tipo) {
        echo '<tr>';
        echo '<td id="idTipoQuimico">' . $tipo->getIdTipoQuimico() . '</td>';
        echo '<td>' . $tipo->getNombre() . '</td>';
        echo '<td><button class="boton boton-primario tool" id="Actualizar" title="Editar" onclick="levantarActualizar(this)"><i class="fa fa-edit" aria-hidden="true"></i></button></td>';
        echo '<td><button class="boton boton-cancelar tool" id="Eliminar" title="Eliminar" onclick="levantarEliminar(' . $tipo->getIdTipoQuimico() . ')"><i class="fa fa-trash" aria-hidden="true"></i></button></td>';
        echo '</tr>';
    }

    echo '</tbody>
            </table>';
