<?php

include './AcodoBusiness.php';

if (isset($_POST['create'])) {
    if (isset($_POST['idLote']) && isset($_POST['idSeccion']) && isset($_POST['planta']) && isset($_POST['cantidad']) && isset($_POST['fecha'])) {
        
        $idLote = $_POST['idLote'];
        $idSeccion = $_POST['idSeccion'];
        $planta = $_POST['planta'];
        $cantidad = $_POST['cantidad'];
        $fecha = $_POST['fecha'];

        $acodoBusiness = new AcodoBusiness();
        $acodo = new Acodo(0, $idLote, $idSeccion, $planta, $cantidad, $fecha);
        $result = $acodoBusiness->insertarTBAcodo($acodo);
        echo $result;
    } else {
        echo '0';
    }
} else if (isset($_POST['update'])) {
    if (isset($_POST['idAcodo']) && isset($_POST['cantidad']) && isset($_POST['fecha']) && isset($_POST['planta'])) {
        $idAcodo = $_POST['idAcodo'];
        $cantidad = $_POST['cantidad'];
        $fecha = $_POST['fecha'];
        $idPlanta = $_POST['planta'];

        $acodoBusiness = new AcodoBusiness();
        $acodo = new Acodo($idAcodo, 0, 0, $idPlanta, $cantidad, $fecha);
        $result = $acodoBusiness->modificarTBAcodo($acodo);
        
        echo $result;
    } else {
        echo '0';
    }
} else if (isset($_POST['delete'])) {
    if (isset($_POST['idAcodo'])) {

        $idAcodo = $_POST['idAcodo'];
        $acodoBusiness = new AcodoBusiness();
        $result = $acodoBusiness->eliminarTBAcodo($idAcodo);
        echo $result;
    } else {
        echo '0';
    }
}

