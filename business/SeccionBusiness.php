<?php

include '../data/SeccionData.php';

class SeccionBusiness {
    
   private $seccionData; 
     
   public function __construct(){
        $this->seccionData = new SeccionData();
    }
    
    public function insertarTBSeccion($seccion){
        return $this->seccionData->insertarTBSeccion($seccion);
    }

    public function eliminarTBSeccion($planta,$numeroseccion,$numerolote){
        return $this->seccionData->eliminarTBSeccion($planta,$numeroseccion,$numerolote);
    }

    public function actualizarTBSeccion($seccion){
        return $this->seccionData->actualizarTBSeccion($seccion);
    }

    public function mostrarTBSeccion($lote,$seccion){
        return $this->seccionData->mostrarTBSeccion($lote,$seccion);
    }

    /*public function mostrarTBLote(){
        return $this->seccionData->mostrarTBLote();
    }*/

    /*public function mostrarTBLoteEspecifico(){
        return $this->seccionData->mostrarTBLoteEspecifico();
    }*/

    /*public function getTotalRegistros(){
        return $this->seccionData->getTotalRegistros();
    }*/

    /*public function mostrarRegistrosPaginados($inicio, $limite){
        return $this->seccionData->mostrarRegistrosPaginados($inicio, $limite);
    }*/

    /*public function mostrarSeccionPorLote($lote,$seccion){
        return $this->seccionData->mostrarSeccionPorLote($lote,$seccion);
    }*/

    /*public function  getTotalRegistrosBuscados($cadena){
        return $this->seccionData-> getTotalRegistrosBuscados($cadena);
    }*/

    /*public function buscarSeccion($cadena,$inicio, $limite){
        return $this->seccionData->buscarSeccion($cadena,$inicio, $limite);
    }*/

    /*public function buscarSeccion($lote,$seccion,$cadena,$inicio, $limite){
        return $this->seccionData->buscarSeccion($lote,$seccion,$cadena,$inicio, $limite);
    }*/

    function getPlantasPorLote($idLote){
        return $this->seccionData->getPlantasPorLote($idLote);        
    }

    function getPlantasSL($idPlanta,$idSeccion,$idLote){
        return $this->seccionData->getPlantasSL($idPlanta,$idSeccion,$idLote);        
    }

    /*function getDiasRestantesParaCosechar($fechaActual, $fechaExtraccion){
        return $this->seccionData->getDiasRestantesParaCosechar($fechaActual, $fechaExtraccion);        
    }*/

    function mostrarSecciones($fechaActual){
        return $this->seccionData->mostrarSecciones($fechaActual);        
    }
    

}
