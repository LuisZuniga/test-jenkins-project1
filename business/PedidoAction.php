<?php

include './PedidoBusiness.php';
include './ClienteBusiness.php';
include './PlantaBusiness.php';

// action pedido.

if (isset($_POST['create'])) {

    if (isset($_POST['pedido'])) {
        $pedido = $_POST['pedido'];
        $pedido = json_decode($pedido);
        $pedidoBusiness = new PedidoBusiness();
        $plantaBusiness = new PlantaBusiness();

        $arrayPedidoClientes = [];
        $montoTotal = 0;
        foreach ($pedido->pedidoCliente as $cliente) {
            $arrayPedidoClientePlantas = [];

            if ($cliente->idCliente != 0) {
                foreach ($cliente->pedidoClientePlanta as $planta) {
                    //($idPlanta, $idPedidoClientePlanta, $idClientePedido, $cantidad)
                    $pedidoClientePlanta = new PedidoClientePlanta($planta->idPlanta, 0, 0, $planta->cantidad);
                    array_push($arrayPedidoClientePlantas, $pedidoClientePlanta);
                    $montoTotal += ($plantaBusiness->getPrecioUnitario($planta->idPlanta) * ($planta->cantidad));
                }
                //($idPedido, $idPedidoCliente, $idCliente, $pedidoClientePlanta)
                $pedidoCliente = new PedidoCliente(0, 0, $cliente->idCliente, $arrayPedidoClientePlantas);
                array_push($arrayPedidoClientes, $pedidoCliente);
            }
        }

        //($idPedido, $montoTotal, $fechaEnvio, $pedidoCliente)
        $pedidoObject = new Pedido(0, $montoTotal, $pedido->fechaPedido, $arrayPedidoClientes);

        $result = $pedidoBusiness->insertarTBPedido($pedidoObject);
        echo $result;
    } else {
        echo '0';
    }
} else if (isset($_POST['delete'])) {
    if (isset($_POST['idPedido'])) {
        $idPedido = $_POST['idPedido'];
        $pedidoBusiness = new PedidoBusiness();
        $result = $pedidoBusiness->eliminarTBPedido($idPedido);

        echo $result;
    } else {
        echo '0';
    }
} else if (isset($_POST['update'])) {
    $pedido = $_POST['pedido'];
    $pedido = json_decode($pedido);
    $pedidoBusiness = new PedidoBusiness();
    $plantaBusiness = new PlantaBusiness();

    $arrayPedidoClientes = [];
    $montoTotal = 0;
    foreach ($pedido->pedidoCliente as $cliente) {
        $arrayPedidoClientePlantas = [];
        foreach ($cliente->pedidoClientePlanta as $planta) {
            //($idPlanta, $idPedidoClientePlanta, $idClientePedido, $cantidad)
            $pedidoClientePlanta = new PedidoClientePlanta($planta->idPlanta, $planta->idPedidoClientePlanta, $planta->idClientePedido, $planta->cantidad);
            array_push($arrayPedidoClientePlantas, $pedidoClientePlanta);
            $montoTotal += ($plantaBusiness->getPrecioUnitario($planta->idPlanta) * ($planta->cantidad));
        }
        $pedidoCliente = new PedidoCliente($cliente->idPedido, $cliente->idPedidoCliente, $cliente->idCliente, $arrayPedidoClientePlantas);
        array_push($arrayPedidoClientes, $pedidoCliente);
    }


    // Comapracion de pedido almacenado con el nuevo pedido.
    $pedidoObject = new Pedido($pedido->id, $montoTotal, $pedido->fechaPedido, $arrayPedidoClientes);
    $clientesAlmacenados = $pedidoBusiness->getDatosActualizar($pedidoObject->getIdPedido());
    $clientesNuevos = $pedidoObject->getPedidoCliente();
    foreach ($clientesAlmacenados as $clienteA) {
        $flagCliente = false;
        foreach ($clientesNuevos as $clienteN) {
            $flagADD = true;
            if ($clienteN->getIdPedidoCliente() == $clienteA->getIdPedidoCliente()) {
                if($clienteN->getIdCliente() != $clienteA->getIdCliente()){
                    $pedidoBusiness->modificarTBPedidoCliente($clienteN->getIdPedidoCliente(),$clienteN->getIdCliente());
                }
                foreach ($clienteA->getPedidoClientePlanta() as $plantaA) {
                    $flagPlanta = false;
                    foreach ($clienteN->getPedidoClientePlanta() as $plantaN) {
                        if ($plantaA->getIdPlanta() == $plantaN->getIdPlanta() && $plantaN->getIdPedidoClientePlanta() != 0) {
                            $flagPlanta = true;
                        } else if ($plantaN->getIdPedidoClientePlanta() == 0 && $flagADD) {
                            $pedidoBusiness->agregarTBPedidoClientePlanta($plantaN);
                        }
                    }
                    $flagADD = false;
                    if (!$flagPlanta) {
                        $pedidoBusiness->quitarTBPedidoClientePlanta($plantaA->getIdClientePedido(), $plantaA->getIdPedidoClientePlanta(), $plantaA->getIdPlanta());
                    }
                }
                $flagADD = true;
                $flagCliente = true;
            } else if ($clienteN->getIdPedidoCliente() == 0) {
                $pedidoBusiness->agregarTBClientePedido($clienteN);
                $clienteN->setIdPedidoCliente(-1);
            }
        }
        if (!$flagCliente) {
            $pedidoBusiness->quitarTBClientePedido($pedido->id, $clienteA->getIdPedidoCliente());
        }
    }
    $result = $pedidoBusiness->modificarTBPedido($pedidoObject);
    echo $result;
} else if (isset($_POST['getDataUp'])) {
    if (isset($_POST['idpedido'])) {
        $idPedido = $_POST['idpedido'];
        $pedidoBusiness = new PedidoBusiness();
        $dataForUpdate = $pedidoBusiness->getDatosActualizar($idPedido);

        header('Content-Type: application/json');
        echo json_encode($dataForUpdate);
    }
}else if(isset($_POST['pedidosRecientes'])) { 
    $pedidoBusiness = new PedidoBusiness();
    $pedidos = $pedidoBusiness->mostrarPedidosRecientes();
    $fechaActual = date_create(date('Y-m-d'));

    echo'<table id="tabla_pedidosRecientes"class="table-cosecha display">
        <caption>Pedidos próximos</caption>
        <thead>
            <tr>
                <th># de pedido</th>
                <th>Fecha de envío</th>
                <th>Monto total</th>
                <th>Días restantes</th>
                <th class="botones no-sort"></th>
            </tr> 
        </thead> 
        <tbody>';
    foreach ($pedidos as $pedidoActual) {
        $FechaPedido = date_create($pedidoActual->getFechaEnvio());
        $dias = date_diff($fechaActual,$FechaPedido)->format('%R%a días');
        echo '<tr>
                <td>' . $pedidoActual->getIdPedido() . '</td>
                <td>' . $pedidoActual->getFechaEnvio() . '</td>
                <td>' . $pedidoActual->getMontoTotal() . '</td>
                <td>' . $dias. '</td>
                <td>
                <button class="boton boton-primario tool" title="Observar" id="Observar" onclick="levantarVerPedidos('.$pedidoActual->getIdPedido().')"><i class="fa fa-eye" aria-hidden="true"></i></button>
                </td>
            </tr>';
    }
    echo '</tbody>
    </table>';

} else if (isset($_POST['pedidos'])) {


    $clienteBusiness = new ClienteBusiness();
    $plantaBusiness = new PlantaBusiness();
    $pedidoBusiness = new PedidoBusiness();


    echo '

    <table id="tabla_pedidos" class="table-boxes display" cellspacing="0" cellpadding="0">
        <thead>
            <tr>
                <th># de pedido</th>
                <th>Fecha de envío</th>
                <th>Monto total</th>
                <th class="botones no-sort"></th>
                <th class="botones no-sort"></th>
            </tr> 
        </thead> 
        <tbody>';
    foreach ($pedidos as $pedidoActual) {
        echo '<tr>
                <td>' . $pedidoActual->getIdPedido() . '</td>
                <td>' . $pedidoActual->getFechaEnvio() . '</td>
                <td>' . $pedidoActual->getMontoTotal() . '</td>
                <td>
                    <button class="boton boton-primario tool" id="Actualizar" title="Editar" onclick="levantarActualizar(this)"><i class="fa fa-edit" aria-hidden="true"></i></button>
                </td>
                <td>
                    <button class="boton boton-cancelar tool" id="Eliminar" title="Eliminar" onclick="levantarEliminar(' . $pedidoActual->getIdPedido() . ')"><i class="fa fa-trash" aria-hidden="true"></i></button>
                </td>
            </tr>';
    }
    echo '</tbody>
    </table>';

} else if (isset($_POST['form'])) {
    if (isset($_POST['tab'])) {
        $clienteBusiness = new ClienteBusiness();
        $plantaBusiness = new PlantaBusiness();

        $plantas = $plantaBusiness->mostrarTBPlanta();
        $clientes = $clienteBusiness->mostrarTBCliente();
        $tab = $_POST['tab'];
        echo '
            <div class="content-tab">
                <div class="input-box">
                    <select class="cliente" onchange="changeClient(' . $tab . ')">';
                    foreach ($clientes as $cliente) {
                        echo "<option value='" . $cliente->getIdCliente() . "'>" . $cliente->getNombre() . "</option>";
                    }
            echo    '</select>
                    <label>Cliente</label>
                    <span class="tooltip-text" id="tooltip-cliente"></span>
                </div>
                <div class="input-box fecha">
                    <input type="date" id="fechaPedido" class="fecha-pedidos" onchange="changeFechaPedido(this)">
                    <label>Fecha</label>
                </div>
                <div class="add-container">
                    <div class="input-box">
                        <select class="idPlanta" style="width: 50%;">';
                            foreach ($plantas as $planta) {
                                echo "<option value='" . $planta->getIdPLanta() . "'>" . $planta->getNombreComun() . "</option>";
                            }
                echo    '</select>
                        <label>Planta</label>
                        <input type="text" placeholder="Cantidad" class="cantidad"  onkeyup="validarAddPlant(' . $tab . ')" style="width: 25%;">
                        <button class="add boton boton-primario" style="width: 20%;" onclick="addPlant(' . $tab . ')"><i class="fa fa-plus" aria-hidden="true"></i></button>
                        <span class="tooltip-text" id="tooltip-planta"></span>
                    </div>
                </div>
                <div class="contenedor-plantas" style="height: 200px; overflow-y:auto">
                    <table class="tabla">
                        <thead>
                            <th>Planta</th>
                            <th>Cantidad</th>
                            <th></th>
                        </thead>
                        <tbody class="tbody">
                        </tbody>
                    </table>
                </div>
            </div>
        ';
    }
} else if (isset($_POST['forma'])) {
    if (isset($_POST['tab'])) {
        $clienteBusiness = new ClienteBusiness();
        $plantaBusiness = new PlantaBusiness();

        $plantas = $plantaBusiness->mostrarTBPlanta();
        $clientes = $clienteBusiness->mostrarTBCliente();
        $tab = $_POST['tab'];
        echo '
        
                <div class="content-tab">
                    <div class="input-box">
                        <select class="nCliente" onchange="changeClientUpdate(' . $tab . ')">';
        foreach ($clientes as $cliente) {
            echo "<option value='" . $cliente->getIdCliente() . "'>" . $cliente->getNombre() . "</option>";
        }
        echo '</select>
                        <label>Cliente</label>
                        <span class="tooltip-text" id="tooltip-cliente"></span>
                    </div>
                    <div class="input-box">
                        <input type="date" id="fechaPedido" class="fecha-pedidos nFecha" onchange="changeFechaPedidoUpdate(this)">
                        <label>Fecha</label>
                    </div>
                    <div class="add-container">
                        <div class="input-box">
                            <select class="nIdPlanta" style="width: 50%;">';
        foreach ($plantas as $planta) {
            echo "<option value='" . $planta->getIdPLanta() . "'>" . $planta->getNombreComun() . "</option>";
        }
        echo '</select>
                            <label>Planta</label>
                            <input type="text" placeholder="Cantidad" class="nCantidad"  onkeyup="validarAddPlantN(' . $tab . ')" style="width: 25%;">                            
                            <button class="add boton boton-primario" style="width: 20%;" onclick="addPlantUpdate(' . $tab . ')"><i class="fa fa-plus" aria-hidden="true"></i></button>
                            <span class="tooltip-text" id="tooltip-planta"></span>
                        </div>
                    </div>
                    <div class="contenedor-plantas">
                        <table class="tabla">
                            <thead>
                                <th>Planta</th>
                                <th>Cantidad</th>
                                <th></th>
                            </thead>
                            <tbody class="nTbody">
                            </tbody>
                        </table>
                    </div>
                </div>
        
        ';
    }
}
