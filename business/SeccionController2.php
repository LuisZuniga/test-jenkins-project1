<?php

include '../business/SeccionBusiness.php';
include '../business/PlantaBusiness.php';
    
    $lote = $_POST['lote'];
    $seccion = $_POST['seccion'];

    $seccionBusiness = new SeccionBusiness();

    $secciones = $seccionBusiness->mostrarTBSeccion($lote,$seccion);

    $plantaBusines = new PlantaBusiness();
    $plantas = $plantaBusines->mostrarTBPlanta();

echo ' <div class="modal fade" id="modalAgregar" tabindex="-1" role="dialog" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-scrollable" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalScrollableTitle">Agregar planta a sección</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form novalidate>
                        <input type="hidden" id="lote" value="'.$lote.'"/>
                        <input type="hidden" id="seccion" value="'.$seccion.'"/>


                        <div class="input-box">
                            <select required onchange="validarAgregarPlantaSeccion(this)" name="planta" id="planta">'; 
                                foreach($plantas as $plantaActual){
                        echo    '<option value="'.$plantaActual->getIdPlanta().'">'.$plantaActual->getNombreComun().'</option>';
                                }
                        
                    echo    '</select>
                            <label>Seleccione una planta <span class="campo-obligatorio"> * </span></label>
                            <span id="tooltip-planta" class="tooltip-text"></span>
                        </div>
                        

                        <div class="input-box">
                            <input required onkeyup="validarAgregarPlantaSeccion(this)" type="text" id="cantidad" name="cantidad"/>
                            <label>Cantidad <span class="campo-obligatorio"> * </span></label>
                            <span id="tooltip-cantidad" class="tooltip-text"></span>
                        </div>    
                        
                        <div class="input-box">
                            <input type="date" onchange="validarAgregarPlantaSeccion(this)" id="fechaSiembra" name="fechaSiembra"/>
                            <label>Seleccione la fecha de siembra <span class="campo-obligatorio"> * </span></label>
                            <span id="tooltip-fechaSiembra" class="tooltip-text"></span>
                        </div>

                        <div class="input-box">
                            <input type="date" onchange="validarAgregarPlantaSeccion(this)" id="fechaExtraccion" name="fechaExtraccion"/>
                            <label>Seleccione la fecha de extracción <span class="campo-obligatorio"> * </span></label>
                            <span id="tooltip-fechaExtraccion" class="tooltip-text"></span>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <input type="button" class="boton boton-success" value="Agregar" name="insert" id="insert" onclick="insertar()">
                    <input type="button" class="boton boton-cancelar" value="Descartar" name="clear" id="clear" onclick="limpiar()">
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modalModificar" tabindex="-1" role="dialog" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-scrollable" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalScrollableTitle">Modificar planta de sección</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form novalidate>
                        <input type="hidden" id="nLote" value="'.$lote.'"/>
                        <input type="hidden" id="nSeccion" value="'.$seccion.'"/>
                        <input type="hidden" id="cPlanta" value=""/>


                        <div class="input-box">
                            <select onchange="validarActualizarPlantaSeccion(this)" name="nPlanta" id="nPlanta">'; 
                                foreach($plantas as $plantaActual){
                            echo    '<option value="'.$plantaActual->getIdPlanta().'">'.$plantaActual->getNombreComun().'</option>';
                                }
                        
                    echo    '</select>
                            <label>Seleccione una planta <span class="campo-obligatorio"> * </span></label>
                            <span id="tooltip-nPlanta" class="tooltip-text"></span>
                        </div>
                    

                        <div class="input-box">
                            <input onkeyup="validarActualizarPlantaSeccion(this)" type="text" id="nCantidad" name="nCantidad"/>
                            <label>Cantidad <span class="campo-obligatorio"> * </span></label>
                            <span id="tooltip-nCantidad" class="tooltip-text"></span>
                        </div>    
                    
                        <div class="input-box">
                            <input type="date" onchange="validarActualizarPlantaSeccion(this)" id="nFechaSiembra" name="nFechaSiembra"/>
                            <label>Seleccione la fecha de siembra <span class="campo-obligatorio"> * </span></label>
                            <span id="tooltip-nFechaSiembra" class="tooltip-text"></span>
                        </div>

                        <div class="input-box">
                            <input type="date" onchange="validarActualizarPlantaSeccion(this)" id="nFechaExtraccion" name="nFechaExtraccion"/>
                            <label>Seleccione la fecha de extracción <span class="campo-obligatorio"> * </span></label>
                            <span id="tooltip-nFechaExtraccion" class="tooltip-text"></span>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <input type="button" class="boton boton-primario" value="Actualizar" name="update" id="update" onclick="actualizar()">
                    <input type="button" class="boton boton-cancelar" value="Descartar" name="clear" id="clear" onclick="limpiarActualizar()">
                </div>
            </div>
        </div>
    </div>';

    echo '<table id="tabla_secciones" class="table-boxes display" cellspacing="0" cellpadding="0">
            <thead>
                <tr>
                    <th hidden></th>
                    <th># Sección</th>
                    <th># Lote</th>
                    <th>Planta</th>
                    <th>Cantidad</th>
                    <th>Fecha siembra</th>
                    <th>Fecha extracción</th>
                    <th class="botones no-sort"></th>
                    <th class="botones no-sort"></th>
                </tr>
            </thead>
            <tbody>';
            foreach ($secciones as $lista){
                echo '<tr>';
                echo '<td hidden>'.$lista->getPlanta().'</td>';
                echo '<td>' . $lista->getNumeroseccion() . '</td>';
                echo '<td>'.$lista->getNumeroLote().'</td>';
                echo '<td>'.$plantaBusines->getNombreComun($lista->getPlanta()).'</td>';
                echo '<td>'.$lista->getCantidad().'</td>';                
                echo '<td>'.$lista->getFechaSiembra().'</td>';                
                echo '<td>'.$lista->getFechaExtraccion().'</td>';                
                echo '<td><button class="boton boton-primario tool" title="Editar" id="Actualizar" onclick="levantarActualizar(this)"><i class="fa fa-edit" aria-hidden="true"></i></button></td>';
                echo '<td><button class="boton boton-cancelar tool" title="Eliminar" id="Eliminar" onclick="levantarEliminar(this)"><i class="fa fa-trash" aria-hidden="true"></i></button></td>';
                echo '</tr>';
            }
            echo '
            </tbody>
        </table>';