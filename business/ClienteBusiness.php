<?php

include '../data/ClienteData.php';

class ClienteBusiness {

    private $clienteData;

    public function __construct() {
        $this->clienteData = new ClienteData();
    }

    public function insertarTBCliente($cliente) {
        return $this->clienteData->insertarTBCliente($cliente);
    }
    
    public function mostrarTBCliente() {
        return $this->clienteData->mostrarTBCliente();
    }
    
    public function eliminarTBCliente($idCliente) {
        return $this->clienteData->eliminarTBCliente($idCliente);
    }
    
    public function actualizarTBCliente($cliente) {
        return $this->clienteData->actualizarTBCliente($cliente);
    }

    public function buscarTBCliente($cadena, $inicio, $limite){
        return $this->clienteData->buscarTBCliente($cadena, $inicio, $limite);
    }

    public function getTotalRegistros(){
        return $this->clienteData->getTotalRegistros();
    }
    
    public function mostrarRegistrosPaginados($inicio, $limite){
        return $this->clienteData->mostrarRegistrosPaginados($inicio, $limite);
    }
    
    public function getTotalRegistrosBuscados($cadena){
        return $this->clienteData->getTotalRegistrosBuscados($cadena);
    }
    
}