<?php

    include '../data/LoginData.php';

    class LoginBusiness{

        private $loginData;

        function __construct(){
            $this->loginData = new LoginData();
        }

        public function iniciarSession($user){
            return $this->loginData->iniciarSession($user);
        }

        public function actualizarNombreUsuario($username){
            return $this->loginData->actualizarNombreUsuario($username);
        }

        // current pass and new pass
        public function actualizarContrasena($cPass,$nPass){
            return $this->loginData->actualizarContrasena($cPass,$nPass);
        }

        public function actualizarEmail($email){
            return $this->loginData->actualizarEmail($email);
        }

        public function actualizarNotificacion($notificacion){
            return $this->loginData->actualizarNotificacion($notificacion);
        }

        public function getTBUsuario() {
            return $this->loginData->getTBUsuario();
        }

        public function getPass($mail){
            return $this->loginData->getPass($mail);
        }
        
    }