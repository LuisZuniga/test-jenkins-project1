<?php

include './MailBusiness.php';
include './LoginBusiness.php';

if (isset($_POST['enviar'])) {
    if (isset($_POST['destinatario'])) {
        $mailBusines = new MailBusiness();
        $loginBusiness = new LoginBusiness();
        $destinatario = $_POST['destinatario'];
        $asunto = 'Recuperando credenciales';
        $cuerpo = 'Tu contraseña de administrador de la aplicación ORNEXP es: ';
        $pass = $loginBusiness->getPass($destinatario);
        if($pass != ''){
            $cuerpo .= $pass;
            echo $mailBusines->enviarEmail($destinatario,$asunto,$cuerpo);
        }else{
            echo '4';
        }
    } else {
        echo '0';
    }
}
