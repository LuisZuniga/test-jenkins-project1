<?php
include '../data/ColaboradorData.php';

class ColaboradorBusiness {
    
    private $colaboradorData; 
     
   public function __construct(){
        $this->colaboradorData = new ColaboradorData();
    }
    
    public function insertarTBColaborador($colaborador){
        return $this->colaboradorData->insertarTBColaborador($colaborador);
    }

    public function eliminarTBColaborador($colaboradorId){
        return $this->colaboradorData->eliminarTBColaborador($colaboradorId);
    }

    public function actualizarTBColaborador($colaborador){
        return $this->colaboradorData->actualizarTBColaborador($colaborador);
    }

    public function mostrarTBColaborador(){
        return $this->colaboradorData->mostrarTBColaborador();
    }
    
    /*public function filtradoColaborador($dato, $inicio,$limite){
        return $this->colaboradorData->filtradoColaborador($dato, $inicio,$limite);
    }

    public function getTotalColaboradores(){
        return $this->colaboradorData->getTotalColaboradores();
    }
    
    public function getTotalColaboradoresBuscados($cadena){
        return $this->colaboradorData->getTotalColaboradoresBuscados($cadena);
    }*/
}
