<?php


include '../business/SeccionBusiness.php';
include '../business/PlantaBusiness.php';

$seccionBusiness = new SeccionBusiness();
$plantaBusiness = new PlantaBusiness();
$lote = $_POST['lote'];
$seccion = $_POST['seccion'];
$plantas = $seccionBusiness->mostrarTBSeccion($lote,$seccion);

if(sizeof($plantas) == 0){
    echo '<h3>Seccion vacia</h3>';
    return;
}


echo '
        <input type="text" hidden id="lote" value='.$lote.'>
        <input type="text" hidden id="seccion" value='.$seccion.'>
        <table class="tabla zebra">
            <thead>
                <th>Planta</th>
                <th>Cantidad</th>
                <th>Fecha de siembra</th>
                <th>Fecha de corte</th>
            </thead>
            <tbody>';
                    foreach($plantas as $plantaActual){
                        echo '<tr>';
                        echo '<td><input readonly type="text" value="'.$plantaBusiness->getNombreComun($plantaActual->getPlanta()).'"></td>';
                        echo '<td><input readonly type="text" value="'.$plantaActual->getCantidad().'"></td>';
                        echo '<td><input readonly type="text" value="'.$plantaActual->getFechaSiembra().'"></td>';
                        echo '<td><input readonly type="text" value="'.$plantaActual->getFechaExtraccion().'"></td>';
                        echo '</tr>';
                    }
               echo '
            </tbody>
        </table>
';