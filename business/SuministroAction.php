<?php

include './SuministroBusiness.php';
$accion = ($_POST['accion']);

if($accion=="Actualizar"){
    if(isset($_POST['idSuministro']) &&  isset($_POST['nombre']) && isset($_POST['cantidad'])){
        $idSuministro = $_POST['idSuministro'];      
        $nombre = $_POST['nombre'];
        $cantidad = $_POST['cantidad'];
        $suministroBusiness = new SuministroBusiness();
        $suministro = new Suministro($idSuministro, $nombre, $cantidad);
        $result = $suministroBusiness->actualizarTBSuministro($suministro);
        echo $result;         
        
    }else{
        echo '0';
    }
}else if($accion=="Eliminar"){
    if(isset($_POST['idSuministro'])){
        $idSuministro = $_POST['idSuministro'];
            $suministroBusiness = new SuministroBusiness();
            $result = $suministroBusiness->eliminarTBSuministro($idSuministro);
            echo $result;
    }else{
        echo '0';
    }

}else if($accion=="Insertar"){
    if(isset($_POST['nombre']) && isset($_POST['cantidad'])){
        $nombre= $_POST['nombre'];
        $cantidad = $_POST['cantidad'];
        $suministroBusiness = new SuministroBusiness();
        $suministro = new Suministro(0, $nombre, $cantidad);
        $result = $suministroBusiness->insertarTBSuministro($suministro);
        echo $result;         
    }else{
        echo '0';
    }
} else if ($accion == "suministrosAgotados") {
    $suministroBusiness = new SuministroBusiness();
    $suministros = $suministroBusiness->mostrarSuministrosAgotados();

    /** Empezamos a mostrar la tabla **/
    echo '<table id="table_suministros" class="table-boxes">
    <caption>Suministros por agotar</caption>
    <thead>
        <tr>
            <th>Tipo Suministro</th>
            <th>Nombre</th>
            <th>Cantidad</th>
            <th>Advertencia</th>
        </tr> 
    </thead> 
    <tbody>';                       
    foreach ($suministros as $lista) { 
        echo '<tr>';
        echo '<td>' . $lista->getTipoSuministro() . '</td>';
        echo '<td>' . $lista->getNombre() . '</td>';
        echo '<td>' . $lista->getCantidad() .'</td>';
        if ($lista->getCantidad() >= 10) {
            echo '<td><i class="fas fa-boxes fa-2x" style="color:green"></i></td>';
        } else if($lista->getCantidad() < 10 && $lista->getCantidad() >= 5) {
            echo '<td><i class="fas fa-boxes fa-2x" style="color:orange"></i></td>';
        } else { 
            echo '<td><i class="fas fa-boxes fa-2x" style="color:red"></i></td>';
        }
        echo '</tr>';
    }
    
    echo '
    </tbody>
    </table>';
}
