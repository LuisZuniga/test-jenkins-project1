<?php

include '../business/PlantaBusiness.php';

    $plantaBusiness = new PlantaBusiness();
    $plantas = $plantaBusiness->mostrarTBPlanta();

    
    echo '   
    <!-- Modal -->
    <div class="modal fade" id="modalAgregar" tabindex="-1" role="dialog" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-scrollable" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalScrollableTitle">Agregar Planta</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form novalidate>
                    <div class="input-box">
                        <input required onkeyup="validarAgregarPlanta(this)" type="text"  class="nombre" name="nombreComun" id="nombreComun">
                        <label>Nombre común<span class="campo-obligatorio"> * </span></label>
                        <span id="tooltip-nombreComun" class="tooltip-text"></span>
                    </div>

                    <div class="input-box">
                        <input required onkeyup="validarAgregarPlanta(this)" type="text"  class="nombre" name="nombreCientifico" id="nombreCientifico">
                        <label>Nombre científico<span class="campo-obligatorio"> * </span></label>
                        <span id="tooltip-nombreCientifico" class="tooltip-text"></span>
                    </div>

                    <div class="input-box">
                        <input required onkeyup="validarAgregarPlanta(this)" type="text" class="decimal" min="0" step="0.01" name="precioUnitario" id="precioUnitario">
                        <label>Precio unitario<span class="campo-obligatorio"> * </span></label>
                        <span id="tooltip-precio" class="tooltip-text" data-tooltip="Solo numeros"></span>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <input type="button" class="boton boton-success" value="Agregar" name="insert" id="insert" onclick="insertar()">
                <input type="button" class="boton boton-cancelar" value="Descartar"  name="clear" id="clear" onclick="limpiar()">
            </div>
            </div>
        </div>
    </div>


    <!-- Modificar -->

    <div class="modal fade" id="modalModificar" tabindex="-1" role="dialog" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-scrollable" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalScrollableTitle">Modificar Planta</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form novalidate>
                    <input type="hidden" id="nIdPlanta"></input>
                    <input type="hidden" name="nCantidad" id="nCantidad""></input>
                
                    <div class="input-box">
                        <input type="text" required onkeyup="validarActualizarPlanta(this)" class="nombre" name="nNombreComun" id="nNombreComun"/><label class="mensaje"></label>
                        <label>Nombre Comun</label>
                        <span id="tooltip-nNombreComun" class="tooltip-text"></span>
                    </div>
                    
                    <div class="input-box">
                        <input type="text" required onkeyup="validarActualizarPlanta(this)" class="nombre" name="nNombreCientifico" id="nNombreCientifico"/><label class="mensaje"></label>
                        <label>Nombre Cientifico</label>
                        <span id="tooltip-nNombreCientifico" class="tooltip-text"></span>
                    </div>
                
                    <div class="input-box">
                        <input type="text" required onkeyup="validarActualizarPlanta(this)" class="decimal" name="nPrecioUnitario" id="nPrecioUnitario"/><label class="mensaje"></label>
                        <label>Precio</label>
                        <span id="tooltip-nPrecio" class="tooltip-text"></span>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
            <input type="button" class="boton boton-primario" value="Confirmar" name="update" id="update" onclick="actualizar()">
            <input type="button" class="boton boton-cancelar" value="Descartar" name="clear" id="clear" data-dismiss="modal">
            </div>
            </div>
        </div>
    </div>';

    echo '<table id="tabla_plantas" class="table-boxes display" cellspacing="0" cellpadding="0">
            <thead>
                <tr>
                    <th hidden></th>
                    <th>Nombre Común</th>
                    <th>Nombre Científico</th>
                    <th>Cantidad</th>
                    <th>Precio Unitario</th>
                    <th class="botones no-sort"></th>
                    <th class="botones no-sort"></th>
                </tr>
            </thead>
            <tbody>
        ';

        foreach ($plantas as $plantaActual) {
            echo '<form method="post" enctype="multipart/form-data" action="../business/plantaAction.php">';
            echo '<tr>';
            echo '<td hidden>' . $plantaActual->getIdPlanta() . '</td>';
            echo '<td>' . $plantaActual->getNombreComun() . '</td>';
            echo '<td>' . $plantaActual->getNombreCientifico() . '</td>';
            echo '<td>'.$plantaActual->getCantidad().'</td>';
            echo '<td class="td-precio">' . $plantaActual->getPrecioUnitario() . '</td>';
            echo '<td><button class="boton boton-primario tool" title="Editar" id="Actualizar" onclick="levantarActualizar(this)"><i class="fa fa-edit" aria-hidden="true"></i></button></td>';
            echo '<td><button class="boton boton-cancelar tool" title="Eliminar" id="Eliminar" onclick="levantarEliminar('.$plantaActual->getIdPlanta().')"><i class="fa fa-trash" aria-hidden="true"></i</button></td>';
            echo '</tr>';
            echo '</form>';
        }

        echo '
            </tbody>
        </table>';