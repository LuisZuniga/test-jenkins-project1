<?php

include './QuimicoBusiness.php';

if (isset($_POST['update'])) {
    if (isset($_POST['idQuimico']) && isset($_POST['nombre']) && isset($_POST['cantidad']) && isset($_POST['precio'])) {
        $idQuimico = $_POST['idQuimico'];
        $nombre = $_POST['nombre'];
        $idTipoQuimico = $_POST['idTipoQuimico'];
        $toxicidad = $_POST['toxicidad'];
        $cantidad = $_POST['cantidad'];
        $precio = $_POST['precio'];
        $quimico = new Quimico($idQuimico, $nombre, $idTipoQuimico,$toxicidad, $cantidad, $precio);
        $quimicoBusiness = new QuimicoBusiness();
        $result = $quimicoBusiness->actualizarTBQuimico($quimico);
        echo $result;
    } else {
        echo '0';
    }
} else if (isset($_POST['delete'])) {
    if (isset($_POST['idQuimico'])) {
        $idQuimico = $_POST['idQuimico'];
        $quimicoBusiness = new QuimicoBusiness();
        $result = $quimicoBusiness->eliminarTBQuimico($idQuimico);
        echo $result;
    } else {
        echo '0';
    }
} else if (isset($_POST['create'])) {
    if (isset($_POST['nombre']) && isset($_POST['idTipoQuimico']) && isset($_POST['toxicidad']) && isset($_POST['cantidad']) && isset($_POST['precio'])) {
        $nombre = $_POST['nombre'];
        $idTipoQuimico = $_POST['idTipoQuimico'];
        $toxicidad = $_POST['toxicidad'];
        $cantidad = $_POST['cantidad'];
        $precio = $_POST['precio'];
        $quimico = new Quimico(0, $nombre, $idTipoQuimico,$toxicidad, $cantidad, $precio);
        $quimicoBusiness = new QuimicoBusiness();
        $result = $quimicoBusiness->insertarTBQuimico($quimico);
        echo $result;
    } else {
        echo '0';
    }
} else if(isset($_POST['quimicosAgotados'])) {
    
    $quimicoBusiness = new QuimicoBusiness();
    $quimicos = $quimicoBusiness->mostrarQuimicosAgotados();
    
    echo '<table id="tabla" title="Químicos" class="table-boxes display">
    <caption>Químicos por agotar</caption>
    <thead>
        <tr>
            <th>Nombre</th>
            <th>Cantidad</th>
            <th>Advertencia</th>
        </tr>
    </thead>
    <tbody>';

        foreach ($quimicos as $quimicoActual) {
            echo '<tr>';
            echo '<td>' . $quimicoActual->getNombre() . '</td>';
            echo '<td>' . $quimicoActual->getCantidad() . '</td>';
            if ($quimicoActual->getCantidad() >= 10) {
                echo '<td><i class="fas fa-thermometer-full fa-2x" style="color:green"></i></td>';
            } else if($quimicoActual->getCantidad() < 10 && $quimicoActual->getCantidad() >= 5) {
                echo '<td><i class="fas fa-thermometer-three-quarters fa-2x" style="color:orange"></i></i></td>';

            } else {
                echo '<td><i class="fas fa-thermometer-empty fa-2x" style="color:red"></i></td>';
            }
            
            
            echo '</tr>';
        }
        
        echo '
    </tbody>
    </table>';

}