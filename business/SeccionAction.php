<?php

include '../business/SeccionBusiness.php';
include '../business/PlantaBusiness.php';

if(isset($_POST['Actualizar'])){
    if(isset($_POST['seccion']) &&  isset($_POST['lote']) && isset($_POST['planta']) && isset($_POST['cantidad']) && isset($_POST['fechaSiembra']) && isset($_POST['fechaExtraccion'])){
        $numeroseccion = $_POST['seccion'];
        $numerolote= $_POST['lote'];
        $plantaActual = $_POST['cPlanta'];
        $numeroPlanta = $_POST['planta'];
        $cantidad = $_POST['cantidad'];
        $fechaSiembra = $_POST['fechaSiembra'];
        $fechaExtraccion = $_POST['fechaExtraccion'];
        $seccionBusiness = new SeccionBusiness();
        $plantaBusiness = new PlantaBusiness();
        $plantasAnterior = $seccionBusiness->getPlantasSL($numeroPlanta,$numeroseccion,$numerolote);
        $seccion = new Seccion($numeroPlanta,$plantaActual,$numeroseccion,$numerolote,$cantidad,$fechaSiembra,$fechaExtraccion);
        $result = $seccionBusiness->actualizarTBSeccion($seccion);
        /*if($result == 1){
            $can = ($plantaBusiness->getCantidadPlanta($numeroPlanta)-$plantasAnterior)+$cantidad;
            $plantaBusiness->setCantidadPlanta($numeroPlanta,$can);
        }*/
        echo $result;
    }else{
        echo '0';
    }
}else if(isset($_POST['Eliminar'])){
    if(isset($_POST['planta']) && isset($_POST['numeroseccion']) && isset($_POST['numerolote'])){
        $numeroseccion = $_POST['numeroseccion'];
        $numerolote = $_POST['numerolote'];
        $numeroPlanta = $_POST['planta'];
        $seccionBusiness = new SeccionBusiness();
        $plantaBusiness = new PlantaBusiness();
        $plantasEliminadas = $seccionBusiness->getPlantasSL($numeroPlanta,$numeroseccion,$numerolote);
        $result = $seccionBusiness->eliminarTBSeccion($numeroPlanta,$numeroseccion,$numerolote);
        /*if($result == 1){
            $can = $plantaBusiness->getCantidadPlanta($numeroPlanta)-$plantasEliminadas;
            $plantaBusiness->setCantidadPlanta($numeroPlanta,$can);
        }*/
        echo $result;   
    }else{
        echo '0';
    }

}else if(isset($_POST['Insertar'])){
    if(isset($_POST['seccion']) &&  isset($_POST['lote']) && isset($_POST['planta']) && isset($_POST['cantidad']) && isset($_POST['fechaSiembra']) && isset($_POST['fechaExtraccion'])){
        $numeroseccion = $_POST['seccion'];
        $numerolote= $_POST['lote'];
        $numeroPlanta = $_POST['planta'];
        $cantidad = $_POST['cantidad'];
        $fechaSiembra = $_POST['fechaSiembra'];
        $fechaExtraccion = $_POST['fechaExtraccion'];
        $seccionBusiness = new SeccionBusiness();
        $plantaBusiness = new PlantaBusiness();
        $seccion = new Seccion($numeroPlanta,$numeroseccion,$numerolote,$cantidad,$fechaSiembra,$fechaExtraccion);
        $result = $seccionBusiness->insertarTBSeccion($seccion);
        /*if($result == 1){
            $can = $plantaBusiness->getCantidadPlanta($numeroPlanta)+$cantidad;
            $plantaBusiness->setCantidadPlanta($numeroPlanta,$can);
        }*/
        echo $result;          
    }else{
        echo '0';
    }
}else if(isset($_POST['total'])){
    $idLote = $_POST['idLote'];
    $seccionBusiness = new SeccionBusiness();
    echo $seccionBusiness->getPlantasPorLote($idLote);
}else if(isset($_POST['diasFaltantesExtraer'])) { /* Dias faltantes para hacer las extraccion en una seccion */ 
    $seccionBusiness = new SeccionBusiness();
    //$lotes = $seccionBusiness->mostrarTBLote();
    $plantaBusines = new PlantaBusiness();
    $fechaActual = date_create(date('Y-m-d'));
    $secciones = $seccionBusiness->mostrarSecciones($fechaActual->format('Y-m-d'));
    echo '<br><table id="tabla_cosecha"class="table-cosecha display">
            <caption>Cosechas próximas</caption>
            <thead>
                <tr>
                    <th>#Sección</th>
                    <th>#Lote</th>
                    <th>Planta</th>
                    <th>Cantidad</th>
                    <th>Días restantes</th>
                </tr>
            </thead>
            <tbody>';
            foreach ($secciones as $lista){
                $extraccion = date_create($lista->getFechaExtraccion());
                $dias = date_diff($fechaActual,$extraccion)->format('%R%a días');
                echo '<tr>';
                echo '<td>'.$lista->getNumeroseccion().'</td>';
                echo '<td>'.$lista->getNumeroLote().'</td>';
                echo '<td>'.$plantaBusines->getNombreComun($lista->getPlanta()).'</td>';
                echo '<td>'.$lista->getCantidad().'</td>';             
                echo '<td>'.$dias.'</td>';                
                echo '</tr>';
            }
            echo '
            </tbody>
        </table>';
}