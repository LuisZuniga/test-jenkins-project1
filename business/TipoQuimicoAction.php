<?php

include './TipoQuimicoBusiness.php';

if(isset($_POST["insert"])){
    $nombre = $_POST['nombre'];
    $tipoQuimicoBusiness = new TipoQuimicoBusiness();
    $tipoQuimico = new TipoQuimico(0,$nombre);
    $result = $tipoQuimicoBusiness->insertarTBTipoQuimico($tipoQuimico);
    echo $result;
}else if(isset($_POST["update"])){
    $idTipoQuimico = $_POST['idtipoquimico'];
    $nombre = $_POST['nombre'];
    $tipoQuimicoBusiness = new TipoQuimicoBusiness();
    $tipoQuimico = new TipoQuimico($idTipoQuimico,$nombre);
    $result = $tipoQuimicoBusiness->modificarTBTipoQuimico($tipoQuimico);
    echo $result;
}else if(isset($_POST["delete"])){
    $idTipoQuimico = $_POST['idtipoquimico'];
    $tipoQuimicoBusiness = new TipoQuimicoBusiness();
    $result = $tipoQuimicoBusiness->eliminarTBTipoQuimico($idTipoQuimico);
    echo $result;
}