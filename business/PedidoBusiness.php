<?php

// logica de negocio.

include '../data/PedidoData.php';

class PedidoBusiness{

    private $pedidoData;

    public function __construct(){
        $this->pedidoData = new PedidoData();
    }

    public function insertarTBPedido($pedido){
        return $this->pedidoData->insertarTBPedido($pedido);
    }

    public function modificarTBPedido($pedido){
        return $this->pedidoData->modificarTBPedido($pedido);
    }

    public function mostrarTBPedido(){
        return $this->pedidoData->mostrarTBPedido();
    }

    public function mostrarPedidosRecientes(){
        return $this->pedidoData->mostrarPedidosRecientes();
    }

    public function eliminarTBPedido($idPedido){
        return $this->pedidoData->eliminarTBPedido($idPedido);
    }

    public function getDatosActualizar($idPedido){
        return $this->pedidoData->getDatosActualizar($idPedido);
    }

    public function modificarTBPedidoCliente($idPedidoCliente,$idCliente){
        return $this->pedidoData->modificarTBPedidoCliente($idPedidoCliente,$idCliente);
    }

    public function quitarTBClientePedido($idPedido,$idClientePedido){
        return $this->pedidoData->quitarTBClientePedido($idPedido,$idClientePedido);
    }

    public function quitarTBPedidoClientePlanta($idPedidoCliente,$idPedidoClientePlanta,$idPlanta){
        return $this->pedidoData->quitarTBPedidoClientePlanta($idPedidoCliente,$idPedidoClientePlanta,$idPlanta);
    }


    // estas lineas no estaban en ftp . 
    public function agregarTBClientePedido($cliente){
        return $this->pedidoData->agregarTBClientePedido($cliente);
    }

    public function agregarTBPedidoClientePlanta($planta){
        return $this->pedidoData->agregarTBPedidoClientePlanta($planta);
    }

    public function getDatosPedido($idPedido){
        return $this->pedidoData->getDatosPedido($idPedido);
    }


}