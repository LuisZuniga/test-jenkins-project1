<?php
    include '../business/SuministroBusiness.php';

    //-----------------Instancias---------------------

    $suministroBusiness = new SuministroBusiness();
    $suministros = $suministroBusiness->mostrarTBSuministro();


//----------------------TABLA SUMINISTRO------------

echo '  <div class="modal fade" id="modalAgregar" tabindex="-1" role="dialog" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-scrollable" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalScrollableTitle">Agregar Suministro</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form novalidate>
                            <div class="input-box">
                                <input onkeyup="validarAgregarSuministro(this)" required type="text" class="nombre" name="nombre" id="nombre"/>
                                <label>Nombre <span class="campo-obligatorio"> * </span> </label>
                                <span id="tooltip-nombre" class="tooltip-text" ></span>
                            </div>
                    
                            <div class="input-box">
                                <input onkeyup="validarAgregarSuministro(this)" required type="text" name="cantidad" id="cantidad"/>
                                <label>Cantidad <span class="campo-obligatorio"> * </span> </label>
                                <span id="tooltip-cantidad" class="tooltip-text" ></span>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <input type="button" class="boton boton-success" value="Agregar" name="insert" id="insert" onclick="insertar()">
                        <input type="button" class="boton boton-cancelar" value="Descartar" name="clear" id="clear" onclick="limpiar()">
                    </div>
                </div>
            </div>
        </div>



        <div class="modal fade" id="modalModificar" tabindex="-1" role="dialog" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-scrollable" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalScrollableTitle">Modificar Suministro</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form novalidate>
                            <input type="hidden" id="nIdSuministro">
                            <div class="input-box">
                                <input type="text" required onkeyup="validarActualizarSuministro(this)" class="nombre" name="nNombre" id="nNombre"/><label class="mensaje"></label>
                                <label>Nombre <span class="campo-obligatorio"> * </span> </label>
                                <span id="tooltip-nNombre" class="tooltip-text" ></span>
                            </div>
                            
                            <div class="input-box">
                                <input type="text" required onkeyup="validarActualizarSuministro(this)" class="nombre" name="nCantidad" id="nCantidad"/><label class="mensaje"></label>
                                <label>Cantidad <span class="campo-obligatorio"> * </span> </label>
                                <span id="tooltip-nCantidad" class="tooltip-text" ></span>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <input type="button" class="boton boton-primario" value="Confirmar" name="update" id="update" onclick="actualizar()">
                        <input type="button" class="boton boton-cancelar" value="Descartar" name="clear" id="clear" onclick="limpiarActualizar()">
                    </div>
                </div>
            </div>
        </div>';

    echo '<table id="tabla_suministros" class="table-boxes display" cellspacing="0" cellpadding="0">
            <thead>
                <tr>
                    <th>Tipo Suministro</th>
                    <th>Nombre</th>
                    <th>Cantidad</th>
                    <th class="botones no-sort"></th>
                    <th class="botones no-sort"></th>
                </tr> 
            </thead> 
            <tbody>';                       
            foreach ($suministros as $lista) { 
                echo '<tr>';
                echo '<input type="hidden" name="idSuministro" id="idSuministro" value="'.$lista->getIdSuministro().'"/>';  
                echo '<td>' . $lista->getTipoSuministro() . '</td>';                 
                echo '<td>' . $lista->getNombre() . '</td>';
                echo '<td>' . $lista->getCantidad() . '</td>';
                echo '<td><button class="boton boton-primario tool" id="Actualizar" title="Editar" onclick="levantarActualizar(this)"><i class="fa fa-edit" aria-hidden="true"></i></button></td>';
                echo '<td><button class="boton boton-cancelar tool" id="Eliminar" title="Eliminar" onclick="levantarEliminar('.$lista->getIdSuministro().')"><i class="fa fa-trash" aria-hidden="true"></i></button></td>';
                echo '</tr>';
            }
            
        echo '</tbody>
        </table>';

 