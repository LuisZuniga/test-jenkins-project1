<?php

include '../data/QuimicoData.php';

class QuimicoBusiness {

    private $quimicoData;

    public function __construct() {
        $this->quimicoData = new QuimicoData();
    }

    public function insertarTBQuimico($quimico) {
        return $this->quimicoData->insertarTBQuimico($quimico);
    }

    public function actualizarTBQuimico($quimico) {
        return $this->quimicoData->actualizarTBQuimico($quimico);
    }

    public function eliminarTBQuimico($idQuimico) {
        return $this->quimicoData->eliminarTBQuimico($idQuimico);
    }

    public function mostrarTBQuimico() {
        return $this->quimicoData->mostrarTBQuimico();
    }

    public function buscarTBQuimico($nombre, $inicio, $limite) {
        return $this->quimicoData->buscarTBQuimico($nombre, $inicio, $limite);
    }

    public function mostrarRegistrosPaginados($inicio, $limite) {
        return $this->quimicoData->mostrarRegistrosPaginados($inicio, $limite);
    }

    public function getTotalRegistros() {
        return $this->quimicoData->getTotalRegistros();
    }

    public function getTotalRegistrosBuscados($cadena) {
        return $this->quimicoData->getTotalRegistrosBuscados($cadena);
    }
    
    public function mostrarQuimicosAgotados() {
        return $this->quimicoData->mostrarQuimicosAgotados();
    }

    public function actualizarCantidad($idQuimico, $cantidad){
        return $this->quimicoData->actualizarCantidad($idQuimico, $cantidad);
    }

    public function getCantidadQuimico($idQuimico) {
        return $this->quimicoData->getCantidadQuimico($idQuimico);
    }
}