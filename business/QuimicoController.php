<?php


    include '../business/QuimicoBusiness.php';
    include '../business/TipoQuimicoBusiness.php';
    //error_reporting(0);

    $quimicoBusiness = new QuimicoBusiness();
    $quimicos = $quimicoBusiness->mostrarTBQuimico();
    $tipoQuimicoBusiness = new TipoQuimicoBusiness();
    $tipoQuimicos = $tipoQuimicoBusiness->mostrarTBTipoQuimico();


echo    '<div class="modal fade" id="modalAgregar" tabindex="-1" role="dialog" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-scrollable" role="document">
                <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalScrollableTitle">Agregar Químico</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form novalidate>
                        <div class="input-box">
                            <input required onkeyup="validarAgregarQuimico(this)" type="text" name="nombre" id="nombre"/>
                            <label>Nombre del químico <span class="campo-obligatorio"> * </span></label>
                            <span id="tooltip-nombreQuimico" class="tooltip-text"></span>
                        </div>

                        <div class="input-box">
                            <select required onchange="validarAgregarQuimico(this)" name="tipoQuimico" id="tipoQuimico">';            
                                foreach ($tipoQuimicos as $lista){
                                    echo "<option value=".$lista->getIdTipoQuimico().">".$lista->getNombre()."</option>";
                                }                  
                            echo'</select>
                            <label>Tipo Suministro <span class="campo-obligatorio"> * </span> </label>
                            <span id="tooltip-tipoQuimico" class="tooltip-text" ></span>
                        </div>

                        <div class="input-box">
                            <input required onkeyup="validarAgregarQuimico(this)" type="color" name="toxicidad" id="toxicidad"/>
                            <label>Toxicidad <span class="campo-obligatorio"> * </span></label>
                            <span id="tooltip-toxicidad" class="tooltip-text"></span>
                        </div>


                        <div class="input-box">
                            <input required onkeyup="validarAgregarQuimico(this)" type="text" name="cantidad" id="cantidad"/>
                            <label>Cantidad <span class="campo-obligatorio"> * </span></label>
                            <span id="tooltip-cantidad" class="tooltip-text"></span>
                        </div>

                        <div class="input-box">
                            <input required onkeyup="validarAgregarQuimico(this)" type="text" name="precio" id="precio"/>
                            <label>Precio <span class="campo-obligatorio"> * </span></label>
                            <span id="tooltip-precio" class="tooltip-text"></span>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <input type="button" class="boton boton-success" value="Agregar" name="insert" id="insert" onclick="insertar()"/>
                    <input type="button" class="boton boton-cancelar" value="Descartar" name="clear" id="clear" onclick="limpiar()"/>
                </div>
                </div>
            </div>
        </div>




        <div class="modal fade" id="modalModificar" tabindex="-1" role="dialog" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-scrollable" role="document">
                <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalScrollableTitle">Modificar Químico</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form novalidate>
                        <input type="hidden" id="nIdQuimico">

                        <div class="input-box">
                            <input type="text" required onkeyup="validarActualizarQuimico(this)" class="nombre" name="nNombre" id="nNombre"/><label class="mensaje"></label>
                            <label>Nombre <span class="campo-obligatorio"> * </span></label>
                            <span id="tooltip-nNombreQuimico" class="tooltip-text"></span>
                        </div>
                        <div class="input-box">
                            <select required onchange="validarAgregarQuimico(this)" name="nTipoQuimico" id="nTipoQuimico">';            
                                foreach ($tipoQuimicos as $lista){
                                    echo "<option value=".$lista->getIdTipoQuimico().">".$lista->getNombre()."</option>";
                                }                  
                        echo'</select>
                            <label>Tipo de químico<span class="campo-obligatorio"> * </span> </label>
                            <span id="tooltip-nTipoQuimico" class="tooltip-text" ></span>
                        </div>
            
                        <div class="input-box">
                            <input required onkeyup="validarAgregarQuimico(this)" type="color" name="nToxicidad" id="nToxicidad"/>
                            <label>Toxicidad <span class="campo-obligatorio"> * </span></label>
                            <span id="tooltip-nToxicidad" class="tooltip-text"></span>
                        </div>
        
                        <div class="input-box">
                            <input type="text" required onkeyup="validarActualizarQuimico(this)" class="nombre" name="nCantidad" id="nCantidad"/><label class="mensaje"></label>
                            <label>Cantidad <span class="campo-obligatorio"> * </span></label>
                            <span id="tooltip-nCantidad" class="tooltip-text"></span>
                        </div>
            
                        <div class="input-box">
                            <input type="text" required onkeyup="validarActualizarQuimico(this)"  class="nombre"  name="nPrecio" id="nPrecio"/><label class="mensaje"></label>
                            <label>Precio <span class="campo-obligatorio"> * </span></label>
                            <span id="tooltip-nPrecio" class="tooltip-text"></span>
                        </div>    
                    </form>
                </div>
                <div class="modal-footer">
                <input type="button" class="boton boton-primario" value="Confirmar" name="update" id="update" onclick="actualizar()"/>
                <input type="button" class="boton boton-cancelar" value="Descartar" name="clear" id="clear" onclick="limpiarActualizar()"/>
                </div>
                </div>
            </div>
        </div>';


    echo '<table id="tabla_quimicos" class="table-boxes display" cellspacing="0" cellpadding="0">
            <thead>
                <tr>
                    <th hidden></th>
                    <th>Nombre del químico</th>
                    <th hidden></th>
                    <th>Tipo de químico</th>
                    <th>Toxicidad</th>
                    <th>Cantidad</th>
                    <th>Precio</th>
                    <th class="botones no-sort"></th>
                    <th class="botones no-sort"></th>
                </tr>
            </thead>
            <tbody>';

        foreach ($quimicos as $quimicoActual) {
            echo '<tr>';
            echo '<td hidden>' . $quimicoActual->getIdQuimico() . '</td>';
            echo '<td>' . $quimicoActual->getNombre() . '</td>';
            echo '<td hidden>' . $quimicoActual->getIdTipoQuimico() . '</td>';
            echo '<td>' . $quimicoActual->getTipoQuimico() . '</td>';
            echo '<td><input type="color" disabled value="' . $quimicoActual->getToxicidad() . '"/></td>';
            echo '<td>' . $quimicoActual->getCantidad() . '</td>';
            echo '<td class="td-precio">' . $quimicoActual->getPrecio() . '</td>';
            echo '<td><button class="boton boton-primario tool" title="Editar" id="Actualizar" onclick="levantarActualizar(this)"><i class="fa fa-edit" aria-hidden="true"></i></button></td>';
            echo '<td><button class="boton boton-cancelar tool" title="Eliminar" id="Eliminar" onclick="levantarEliminar('.$quimicoActual->getIdQuimico().')"><i class="fa fa-trash" aria-hidden="true"></i></button></td>';
            echo '</tr>';
        }
        
        echo '
            </tbody>
        </table>';