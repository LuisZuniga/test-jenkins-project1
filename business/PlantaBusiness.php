<?php

include '../data/PlantaData.php';

class PlantaBusiness{

    private $plantaData;

    public function __construct(){
        $this->plantaData = new PlantaData();
    }

    public function insertarTBPlanta($planta){
        return $this->plantaData->insertarTBPlanta($planta);
    }

    public function eliminarTBPlanta($idPlanta){
        return $this->plantaData->eliminarTBPlanta($idPlanta);
    }

    public function actualizarTBPlanta($planta){
        return $this->plantaData->actualizarTBPlanta($planta);
    }

    public function mostrarTBPlanta(){
        return $this->plantaData->mostrarTBPlanta();
    }

    public function getNombreComun($idPlanta){
        return $this->plantaData->getNombreComun($idPlanta);
    }

    public function getTotalPlantas(){
        return $this->plantaData->getTotalPlantas();
    }

    /*public function buscarPlantas($buscar,$inicio,$limite){
        return $this->plantaData->buscarPlantas($buscar,$inicio,$limite);
    }

    public function getTotalRegistros(){
        return $this->plantaData->getTotalRegistros();
    }

    public function getTotalRegistrosBuscados($cadena){
        return $this->plantaData->getTotalRegistrosBuscados($cadena);
    }

    public function mostrarRegistrosPaginados($inicio, $limite){
        return $this->plantaData->mostrarRegistrosPaginados($inicio, $limite);
    }*/

    function getCantidadPlanta($idPlanta){
        return $this->plantaData->getCantidadPlanta($idPlanta);        
    }

    function setCantidadPlanta($idPlanta,$cantidad){
        return $this->plantaData->setCantidadPlanta($idPlanta,$cantidad);        
    }

    function getPrecioUnitario($idPlanta) {
        return $this->plantaData->getPrecioUnitario($idPlanta);
    }
}