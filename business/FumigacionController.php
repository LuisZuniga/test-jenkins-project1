<?php

include '../business/FumigacionBusiness.php';
include '../business/ColaboradorBusiness.php';
include '../business/QuimicoBusiness.php';
   

    $lote = $_POST['lote'];
    $seccion = $_POST['seccion'];

    $FumigacionBusiness = new FumigacionBusiness();
    $ColaboradorBusiness = new ColaboradorBusiness();
    $colaboradores = $ColaboradorBusiness->mostrarTBColaborador();
    $QuimicoBusiness = new QuimicoBusiness();
    $quimicos = $QuimicoBusiness->mostrarTBQuimico();
    $fumigaciones = $FumigacionBusiness->mostrarTBFumigacion($seccion, $lote);

    echo '<div class="modal fade" id="modalAgregar" tabindex="-1" role="dialog" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-scrollable" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalScrollableTitle">Agregar Fumigación</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form novalidate>
                            <input type="hidden" id="lote" value="'.$lote.'"/>
                            <input type="hidden" id="seccion" value="'.$seccion.'"/>

                            <div class="input-box">
                                <input  required onkeyup="validarAgregarFumigacion(this)" type="text" name="dosis" id="dosis">
                                <label>Dosis <span class="campo-obligatorio"> * </span> </label>
                                <span id="tooltip-dosis" class="tooltip-text" ></span>
                            </div>

                            <div class="input-box">
                                <input type="date"  onchange="validarAgregarFumigacion(this)" name="fechaFumigacion" id="fechaFumigacion">
                                <label>Selecione fecha de fumigación <span class="campo-obligatorio"> * </span> </label>
                                <span id="tooltip-fechaFumigacion" class="tooltip-text" ></span>
                            </div>

                            <div class="input-box">
                                <select required onchange="validarAgregarFumigacion(this)" name="quimico" id="quimico">';                    
                                    foreach($quimicos as $quimicoActual){
                                echo'<option value="'.$quimicoActual->getIdQuimico().'">'.$quimicoActual->getNombre().'</option>';
                                    }
                            
                            echo'</select>
                                <label>Químico<span class="campo-obligatorio"> * </span></label>
                                <span id="tooltip-quimico" class="tooltip-text"></span>
                            </div>

                            <div class="input-box">
                                <select required onchange="validarAgregarFumigacion(this)" name="colaboradorEncargado" id="colaboradorEncargado">';                    
                                    foreach($colaboradores as $colaboradorActual){
                                echo'<option value="'.$colaboradorActual->getIdColaborador().'">'.$colaboradorActual->getNombre().' '.$colaboradorActual->getApellido1().' '.$colaboradorActual->getApellido2().'</option>';
                                    }
                            
                            echo'</select>
                                <label>Colaborador encargado <span class="campo-obligatorio"> * </span></label>
                                <span id="tooltip-colaboradorEncargado" class="tooltip-text"></span>
                            </div>
                        </form>
                    </div>
                        <div class="modal-footer">
                            <input type="button" class="boton boton-success" value="Agregar" name="insert" id="insert" onclick="insertar()">
                            <input type="button" class="boton boton-cancelar" value="Descartar" name="clear" id="clear" onclick="limpiar()">
                        </div>
                    </div>
                </div>
            </div>
        </div>';

echo    '<div class="modal fade" id="modalModificar" tabindex="-1" role="dialog" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-scrollable" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalScrollableTitle">Modificar Fumigación</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form novalidate>

                            <input type="hidden" id="nLote" value="'.$lote.'"/>
                            <input type="hidden" id="nSeccion" value="'.$seccion.'"/>
                            <input type="hidden" id="nFumigacion" value=""/>

                            <div class="input-box">
                                <input  onkeyup="validarActualizarFumigacion(this)" type="text"  class="nombre" name="nDosis" id="nDosis">
                                <label>Dosis <span class="campo-obligatorio"> * </span> </label>
                                <span id="tooltip-nDosis" class="tooltip-text" data-tooltip="Solo numeros"></span>
                            </div>

                            <div class="input-box">
                                <input type="date"  onchange="validarActualizarFumigacion(this)" name="nFechaFumigacion" id="nFechaFumigacion">
                                <label>Selecione la fecha de fumigación <span class="campo-obligatorio"> * </span> </label>
                                <span id="tooltip-nFechaFumigacion" class="tooltip-text"></span>
                            </div>

                            <div class="input-box">
                                <select onchange="validarActualizarFumigacion(this)" name="nQuimico" id="nQuimico">';                    
                                    foreach($quimicos as $quimicoActual){
                                        echo'<option value="'.$quimicoActual->getIdQuimico().'">'.$quimicoActual->getNombre().'</option>';
                                    }
                            
                            echo'</select>
                                <label>Químico <span class="campo-obligatorio"> * </span> </label>
                                <span id="tooltip-nQuimico" class="tooltip-text" data-tooltip="Solo numeros"></span>
                            </div>

                            <div class="input-box">
                                <select onchange="validarActualizarFumigacion(this)" name="nColaboradorEncargado" id="nColaboradorEncargado">';                    
                                    foreach($colaboradores as $colaboradorActual){
                                        echo'<option value="'.$colaboradorActual->getIdColaborador().'">'.$colaboradorActual->getNombre().' '.$colaboradorActual->getApellido1().' '.$colaboradorActual->getApellido2().'</option>';
                                    }
                            
                            echo'</select>
                                <label>Colaborador encargado <span class="campo-obligatorio"> * </span> </label>
                                <span id="tooltip-nColaboradorEncargado" class="tooltip-text" data-tooltip="Solo numeros"></span>
                            </div>
                    </form>
                </div>
                    <div class="modal-footer">
                        <input type="button" class="boton boton-primario" value="Actualizar" name="update" id="update" onclick="actualizar()">
                        <input type="button" class="boton boton-cancelar" value="Descartar" name="clear" id="clear" onclick="limpiarActualizar()">
                    </div>
                </div>
            </div>
        </div>
    </div>';

    
    echo '<table id="tabla_fumigaciones" class="table-boxes display" cellpadding="0" cellspacing="0">
            <thead>
                <tr>
                    <th hidden></th>
                    <th># Sección</th>
                    <th># Lote</th>
                    <th hidden></th>
                    <th>Químico</th>
                    <th>Docis</th>
                    <th>Fecha fumigación</th>
                    <th hidden></th>
                    <th>Colaborador encargado</th>
                    <th class="botones no-sort"></th>
                    <th class="botones no-sort"></th>
                </tr>
            </thead>
            <tbody>
        ';

        
        foreach ($fumigaciones as $fumigacionActual) {
            //echo '<form method="post" enctype="multipart/form-data" action="../business/FumigacionAction.php">';
            echo '<tr>';
            echo '<td hidden>' . $fumigacionActual->getIdFumigacion() . '</td>';
            echo '<td>' . $fumigacionActual->getIdSeccion() . '</td>';
            echo '<td>' . $fumigacionActual->getIdLote() . '</td>';
            echo '<td hidden>' . $fumigacionActual->getIdQuimico() . '</td>';
            echo '<td>' . $fumigacionActual->getNombreQuimico() . '</td>';
            echo '<td>' . $fumigacionActual->getDosis() . '</td>';
            echo '<td>' . $fumigacionActual->getFechaFumigacion() . '</td>';
            echo '<td hidden>'.$fumigacionActual->getIdColaborador().'</td>';
            echo '<td>'.$fumigacionActual->getNombreColaborador().'</td>';
            echo '<td><button class="boton boton-primario tool" title="Editar" id="Actualizar" onclick="levantarActualizar(this)"><i class="fa fa-edit" aria-hidden="true"></i></button></td>';
            echo '<td><button class="boton boton-cancelar tool" title="Borrar" id="Eliminar" onclick="levantarEliminar('.$fumigacionActual->getIdFumigacion().')"><i class="fa fa-trash" aria-hidden="true"></i></button></td>';
            echo '</tr>';
            //echo '</form>';
        }

        echo '
        
            </tbody>
        </table>';