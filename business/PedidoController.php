<?php

// controladora tabla.

include '../business/ClienteBusiness.php';
include '../business/PlantaBusiness.php';
include '../business/PedidoBusiness.php';

$clienteBusiness = new ClienteBusiness();
$plantaBusiness = new PlantaBusiness();
$pedidoBusiness = new PedidoBusiness();

$plantas = $plantaBusiness->mostrarTBPlanta();
$clientes = $clienteBusiness->mostrarTBCliente();
$pedidos = $pedidoBusiness->mostrarTBPedido();

echo '
        <div class="modal" id="form-actualizar">
            <div class="modal-form form">
                <div class="modal-header">
                    <h3>Actualizar Pedido</h3>
                    <div class="tabButton">
                        <div class="tab"><button onclick="showPanel(3)"><i class="fa fa-user" aria-hidden="true"> 1</i></button></div>
                        <div class="tab"><button onclick="showPanel(4)"><i class="fa fa-user" aria-hidden="true"> 2</i></button><button onclick="closePanelN(this,1)"
                                class="x"><i class="fa fa-times" aria-hidden="true"></i></button></div>
                        <div class="tab"><button onclick="showPanel(5)"><i class="fa fa-user" aria-hidden="true"> 3</i></button><button onclick="closePanelN(this,2)"
                                class="x"><i class="fa fa-times" aria-hidden="true"></i></button></div>
                        <div class="plus"><button onclick="addPanelN()"><i class="fa fa-plus" aria-hidden="true"></i></button></div>
                    </div>
                </div>
                <div class="tabPanel">
                    
                </div>
                <div class="tabPanel">

                </div>
                <div class="tabPanel">

                </div>
                
                <div class="input-box fecha">
                    <input type="date" id="nFechaPedido" onchange="validarUpdate()">
                    <label>Fecha</label>
                </div>

                <label class="mensaje" id="nMensaje">Se deben completar todos los campos</label>

                <div class="modal-footer">
                    
                    <input type="button" class="boton boton-primario" value="Actualizar" name="update" id="update"
                        onclick="actualizar()">
                    <input type="button" class="boton boton-cancelar" value="Descartar" name="clear" id="clear"
                        onclick="limpiar()">
                </div>
            </div>
        </div>
';


echo '

    <table id="tabla_pedidos" class="table-boxes display" cellspacing="0" cellpadding="0">
        <thead>
            <tr>
                <th># de pedido</th>
                <th>Fecha de envío</th>
                <th>Monto total</th>
                <th class="botones no-sort"></th>
                <th class="botones no-sort"></th>
                <th class="botones no-sort"></th>
            </tr> 
        </thead> 
        <tbody>';
foreach ($pedidos as $pedidoActual) {
    echo '<tr>
                <td>' . $pedidoActual->getIdPedido() . '</td>
                <td>' . $pedidoActual->getFechaEnvio() . '</td>
                <td>' . $pedidoActual->getMontoTotal() . '</td>
                <td>
                    <button class="btn-actualizar-table boton boton-primario tool" id="Actualizar" title="Generar PDF" onclick="cargarPDF('.$pedidoActual->getIdPedido().')"><i class="fas fa-file-pdf"></i></i></button>
                </td>
                <td>
                    <button class="btn-actualizar-table boton boton-primario tool" id="Actualizar" title="Editar" onclick="levantarActualizar(this)"><i class="fa fa-edit" aria-hidden="true"></i></button>
                </td>
                <td>
                    <button class="boton boton-cancelar tool" id="Eliminar" title="Eliminar" onclick="levantarEliminar(' . $pedidoActual->getIdPedido() . ')"><i class="fa fa-trash" aria-hidden="true"></i></button>
                </td>
            </tr>';
}
echo '</tbody>
    </table>

';
