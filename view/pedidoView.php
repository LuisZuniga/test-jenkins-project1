<?php
session_start();
if (!isset($_SESSION['role'])) {
    header("location:./login.php");
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Pedidos</title>
    <link rel="shortcut icon" href="../sources/images/Ornexp.png" type="image/x-icon">
    <link rel="stylesheet" href="../sources/css/bootstrap/bootstrap.min.css">
    <script src="../sources/js/jquery-3.3.1.min.js" ></script>
    <script src="../sources/js/bootstrap/popper.min.js" ></script>
    <script src="../sources/js/bootstrap/bootstrap.min.js"></script>  

    <script src="../sources/js/menu.js"></script>
    <script src="../sources/js/all.js"></script>
    <script src="../sources/js/sweetalert.min.js"></script>
    <script src="../sources/js/jquery.dataTables.min.js"></script>
    <script src="../sources/js/jquery-ui.js"></script>
    <script src="../sources/js/validaciones.js"></script>
    <script src="../sources/js/toastr.min.js" ></script>   
    <script src="../sources/js/alertas.js"></script>

    <link rel="stylesheet" href="../sources/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="../sources/css/dataTables.min.css">
    <link rel="stylesheet" href="../sources/css/jquery-ui.css">
    <link rel="stylesheet" href="../sources/css/index.css">
    <link rel="stylesheet" href="../sources/css/crud.css">
    <link rel="stylesheet" href="../sources/css/tooltip.css">
    <link rel="stylesheet" href="../sources/css/toastr.min.css">
    <link rel="stylesheet" href="../sources/css/menu.css">
    <link rel="stylesheet" href="../sources/css/all.css">
    <link rel="stylesheet" href="../sources/css/pedido.css">
    <link rel="stylesheet" href="../sources/css/loading.css">
    <script src="../sources/js/pedido.js"></script>  
</head>

<body onload="desplegarDatos()">
    <header>
        <div class="logo"><img src="../sources/images/Ornexp.png" alt="ornexpsa"></div>
        <nav id="menu">
            <ul id="ul">
                <li class="menu-item">
                    <a href="../index.php" class="menu-btn"><i class="fas fa-home"></i> Inicio</a>
                </li>
                <li class="menu-item">
                    <a href="./mapeoView.php" class="menu-btn"><i class="fas fa-map-marked-alt"></i> Mapeo</a>
                </li>
                <li class="menu-item active">
                    <a href="./pedidoView.php" class="menu-btn"><i class="fas fa-luggage-cart"></i> Pedidos</a>
                </li>
                <li class="menu-item">
                    <a href="./plantaView.php" class="menu-btn"><i class="fas fa-seedling"></i> Plantas</a>
                </li>
                <li class="menu-item" id='quimico'>
                    <a href="#quimico" class="menu-btn" onmouseover="dropDown(this)" onmouseleave = "dropUp(this)" name="quimico"><i class="fas fa-flask"></i> Químicos <i id="carett" class="fa fa-caret-down" aria-hidden="true"></i></a>
                    <div class="sub-menu" >
                        <a href="./quimicoView.php">Químicos</a>
                        <a href="./tipoQuimicoView.php">Tipo de químicos</a>
                    </div>
                </li>
                <li class="menu-item">
                    <a href="./suministroView.php" class="menu-btn"><i class="fas fa-boxes"></i> Suministros</a>
                </li>
                <li class="menu-item">
                    <a href="./clienteView.php" class="menu-btn"><i class="fas fa-user-tie"></i> Clientes</a>
                </li>
                <li class="menu-item">
                    <a href="./colaboradorView.php" class="menu-btn"><i class="fas fa-people-carry"></i> Colaboradores</a>
                </li>
                <li class="menu-item" id="ajustes">
                    <a href="#ajustes" class="menu-btn ajustes" onmouseover="dropDown(this)" onmouseleave = "dropUp(this)" name="ajustes"><i class="fas fa-cog"></i> Ajustes <i id="caret" class="fa fa-caret-down" aria-hidden="true"></i></a>
                    <div class="sub-menu" >
                        <a href="./gestionarCuentaView.php"><i class="fas fa-users-cog"></i> Gestionar cuenta</a>
                        <a href="./logout.php"><i class="fas fa-power-off"></i> Cerrar sessión</a>
                    </div>
                </li>
            </ul>
        </nav>
        <div class="toggle">
            <i class="fas fa-bars menu" onclick=displayMenu()></i>
        </div>
    </header>
    <div class="wrapper">
        <section class="top-container">
            <section id="fila-principal">
                <button class="boton boton-success tool" id="agregar" title="Agregar" onclick="mostrarModalAgregar()"><i class="fa fa-plus" aria-hidden="true"></i></button>
                <input hidden type="search" placeholder="Buscar" class="campo" id="buscar" name="buscar" value="" onkeyup="desplegarDatos()">
            </section>

            <div class="modal fade" id="confirmar" tabindex="-1" role="dialog" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalScrollableTitle">Eliminar Registro</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <p center>¿Está seguro que desea eliminar el registro?</p>
                        </div>
                        <div class="modal-footer">
                            <input type="button" class="boton boton-primario" id="confirm" value="Confirmar">
                            <input type="button" class="boton boton-cancelar" id="cancel"  value="Descartar">
                        </div>
                    </div>
                </div>
            </div>
            <div class="table-container">
                <section id="tab"></section>
            </div>
        </section>
        <!--a class="aboutUs-button" id="btn-aboutUs" data-toggle="modal" data-target="#modalLRForm">Nosotros</a-->
        <!--Modal-->
        <div class="modal fade" id="modalLRForm" style="z-index:9999999;" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog cascading-modal modal-dialog-scrollable" role="document">
                <!--Content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalScrollableTitle">Agregar Pedido</h5>
                        <!--button  type="button" class="close" onclick="addTab(event)" >+</button-->
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <!--Modal cascading tabs-->
                    <div class="modal-body">
                        <div class="modal-c-tabs">
                            <!-- Nav tabs -->
                            <ul id="myTab" class="nav nav-tabs md-tabs tabs-2 light-blue darken-3" role="tablist">
                                <li id="li1" class="nav-item"><a class="nav-link active" href="#tab1" role="tab" data-toggle="tab"><i class="fa fa-user mr-1"></i>1</a></li>
                                <li id="last" class="nav-link"><a href="#addTab"><i class="fa fa-plus" aria-hidden="true"></i></a></li>
                            </ul>
                            <!-- Tab panels -->
                            <div class="tab-content" id="tab-content">
                                <!--Panel 1-->
                                <div class="tab-pane fade in show active" id="tab1" role="tabpanel">
                                </div>
                                    <!--/.Panel 1-->
                            </div>
                        </div>
                    </div>
                    
                    <div class="modal-footer">
                        <input type="button" class="boton boton-success" value="Agregar" name="insertar" id="insertar"
                            onclick="insertar()">
                        <input type="button" class="boton boton-cancelar" value="Descartar" name="clear" id="clear"
                            data-dismiss="modal">
                    </div>
                </div>
                <!--/.Content-->
            </div>
        </div>
    </div>
    <!-- Modal de modificar -->
    <div class="modal fade" id="modalModificar" style="z-index:9999999;" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog cascading-modal modal-dialog-scrollable" role="document">
                <!--Content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalScrollableTitle">Modificar Pedido</h5>
                        <!--button  type="button" class="close" onclick="addTab(event)" >+</button-->
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <!--Modal cascading tabs-->
                    <div class="modal-body">
                        <!--div class="spinner-border text-success position-absolute" role="status">
                            <span class="sr-only">Loading...</span>
                        </div-->
                        <div class="loader" >
                        </div>
                        <div class="modal-c-tabs">
                            <!-- Nav tabs -->
                            <ul id="nmyTab" class="nav nav-tabs md-tabs tabs-2 light-blue darken-3" role="tablist">
                                <li id="nlast" class="nav-link"><a href="#addTab"><i class="fa fa-plus" aria-hidden="true"></i></a></li>
                            </ul>
                            <!-- Tab panels -->
                            <div class="tab-content" id="nTab-content">
                            </div>
                        </div>
                    </div>
                    
                    <div class="modal-footer">
                        <input type="button" class="boton boton-primario" value="Confirmar" name="modificar" id="modificar"
                            onclick="actualizar()">
                        <input type="button" class="boton boton-cancelar" value="Descartar" name="clear" id="clear"
                        data-dismiss="modal">
                    </div>
                </div>
                <!--/.Content-->
            </div>
        </div>
    </div>
    <!-- Cierre del modal de modificar-->
    <footer id="pie">
        <p>Ornexp Derechos Reservados &copy; 2018-2019</p>
    </footer>

</body>

</html> 