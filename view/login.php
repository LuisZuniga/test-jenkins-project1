<?php
    session_start();
    if(isset($_SESSION['role'])){
        header("location:../index.php");
    }
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Login</title>
    <link rel="shortcut icon" href="../sources/images/Ornexp.png" type="image/x-icon">
    <link rel="stylesheet" href="../sources/css/menu.css">
    <link rel="stylesheet" href="../sources/css/crud.css">
    <link rel="stylesheet" href="../sources/css/login.css">
    <link rel="stylesheet" href="../sources/css/all.css">    
    <script src="../sources/js/menu.js"></script>
    <script src="../sources/js/push.min.js"></script>
    <script src="../sources/js/alertas.js"></script>
    <script src="../sources/js/login.js"></script>
</head>

<body src="../sources/images/finca1.jpeg">
    <header>
        <div class="logo"><img src="../sources/images/Ornexp.png" alt="ornexpsa"></div>
        <nav id="menu" class="">
            <ul id="ul">
                <li class="active"><a href="./login.php">Iniciar sesión</a></li>
            </ul>
        </nav>
        <div class="toggle">
            <i class="fas fa-bars menu" onclick=displayMenu()></i>
        </div>
    </header>
    <div class="login-container">
        <div class="login-box">
            <img src="../sources/images/Ornexp.png" class="logo">
            <h1>Ingrese aquí</h1>
            <form autocomplete="off">
                <label for="username">Usuario</label>
                <div class="input"><input onkeyup="lowerCase()" type="text" name="username" id="username" placeholder="Ingrese usuario" onkeyup="enterLogin(event)"></div>
                <div id="usermsj" class="mensaje">Usuario no encontrado</div>
                <label for="password">Contraseña</label>
                <div class="input"><input type="password" name="password" id="password" placeholder="Ingrese contraseña" onkeyup="enterLogin(event)"><i id="eye" class="fa fa-eye-slash" aria-hidden="true" onclick="showPassword()"></i></div>
                <div id="passmsj" class="mensaje">La contraseña no es valida</div>
                <input type="button" name="login" id="login" value="Ingresar" onclick="iniciarSession()">
                <a href="./passView.php">¿Olvidó su contraseña?</a>
            </form>
        </div>
    </div>
    <footer id="pie">
            <p>Ornexp Derechos Reservados &copy; 2018-2019</p>
    </footer>
</body>

</html>