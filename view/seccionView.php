<?php
    session_start();
    if(!isset($_SESSION['role'])){
        header("location:./login.php");
    }
?>
<!DOCTYPE html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Secciones</title>
    <link rel="shortcut icon" href="../sources/images/Ornexp.png" type="image/x-icon">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="../sources/css/menu.css">
    <link rel="stylesheet" href="../sources/css/crud.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
    <script src="../sources/js/menu.js" ></script>
    <script src="../sources/js/seccion.js"></script>
    <script src="../sources/js/validaciones.js"></script>
    <script src="../sources/js/alertas.js"></script>
    <script src="../sources/js/push.min.js"></script>
    

</head>
<body onload="desplegarDatos()">
    <header>
        <div class="logo"><img src="../sources/images/Ornexp.png" alt="ornexpsa"></div>
        <nav id="menu">
            <ul id="ul">
                <li class="menu-item">
                    <a href="../index.php" class="menu-btn"><i class="fas fa-home"></i> Inicio</a>
                </li>
                <li class="menu-item">
                    <a href="./mapeoView.php" class="menu-btn"><i class="fas fa-map-marked-alt"></i> Mapeo</a>
                </li>
                <li class="menu-item">
                    <a href="./pedidoView.php" class="menu-btn"><i class="fas fa-luggage-cart"></i> Pedidos</a>
                </li>
                <li class="menu-item">
                    <a href="./plantaView.php" class="menu-btn"><i class="fas fa-seedling"></i> Plantas</a>
                </li>
                <li class="menu-item" id='quimico'>
                    <a href="#quimico" class="menu-btn" onmouseover="dropDown(this)" onmouseleave = "dropUp(this)" name="quimico"><i class="fas fa-flask"></i> Químicos <i id="carett" class="fa fa-caret-down" aria-hidden="true"></i></a>
                    <div class="sub-menu" >
                        <a href="./quimicoView.php">Químicos</a>
                        <a href="./tipoQuimicoView.php">Tipo de químicos</a>
                    </div>
                </li>
                <li class="menu-item">
                    <a href="./suministroView.php" class="menu-btn"><i class="fas fa-boxes"></i> Suministros</a>
                </li>
                <li class="menu-item">
                    <a href="./clienteView.php" class="menu-btn"><i class="fas fa-user-tie"></i> Clientes</a>
                </li>
                <li class="menu-item">
                    <a href="./colaboradorView.php" class="menu-btn"><i class="fas fa-people-carry"></i> Colaboradores</a>
                </li>
                <li class="menu-item" id="ajustes">
                    <a href="#ajustes" class="menu-btn ajustes" onmouseover="dropDown(this)" onmouseleave = "dropUp(this)" name="ajustes"><i class="fas fa-cog"></i> Ajustes <i id="caret" class="fa fa-caret-down" aria-hidden="true"></i></a>
                    <div class="sub-menu" >
                        <a href="./gestionarCuentaView.php"><i class="fas fa-users-cog"></i> Gestionar cuenta</a>
                        <a href="#"><i class="fas fa-power-off"></i> Cerrar sessión</a>
                    </div>
                </li>
            </ul>
        </nav>
        <div class="toggle">
            <i class="fas fa-bars menu" onclick=displayMenu()></i>
        </div>
        </header>
    </header> 
    
    <div class="container">

        <section id="fila-principal">
            <button class="boton boton-success"  id="agregar" onclick="levantarForm()"><i class="fa fa-plus" aria-hidden="true"></i><p class="tool-tip">Agregar</p></button>
            <input type="search" placeholder="Buscar" class="campo" id="buscar" name="buscar" value="" onkeyup="desplegarDatos()">
        </section>

        <section class="modal" id="confirmar">
            <div class="modal-form">
                    <span id="legend"></span>
                <div class="modal-footer">
                    <input type="button" class="boton boton-primario" id="confirm" value="Confirmar">
                    <input type="button" class="boton boton-cancelar" id="cancel" value="Descartar">
                </div>
            </div>
        </section>
        <section id="tab">
        </section>
    </div>

    <footer>
            <p>Ornexp &copy; 2018</p>
    </footer>

</body>
</html>