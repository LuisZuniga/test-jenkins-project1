<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Recuperación</title>
    <script src="../sources/js/all.js"></script>
    <script src="../sources/js/mail.js"></script>
    <link rel="shortcut icon" href="../sources/images/Ornexp.png" type="image/x-icon">
    <link rel="stylesheet" href="../sources/css/menu.css">
    <link rel="stylesheet" href="../sources/css/crud.css">
    <link rel="stylesheet" href="../sources/css/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" href="../sources/css/all.css">
    <script src="../sources/js/menu.js"></script>
    <style>
        .bd {
            border: 1px solid #17A2B8;
            -moz-box-shadow: rgb(150, 150, 150) 5px 5px 10px;
            -webkit-box-shadow: rgb(150, 150, 150) 5px 5px 10px;
            box-shadow: rgb(150, 150, 150) 5px 5px 10px;
        }
    </style>
</head>

<body>
    <header>
        <div class="logo"><img src="../sources/images/Ornexp.png" alt="ornexpsa"></div>
        <nav id="menu" class="">
            <ul id="ul">
                <li class="active"><a href="./login.php">Iniciar sesión</a></li>
            </ul>
        </nav>
        <div class="toggle">
            <i class="fas fa-bars menu" onclick=displayMenu()></i>
        </div>
    </header>



    <div class="container">



        <div class="row mt-5">
            <div class="col-md-4 col-md-offset-4"></div>

            <div class="col-md-4 col-md-offset-4 mt-5 bd">
                <div class="text-center mt-5">
                    <h3><i class="fa fa-lock fa-4x"></i></h3>
                    <h2 class="text-center">¿Olvidaste tu contraseña?</h2>
                    <p>Puedes recuperar tu contraseña aquí.</p>
                    <div class="panel-body">
                        <div class="form-group">
                            <div class="input-group">
                                <input type="text" class="form-control" id="correo" placeholder="Ingresa tu correo">
                                <div class="valid-feedback">
                                    <p> La direccion de correo es válida! </p>
                                </div>
                                <div class="invalid-feedback">
                                    <p> La direccion de correo no es válida! </p>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <button id="btn-enviar" class="btn btn-lg btn-info btn-block">Reestablecer
                                contraseña</button>
                            <p class="mt-2" id="msj"></p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-md-offset-4"></div>
        </div>
    </div>


    <footer id="pie">
        <p>Ornexp Derechos Reservados &copy; 2018-2019</p>
    </footer>
</body>

</html>