<?php
    session_start();
    if(!isset($_SESSION['role'])){
        header("location:./login.php");
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Gestionar cuenta</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="../sources/css/bootstrap/bootstrap.min.css">
    <script src="../sources/js/jquery-3.3.1.min.js" ></script>
    <script src="../sources/js/bootstrap/popper.min.js" ></script>
    <script src="../sources/js/bootstrap/bootstrap.min.js"></script>  
    <link rel="stylesheet" href="../sources/css/menu.css">
    <link rel="stylesheet" href="../sources/css/index.css">
    <link rel="stylesheet" href="../sources/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="../sources/css/all.css"> 
    <link rel="stylesheet" href="../sources/css/crud.css">
    <link rel="stylesheet" href="../sources/css/gestionarCuenta.css">
    <link rel="stylesheet" href="../sources/css/toastr.min.css">
  </head>

  <body>
        <header>
            <div class="logo"><img src="../sources/images/Ornexp.png" alt="ornexpsa"></div>
            <nav id="menu">
                <ul id="ul">
                    <li class="menu-item">
                        <a href="../index.php" class="menu-btn"><i class="fas fa-home"></i> Inicio</a>
                    </li>
                    <li class="menu-item">
                        <a href="./mapeoView.php" class="menu-btn"><i class="fas fa-map-marked-alt"></i> Mapeo</a>
                    </li>
                    <li class="menu-item">
                        <a href="./pedidoView.php" class="menu-btn"><i class="fas fa-luggage-cart"></i> Pedidos</a>
                    </li>
                    <li class="menu-item">
                        <a href="./plantaView.php" class="menu-btn"><i class="fas fa-seedling"></i> Plantas</a>
                    </li>
                    <li class="menu-item" id='quimico'>
                        <a href="#" class="menu-btn" onmouseover="dropDown(this)" onmouseleave = "dropUp(this)" name="quimico"><i class="fas fa-flask"></i> Químicos <i id="carett" class="fa fa-caret-down" aria-hidden="true"></i></a>
                        <div class="sub-menu" >
                            <a href="./quimicoView.php">Químicos</a>
                            <a href="./tipoQuimicoView.php">Tipo de químicos</a>
                        </div>
                    </li>
                    <li class="menu-item">
                        <a href="./suministroView.php" class="menu-btn"><i class="fas fa-boxes"></i> Suministros</a>
                    </li>
                    <li class="menu-item">
                        <a href="./clienteView.php" class="menu-btn"><i class="fas fa-user-tie"></i> Clientes</a>
                    </li>
                    <li class="menu-item">
                        <a href="./colaboradorView.php" class="menu-btn"><i class="fas fa-people-carry"></i> Colaboradores</a>
                    </li>
                    <li class="menu-item active" id="ajustes">
                        <a href="#" class="menu-btn ajustes" onmouseover="dropDown(this)" onmouseleave = "dropUp(this)" name="ajustes"><i class="fas fa-cog"></i> Ajustes <i id="caret" class="fa fa-caret-down" aria-hidden="true"></i></a>
                        <div class="sub-menu" >
                            <a href="./gestionarCuentaView.php"><i class="fas fa-users-cog"></i> Gestionar cuenta</a>
                            <a href="./logout.php"><i class="fas fa-power-off"></i> Cerrar sessión</a>
                        </div>
                    </li>
                </ul>
            </nav>
            <div class="toggle">
                <i class="fas fa-bars menu" onclick=displayMenu()></i>
            </div>
        </header>

        <div class="wrapper">
        <section class="top-container" id="top-container">
            <div class="table-container">
                <div class="form-container">
                    <div class="form-header">
                        <h1>Gestionar cuenta</h1>
                    </div>
                    <form id="form" novalidate>
                        <div class="input-container">
                        <label for="">Usuario</label>
                        
                        <table>
                            <tbody>
                                <tr name="test">
                                    <td><i class="fas fa-user active"></i></td>
                                    <td class="td-input"><input readonly type="text" id="usuario" name="usuario" onkeyup="validarGestionarCuenta(this)"></td>
                                    <td><i class="fas fa-edit" onclick="activarEditable(this)"></i></td>
                                    <td><i class="fas fa-save off" onclick="handleUpdate(this)" id="btn-save-user"></i></td>
                                    <td><i class="fas fa-times off" onclick="cancelUpdate(this)"></i></td>
                                    <span id="tooltip-usuario" class="tooltip"></span>
                                </tr>  
                            </tbody>
                        </table>

                        </div>
                        <div class="input-container">
                            <label for="">Email</label>    
                            <table>
                                <tbody>
                                    <tr>
                                        <td><i class="fas fa-envelope active"></i></td>
                                        <td class="td-input"><input readonly type="text" name="email" id="email" onkeyup="validarGestionarCuenta(this)"></td>
                                        <!--i id="eye" class="fa fa-eye-slash" aria-hidden="true" onclick="showPassword()"></i-->
                                        <td><i class="fas fa-edit" onclick="activarEditable(this)"></i></td>
                                        <td><i class="fas fa-save off" onclick="handleUpdate(this)" id="btn-save-email"></i></td>
                                        <td><i class="fas fa-times off" onclick="cancelUpdate(this)"></i></td>
                                        <span id="tooltip-email" class="tooltip"></span>
                                    </tr>
                                    
                                </tbody>
                            </table>
                        </div>

                        <div class="input-container">
                            <button id="btn-change-pass" class="btn-change-pass" onclick="showFormChangePassword()">Cambiar contraseña</button>
                            <div id="change-password-container" class="change-password-container">
                                <div class="input-container">
                                <label for="">Contraseña actual</label>
                                    <table>
                                        <tbody>
                                            <tr>
                                                <td><i class="fas fa-key active"></i></td>
                                                <td class="td-input"><input type="password" id="passActual" name="passActual" onkeyup="validarCambiarContra(this)"></td>
                                            </tr>
                                        </tbody>
                                    </table>   
                                </div>                     
                                <div class="input-container">
                                    <label for="">Contraseña nueva</label>  
                                    <table>
                                        <tbody>
                                            <tr>
                                                <td> <i class="fas fa-key active"></i></td>
                                                <td class="td-input"><input type="password" id="pass" name="pass" onkeyup="validarCambiarContra(this)"></td>
                                                <span id="tooltip-pass" class="tooltip"></span>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>

                                <div class="input-container">
                                    <label for="pass2">Verificar contraseña</label>
                                    <table>
                                        <tbody>
                                            <tr>
                                                <td><i class="fas fa-key active"></i></td>
                                                <td class="td-input"><input type="password" id="pass2" name="pass2" onkeyup="validarCambiarContra(this)"></td>
                                                <span id="tooltip-pass2" class="tooltip"></span>
                                            </tr>
                                        </tbody>
                                    </table>    
                                </div>
                                <button onclick="showFormChangePassword()" class="btn-cancel-pass">Cancelar</button>
                                <button class="btn-save-pass" id="btn-save-pass">Guardar</button>
                            </div>
                        </div>
                        <div class="checkbox-container">
                            <label for="">Sweet Alert</label>
                            <input type="checkbox" name="notificacion" id="sweetalert" onclick="gestionarNotificaciones(this, event)">
                            <!--span class="span-question">?</span-->
                        </div>
                        <div class="checkbox-container">
                            <label for="">Toastr</label>
                            <input type="checkbox" name="notificacion" id="toastr" onclick="gestionarNotificaciones(this, event)">
                            <!--span class="span-question">?</span-->
                        </div>                        
                    </form>
                </div>
            </div>
        </section>
        <!--section class="modal" id="confirmar">
                <div class="modal-form">
                        <span id="legend"></span>
                    <div class="modal-footer">
                        <input type="button" class="boton boton-primario" id="confirm" value="Confirmar">
                        <input type="button" class="boton boton-cancelar" id="cancel" value="Descartar">
                    </div>
                </div>
        </section-->      
                
        <div class="modal fade" id="confirmar" tabindex="-1" role="dialog" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalScrollableTitle">Actualizar Registro</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <p center>¿Desea actualizar este registro?</p>
                        </div>
                        <div class="modal-footer">
                            <input type="button" class="boton boton-primario" id="confirm" value="Confirmar">
                            <input type="button" class="boton boton-cancelar" id="cancel"  value="Descartar">
                        </div>
                    </div>
                </div>
            </div>
        <footer id="pie">
            <p>Ornexp Derechos Reservados &copy; 2018-2019</p>
        </footer>
    </div>
    <script src="../sources/js/validaciones.js" ></script>     
    <script src="../sources/js/jquery-3.3.1.min.js" ></script>
    <script src="../sources/js/menu.js" ></script> 
    <script src="../sources/js/sweetalert.min.js"></script>
    <script src="../sources/js/alertas.js" ></script> 
    <script src="../sources/js/toastr.min.js" ></script>   
    <script src="../sources/js/gestionarCuenta.js" ></script>    
</body>
</html>
