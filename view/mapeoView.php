<?php
    session_start();
    if(!isset($_SESSION['role'])){
        header("location:./login.php");
    }
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Mapeo</title>
    <link rel="shortcut icon" href="../sources/images/Ornexp.png" type="image/x-icon">
    
    <!-- Scripts y css para el modal -->
    <link rel="stylesheet" href="../sources/css/bootstrap/bootstrap.min.css">
    <script src="../sources/js/jquery-3.3.1.min.js" ></script>
    <script src="../sources/js/jquery.mask.min.js" ></script>    
    <script src="../sources/js/bootstrap/popper.min.js" ></script>
    <script src="../sources/js/bootstrap/bootstrap.min.js"></script>

    <script src="../sources/js/all.js"></script>
    <script src="../sources/js/sweetalert.min.js"></script>
    <script src="../sources/js/jquery.dataTables.min.js" ></script>
    <script src="../sources/js/seccion.js"></script>
    <script src="https://www.amcharts.com/lib/3/ammap.js"></script>
    <script src="../maps/js/mapa.js" type="text/javascript"></script>
    <script src="../maps/js/ammap.js" type="text/javascript"></script>
    <script src="../sources/js/menu.js" ></script>
    <script src="../sources/js/seccionPlanta.js" ></script>
    <script src="../sources/js/alertas.js" ></script>
    <script src="../sources/js/push.min.js" ></script>
    <script src="../sources/js/validaciones.js" ></script>
    <link rel="stylesheet" href="../sources/css/menu.css">
    <link rel="stylesheet" href="../sources/css/crud.css">
    <link rel="stylesheet" href="../sources/css/index.css">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="../sources/css/all.css"> 
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:400,300|Raleway:300,400,900,700italic,700,300,600">
    <link rel="stylesheet" href="../sources/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="../sources/css/mapa.css">
  </head>
  <body>
  <div class="wrapper">
    <header>
        <div class="logo"><img src="../sources/images/Ornexp.png" alt="ornexpsa"></div>
        <nav id="menu">
            <ul id="ul">
                <li class="menu-item">
                    <a href="../index.php" class="menu-btn"><i class="fas fa-home"></i> Inicio</a>
                </li>
                <li class="menu-item active">
                    <a href="./mapeoView.php" class="menu-btn"><i class="fas fa-map-marked-alt"></i> Mapeo</a>
                </li>
                <li class="menu-item">
                    <a href="./pedidoView.php" class="menu-btn"><i class="fas fa-luggage-cart"></i> Pedidos</a>
                </li>
                <li class="menu-item">
                    <a href="./plantaView.php" class="menu-btn"><i class="fas fa-seedling"></i> Plantas</a>
                </li>
                <li class="menu-item" id='quimico'>
                    <a href="#quimico" class="menu-btn" onmouseover="dropDown(this)" onmouseleave = "dropUp(this)" name="quimico"><i class="fas fa-flask"></i> Químicos <i id="carett" class="fa fa-caret-down" aria-hidden="true"></i></a>
                    <div class="sub-menu" >
                        <a href="./quimicoView.php">Químicos</a>
                        <a href="./tipoQuimicoView.php">Tipo de químicos</a>
                    </div>
                </li>
                <li class="menu-item">
                    <a href="./suministroView.php" class="menu-btn"><i class="fas fa-boxes"></i> Suministros</a>
                </li>
                <li class="menu-item">
                    <a href="./clienteView.php" class="menu-btn"><i class="fas fa-user-tie"></i> Clientes</a>
                </li>
                <li class="menu-item">
                    <a href="./colaboradorView.php" class="menu-btn"><i class="fas fa-people-carry"></i> Colaboradores</a>
                </li>
                <li class="menu-item" id="ajustes">
                    <a href="#ajustes" class="menu-btn ajustes" onmouseover="dropDown(this)" onmouseleave = "dropUp(this)" name="ajustes"><i class="fas fa-cog"></i> Ajustes <i id="caret" class="fa fa-caret-down" aria-hidden="true"></i></a>
                    <div class="sub-menu" >
                        <a href="./gestionarCuentaView.php"><i class="fas fa-users-cog"></i> Gestionar cuenta</a>
                        <a href="./logout.php"><i class="fas fa-power-off"></i> Cerrar sessión</a>
                    </div>
                </li>
            </ul>
        </nav>
        <div class="toggle">
            <i class="fas fa-bars menu" onclick=displayMenu()></i>
        </div>
    </header>
    <!---->

    <div class="card text-center map-container">
        <div class="card-header">
            ORNEXP
        </div>
        <div class="card-body">
            <div id="mapdiv" style="background-color:#EEEEEE;"></div>
            </div>
        <div class="card-footer text-muted">
        <div class="row">
            <div class="col-sm-4">
                <div class="card">
                <div class="card-body">
                    <h5 class="card-title">Lote #1 - 26 Secciones</h5>
                    <p class="card-text">Cantidad de plantas:</p>
                </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Lote #2 - 14 Secciones</h5>
                        <p class="card-text">Cantidad de plantas:</p>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Lote #3 - 22 Secciones</h5>
                        <p class="card-text">Cantidad de plantas:</p>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </div>

    <!--div class="container">
        <div class="fila">  
            <div class="col">
                <h1>ORNEXP</h1>
                <div class="linea"></div>
                
            </div>
        </div-->
        <!--section class="modal" id="datos">
            <div class="modal-content">
                    <span class="close" onclick="cerrar()">&times;</span>
                    <input type="hidden" id="lote">
                    <input type="hidden" id="seccion">
                    <h3 id="legend"></h3>
                <div class="modal-footer">
                    <section id="modal-tab"></section> 
                    <button class="boton boton-linea-negro" onclick="verSeccion()">Ver más</button>

                </div>
            </div>
        </section-->


        <div class="modal fade" id="datos" tabindex="-1" role="dialog" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="legend"></h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <input type="hidden" id="lote">
                        <input type="hidden" id="seccion">
                        <section id="modal-tab"></section> 
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-success" onclick="verSeccion()">Ver más</button>
                    </div>
                </div>
            </div>
        </div>


    </div>
    

    <script src="../maps/js/mapaSettings.js" type="text/javascript"></script>
    <footer id="pie">
        <p>Ornexp Derechos Reservados &copy; 2018-2019</p>
    </footer>
</body>
</html>
