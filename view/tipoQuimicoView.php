<?php
    session_start();
    if(!isset($_SESSION['role'])){
        header("location:./login.php");
    }
?>
<!DOCTYPE html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Tipo Químico</title>
    <link rel="shortcut icon" href="../sources/images/Ornexp.png" type="image/x-icon">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
      <!-- Scripts y css para el modal -->
      <link rel="stylesheet" href="../sources/css/bootstrap/bootstrap.min.css">
    <script src="../sources/js/jquery-3.3.1.min.js" ></script>
    <script src="../sources/js/jquery.mask.min.js" ></script>    
    <script src="../sources/js/bootstrap/popper.min.js" ></script>
    <script src="../sources/js/bootstrap/bootstrap.min.js"></script>
    <link rel="stylesheet" href="../sources/css/menu.css">
    <link rel="stylesheet" href="../sources/css/index.css">
    <link rel="stylesheet" href="../sources/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="../sources/css/dataTables.min.css">
    <link rel="stylesheet" href="../sources/css/jquery-ui.css">
    <link rel="stylesheet" href="../sources/css/all.css">
    <link rel="stylesheet" href="../sources/css/loading.css">
    <link rel="stylesheet" href="../sources/css/crud.css">
    <link rel="stylesheet" href="../sources/css/toastr.min.css">
    <link rel="stylesheet" href="../sources/css/tooltip.css">
    <!-- Scripts -->
    <script src="../sources/js/sweetalert.min.js"></script>
    <script src="../sources/js/all.js" ></script>
    <script src="../sources/js/jquery.dataTables.min.js" ></script>
    <script src="../sources/js/jquery-ui.js" ></script>
    <script src="../sources/js/tipoQuimico.js"></script>
    <script src="../sources/js/validaciones.js"></script>
    <script src="../sources/js/push.min.js"></script>
    <script src="../sources/js/alertas.js"></script>
    <script src="../sources/js/menu.js" ></script>
    <script src="../sources/js/toastr.min.js" ></script>   

</head>
<body onload="desplegarDatos()">
    <header>
        <div class="logo"><img src="../sources/images/Ornexp.png" alt="ornexpsa"></div>
        <nav id="menu">
            <ul id="ul">
                <li class="menu-item">
                    <a href="../index.php" class="menu-btn"><i class="fas fa-home"></i> Inicio</a>
                </li>
                <li class="menu-item">
                    <a href="./mapeoView.php" class="menu-btn"><i class="fas fa-map-marked-alt"></i> Mapeo</a>
                </li>
                <li class="menu-item">
                    <a href="./pedidoView.php" class="menu-btn"><i class="fas fa-luggage-cart"></i> Pedidos</a>
                </li>
                <li class="menu-item">
                    <a href="./plantaView.php" class="menu-btn"><i class="fas fa-seedling"></i> Plantas</a>
                </li>
                <li class="menu-item active" id='quimico'>
                    <a href="#quimico" class="menu-btn" onmouseover="dropDown(this)" onmouseleave = "dropUp(this)" name="quimico"><i class="fas fa-flask"></i> Químicos <i id="carett" class="fa fa-caret-down" aria-hidden="true"></i></a>
                    <div class="sub-menu" >
                        <a href="./quimicoView.php">Químicos</a>
                        <a href="./tipoQuimicoView.php">Tipo de químicos</a>
                    </div>
                </li>
                <li class="menu-item">
                    <a href="./suministroView.php" class="menu-btn"><i class="fas fa-boxes"></i> Suministros</a>
                </li>
                <li class="menu-item">
                    <a href="./clienteView.php" class="menu-btn"><i class="fas fa-user-tie"></i> Clientes</a>
                </li>
                <li class="menu-item">
                    <a href="./colaboradorView.php" class="menu-btn"><i class="fas fa-people-carry"></i> Colaboradores</a>
                </li>
                <li class="menu-item" id="ajustes">
                    <a href="#ajustes" class="menu-btn ajustes" onmouseover="dropDown(this)" onmouseleave = "dropUp(this)" name="ajustes"><i class="fas fa-cog"></i> Ajustes <i id="caret" class="fa fa-caret-down" aria-hidden="true"></i></a>
                    <div class="sub-menu" >
                        <a href="./gestionarCuentaView.php"><i class="fas fa-users-cog"></i> Gestionar cuenta</a>
                        <a href="./logout.php"><i class="fas fa-power-off"></i> Cerrar sessión</a>
                    </div>
                </li>
            </ul>
        </nav>
        <div class="toggle">
            <i class="fas fa-bars menu" onclick=displayMenu()></i>
        </div>
    </header>
    <div class="wrapper">
        <section class="top-container">
            <section id="fila-principal">
                <button class="boton boton-success tool" id="agregar" title="Agregar" onclick="mostrarModalAgregar()"><i class="fa fa-plus" aria-hidden="true"></i></button>
            </section>

            <div class="modal fade" id="confirmar" tabindex="-1" role="dialog" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalScrollableTitle">Eliminar Registro</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <p center>¿Está seguro que desea eliminar el registro?</p>
                    </div>
                    <div class="modal-footer">
                        <input type="button" class="boton boton-primario" id="confirm" value="Confirmar">
                        <input type="button" class="boton boton-cancelar" id="cancel"  value="Descartar">
                    </div>
                    </div>
                </div>
            </div>
            <div class="table-container">
                <section id="tab">
                </section>
            </div>
        </section>    
    </div>
    <footer>
        <p>Ornexp Derechos Reservados &copy; 2018-2019</p>
    </footer>
</body>
</html>