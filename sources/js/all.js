const loading = "<ul class='animacion'><li class='c'></li><li class='c'></li><li class='c'></li><li class='c'></li><li class='c'></li></ul>";


const ring = "<div class='ring'>Cargando<span></span></div>";

let idioma = {
    sProcessing: "Procesando...",
    sLengthMenu: "Mostrar _MENU_ registros",
    sZeroRecords: "No se encontraron resultados.",
    sEmptyTable: "No se encontraron registros.",
    sInfo:
      "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
    sInfoEmpty: "Mostrando registros del 0 al 0 de un total de 0 registros",
    sInfoFiltered: "(filtrado de un total de _MAX_ registros)",
    sInfoPostFix: "",
    sSearch: "Buscar:",
    sUrl: "",
    sInfoThousands: ",",
    sLoadingRecords: "Cargando...",
    oPaginate: {
      sFirst: "Primero",
      sLast: "Ultimo",
      sNext: "Siguiente",
      sPrevious: "Anterior"
    },
    oAria: {
      sSortAscending: ": Activar para ordenar la columna de manera ascendente",
      sSortDescending: ": Activar para ordenar la columna de manera descendente"
    },
    buttons: {
      copyTitle: "Informacion copiada",
      copyKeys: "Use your keyboard or menu to select the copy command",
      copySuccess: {
        _: "%d filas copiadas al portapapeles",
        "1": "1 fila copiada al portapapeles"
      },
  
      pageLength: {
        _: "Mostrar %d filas",
        "-1": "Mostrar Todo"
      }
    }
  };

function nuevoAjax(){
    var xmlhttp = false;
    try{
        xmlhttp = new XMLHttpRequest();
    }catch(E){
        xmlhttp = false;
    }
    return xmlhttp;
}

function closeTool(){
  var tools = document.getElementsByClassName("tool");
  Object.keys(tools).forEach(function (element) {
      tools[element].addEventListener('click',function(){
          setTimeout(function(){
              $(".tool").tooltip("close");
          },500);
      });
  });
}