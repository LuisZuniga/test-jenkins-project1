/*let idioma = {
  sProcessing: "Procesando...",
  sLengthMenu: "Mostrar _MENU_ registros",
  sZeroRecords: "No se encontraron resultados.",
  sEmptyTable: "Plantas no encontradas.",
  sInfo:
    "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
  sInfoEmpty: "Mostrando registros del 0 al 0 de un total de 0 registros",
  sInfoFiltered: "(filtrado de un total de _MAX_ registros)",
  sInfoPostFix: "",
  sSearch: "Buscar:",
  sUrl: "",
  sInfoThousands: ",",
  sLoadingRecords: "Cargando...",
  oPaginate: {
    sFirst: "Primero",
    sLast: "Ultimo",
    sNext: "Siguiente",
    sPrevious: "Anterior"
  },
  oAria: {
    sSortAscending: ": Activar para ordenar la columna de manera ascendente",
    sSortDescending: ": Activar para ordenar la columna de manera descendente"
  },
  buttons: {
    copyTitle: "Informacion copiada",
    copyKeys: "Use your keyboard or menu to select the copy command",
    copySuccess: {
      _: "%d filas copiadas al portapapeles",
      "1": "1 fila copiada al portapapeles"
    },

    pageLength: {
      _: "Mostrar %d filas",
      "-1": "Mostrar Todo"
    }
  }
};

function nuevoAjax() {
  var xmlhttp = false;
  try {
    xmlhttp = new XMLHttpRequest();
  } catch (E) {
    xmlhttp = false;
  }
  return xmlhttp;
}*/

function desplegarDatos() {
  var capa = document.getElementById("tab");
  var buscar = document.getElementById("buscar").value;
  var ajax = new nuevoAjax();
  capa.innerHTML = loading;
  ajax.open("POST", "../business/PlantaController.php", true);
  ajax.onreadystatechange = function () {
    if (ajax.readyState === 4) {
      capa.innerHTML = ajax.responseText;
      $("#tabla_plantas").DataTable({
        paging: true,
        lengthChange: false,
        searching: true,
        ordering: true,
        info: false,
        autoWidth: true,
        lengthMenu: [[5]],
        language: idioma,
        columnDefs: [{
          "targets": 'no-sort',
          "orderable": false,
        }]
      });
      $(".tool").tooltip({
        tooltipClass: "mytooltip"
      });
      closeTool();
      /* Mascara del precio unitario */
      $('#precioUnitario').mask("##0.00", { reverse: true });
      $('#nPrecioUnitario').mask("##0.00", { reverse: true });
    }
  };
  ajax.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  ajax.send("buscar=" + buscar);
}

function limpiar() {
  let nombreComun = document.getElementById("nombreComun");
  let tooltipNombreComun = document.getElementById("tooltip-nombreComun");
  limpiarInput(nombreComun);
  asignarClaseExito(nombreComun, tooltipNombreComun);

  let nombreCientifico = document.getElementById("nombreCientifico");
  let tooltipNombreCientifico = document.getElementById("tooltip-nombreCientifico");
  limpiarInput(nombreCientifico);
  asignarClaseExito(nombreCientifico, tooltipNombreCientifico);


  let precioU = document.getElementById("precioUnitario");
  let tooltipPrecioU = document.getElementById("tooltip-precio");
  limpiarInput(precioU);
  asignarClaseExito(precioU, tooltipPrecioU);

  /* Vaciamos el formulario de modificar */
  let nPrecio = document.getElementById("nPrecioUnitario");
  let tooltipNPrecio = document.getElementById("tooltip-nPrecio");
  asignarClaseExito(nPrecio, tooltipNPrecio);

  let nNombreCientifico = document.getElementById("nNombreCientifico");
  tooltipNNombreCientifco = document.getElementById("tooltip-nNombreCientifico");
  asignarClaseExito(nNombreCientifico, tooltipNNombreCientifco);

  let nNombreComun = document.getElementById("nNombreComun");
  tooltipNNombreComun = document.getElementById("tooltip-nNombreComun");
  asignarClaseExito(nNombreComun, tooltipNNombreComun);

  $('#modalModificar').modal('hide');
  $('#modalAgregar').modal('hide');
  //document.getElementById("form").style.display = "none";
  //document.getElementById("form-actualizar").style.display = "none";
}

function limpiarInput(input) {
  input.value = "";
}

function insertar() {
  var nombreComun = document.getElementById("nombreComun").value;
  var nombreCientifico = document.getElementById("nombreCientifico").value;
  var precioUnitario = document.getElementById("precioUnitario").value;
  var ajax = new nuevoAjax();
  ajax.open("POST", "../business/PlantaAction.php", true);
  ajax.onreadystatechange = function () {
    $('#modalAgregar').modal('hide');
    if (ajax.readyState === 4) {
      if (ajax.responseText == "1") {
        operacionExitosa();
      } else {
        operacionFallida();
      }
      desplegarDatos();
    }
  };
  ajax.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  ajax.send(
    "create=1&nombreComun=" +
    nombreComun +
    "&nombreCientifico=" +
    nombreCientifico +
    "&precioUnitario=" +
    precioUnitario
  );
}

function actualizar() {
  $('#modalModificar').modal('hide');
  var idPlanta = document.getElementById("nIdPlanta").value;
  var nombreComun = document.getElementById("nNombreComun").value;
  var nombreCientifico = document.getElementById("nNombreCientifico").value;
  var cantidad = document.getElementById("nCantidad").value;
  var precioUnitario = document.getElementById("nPrecioUnitario").value;
  var ajax = new nuevoAjax();
  ajax.open("POST", "../business/PlantaAction.php", true);
  ajax.onreadystatechange = function () {
    if (ajax.readyState === 4) {
      if (ajax.responseText == "1") {
        operacionExitosa();
      } else {
        operacionFallida();
      }
      cerrarConfirmar();
      desplegarDatos();
    }
  };
  ajax.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  ajax.send(
    "update=1&idPlanta=" +
    idPlanta +
    "&nombreComun=" +
    nombreComun +
    "&nombreCientifico=" +
    nombreCientifico +
    "&cantidad=" +
    cantidad +
    "&precioUnitario=" +
    precioUnitario
  );
}

function eliminar(idPlanta) {
  var ajax = new nuevoAjax();
  ajax.open("POST", "../business/PlantaAction.php", true);
  ajax.onreadystatechange = function () {
    if (ajax.readyState === 4) {
      $('#confirmar').modal('hide');
      if (ajax.responseText == "1") {
        operacionExitosa();
      } else {
        operacionFallida();
      }
      cerrarConfirmar();
      desplegarDatos();
    }
  };
  ajax.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  ajax.send("delete=1&idPlanta=" + idPlanta);
}

///////////////////////////// CONFIRMACIONES ////////////////////////////////////

function cerrarConfirmar() {
  //document.getElementById("confirmar").style.display = "none";
  $('#confirmar').modal('hide');
}

function cancelar() {
  document.getElementById("confirmar").innerHTML = document.getElementById(
    "confirmar"
  ).innerHTML;
  cerrarConfirmar();
}

function levantarActualizar(parametro) {

  /*var row = parametro.parentNode.parentNode;
  document.getElementById("form-actualizar").style.display = "block";*/
  $('#modalModificar').modal('show');
  document.getElementById("nIdPlanta").value = $(parametro).parents("tr").find("td").eq(0).html();
  document.getElementById("nNombreComun").value =
    $(parametro).parents("tr").find("td").eq(1).html();
  document.getElementById("nNombreCientifico").value =
    $(parametro).parents("tr").find("td").eq(2).html();
  document.getElementById("nCantidad").value =
    $(parametro).parents("tr").find("td").eq(3).html();
  document.getElementById("nPrecioUnitario").value =
    $(parametro).parents("tr").find("td").eq(4).html();
}

function confirmarEliminar(parametro) {
  var confirm = document.getElementById("confirm");
  var cancel = document.getElementById("cancel");
  //document.getElementById("legend").innerHTML = "¿Desea eliminar este registro?";
  confirm.addEventListener("click", function () {
    eliminar(parametro);
  });
  cancel.addEventListener("click", cancelar);
}

function levantarEliminar(parametro) {
  $('#confirmar').modal('show');
  //document.getElementById("confirmar").style.display = "block";
  confirmarEliminar(parametro);
}



/*--------------VALIDACIONES NUEVAS DE LA PLANTAS---------------*/

function validarAgregarPlanta(e) {
  var inputName = e.name;
  var valor = e.value;

  if (inputName === 'nombreComun') {
    let tooltip = document.getElementById("tooltip-nombreComun");

    if (valor.length == 0)
      asignarClaseError(e, tooltip, "Por favor completar este campo.");
    else if (validarNombre(valor))
      asignarClaseExito(e, tooltip);
    else
      asignarClaseError(e, tooltip, "Solamente son permitidos los carácteres de [A-Z/a-z]");

  } else if (inputName === 'nombreCientifico') {
    let tooltip = document.getElementById("tooltip-nombreCientifico");
    if (valor.length == 0)
      asignarClaseError(e, tooltip, "Por favor completar este campo.");
    else if (validarNombre(valor))
      asignarClaseExito(e, tooltip);
    else
      asignarClaseError(e, tooltip, "Solamente son permitidos los carácteres de [A-Z/a-z]");

  } else if (inputName === 'precioUnitario') {
    let tooltip = document.getElementById("tooltip-precio");
    if (valor.length == 0)
      asignarClaseError(e, tooltip, "Por favor completar este campo.");
    else if (validarDecimal(valor))
      asignarClaseExito(e, tooltip);
    else
      asignarClaseError(e, tooltip, "Solamente son permitidos números mayores a 0.");
  }
  gestorBtnAgregar();
}

/* asignacion  de clases para los errores en formularios */

function asignarClaseError(input, tooltip, textoValidacion) {
  input.className = "input-error";
  tooltip.setAttribute("data-tooltip", textoValidacion);
  tooltip.className += " active";
}

/* Asignacion de la clase normal(sin errores) */

function asignarClaseExito(input, tooltip) {
  input.className = "";
  tooltip.className = "tooltip-text";
}

/** Apagar o encender el btn de agregar  */

function gestorBtnAgregar() {
  var nombreComun = document.getElementById("nombreComun").value;
  var nombreCientifico = document.getElementById("nombreCientifico").value;
  var precioUnitario = document.getElementById("precioUnitario").value;
  var bandera = true;
  /** 1 - 0 -> 0
   *  1 - 1 -> 1
   *  0 - 1 -> 0
   *  0 - 0 -> 0
   */
  bandera = bandera && (validarNombre(nombreComun) && !validarCampoVacio(nombreComun));
  //console.error(bandera);
  bandera = bandera && (validarNombre(nombreCientifico) && !validarCampoVacio(nombreCientifico));
  bandera = bandera && (validarDecimal(precioUnitario) && !validarCampoVacio(precioUnitario));
  administrarBtnInsertar(!bandera);
}


function validarActualizarPlanta(e) {
  var inputName = e.name;
  var valor = e.value;

  if (inputName === 'nNombreComun') {
    let tooltip = document.getElementById("tooltip-nNombreComun");

    if (valor.length == 0)
      asignarClaseError(e, tooltip, "Por favor completar este campo.");
    else if (validarNombre(valor))
      asignarClaseExito(e, tooltip);
    else
      asignarClaseError(e, tooltip, "Solamente son permitidos los carácteres de [A-Z/a-z]");

  } else if (inputName === 'nNombreCientifico') {
    let tooltip = document.getElementById("tooltip-nNombreCientifico");
    if (valor.length == 0)
      asignarClaseError(e, tooltip, "Por favor completar este campo.");
    else if (validarNombre(valor))
      asignarClaseExito(e, tooltip);
    else
      asignarClaseError(e, tooltip, "Solamente son permitidos los carácteres de [A-Z/a-z]");

  } else if (inputName === 'nPrecioUnitario') {
    let tooltip = document.getElementById("tooltip-nPrecio");
    if (valor.length == 0)
      asignarClaseError(e, tooltip, "Por favor completar este campo.");
    else if (validarDecimal(valor))
      asignarClaseExito(e, tooltip);
    else
      asignarClaseError(e, tooltip, "Solamente son permitidos números mayores a 0.");
  }
  gestorBtnActualizar();
}


/*Apagar o encender el btn de actualizar*/
function gestorBtnActualizar() {
  var nombreComun = document.getElementById("nNombreComun").value;
  var nombreCientifico = document.getElementById("nNombreCientifico").value;
  var precioUnitario = document.getElementById("nPrecioUnitario").value;
  var bandera = true;
  /** 1 - 0 -> 0
   *  1 - 1 -> 1
   *  0 - 1 -> 0
   *  0 - 0 -> 0
   */
  bandera = bandera && (validarNombre(nombreComun) && !validarCampoVacio(nombreComun));
  bandera = bandera && (validarNombre(nombreCientifico) && !validarCampoVacio(nombreCientifico));
  bandera = bandera && (validarDecimal(precioUnitario) && !validarCampoVacio(precioUnitario));
  administrarBtnActualizar(!bandera);
}


function mostrarModalAgregar() {
  limpiar();
  administrarBtnInsertar(true);
  $('#modalAgregar').modal('show');
  $('#modalAgregar').on('shown.bs.modal', function () {
    $('#nombreComun').trigger('focus');
  });
}

///////////////////////MANUAL DE USUARIO////////////////////////////////////


function sp() {
  let enjoy = document.querySelectorAll('.enjoyhint_disable_events');
  console.log(enjoy.length);
  enjoy.forEach((element) => {
    enjoy[element].style.zIndex = "1";
    console.log(element);
  });
}

function manualAgregar() {

  var enjoyhint_instance = new EnjoyHint({});
  var enjoyhint_script_steps = [
    {
      'click #agregar': '¡Bienvenido al sistema de ayuda! presiona el botón + para agregar una nueva planta.',
      'skipButton': { className: "mySkip", text: "Saltar" }
    },
    {
      'timeout': 500,
      'type #nombreComun': 'Ingrese el nombre de la planta usando caracteres alfabéticos.',
      'skipButton': { className: "mySkip", text: "Saltar" },
      'nextButton': { className: "mySkip", text: "Siguiente" },
      'showNext': true
    },
    {
      'type #nombreCientifico': 'Ingrese la variedad de la planta usando caracteres alfabéticos.',
      'skipButton': { className: "mySkip", text: "Saltar" },
      'nextButton': { className: "mySkip", text: "Siguiente" },
      'showNext': true
    },
    {
      'type #precioUnitario': 'Ingrese el precio de la planta usando caracteres numéricos.',
      'skipButton': { className: "mySkip", text: "Saltar" },
      'nextButton': { className: "mySkip", text: "Siguiente" },
      'showNext': true
    },
    {
      'click #insert': 'Presione el botón Agregar para guardar la planta.',
      'skipButton': { className: "mySkip", text: "Saltar" }
    }

  ];
  enjoyhint_instance.set(enjoyhint_script_steps);
  enjoyhint_instance.run();

}


function manualActualizar() {
  var enjoyhint_instance = new EnjoyHint({});
  var enjoyhint_script_steps = [
    {
      'click #Actualizar': '¡Bienvenido al sistema de ayuda! presiona el botón lápiz para editar una planta.',
      'skipButton': { className: "mySkip", text: "Saltar" }
    },
    {
      'timeout': 500,
      'type #nNombreComun': 'Edite el nombre de la planta usando caracteres alfabéticos.',
      'skipButton': { className: "mySkip", text: "Saltar" },
      'nextButton': { className: "mySkip", text: "Siguiente" },
      'showNext': true
    },
    {
      'type #nNombreCientifico': 'Edite la variedad de la planta usando caracteres alfabéticos.',
      'skipButton': { className: "mySkip", text: "Saltar" },
      'nextButton': { className: "mySkip", text: "Siguiente" },
      'showNext': true
    },
    {
      'type #nPrecioUnitario': 'Edite el precio de la planta usando caracteres numéricos.',
      'skipButton': { className: "mySkip", text: "Saltar" },
      'nextButton': { className: "mySkip", text: "Siguiente" },
      'showNext': true
    },
    {
      'click #update': 'Presione el botón Confirmar para guardar la planta.',
      'skipButton': { className: "mySkip", text: "Saltar" }
    }

  ];
  enjoyhint_instance.set(enjoyhint_script_steps);
  enjoyhint_instance.run();
}


function manualEliminar() {
  var enjoyhint_instance = new EnjoyHint({});
  var enjoyhint_script_steps = [
    {
      'click #Eliminar': '!Bienvenido al sistema de ayuda¡ presiona el botón basurero para eliminar una planta.',
      'skipButton': { className: "mySkip", text: "Saltar" }
    },
    {
      'timeout': 500,
      'click #confirm': 'Presione el botón Confirmar para eliminar la planta seleccionada.',
      'skipButton': { className: "mySkip", text: "Saltar" }
    }

  ];
  enjoyhint_instance.set(enjoyhint_script_steps);
  enjoyhint_instance.run();
}


