/*let idioma = {
    sProcessing: "Procesando...",
    sLengthMenu: "Mostrar _MENU_ registros",
    sZeroRecords: "No se encontraron resultados.",
    sEmptyTable: "Colaboradores no encontrados.",
    sInfo:
      "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
    sInfoEmpty: "Mostrando registros del 0 al 0 de un total de 0 registros",
    sInfoFiltered: "(filtrado de un total de _MAX_ registros)",
    sInfoPostFix: "",
    sSearch: "Buscar:",
    sUrl: "",
    sInfoThousands: ",",
    sLoadingRecords: "Cargando...",
    oPaginate: {
      sFirst: "Primero",
      sLast: "Ultimo",
      sNext: "Siguiente",
      sPrevious: "Anterior"
    },
    oAria: {
      sSortAscending: ": Activar para ordenar la columna de manera ascendente",
      sSortDescending: ": Activar para ordenar la columna de manera descendente"
    },
    buttons: {
      copyTitle: "Informacion copiada",
      copyKeys: "Use your keyboard or menu to select the copy command",
      copySuccess: {
        _: "%d filas copiadas al portapapeles",
        "1": "1 fila copiada al portapapeles"
      },
  
      pageLength: {
        _: "Mostrar %d filas",
        "-1": "Mostrar Todo"
      }
    }
  };

function nuevoAjax(){
    var xmlhttp = false;
    try{
        xmlhttp = new XMLHttpRequest();
    }catch(E){
        xmlhttp = false;
    }
    return xmlhttp;
}*/

function desplegarDatos() {
    var capa = document.getElementById("tab");
    var buscar = document.getElementById("buscar").value;
    var ajax = nuevoAjax();
    //capa.innerHTML = "<h1 id='loading' data-text='Cargando...'>Cargando...</h1>";
    capa.innerHTML = loading;
    ajax.open("POST", "../business/ColaboradorController.php",true);
    ajax.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    ajax.send("buscar="+buscar);
    ajax.onreadystatechange = function () {
        if (ajax.readyState === 4) {
            capa.innerHTML = ajax.responseText;
            $("#tabla_colaboradores").DataTable({
                paging: true,
                lengthChange: false,
                searching: true,
                ordering: true,
                info: false,
                autoWidth: true,
                lengthMenu: [[5]],
                language: idioma,
                columnDefs: [ {
                    "targets": 'no-sort',
                    "orderable": false,
                } ]
            });
            $('.tool').tooltip({
                tooltipClass: "mytooltip",
            });
            closeTool();
        }
    };
}


function insertar() {
    var cedula = document.getElementById("cedula").value;
    var nombre = document.getElementById("nombre").value;
    var ap1 = document.getElementById("apellido1").value;
    var ap2 = document.getElementById("apellido2").value;
    var direccion = document.getElementById("direccion").value;
    var telefono = document.getElementById("telefono").value;
    var ajax = new nuevoAjax();

    ajax.open("POST","../business/ColaboradorAction.php",true);
        ajax.onreadystatechange=function(){
            $('#modalAgregar').modal('hide');
            if(ajax.readyState===4){
                if(ajax.responseText == "1"){
                    operacionExitosa();
                }else{
                    operacionFallida();
                }
                desplegarDatos();
            }
        }
    ajax.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    ajax.send("accion=Insertar&cedula="+cedula+"&nombre="+nombre+"&apellido1="+ap1+"&apellido2="+ap2+"&direccion="+direccion+"&telefono="+telefono);
}


function eliminar(cedula) {
    var ajax = new nuevoAjax();
    ajax.open("POST","../business/ColaboradorAction.php",true);
    ajax.onreadystatechange=function(){
        if(ajax.readyState===4){
            $('#confirmar').modal('hide');
            if(ajax.responseText == "1"){
                operacionExitosa();
            }else{
                operacionFallida();
            }
            cerrarConfirmar();
            desplegarDatos();
        }
    }
    ajax.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    ajax.send("accion=Eliminar&cedula="+cedula);
}


function actualizar() {
    var idColaborador = document.getElementById("idColaborador").value; 
    var cedula = document.getElementById("nCedula").value;
    var nombre = document.getElementById("nNombre").value;
    var apellido1 = document.getElementById("nApellido1").value;
    var apellido2 = document.getElementById("nApellido2").value;
    var direccion = document.getElementById("nDireccion").value;
    var telefono = document.getElementById("nTelefono").value;
    var ajax = nuevoAjax();
    var ajax = new nuevoAjax();
    ajax.open("POST","../business/ColaboradorAction.php",true);
    ajax.onreadystatechange=function(){
        $('#modalModificar').modal('hide');
        if(ajax.readyState===4){
            if(ajax.responseText == "1"){
                operacionExitosa();
            }else{
                operacionFallida();
            }
            cerrarConfirmar();
            desplegarDatos();
        }
    }
    ajax.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    ajax.send("accion=Actualizar&idColaborador="+idColaborador+"&cedula="+cedula+"&nombre="+nombre+"&apellido1="+apellido1+"&apellido2="+apellido2+"&direccion="+direccion+"&telefono="+telefono);   
}


function limpiar() {
    let cedula = document.getElementById("cedula");
    let tooltipCedula = document.getElementById("tooltip-cedula");
    limpiarInput(cedula);
    asignarClaseExito(cedula, tooltipCedula);

    let nombre = document.getElementById("nombre");
    let tooltipNombre = document.getElementById("tooltip-nombre");
    limpiarInput(nombre);
    asignarClaseExito(nombre, tooltipNombre);
  
    let apellido1 = document.getElementById("apellido1");
    let tooltipApellido1 = document.getElementById("tooltip-apellido1");
    limpiarInput(apellido1);
    asignarClaseExito(apellido1, tooltipApellido1);

    let apellido2 = document.getElementById("apellido2");
    let tooltipApellido2 = document.getElementById("tooltip-apellido2");
    limpiarInput(apellido2);
    asignarClaseExito(apellido2, tooltipApellido2);

    let direccion = document.getElementById("direccion");
    let tooltipDireccion = document.getElementById("tooltip-direccion");
    limpiarInput(direccion);
    asignarClaseExito(direccion, tooltipDireccion);

    let telefono = document.getElementById("telefono");
    let tooltipTelefono = document.getElementById("tooltip-telefono");
    limpiarInput(telefono);
    asignarClaseExito(telefono, tooltipTelefono);

    $('#modalAgregar').modal('hide');
    /*document.getElementById("form").style.display = "none";*/
  }
  
  
  function limpiarActualizar() {
    let nCedula = document.getElementById("nCedula");
    let tooltipNCedula = document.getElementById("tooltip-nCedula");
    limpiarInput(nCedula);
    asignarClaseExito(nCedula, tooltipNCedula);

    let nNombre = document.getElementById("nNombre");
    let tooltipnNombre = document.getElementById("tooltip-nNombre");
    limpiarInput(nNombre);
    asignarClaseExito(nNombre, tooltipnNombre);
  
    let nApellido1 = document.getElementById("nApellido1");
    let tooltipNApellido1 = document.getElementById("tooltip-nApellido1");
    limpiarInput(nApellido1);
    asignarClaseExito(nApellido1, tooltipNApellido1);

    let nApellido2 = document.getElementById("nApellido2");
    let tooltipNApellido2 = document.getElementById("tooltip-nApellido2");
    limpiarInput(nApellido2);
    asignarClaseExito(nApellido2, tooltipNApellido2);

    let nDireccion = document.getElementById("nDireccion");
    let tooltipNDireccion = document.getElementById("tooltip-direccion");
    limpiarInput(nDireccion);
    asignarClaseExito(nDireccion, tooltipNDireccion);

    let nTelefono = document.getElementById("nTelefono");
    let tooltipNTelefono = document.getElementById("tooltip-nTelefono");
    limpiarInput(nTelefono);
    asignarClaseExito(nTelefono, tooltipNTelefono);
    
    $('#modalModificar').modal('hide');  
    //document.getElementById("form-actualizar").style.display = "none";
  }
  
  function limpiarInput(input) {
    input.value = "";
  }

///////////////////////////// CONFIRMACIONES ////////////////////////////////////



/* asignacion  de clases para los errores en formularios */

function asignarClaseError(input, tooltip, textoValidacion) {
    input.className = "input-error";
    tooltip.setAttribute("data-tooltip", textoValidacion);
    tooltip.className += " active";
  }
  
  /* Asignacion de la clase normal(sin errores) */
  
  function asignarClaseExito(input, tooltip) {
    input.className = "";
    tooltip.className = "tooltip-text";
  }

function cerrarConfirmar(){
    $('#confirmar').modal('hide');    
    //document.getElementById("confirmar").style.display = "none";
}

function cancelar(){
    document.getElementById("confirmar").innerHTML = document.getElementById("confirmar").innerHTML;
    cerrarConfirmar();
}

function levantarActualizar(parametro){
    //row = parametro.parentNode.parentNode;
    //document.getElementById("form-actualizar").style.display = "block";
    document.getElementById("idColaborador").value = $(parametro).parents("tr").find("td").eq(0).html();;
    document.getElementById("nCedula").value = $(parametro).parents("tr").find("td").eq(1).html();;
    document.getElementById("nNombre").value = $(parametro).parents("tr").find("td").eq(2).html();;
    document.getElementById("nApellido1").value = $(parametro).parents("tr").find("td").eq(3).html();;
    document.getElementById("nApellido2").value = $(parametro).parents("tr").find("td").eq(4).html();;
    document.getElementById("nDireccion").value = $(parametro).parents("tr").find("td").eq(5).html();;
    document.getElementById("nTelefono").value = $(parametro).parents("tr").find("td").eq(6).html();;
    $('#modalModificar').modal('show');
}

function confirmarEliminar(parametro){
    var confirm = document.getElementById("confirm");
    var cancel = document.getElementById("cancel");
    confirm.addEventListener("click",function(){eliminar(parametro)});
    cancel.addEventListener("click",cancelar);
}

function levantarEliminar(parametro){
    $('#confirmar').modal('show');
    //document.getElementById("confirmar").style.display = "block";
    confirmarEliminar(parametro);
}

/*--------------VALIDACIONES NUEVAS DE COLABORADOR---------------*/

function validarActualizarColaborador(e){
    var inputName = e.name;
    var valor = e.value;

    if(inputName === 'nCedula') {
      let tooltip = document.getElementById("tooltip-nCedula");  
      if(valor.length == 0) 
          asignarClaseError(e, tooltip, "Por favor completar este campo.");
      else if (validarCedula(valor)) 
          asignarClaseExito(e, tooltip);
      else 
      asignarClaseError(e, tooltip,"Solamente se permiten cedulas de 9 a 16 digítos.");
  
    }else if (inputName === 'nNombre') { 
      let tooltip = document.getElementById("tooltip-nNombre"); 
  
      if(valor.length == 0) 
          asignarClaseError(e, tooltip, "Por favor completar este campo.");
      else if (validarNombre(valor)) 
          asignarClaseExito(e, tooltip);
      else 
          asignarClaseError(e, tooltip,"Solamente son permitidos los carácteres de [A-Z/a-z]");
  
      }else if(inputName === 'nApellido1') {
          let tooltip = document.getElementById("tooltip-nApellido1");  
           if (validarNombre(valor)) 
              asignarClaseExito(e, tooltip);
          else 
          asignarClaseError(e, tooltip,"Solamente son permitidos los carácteres de [A-Z/a-z]");
  
      }else if(inputName === 'nApellido2') {
        let tooltip = document.getElementById("tooltip-nApellido2");  
        if (validarNombre(valor)) 
            asignarClaseExito(e, tooltip);
        else 
        asignarClaseError(e, tooltip,"Solamente son permitidos los carácteres de [A-Z/a-z]");

      }else if(inputName === 'nTelefono') {
          let tooltip = document.getElementById("tooltip-nTelefono");  
          if(valor.length == 0) 
              asignarClaseError(e, tooltip, "Por favor completar este campo.");
          else if (validarTelefono(valor)) 
              asignarClaseExito(e, tooltip);
          else 
          asignarClaseError(e, tooltip,"Debe contener 8 digítos.");
      }
    gestorBtnActualizar();
}

function gestorBtnActualizar() {
    var cedula = document.getElementById("nCedula").value;
    var nombre = document.getElementById("nNombre").value;
    var apellido1 = document.getElementById("nApellido1").value;
    var apellido2 = document.getElementById("nApellido2").value;
    var direccion = document.getElementById("nDireccion").value;
    var telefono = document.getElementById("nTelefono").value;
    var bandera = true;
    /** 1 - 0 -> 0
     *  1 - 1 -> 1
     *  0 - 1 -> 0
     *  0 - 0 -> 0
     */
    bandera = bandera && (validarCedula(cedula) && !validarCampoVacio(cedula));
    bandera = bandera && (validarNombre(nombre) && !validarCampoVacio(nombre));
    bandera = bandera && (validarNombre(apellido1));
    bandera = bandera && (validarNombre(apellido2));
    bandera = bandera && (validarTelefono(telefono) && !validarCampoVacio(telefono));
    administrarBtnActualizar(!bandera);
}


function validarAgregarColaborador(e){
    var inputName = e.name;
    var valor = e.value;
    
    if(inputName === 'cedula') {
      let tooltip = document.getElementById("tooltip-cedula");  
      if(valor.length == 0) 
          asignarClaseError(e, tooltip, "Por favor completar este campo.");
      else if (validarCedula(valor)) 
          asignarClaseExito(e, tooltip);
      else 
      asignarClaseError(e, tooltip,"Solamente se permiten cedulas de 9 a 16 digítos.");
  
    }else if (inputName === 'nombre') { 
      let tooltip = document.getElementById("tooltip-nombre"); 
  
      if(valor.length == 0) 
          asignarClaseError(e, tooltip, "Por favor completar este campo.");
      else if (validarNombre(valor)) 
          asignarClaseExito(e, tooltip);
      else 
          asignarClaseError(e, tooltip,"Solamente son permitidos los carácteres de [A-Z/a-z]");
  
      }else if(inputName === 'apellido1') {
          let tooltip = document.getElementById("tooltip-apellido1");  
           if (validarNombre(valor)) 
              asignarClaseExito(e, tooltip);
          else 
          asignarClaseError(e, tooltip,"Solamente son permitidos los carácteres de [A-Z/a-z]");
  
      }else if(inputName === 'apellido2') {
        let tooltip = document.getElementById("tooltip-apellido2");  
        if (validarNombre(valor)) 
            asignarClaseExito(e, tooltip);
        else 
        asignarClaseError(e, tooltip,"Solamente son permitidos los carácteres de [A-Z/a-z]");

      }else if(inputName === 'telefono') {
          let tooltip = document.getElementById("tooltip-telefono");  
          if(valor.length == 0) 
              asignarClaseError(e, tooltip, "Por favor completar este campo.");
          else if (validarTelefono(valor)) 
              asignarClaseExito(e, tooltip);
          else 
          asignarClaseError(e, tooltip,"Debe contener 8 digítos.");
      }
    gestorBtnAgregar();
}

function gestorBtnAgregar() {
    var cedula = document.getElementById("cedula").value;
    var nombre = document.getElementById("nombre").value;
    var apellido1 = document.getElementById("apellido1").value;
    var apellido2 = document.getElementById("apellido2").value;
    var direccion = document.getElementById("direccion").value;
    var telefono = document.getElementById("telefono").value;
    var bandera = true;
    /** 1 - 0 -> 0
     *  1 - 1 -> 1
     *  0 - 1 -> 0
     *  0 - 0 -> 0
     */
    bandera = bandera && (validarCedula(cedula) && !validarCampoVacio(cedula));
    bandera = bandera && (validarNombre(nombre) && !validarCampoVacio(nombre));
    bandera = bandera && (validarNombre(apellido1));
    bandera = bandera && (validarNombre(apellido2));
    bandera = bandera && (validarTelefono(telefono) && !validarCampoVacio(telefono));
    administrarBtnInsertar(!bandera);
}

/* Levantar formulario de agregar colab */
function mostrarModalAgregar(){
    limpiar();
    administrarBtnInsertar(true);
    $('#modalAgregar').modal('show'); 
    $('#modalAgregar').on('shown.bs.modal', function () {
      $('#cedula').trigger('focus');
    });
}
  