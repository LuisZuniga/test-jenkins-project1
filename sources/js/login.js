function nuevoAjax(){
    var xmlhttp = false;
    try{
        xmlhttp = new XMLHttpRequest();
    }catch(E){
        xmlhttp = false;
    }
    return xmlhttp;
}

function enterLogin(event){
    if(event.keyCode === 13){
        iniciarSession();
    }
}

function lowerCase(){
    var username = document.getElementById("username");
    var user = username.value.toLowerCase();
    username.value = user;
}

function iniciarSession(){
    var username = document.getElementById("username").value;
    var password = document.getElementById("password").value;
    var ajax = new nuevoAjax();
    ajax.open("POST","../business/LoginAction.php",true);
    ajax.onreadystatechange=function(){
        if(ajax.readyState===4){
            var res = ajax.responseText;
            if(res == 0){
                document.getElementById("usermsj").style.display = "block";
                document.getElementById("passmsj").style.display = "none";
            }else if(res == 1){
                document.getElementById("passmsj").style.display = "block";
                document.getElementById("usermsj").style.display = "none";
            }else{
                window.location.href = res;
            }
        }
    }
    ajax.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    ajax.send("login=1&username="+username+"&password="+password);
}

function showPassword(){
    var pass = document.getElementById("password");
    var eye = document.getElementById("eye");
    attr = pass.getAttribute("type");
    if(attr == "password"){
        pass.removeAttribute(attr);
        pass.setAttribute("type","text");
        eye.classList.remove("fa-eye-slash");
        eye.classList.add("fa-eye");

    }else{
        pass.removeAttribute(attr);
        pass.setAttribute("type","password");
        eye.classList.remove("fa-eye");
        eye.classList.add("fa-eye-slash");
    }
}