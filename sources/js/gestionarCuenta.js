function nuevoAjax() {
    var xmlhttp = false;
    try {
      xmlhttp = new XMLHttpRequest();
    } catch (E) {
      xmlhttp = false;
    }
    return xmlhttp;
  }

  
$("form").submit(function(e){
    e.preventDefault();
});


$( document ).ready(function() {
    fillForm();
});

function activarEditable(colunma) {
    let campo = $(colunma).parents("tr").find("td").eq(1).find("input").eq(0);
    $(campo).attr("readonly", false);
    $(campo).focus();
    $(campo).select();
    handleIcons(colunma);
}

/*  Recibo el icon de save y obtengo el input*/
function handleUpdate(saveIcon) {
    let campo = $(saveIcon).parents("tr").find("td").eq(1).find("input").eq(0);
    if (campo.attr("id") === "usuario") {
        actualizarUsuario($(campo).val());
        handleIcons(saveIcon);
    } else if (campo.attr("id") === "email") {
        actualizarEmail($(campo).val());
        handleIcons(saveIcon);
    }
    $(campo).attr("readonly", true); // Hace que el td deje de ser editable.
}


/* Cancelar update */
function cancelUpdate(cancelIcon) {
    let campo = $(cancelIcon).parents("tr").find("td").eq(1).find("input").eq(0);
    $(campo).attr("readonly", true);
    fillForm();
    handleIcons(cancelIcon);
    asignarClaseExito(document.getElementById("usuario"), document.getElementById("tooltip-usuario"));
    asignarClaseExito(document.getElementById("email"), document.getElementById("tooltip-email"));
}

/* Confirmar actualizar */
function confirmarActualizar(parametro){
    //document.getElementById("confirmar").style.display = "block";
    $('#confirmar').modal('show');
    var confirm = document.getElementById("confirm");
    var cancel = document.getElementById("cancel");
    document.getElementById("legend").innerHTML = "Desea actualizar este registro?";
    confirm.addEventListener("click",function(){gestionarActualizar(parametro)});
    cancel.addEventListener("click",cancelar);
}

/* cierra el metodo de confirmarActualizar */
function cerrarConfirmar() {
    document.getElementById("confirmar").style.display = "none";
}

function cancelar() {
    document.getElementById("confirmar").innerHTML = document.getElementById("confirmar").innerHTML;
    fillForm();
    cerrarConfirmar();
}
  

/* Metodo para actualizar el usuario */
function actualizarUsuario(username) {
    cerrarConfirmar();
    var ajax = new nuevoAjax();
    ajax.open("POST", "../business/LoginAction.php", true);
    ajax.onreadystatechange = function() {
        if (ajax.readyState === 4) {
            if (ajax.responseText === "1") {
                operacionExitosa();
            } else {
                operacionFallida();
            }
        }
    };
    ajax.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    ajax.send("changename=1&username="+username);
}

/* Metodo para actualizar el email */

function actualizarEmail(email) {
    var ajax = new nuevoAjax();
    ajax.open("POST", "../business/LoginAction.php", true);
    ajax.onreadystatechange = function() {
      if (ajax.readyState === 4) {
         if (ajax.responseText === '1') {
            operacionExitosa();
         } else {
            operacionFallida();
         }
      }
    };
    ajax.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    ajax.send("changemail=1&email="+email);
}

/* Me llena el formulario con los cambios que he vienen desde la db */
function fillForm() {
    var ajax = new nuevoAjax();
    var username = document.getElementById("usuario");
    var email = document.getElementById("email");
    ajax.open("POST", "../business/LoginAction.php", true);
    ajax.onreadystatechange = function() {
      if (ajax.readyState === 4) {
          var json = JSON.parse(ajax.responseText);
          username.value = json[1];
          email.value = json[3];
          activarChecked(json[4]);
      }
    };
    ajax.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    ajax.send("getUsuario=1");
}

/* Me activa los checked del checkbox dependiendo del valor que mandan de la base de datos. */
function activarChecked(notificacion) {
    if(notificacion === "0") { // 0 -> SweetAlert
        $("#toastr")[0].checked = false;
        $("#sweetalert")[0].checked = true;
    } else {
        $("#toastr")[0].checked = true;
        $("#sweetalert")[0].checked = false;
    }
}

/* Cambiar notificaciones */

function gestionarNotificaciones(checkbox, event) {
    event.preventDefault();
    if (checkbox.id === "sweetalert") {
        if ($(checkbox).is(':checked')) {
            // Enviamos un 0 porque 0 = sweetalert
            actualizarNotificacion(0);
        } else {
            // enviamos un 1 porque vamos a desactivar la notificacion
            actualizarNotificacion(1);
        }
    } else {
        if ($(checkbox).is(':checked')) {
            actualizarNotificacion(1);
        } else {
            // enviamos un 0 porque vamos a desactivar la notificacion
            actualizarNotificacion(0);
        }
    }
}

function actualizarNotificacion(notificacion) {
    var ajax = new nuevoAjax();
    ajax.open("POST", "../business/LoginAction.php", true);
    ajax.onreadystatechange = function() {
      if (ajax.readyState === 4) {
          if (ajax.responseText === "1") {
            operacionExitosa();
          } else {
            operacionFallida();
          }
      }
      fillForm();
    };
    ajax.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    ajax.send("changenotification=1&notification="+notificacion);
}



/* Metodo encargado de cambiar la contrasena */
function cambiarPass() {
    var passActual = document.getElementById("passActual").value;
    var pass = document.getElementById("pass").value;
    var ajax = new nuevoAjax();
    ajax.open("POST", "../business/LoginAction.php", true);
    ajax.onreadystatechange = function() {
        if (ajax.readyState === 4) {
            if (ajax.responseText === "1") {
                operacionExitosa();
                showFormChangePassword();// Escondemos el form
                handleSavePassBtn(); // Desactivamos el boton de guardar pass
            } else if (ajax.responseText === "2"){
                currentPassIncorrect();
                $("#passActual").focus(); // Ponemos el focus en la pass actual para que intente cambiarla de nuevo.
                $("#passActual").select(); // Seleccionamos todo el texto. 
                handleSavePassBtn();// Desactivamos el boton de guardar pass
            } else {
                operacionFallida();
                showFormChangePassword(); // Escondemos el form
                handleSavePassBtn();// Desactivamos el boton de guardar pass
            }
        }
        fillForm();
    };
    ajax.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    ajax.send("changepass=1&npass="+pass+"&cpass="+passActual);
}


/* Verifica si las contrasena es igual a la actual */
function validarCambiarContra(campo) {
    var tooltipPass = document.getElementById("tooltip-pass"); // Tooltip de contrasena normal
    var tooltipVPass = document.getElementById("tooltip-pass2"); // Tooltip de verificar contrasena

    if (campo.id === "passActual") { 
        

    } else if (campo.id === "pass") {
        if (validarPassword(campo.value)) {
            asignarClaseExito(campo, tooltipPass);
        } else {
            asignarClaseError(campo, tooltipPass, 'Debe contener de 8 a 16 carácteres, al menos una mayúscula, una minúscula y un número.');
            
            // Debemos preguntar si la contraseña nueva es igual al verificar, así mostramos el otro tooltip
            if (document.getElementById("pass2").value === campo.value) {
                asignarClaseExito(campo, tooltipVPass);
            } else {
                asignarClaseError(campo, tooltipVPass, 'Debe coincidir con la contraseña nueva.');
            }
        }

    } else if (campo.id === "pass2") {
        if (document.getElementById("pass").value === campo.value) {
            asignarClaseExito(campo, tooltipVPass);
        } else {
            asignarClaseError(campo, tooltipVPass, 'Debe coincidir con la contraseña nueva.');
        }
    }
    handleSavePassBtn(); // Controlador del boton de guardar contrasena
}

// Controlador del boton de guardar contrasena
function handleSavePassBtn() {
    var currentPass = document.getElementById("passActual");
    var password = document.getElementById("pass");
    var vPassword = document.getElementById("pass2");
    var bandera = true;

    // Validamos la currentPass.
    bandera = bandera && (!validarCampoVacio(currentPass.value));
    console.log("Bandera PassC->"+bandera);
    // Validamos la contra nueva
    bandera = bandera && (validarPassword(password.value));
    console.log("Bandera PassN->"+bandera);
    // Validamos la verificacion de contrasena
    bandera = bandera && (!validarCampoVacio(vPassword.value) && (password.value === vPassword.value));
    console.log("Bandera PassV->"+bandera);
    console.log("Bandera->"+bandera);
    if (bandera) {
        $("#btn-save-pass").attr('onclick', 'cambiarPass()'); // agregamos el evento del boton.
    } else {
        $("#btn-save-pass").removeAttr('onclick'); // quitamos el evento del boton.
    }
}

/* Validar cambios en la cuenta del usuario */
function validarGestionarCuenta(input) {
    
    if(input.id === "usuario") {
        var tooltip = document.getElementById("tooltip-usuario");
        var btnSave = document.getElementById("btn-save-user");
        if(!validarEspacios(input.value)) {
            if (validarTamanoTexto(input.value, 16)) { // Debe tener 16 como maximo
                asignarClaseExito(input, tooltip);
                handleButtonSave(btnSave, false);
            } else {
                asignarClaseError(input, tooltip, 'Solo longitudes menores a '+ 16+' carácteres.');
                handleButtonSave(btnSave, true);
            }
        } else {
            asignarClaseError(input, tooltip, 'No se permiten espacios en el usuario.');
            handleButtonSave(btnSave, true);
        }
    } else if (input.id === "email") {
        var tooltip = document.getElementById("tooltip-email");
        var btnSave = document.getElementById("btn-save-email");
        if (validarEmail(input.value)) {
            asignarClaseExito(input, tooltip);
            handleButtonSave(btnSave, false);
        } else {
            asignarClaseError(input, tooltip, 'test_123@gmail.com: ejemplo válido.');
            handleButtonSave(btnSave, true);
        }
    }
}


/* Limpiar los inputs */
function limpiarInput(input) {
    input.value = "";
}

/* asignacion  de clases para los errores en formularios */

function asignarClaseError(input, tooltip, textoValidacion) {
    input.className = "input-error";
    tooltip.setAttribute("data-tooltip", textoValidacion);
    tooltip.className += " tooltip-active";
  }
  
  /* Asignacion de la clase normal(sin errores) */
  
function asignarClaseExito(input, tooltip) {
    input.className = "";
    tooltip.className = "tooltip";
}  

/* Desactivar btn */
function handleButtonSave(button, disabled) {
    if (disabled) {
        $(button).removeAttr("onclick");
    } else {
        $(button).attr('onclick', 'handleUpdate(this)');
    }
}
/* muestra el formulario de cambiar contraseña */
function showFormChangePassword () {
    $("#change-password-container").slideToggle(500);
    $("#btn-change-pass").fadeToggle(0.5, "linear" );
    limpiarInput(document.getElementById("passActual"));
    limpiarInput(document.getElementById("pass"));
    limpiarInput(document.getElementById("pass2"));
}

/* Gestionar iconos */
function handleIcons(iconTouched) { // Recibe el icono que fue tocado..
    let updateIcon = $(iconTouched).parents("tr").find("td").eq(2).find("i").eq(0);
    let saveIcon = $(iconTouched).parents("tr").find("td").eq(3).find("i").eq(0);
    let cancelIcon = $(iconTouched).parents("tr").find("td").eq(4).find("i").eq(0);
    
    $(updateIcon).fadeToggle("fast", "linear");
    $(saveIcon).fadeToggle("slow", "linear");
    $(cancelIcon).fadeToggle("slow", "linear");
    
}