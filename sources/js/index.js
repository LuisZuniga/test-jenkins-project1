let idioma = {
  sProcessing: "Procesando...",
  sLengthMenu: "Mostrar _MENU_ registros",
  sZeroRecords: "No se encontraron resultados",
  sEmptyTable: "No se encontraron resultados.",
  sInfo:
    "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
  sInfoEmpty: "Mostrando registros del 0 al 0 de un total de 0 registros",
  sInfoFiltered: "(filtrado de un total de _MAX_ registros)",
  sInfoPostFix: "",
  sSearch: "Buscar:",
  sUrl: "",
  sInfoThousands: ",",
  sLoadingRecords: "Cargando...",
  oPaginate: {
    sFirst: "Primero",
    sLast: "Ultimo",
    sNext: "Siguiente",
    sPrevious: "Anterior"
  },
  oAria: {
    sSortAscending: ": Activar para ordenar la columna de manera ascendente",
    sSortDescending: ": Activar para ordenar la columna de manera descendente"
  },
  buttons: {
    copyTitle: "Informacion copiada",
    copyKeys: "Use your keyboard or menu to select the copy command",
    copySuccess: {
      _: "%d filas copiadas al portapapeles",
      "1": "1 fila copiada al portapapeles"
    },

    pageLength: {
      _: "Mostrar %d filas",
      "-1": "Mostrar Todo"
    }
  }
};

function nuevoAjax() {
  var xmlhttp = false;
  try {
    xmlhttp = new XMLHttpRequest();
  } catch (E) {
    xmlhttp = false;
  }
  return xmlhttp;
}

$(document).ready(function() {
  cargarTablaPedidos();
  cargarTablaQuimicos();
  cargarTablaSuministros();
  cargarTablaSeccion();
  $("#table").DataTable();
});
/** Esta funcion se va a ejecutar siempre que se haya cargado toda la pagina 
document.addEventListener('DOMContentLoaded', function() {
    cargarTablaQuimicos();
    cargarTablaSuministros();
    cargarTablaSeccion();
 });**/

/** Cargamos la tabla de quimicos que estan por agotar **/
function cargarTablaQuimicos() {
  var capa = document.getElementById("quimicos-container");
  var ajax = new nuevoAjax();
  ajax.open("POST", "./business/QuimicoAction.php", true);
  ajax.onreadystatechange = function() {
    if (ajax.readyState === 4) {
      capa.innerHTML = ajax.responseText;
      $("#tabla").DataTable({
        paging: true,
        lengthChange: false,
        searching: false,
        ordering: true,
        info: false,
        autoWidth: true,
        lengthMenu: [[4]],
        language: idioma
      });
    }
  };
  ajax.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  ajax.send("quimicosAgotados");
}

/** Cargamos la tabla de suministros que estan por agotar **/

function cargarTablaSuministros() {
  var capa = document.getElementById("suministros-container");
  var ajax = new nuevoAjax();
  ajax.open("POST", "./business/SuministroAction.php", true);
  ajax.onreadystatechange = function() {
    if (ajax.readyState === 4) {
      capa.innerHTML = ajax.responseText;
      $("#table_suministros").DataTable({
        paging: true,
        lengthChange: false,
        searching: false,
        ordering: true,
        info: false,
        autoWidth: true,
        lengthMenu: [[4], [4]],
        language: idioma
      });
    }
  };
  ajax.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  ajax.send("accion=suministrosAgotados");
}

/** Mostrar tabla de siembra y extraccion */

function cargarTablaSeccion() {
  var capa = document.getElementById("secciones-container");
  var ajax = new nuevoAjax();
  ajax.open("POST", "./business/SeccionAction.php", true);
  ajax.onreadystatechange = function() {
    if (ajax.readyState === 4) {
      capa.innerHTML = ajax.responseText;
      $("#tabla_cosecha").DataTable({
        paging: true,
        lengthChange: false,
        searching: false,
        ordering: true,
        info: false,
        autoWidth: true,
        lengthMenu: [[4]],
        language: idioma
      });
    }
  };
  ajax.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  ajax.send("diasFaltantesExtraer");
}

function levantarVerPedidos(idPedido) {
  alert(idPedido +" IdPedido"); 
  var ajax = new nuevoAjax();
  ajax.open("POST", "../business/PedidoAction.php", true);
  /*ajax.onreadystatechange = function() {
    if (ajax.readyState === 4) {
      if (ajax.responseText == "1") {
        operacionExitosa();
      } else {
        operacionFallida();
      }
      cerrarConfirmar();
      desplegarDatos();
    }
  };*/
  ajax.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  ajax.send("verPedidos=1&idPedido=" + idPedido);
}

/** Mostrar tabla de pedidos */
function cargarTablaPedidos() {
  var capa = document.getElementById("pedidos-container");
  var ajax = new nuevoAjax();
  ajax.open("POST", "./business/PedidoAction.php", true);
  ajax.onreadystatechange = function() {
    if (ajax.readyState === 4) {
      capa.innerHTML = ajax.responseText;
      $("#tabla_pedidosRecientes").DataTable({
        paging: true,
        lengthChange: false,
        searching: false,
        ordering: true,
        info: false,
        autoWidth: true,
        lengthMenu: [[4]],
        language: idioma
      });
    }
  };
  ajax.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  ajax.send("pedidosRecientes");
}
