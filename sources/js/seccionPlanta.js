/*let idioma = {
    sProcessing: "Procesando...",
    sLengthMenu: "Mostrar _MENU_ registros",
    sZeroRecords: "No se encontraron resultados.",
    sEmptyTable: "Secciones no encontradas.",
    sInfo:
      "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
    sInfoEmpty: "Mostrando registros del 0 al 0 de un total de 0 registros",
    sInfoFiltered: "(filtrado de un total de _MAX_ registros)",
    sInfoPostFix: "",
    sSearch: "Buscar:",
    sUrl: "",
    sInfoThousands: ",",
    sLoadingRecords: "Cargando...",
    oPaginate: {
      sFirst: "Primero",
      sLast: "Ultimo",
      sNext: "Siguiente",
      sPrevious: "Anterior"
    },
    oAria: {
      sSortAscending: ": Activar para ordenar la columna de manera ascendente",
      sSortDescending: ": Activar para ordenar la columna de manera descendente"
    },
    buttons: {
      copyTitle: "Informacion copiada",
      copyKeys: "Use your keyboard or menu to select the copy command",
      copySuccess: {
        _: "%d filas copiadas al portapapeles",
        "1": "1 fila copiada al portapapeles"
      },
  
      pageLength: {
        _: "Mostrar %d filas",
        "-1": "Mostrar Todo"
      }
    }
  };
*/

/*function nuevoAjax(){
    var xmlhttp = false;
    try{
        xmlhttp = new XMLHttpRequest();
    }catch(E){
        xmlhttp = false;
    }
    return xmlhttp;
}

function ocultar(){
    document.getElementById("fila-principal").style.display = "none";
}*/

function desplegarDatosSP(array){
    var capa  = document.getElementById("modal-tab");
    var lote = array[0];
    var seccion = array[1];
    document.getElementById("lote").value = lote;
    document.getElementById("seccion").value = seccion;
    var ajax = nuevoAjax();
    capa.innerHTML = "<span>Cargando...</span>";
    ajax.open("POST","../business/SeccionPlantaController.php");
    ajax.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    ajax.onreadystatechange=function(){
        if(ajax.readyState===4){
            capa.innerHTML=ajax.responseText;
        }
    }
    ajax.send("lote="+lote+"&seccion="+seccion);
}

function cerrar(){
    document.getElementById("datos").style.display = "none";
}

function verSeccion(){
  var lote = document.getElementById("lote").value;
  var seccion = document.getElementById("seccion").value;
  //window.alert(lote + seccion);
  localStorage.setItem("lote",lote);
  localStorage.setItem("seccion",seccion);
  location.href = "../view/mapeoSeccionView.php";
}

/*
function verSeccion(){
    var modal = document.getElementById("datos");
    modal.style.display = "none";
    var capa = document.getElementById("tab");
    capa.style.display = "block";
    var lote = document.getElementById("lote").value;
    var seccion = document.getElementById("seccion").value;
    var ajax = nuevoAjax();
    ajax.open("POST","../business/SeccionController2.php");
    ajax.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    ajax.onreadystatechange=function(){
        if(ajax.readyState===4){
            document.getElementById("fila-principal").style.display = "block";
            capa.innerHTML= ajax.responseText;
            document.getElementsByClassName("fila")[0].style.display = "none";
            $("#tabla_secciones").DataTable({
                paging: true,
                lengthChange: false,
                searching: true,
                ordering: true,
                info: false,
                autoWidth: true,
                lengthMenu: [[5]],
                language: idioma
            });
        }
    }
    ajax.send("lote="+lote+"&seccion="+seccion);
    
}
*/

