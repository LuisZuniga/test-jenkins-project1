
// realiza la validacion en el evento onblur.
window.onload = function () {
    document.getElementById('correo').addEventListener('blur', function () {
        validarCorreo(event.target);
    });

    document.getElementById('btn-enviar').addEventListener('click', enviar, false);
    //document.getElementById('btn-enviar').removeEventListener('click', enviar, false);
}


// valida el correo con reglas lexicas.
function validarCorreo(mail) {
    emailRegex = /^[-\w.%+]{1,64}@(?:[A-Z0-9-]{1,63}\.){1,125}[A-Z]{2,63}$/i;
    if (emailRegex.test(mail.value)) {
        mail.classList.add('is-valid');
        mail.classList.remove('is-invalid');
        return true;
    } else {
        mail.classList.remove('is-valid');
        mail.classList.add('is-invalid');
        return false;
    }
}


// realiza el envio de contraseña si el correo es valido.
function enviar() {
    let correo = document.querySelector('#correo');
    if (validarCorreo(correo)) {
        let btn = document.querySelector('#btn-enviar');
        let mensaje = document.querySelector('#msj');
        mensaje.classList.remove('text-success');
        mensaje.classList.add('text-danger');
        btn.innerHTML = 'Enviando...';
        let resultado = '0';
        let destinatario = correo.value;
        let ajax = nuevoAjax();
        ajax.open('POST', '../business/MailAction.php', true);
        ajax.onreadystatechange = function () {
            if (ajax.readyState === 4) {
                resultado = ajax.responseText;
                alert(resultado);
                if (resultado == 1) {
                    mensaje.innerHTML = 'Revisa tu correo electrónico!';
                    mensaje.classList.remove('text-danger');
                    mensaje.classList.add('text-success');
                    btn.innerHTML = 'Enviado';
                } else if (resultado == 4) {
                    mensaje.innerHTML = 'Este correo no está registrado en el sistema!';
                } else {
                    mensaje.innerHTML = 'Ocurrió un error en la operacion!';
                }
                setTimeout(function () {
                    btn.innerHTML = 'Recuperar contraseña';
                }, 2000);
            }
        }
        ajax.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
        ajax.send('enviar=1' + '&destinatario=' + destinatario);
    }

}