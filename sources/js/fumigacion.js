/*let idioma = {
  sProcessing: "Procesando...",
  sLengthMenu: "Mostrar _MENU_ registros",
  sZeroRecords: "No se encontraron resultados.",
  sEmptyTable: "Fumigaciones no encontradas.",
  sInfo:
    "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
  sInfoEmpty: "Mostrando registros del 0 al 0 de un total de 0 registros",
  sInfoFiltered: "(filtrado de un total de _MAX_ registros)",
  sInfoPostFix: "",
  sSearch: "Buscar:",
  sUrl: "",
  sInfoThousands: ",",
  sLoadingRecords: "Cargando...",
  oPaginate: {
    sFirst: "Primero",
    sLast: "Ultimo",
    sNext: "Siguiente",
    sPrevious: "Anterior"
  },
  oAria: {
    sSortAscending: ": Activar para ordenar la columna de manera ascendente",
    sSortDescending: ": Activar para ordenar la columna de manera descendente"
  },
  buttons: {
    copyTitle: "Informacion copiada",
    copyKeys: "Use your keyboard or menu to select the copy command",
    copySuccess: {
      _: "%d filas copiadas al portapapeles",
      "1": "1 fila copiada al portapapeles"
    },

    pageLength: {
      _: "Mostrar %d filas",
      "-1": "Mostrar Todo"
    }
  }
};

function nuevoAjax() {
  var xmlhttp = false;
  try {
    xmlhttp = new XMLHttpRequest();
  } catch (E) {
    xmlhttp = false;
  }
  return xmlhttp;
}*/

function desplegarDatos() {
  
  var capa = document.getElementById("tab");
  var buscar = document.getElementById("buscar").value;
  var lote = localStorage.getItem("lote");
  var seccion =   localStorage.getItem("seccion");

  var ajax = new nuevoAjax();
  capa.innerHTML = loading;
  ajax.open("POST", "../business/FumigacionController.php", true);
  ajax.onreadystatechange = function() {
    if (ajax.readyState === 4) {
      capa.innerHTML = ajax.responseText;
      $("#tabla_fumigaciones").DataTable({
        paging: true,
        lengthChange: false,
        searching: true,
        ordering: true,
        info: false,
        autoWidth: true,
        lengthMenu: [[5]],
        language: idioma,
        columnDefs: [{
          "targets": 'no-sort',
          "orderable": false,
        }]
      });
      $(".tool").tooltip({
        tooltipClass: "mytooltip",
      });
      closeTool();
    }
  };
  ajax.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  ajax.send("buscar=" + buscar +"&lote="+lote+"&seccion="+seccion);
}

function limpiar() {
  let dosis = document.getElementById("dosis");
  let tooltipDosis = document.getElementById("tooltip-dosis");
  limpiarInput(dosis);
  asignarClaseExito(dosis, tooltipDosis);

  let fechaFumigacion = document.getElementById("fechaFumigacion");
  let tooltipFechaFumigacion = document.getElementById("tooltip-fechaFumigacion");
  limpiarInput(fechaFumigacion);
  asignarClaseExito(fechaFumigacion, tooltipFechaFumigacion);


  let quimico = document.getElementById("quimico");
  let tooltipQuimico = document.getElementById("tooltip-quimico");
  limpiarInput(quimico);
  asignarClaseExito(quimico, tooltipQuimico);

  let colaboradorEncargado = document.getElementById("colaboradorEncargado");
  let tooltipColaboradorEncargado = document.getElementById("tooltip-colaboradorEncargado");
  limpiarInput(colaboradorEncargado);
  asignarClaseExito(colaboradorEncargado, tooltipColaboradorEncargado);

  $('#modalAgregar').modal('hide'); 
  //document.getElementById("form").style.display = "none";
}


function limpiarActualizar() {
  let dosis = document.getElementById("nDosis");
  let tooltipDosis = document.getElementById("tooltip-nDosis");
  limpiarInput(dosis);
  asignarClaseExito(dosis, tooltipDosis);

  let fechaFumigacion = document.getElementById("nFechaFumigacion");
  let tooltipFechaFumigacion = document.getElementById("tooltip-nFechaFumigacion");
  limpiarInput(fechaFumigacion);
  asignarClaseExito(fechaFumigacion, tooltipFechaFumigacion);


  let quimico = document.getElementById("nQuimico");
  let tooltipQuimico = document.getElementById("tooltip-nQuimico");
  limpiarInput(quimico);
  asignarClaseExito(quimico, tooltipQuimico);

  let colaboradorEncargado = document.getElementById("nColaboradorEncargado");
  let tooltipColaboradorEncargado = document.getElementById("tooltip-nColaboradorEncargado");
  limpiarInput(colaboradorEncargado);
  asignarClaseExito(colaboradorEncargado, tooltipColaboradorEncargado);

  $('#modalModificar').modal('hide'); 
  //document.getElementById("form-actualizar").style.display = "none";
}

function limpiarInput(input) {
  input.value = "";
}

function insertar() {
  var seccion = document.getElementById("seccion").value;
  var lote = document.getElementById("lote").value;
  var colaboradorEncargado = document.getElementById("colaboradorEncargado").value;
  var dosis = document.getElementById("dosis").value;
  var fechaFumigacion = document.getElementById("fechaFumigacion").value;
  var quimico = document.getElementById("quimico").value;
  var ajax = new nuevoAjax();
  ajax.open("POST", "../business/FumigacionAction.php", true);
  ajax.onreadystatechange = function() {
    $('#modalAgregar').modal('hide'); 
    if (ajax.readyState === 4) {
      if (ajax.responseText == "1") {
        operacionExitosa();
      } else if(ajax.responseText === "2") {
        // cantidad ingresada es mayor que la del stock.
        cantidadNoAceptada();
      } else {
        operacionFallida();
      }
      desplegarDatos();
    }
  };
  ajax.setRequestHeader("Content-type", "application/x-www-form-urlencoded");


  ajax.send(
      "create=1&idSeccion=" + 
      seccion +
      "&idLote=" + 
      lote +
      "&colaboradorEncargado=" +
      colaboradorEncargado +
      "&dosis=" +
      dosis +
      "&fechaFumigacion=" +
      fechaFumigacion +
      "&idQuimico=" +
      quimico 
  );
}

function actualizar() {
  /*var seccion = document.getElementById("nSeccion").value;
  var lote = document.getElementById("nLote").value;*/
  var fumigacion = document.getElementById("nFumigacion").value;
  var colaboradorEncargado = document.getElementById("nColaboradorEncargado").value;
  var dosis = document.getElementById("nDosis").value;
  var fechaFumigacion = document.getElementById("nFechaFumigacion").value;
  var quimico = document.getElementById("nQuimico").value;
  var ajax = new nuevoAjax();
  ajax.open("POST", "../business/FumigacionAction.php", true);
  ajax.onreadystatechange = function() {
    $('#modalModificar').modal('hide'); 
    if (ajax.readyState === 4) {
      if (ajax.responseText == "1") {
        operacionExitosa();
      } else {
        operacionFallida();
      }
      desplegarDatos();
    }
  };
  ajax.setRequestHeader("Content-type", "application/x-www-form-urlencoded");


  ajax.send(
      "update=1&colaboradorEncargado=" +
      colaboradorEncargado +
      "&dosis=" +
      dosis +
      "&fechaFumigacion=" +
      fechaFumigacion +
      "&idQuimico=" +
      quimico +
      "&idFumigacion=" + 
      fumigacion
  );
}

function eliminar(idFumigacion) {
  var ajax = new nuevoAjax();
  ajax.open("POST","../business/FumigacionAction.php",true);
  ajax.onreadystatechange=function(){
    $('#confirmar').modal('hide'); 
      if(ajax.readyState===4){
          if(ajax.responseText == "1"){
              operacionExitosa();
          }else{
              operacionFallida();
          }
          cerrarConfirmar();
          desplegarDatos();
      }
  }
  ajax.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  ajax.send("delete=1&idFumigacion="+idFumigacion);
}


///////////////////////////// CONFIRMACIONES ////////////////////////////////////


function confirmarEliminar(parametro){
  var confirm = document.getElementById("confirm");
  var cancel = document.getElementById("cancel");
  //document.getElementById("legend").innerHTML = "Desea eliminar este registro?";
  confirm.addEventListener("click",function(){eliminar(parametro)});
  cancel.addEventListener("click",cancelar);
}

function levantarEliminar(parametro){
  $('#confirmar').modal('show'); 
  //document.getElementById("confirmar").style.display = "block";
  confirmarEliminar(parametro);
}

function cerrarConfirmar() {
  $('#confirmar').modal('hide'); 
  //document.getElementById("confirmar").style.display = "none";
}

function cancelar() {
  document.getElementById("confirmar").innerHTML = document.getElementById(
    "confirmar"
  ).innerHTML;
  cerrarConfirmar();
}

function levantarActualizar(parametro) {
    
  //var row = parametro.parentNode.parentNode;
  //document.getElementById("form-actualizar").style.display = "block";
  document.getElementById("nFumigacion").value = 
  $(parametro).parents("tr").find("td").eq(0).html();
  document.getElementById("nSeccion").value = 
  $(parametro).parents("tr").find("td").eq(1).html();
  document.getElementById("nLote").value = 
  $(parametro).parents("tr").find("td").eq(2).html();
  document.getElementById("nQuimico").value =
  $(parametro).parents("tr").find("td").eq(3).html();
  document.getElementById("nDosis").value =
  $(parametro).parents("tr").find("td").eq(5).html();
  document.getElementById("nFechaFumigacion").value =
  $(parametro).parents("tr").find("td").eq(6).html();
  document.getElementById("nColaboradorEncargado").value =
  $(parametro).parents("tr").find("td").eq(7).html();
  $('#modalModificar').modal('show'); 
}
/* Levantar modal de agregar fumigacion */

function mostrarModalAgregar(){
  limpiar();
  administrarBtnInsertar(true);
  $('#modalAgregar').modal('show'); 
  $('#modalAgregar').on('shown.bs.modal', function () {
    $('#dosis').trigger('focus');
  });
}
/*--------------VALIDACIONES FUMIGACIÓN---------------*/

function validarAgregarFumigacion(e) {
  var inputName = e.name;
  var valor = e.value;

  if (inputName === 'dosis') { 
    let tooltip = document.getElementById("tooltip-dosis"); 

    if(valor.length == 0) 
        asignarClaseError(e, tooltip, "Por favor completar este campo.");
    else if (validarNumero(valor)) 
        asignarClaseExito(e, tooltip);
    else 
        asignarClaseError(e, tooltip,"Solamente son permitidos números mayores a 0.");

    }else if(inputName === 'colaboradorEncargado') {
        let tooltip = document.getElementById("tooltip-colaboradorEncargado");  
        if(valor.length == 0) 
            asignarClaseError(e, tooltip, "Por favor completar este campo.");
        else if (validarNumero(valor)) 
            asignarClaseExito(e, tooltip);
        else 
            asignarClaseError(e, tooltip, "Debe seleccionar un colaborador.");

    }else if(inputName === 'quimico') {
      let tooltip = document.getElementById("tooltip-quimico");  
      if(valor.length == 0) 
          asignarClaseError(e, tooltip, "Por favor completar este campo.");
      else if (validarNumero(valor)) 
          asignarClaseExito(e, tooltip);
      else 
          asignarClaseError(e, tooltip, "Debe seleccionar un químico.");
          
    }else if(inputName === 'fechaFumigacion') {
        let tooltip = document.getElementById("tooltip-fechaFumigacion");  
        if (validarFecha(valor))
                asignarClaseExito(e, tooltip); 
        else
            asignarClaseError(e, tooltip, "Solamente son permitidas las fechas con el año igual o un año mayor al año actual.");
    }
  gestorBtnAgregar();
}  


/* asignacion  de clases para los errores en formularios */

function asignarClaseError(input, tooltip, textoValidacion) {
  input.className = "input-error";
  tooltip.setAttribute("data-tooltip", textoValidacion);
  tooltip.className += " active";
}

/* Asignacion de la clase normal(sin errores) */

function asignarClaseExito(input, tooltip) {
  input.className = "";
  tooltip.className = "tooltip-text";
}


function validarActualizarFumigacion(e) {
  var inputName = e.name;
  var valor = e.value;


  if (inputName === 'nDosis') { 
    let tooltip = document.getElementById("tooltip-nDosis"); 

    if(valor.length == 0) 
        asignarClaseError(e, tooltip, "Por favor completar este campo.");
    else if (validarNumero(valor)) 
        asignarClaseExito(e, tooltip);
    else 
        asignarClaseError(e, tooltip,"Solamente son permitidos números mayores a 0.");

    }else if(inputName === 'nColaboradorEncargado') {
        let tooltip = document.getElementById("tooltip-nColaboradorEncargado");  
        if(valor.length == 0) 
            asignarClaseError(e, tooltip, "Por favor completar este campo.");
        else if (validarNumero(valor)) 
            asignarClaseExito(e, tooltip);
        else 
            asignarClaseError(e, tooltip, "Debe seleccionar un colaborador.");

    }else if(inputName === 'nQuimico') {
      let tooltip = document.getElementById("tooltip-nQuimico");  
      if(valor.length == 0) 
          asignarClaseError(e, tooltip, "Por favor completar este campo.");
      else if (validarNumero(valor)) 
          asignarClaseExito(e, tooltip);
      else 
          asignarClaseError(e, tooltip, "Debe seleccionar un químico.");
          
    }else if(inputName === 'nFechaFumigacion') {
        let tooltip = document.getElementById("tooltip-nFechaFumigacion");  
        if (validarFecha(valor))
                asignarClaseExito(e, tooltip); 
        else
            asignarClaseError(e, tooltip, "Solamente son permitidas las fechas con el año igual o un año mayor al año actual.");
    }
  gestorBtnActualizar();
}

/** Apagar o encender el btn de agregar  */

function gestorBtnAgregar() {
  var dosis = document.getElementById("dosis").value;
  var fechaFumigacion = document.getElementById("fechaFumigacion").value;
  var colaboradorEncargado = document.getElementById("colaboradorEncargado").value;
  var quimico = document.getElementById("quimico").value;
  var bandera = true;
  /** 1 - 0 -> 0
   *  1 - 1 -> 1
   *  0 - 1 -> 0
   *  0 - 0 -> 0
   */
  bandera = bandera && (validarNumero(dosis) && !validarCampoVacio(dosis));
  bandera = bandera && (!validarCampoVacio(fechaFumigacion) && validarFecha(fechaFumigacion));
  bandera = bandera && (validarNumero(colaboradorEncargado) && !validarCampoVacio(colaboradorEncargado));
  bandera = bandera && (validarNumero(quimico) && !validarCampoVacio(quimico));
  administrarBtnInsertar(!bandera);
}

function gestorBtnActualizar() {
  var dosis = document.getElementById("nDosis").value;
  var fechaFumigacion = document.getElementById("nFechaFumigacion").value;
  var colaboradorEncargado = document.getElementById("nColaboradorEncargado").value;
  var quimico = document.getElementById("nQuimico").value;
  var bandera = true;
  /** 1 - 0 -> 0
   *  1 - 1 -> 1
   *  0 - 1 -> 0
   *  0 - 0 -> 0
   */
  bandera = bandera && (validarNumero(dosis) && !validarCampoVacio(dosis));
  //console.error(bandera);
  bandera = bandera && !validarCampoVacio(fechaFumigacion) && validarFecha(fechaFumigacion);
  bandera = bandera && (validarNumero(colaboradorEncargado) && !validarCampoVacio(colaboradorEncargado));
  bandera = bandera && (validarNumero(quimico) && !validarCampoVacio(quimico));
  administrarBtnActualizar(!bandera);
  //console.log(bandera);
}

function verMapa(){
  location.href = "../view/mapeoView.php";
}


///////////////////////MANUAL DE USUARIO////////////////////////////////////


function manualAgregar() {
  var enjoyhint_instance = new EnjoyHint({});
  var enjoyhint_script_steps = [
    {
      'click #agregar': '¡Bienvenido al sistema de ayuda! presiona el botón + para agregar una nueva fumigación.',
      'skipButton': { className: "mySkip", text: "Saltar" }
    },
    {
      'timeout': 500,
      'type #dosis': 'Ingrese la dosis de la fumicación usando caracteres numéricos.',
      'skipButton': { className: "mySkip", text: "Saltar" },
      'nextButton': { className: "mySkip", text: "Siguiente" },
      'showNext': true
    },
    {
      'select #fechaFumigacion': 'Seleccione la fecha de la fumigación en el calendario.',
      'skipButton': { className: "mySkip", text: "Saltar" },
      'nextButton': { className: "mySkip", text: "Siguiente" },
      'showNext': true
    },
    {
      'type #quimico': 'Seleccione el químico de la fumigación de la lista.',
      'skipButton': { className: "mySkip", text: "Saltar" },
      'nextButton': { className: "mySkip", text: "Siguiente" },
      'showNext': true
    },
    {
      'type #colaboradorEncargado': 'Seleccione el colaborador encargado de la fumigación de la lista.',
      'skipButton': { className: "mySkip", text: "Saltar" },
      'nextButton': { className: "mySkip", text: "Siguiente" },
      'showNext': true
    },
    {
      'click #insert': 'Presione el botón Agregar para guardar la fumigación.',
      'skipButton': { className: "mySkip", text: "Saltar" }
    }

  ];
  enjoyhint_instance.set(enjoyhint_script_steps);
  enjoyhint_instance.run();
}


function manualActualizar() {
  var enjoyhint_instance = new EnjoyHint({});
  var enjoyhint_script_steps = [
    {
      'click #Actualizar': '¡Bienvenido al sistema de ayuda! presiona el botón lápiz para editar una fumigación.',
      'skipButton': { className: "mySkip", text: "Saltar" }
    },
    {
      'timeout': 500,
      'type #nDosis': 'Edite la dosis de la fumicación usando caracteres numéricos.',
      'skipButton': { className: "mySkip", text: "Saltar" },
      'nextButton': { className: "mySkip", text: "Siguiente" },
      'showNext': true
    },
    {
      'select #nFechaFumigacion': 'Seleccione la fecha de la fumigación en el calendario.',
      'skipButton': { className: "mySkip", text: "Saltar" },
      'nextButton': { className: "mySkip", text: "Siguiente" },
      'showNext': true
    },
    {
      'type #nQuimico': 'Seleccione el químico de la fumigación de la lista.',
      'skipButton': { className: "mySkip", text: "Saltar" },
      'nextButton': { className: "mySkip", text: "Siguiente" },
      'showNext': true
    },
    {
      'type #nColaboradorEncargado': 'Seleccione el colaborador encargado de la fumigación de la lista.',
      'skipButton': { className: "mySkip", text: "Saltar" },
      'nextButton': { className: "mySkip", text: "Siguiente" },
      'showNext': true
    },
    {
      'click #update': 'Presione el botón Confirmar para guardar los cambios.',
      'skipButton': { className: "mySkip", text: "Saltar" }
    }

  ];
  enjoyhint_instance.set(enjoyhint_script_steps);
  enjoyhint_instance.run();
}


function manualEliminar() {
  var enjoyhint_instance = new EnjoyHint({});
  var enjoyhint_script_steps = [
    {
      'click #Eliminar': '!Bienvenido al sistema de ayuda¡ presiona el botón basurero para eliminar una fumigación.',
      'skipButton': { className: "mySkip", text: "Saltar" }
    },
    {
      'timeout': 500,
      'click #confirm': 'Presione el botón Confirmar para eliminar la fumigación seleccionada.',
      'skipButton': { className: "mySkip", text: "Saltar" }
    }

  ];
  enjoyhint_instance.set(enjoyhint_script_steps);
  enjoyhint_instance.run();
}


