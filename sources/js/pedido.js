$(function () {
	$('#myTab a[href="#addTab"]').on('click', function () { // Click event on the "Add Tab" button
		var nbrLiElem = ($('ul#myTab li').length) - 1; // Count how many <li> there are (minus 1 because one <li> is the "Add Tab" button)
		
		// Add a <li></li> line before the last-child
		// Including the complete structure: the li ID, the <a href=""></a> etc... check the Bootstrap togglable tabs structure
		$('ul#myTab li:last-child').before('<li id="li' + (nbrLiElem + 1) + '"><a class="nav-link" href="#tab' + (nbrLiElem + 1) + '" role="tab" data-toggle="tab"><i class="fa fa-user mr-1"></i> ' + (nbrLiElem + 1) + ' <button type="button" class="btn btn-delete-pedido" onclick="removeTab(' + (nbrLiElem + 1) + ');"><i class="fas fa-times-circle"></i></button></a>');
		
		// Add a <div></div> markup after the last-child of the <div class="tab-content">
		//$('div#tab-content div:last-child').after('<div class="tab-pane fade" id="tab' + (nbrLiElem + 1) + '"></div>');        
        addPanelTab((nbrLiElem+1));
        nbrLiElem = nbrLiElem + 1; // 1 more element in the tab system
        validarInsert();
    });
    
    $('#nmyTab a[href="#addTab"]').on('click', function () { // Click event on the "Add Tab" button
		var nbrLiElem = ($('ul#nmyTab li').length) - 1; // Count how many <li> there are (minus 1 because one <li> is the "Add Tab" button)
		if(($('ul#nmyTab > li').not('#nlast').length) === 1){
            $('#nmyTab a[href="#ntab1"]').append('<button type="button" class="btn btn-delete-pedido" onclick="removeTabModificar(1);"><i class="fas fa-times-circle"></i></button>'); 
        }
		
		$('ul#nmyTab li:last-child').before('<li id="nli' + (nbrLiElem + 1) + '"><a class="nav-link modificar" href="#ntab' + (nbrLiElem + 1) + '" role="tab" data-toggle="tab"><i class="fa fa-user mr-1"></i> ' + (nbrLiElem + 1) + ' <button type="button" class="btn btn-delete-pedido" onclick="removeTabModificar(' + (nbrLiElem + 1) + ');"><i class="fas fa-times-circle"></i></button></a>');
        addPanelTabModificar((nbrLiElem+1), true);
        nbrLiElem = nbrLiElem + 1; // 1 more element in the tab system
        //validarInsert();
	});
});
 
function removeTab(liElem) { // Function remove tab with the <li> number
	//if (confirm("Are you sure?")) { // Display "Are you sure message" and wait for you to press "Ok"
		$('ul#myTab > li#li' + liElem).fadeOut(1000, function () { 
			$(this).remove(); // Remove the <li></li> with a fadeout effect
			$('#messagesAlert').text(''); // Empty the <div id="messagesAlert"></div>
		});
		
		$('div.tab-content div#tab' + liElem).remove(); // Also remove the correct <div> inside <div class="tab-content">
        quitClient(liElem);	
		$('ul#myTab > li').not('#last').not('#li' + liElem).each(function(i){ // Select all <li> from <ul id="myTab"> except the last (with is the "Add Tab" button) and without the one we deleted
			var getAttr = $(this).attr('id').split('li'); // We get the <li> div attribute
			$('ul#myTab li#li' + getAttr[1]).attr('id', 'li' + (i + 1)); // We change the div attribute of all <li>: the first is 1, then 2, then 3...
			
            var tabContent = '<i class="fa fa-user mr-1"></i>' + (i + 1); // 
            //<button type="button" class="btn btn-warning btn-xs" onclick="removeTab(' + (i + 1) + ');"><span class="glyphicon glyphicon-remove"></span></button>';
			if (getAttr[1] != 1) tabContent += '<button type="button" class="btn btn-delete-pedido" onclick="removeTab(' + (i + 1) + ');"><i class="fas fa-times-circle"></i></button>';
			$('#myTab a[href="#tab' + getAttr[1] + '"]').html(tabContent) // tabContent variable, inside the <li>. We change the number also, 1, then 2, then3...
														.attr('href', '#tab' + (i + 1)); // Same for the href attribute
			
			$('div.tab-content div#tab' + getAttr[1]).attr('id', 'tab' + (i + 1)); // Same for the id attribute
            // Quitamos el pedido cliente del json
            reasignarEventoAgregar();
        });
        validarClientes(true);
        validarInsert();
        $('#li1').find('a').click();
	//}
	return false;
}

// La validaciones y botones tienen un parametro que es el id del tab, entonces cuando elimino hay que cambiar ese numero.
function reasignarEventoAgregar () {
    $('.cliente').each(function(i){
        $(this).attr('onchange', 'changeClient('+(i+1)+')');
    });

    $('.cantidad').each(function(j){
        $(this).attr('onkeyup', 'validarAddPlant('+(j+1)+')');
        $(this).parents('.add-container').find('button').eq(0).attr('onclick', 'addPlant('+(j+1)+')');
    });
}

// La validaciones y botones tienen un parametro que es el id del tab, entonces cuando elimino hay que cambiar ese numero.
function reasignarEventoModificar () {
    $('.nCliente').each(function(i){
        $(this).attr('onchange', 'changeClientUpdate('+(i+1)+')');
    });

    $('.nCantidad').each(function(j){
        $(this).attr('onkeyup', 'validarAddPlantN('+(j+1)+')');
        $(this).parents('.add-container').find('button').eq(0).attr('onclick', 'addPlantUpdate('+(j+1)+')');
    });
}

/* Quitar los tabs del modal de modificar  */
function removeTabModificar(liElem) { 
    $('ul#nmyTab > li#nli' + liElem).fadeOut(1000, function () { 
        $(this).remove(); // Remove the <li></li> with a fadeout effect
        $('#messagesAlert').text(''); // Empty the <div id="messagesAlert"></div>
    });

    $('div.tab-content div#ntab' + liElem).remove(); // Also remove the correct <div> inside <div class="tab-content">
    quitClient(liElem);	
    let cantidadPedidos = $('ul#nmyTab > li').not('#nlast').not('#nli' + liElem).length;
    $('ul#nmyTab > li').not('#nlast').not('#nli' + liElem).each(function(i){ // Select all <li> from <ul id="myTab"> except the last (with is the "Add Tab" button) and without the one we deleted
        
        var getAttr = $(this).attr('id').split('nli'); // We get the <li> div attribute
        $('ul#nmyTab li#nli' + getAttr[1]).attr('id', 'nli' + (i + 1)); // We change the div attribute of all <li>: the first is 1, then 2, then 3...
        var tabContent = '<i class="fa fa-user mr-1"></i>' + (i + 1); // 
        if(cantidadPedidos != 1) tabContent += '<button type="button" class="btn btn-delete-pedido" onclick="removeTabModificar(' + (i + 1) + ');"><i class="fas fa-times-circle"></i></button>';
        $('#nmyTab a[href="#ntab' + getAttr[1] + '"]').html(tabContent) // tabContent variable, inside the <li>. We change the number also, 1, then 2, then3...
                                                    .attr('href', '#ntab' + (i + 1)); // Same for the href attribute
        
        $('div.tab-content div#ntab' + getAttr[1]).attr('id', 'ntab' + (i + 1)); // Same for the id attribute
        // Quitamos el pedido cliente del json
        reasignarEventoModificar();
        validarUpdate();
    });
    
    $('#nli1').find('a').click();
//}
return false;
}



function addPanelTab(tabRef) {
    /** Para hacer un panel necesito la referencia del tab que cree 
     * Por eso, creo un nuevo tabPanel+tabCreado
     * y así aprovechamos la utilidad del boostrap y jquery para mostrar los paneles. */
    $('#tab-content').append("<div class='tab-pane fade' id='tab" +tabRef+ "' role='tabpanel'>");
    //document.getElementById("tab-content").innerHTML += "<div class='tab-pane fade' id='tab" +tabRef+ "' role='tabpanel'>";
    setPanel('tab'+tabRef);
}

function mostrarModalAgregar(){
    //limpiar();
    //administrarBtnInsertar(true);
    cerrarModalPedido();
    cerrarModalModificar();
    localStorage.clear();
    localStorage.removeItem('pedido');
    localStorage.setItem("pedido", JSON.stringify(pedido));
    console.log(pedido);
    setPanel("tab1");
    $('#modalLRForm').modal('show'); 
    $('#modalLRForm').on('shown.bs.modal', function () {
//      $('#nombreComun').trigger('focus');
        //levantarForm();
        //validarInsert();
    });
}





/** Estos son metodos del pedido antiguo */

// limpia localStorage al cargar la pagina.
window.onload = function () {
    localStorage.clear();
};

// OBJETOS JS
var bandera = false;
var planta = {
    idPedidoClientePlanta: 0,
    idPlanta: 0,
    idClientePedido: 0,
    nombrePlanta: "",
    cantidad: 0,
};
var cliente = {
    idPedidoCliente: 0,
    idPedido: 0,
    idCliente: 0,
    pedidoClientePlanta: []
};

var pedido = {
    id: 0,
    fechaPedido: "",
    pedidoCliente: []
};


function reset() {
    pedido.id = 0;
    pedido.fechaPedido = "";
    pedido.pedidoCliente = [];  
}

function desplegarDatos() {
    var capa = document.getElementById("tab");
    var buscar = document.getElementById("buscar").value;
    var ajax = new nuevoAjax();
    //capa.innerHTML = loading;
    ajax.open("POST", "../business/PedidoController.php", true);
    ajax.onreadystatechange = function () {
        if (ajax.readyState === 4) {
            capa.innerHTML = ajax.responseText;
            $("#tabla_pedidos").DataTable({
                paging: true,
                lengthChange: false,
                searching: true,
                ordering: true,
                info: false,
                autoWidth: true,
                lengthMenu: [[5]],
                language: idioma,
                columnDefs: [{
                    "targets": 'no-sort',
                    "orderable": false,
                }]
            });
            $(".tool").tooltip({
                tooltipClass: "mytooltip",
            });
            closeTool();
            pedido.id = 0;
            pedido.fechaPedido = "";
            pedido.pedidoCliente = [];
        }
    };
    ajax.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    ajax.send("buscar=" + buscar);
}


// carga un panel con un formulario respectivo.
function setPanel(panelId) {
    var tab = panelId.split('tab');
    /*console.log(tab[1]);*/
    var panel = document.getElementById(panelId);
    var ajax = nuevoAjax();
    ajax.open("POST", "../business/PedidoAction.php", true);
    ajax.onreadystatechange = function () {
        if (ajax.readyState === 4) {
            panel.innerHTML = ajax.responseText;
            addClient(tab[1], true);
            validarAddPlant(tab[1]);
            validarClientes(true);
            asignarFechaNewPanel(tab[1]-1, true);
            validarInsert();
        }
    };
    ajax.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    ajax.send("form=" + 1 + "&tab=" + tab[1]);
}


function asignarFechaNewPanel(i, insert) {
    if (insert) {
        var fecha = document.getElementsByClassName('fecha-pedidos')[0].value;
        document.getElementsByClassName('fecha-pedidos')[i].value = fecha;
    } else {
        var fecha = document.getElementsByClassName('nFecha')[0].value;
    document.getElementsByClassName('nFecha')[i].value = fecha;
    }
    
}

// Agrega el cliente al pedido
function addClient(i, insert) {
    if (insert) 
        var idCliente = document.getElementsByClassName("cliente")[i-1].value;   
     else 
        var idCliente = document.getElementsByClassName("nCliente")[i-1].value;
    
    var pedido = localStorage.getItem("pedido");
    pedido = JSON.parse(pedido);
    var nCliente = cliente;
    nCliente.idCliente = idCliente;
    nCliente.idPedido = pedido.id;
    pedido.pedidoCliente.push(nCliente);
    localStorage.setItem("pedido", JSON.stringify(pedido));
//    console.log(pedido);
}

function changeClient(i) {
    var idCliente = document.getElementsByClassName("cliente")[i-1].value;
    var pedido = JSON.parse(localStorage.getItem("pedido"));
    pedido.pedidoCliente[i-1].idCliente = idCliente;
    localStorage.setItem("pedido", JSON.stringify(pedido));
    //console.log(pedido);
    validarClientes(true);
    validarInsert();
}

// Cuando elimino un cliente 
function quitClient(i) {
    var pedido = localStorage.getItem("pedido");
    pedido = JSON.parse(pedido);
    var arrayC = [];
    for (let j = 0; j < pedido.pedidoCliente.length; j++) {
        if (j != i-1) {
            arrayC.push(pedido.pedidoCliente[j]);
        }
    }
    pedido.pedidoCliente = arrayC;
    localStorage.setItem("pedido", JSON.stringify(pedido));
    console.log(pedido);
}


/* Pinta la tabla */
function pintar(i) {
    array = JSON.parse(localStorage.getItem("pedido")).pedidoCliente[i].pedidoClientePlanta;
    var tbody = document.getElementsByClassName("tbody")[i];
    tbody.innerHTML = "";
    for (let j = 0; j < array.length; j++) {
        tbody.innerHTML += "<tr><td>" + array[j].nombrePlanta + "</td><td>" + array[j].cantidad + "</td><td><button class='boton boton-secundario tool' onclick='quitPlant(" + array[j].idPlanta + "," + i + ",true)'><i class='fa fa-minus' aria-hidden='true'></i></button></td><tr>";
    }
}

function arrayPop(id, i) {
    var pedido = localStorage.getItem("pedido");
    var pedido = JSON.parse(pedido);
    var arrayTemp = [];
    var pedidoClientePlanta = pedido.pedidoCliente[i].pedidoClientePlanta;
    for (let j = 0; j < pedidoClientePlanta.length; j++) {
        if (pedidoClientePlanta[j].idPlanta != id) {
            arrayTemp.push(pedidoClientePlanta[j]);
        }
    }
    pedido.pedidoCliente[i].pedidoClientePlanta = arrayTemp;
    localStorage.setItem("pedido", JSON.stringify(pedido));
}

// push y pop para plantas.
function arrayPush(array, objeto, i, insert) {
    var arrayp = array.pedidoCliente[i].pedidoClientePlanta;
    var tooltip = document.getElementById("tooltip-planta");
    var flag = true;
    for (let j = 0; j < arrayp.length; j++) {
        if (arrayp[j].idPlanta == objeto.idPlanta) {
            flag = false;
        }
    }
    if (flag) {
        arrayp.push(objeto);
        array.pedidoCliente[i].pedidoClientePlanta = arrayp;
        tooltip.className = "tooltip-text";
    } else {
        if (insert) { // Si es un insert
            var tooltip = $(`#tab${i+1}`).find("#tooltip-planta");
        } else { // entonces es un update
            var tooltip = $(`#ntab${i+1}`).find("#tooltip-planta");
        }
        // Acá mostramos el tooltip por un par de segundos.
       
        tooltip.attr("data-tooltip", "La planta ya ha sido ingresada con este cliente.");
        tooltip.addClass(" active");
        // Desaparecemos el tooltip
        setTimeout(function () {
            tooltip.removeClass(" active");
        }, 3000);
    }
    localStorage.setItem("pedido", JSON.stringify(array));
}

function addPlant(i) {
    var idPlanta = document.getElementsByClassName("idPlanta")[i-1];
    var cantidad = document.getElementsByClassName("cantidad")[i-1];
    var pedido = localStorage.getItem("pedido");
    pedido = JSON.parse(pedido);
    planta.idPlanta = idPlanta.value;
    planta.nombrePlanta = idPlanta.options[idPlanta.selectedIndex].text;
    planta.cantidad = cantidad.value;
    arrayPush(pedido, planta, i-1, true);
    cantidad.value = "";
    pintar(i-1);
    validarInsert();
    validarAddPlant(i);
}

/* Quita una planta (-) */
function quitPlant(id, i, flag) {
    arrayPop(id, i);
    if (flag) {
        validarInsert();
        pintar(i);
    } else {
        validarUpdate();
        addPlantToTable(i);
    }
}

function validarAddPlant(i) {
    var cantidad = document.getElementsByClassName("cantidad")[i-1];
    var flag;
    if (validarNumero(cantidad.value) && cantidad.value > 0) {
        cantidad.style.borderBottom = "1px solid green";
        flag = true;
    } else {
        cantidad.style.borderBottom = "1px solid red";
        flag = false;
    }
    adminBtnAdd(!flag, i-1);
}

function insertar() {
    var pedido = localStorage.getItem("pedido");
    //var fechaPedido = document.getElementById("fechaPedido").value;
    var fechaPedido = document.getElementsByClassName('fecha-pedidos')[0].value;
    pedido = JSON.parse(pedido);
    pedido.fechaPedido = fechaPedido;
    localStorage.setItem("pedido", JSON.stringify(pedido));
    var pedido = localStorage.getItem("pedido");
    var ajax = new nuevoAjax();
    ajax.open("POST", "../business/PedidoAction.php", true);
    ajax.onreadystatechange = function () {
        $('#modalLRForm').modal('hide'); 
        if (ajax.readyState === 4) {
            if (ajax.responseText == 0) {
                operacionFallida();
            } else {
                operacionExitosa();
            }
            localStorage.clear();
            desplegarDatos();
        }
    }
    ajax.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    ajax.send("create=1&pedido=" + pedido);
}

/* Administra el boton + */
function adminBtnAdd(flag, i) {
    var boton = document.getElementsByClassName("add")[i];
    var color = "#007bff";
    if (flag) {
        color = "gray";
    }
    boton.disabled = flag;
    boton.style.backgroundColor = color;
}


/* Asignacion de la clase de error */
function asignarClaseError(input, tooltip, textoValidacion) {
    // if (input != null) {input.className = "input-error";}
    tooltip.setAttribute("data-tooltip", textoValidacion);
    tooltip.className += " tooltip-active";
}

/* Asignacion de la clase normal(sin errores) */

function asignarClaseExito(input, tooltip) {
    input.className = "";
    tooltip.className = "tooltip";
}  

function changeFechaPedido(fecha) {
    //console.log(fecha.value);
    var fechas = document.getElementsByClassName('fecha-pedidos');
    for (let i = 0; i < fechas.length; i++) {
        fechas[i].value = fecha.value;
    }
    validarInsert();
}

function validarInsert() {
    var fechas = document.getElementsByClassName('fecha-pedidos');
    var bandera = true;
    bandera = bandera && !validarCampoVacio(fechas[0].value);
    bandera = bandera && validarClientes(true);
    bandera = bandera && validarPedido();
    botonAgregarToggle(!bandera);
}

function validarClientes(insert) {
    var pedido = localStorage.getItem("pedido");
    pedido = JSON.parse(pedido);
    var pedidoCliente = pedido.pedidoCliente;
    var cont = 0;
    var bandera = true;
    for (let i = 0; i < pedidoCliente.length; i++) {
        for (let j = 0; j < pedidoCliente.length; j++) {
            if (pedidoCliente[i].idCliente == pedidoCliente[j].idCliente) {
                cont++;
                if (cont > 1) { // Ya debemos asignar los tooltips
                    if (insert) { // Si es un insert
                        var tooltip = $(`#tab${i+1}`).find("#tooltip-cliente");    
                    } else { // Es un update
                        var tooltip = $(`#ntab${i+1}`).find("#tooltip-cliente");
                    }
                    
                    tooltip.attr("data-tooltip", "El cliente ya está dentro de este pedido.");
                    tooltip.addClass(" active");
                    bandera = false;
                } else {
                    if (insert) { // Si es un insert
                        var tooltip = $(`#tab${i+1}`).find("#tooltip-cliente");    
                    } else { // Es un update
                        var tooltip = $(`#ntab${i+1}`).find("#tooltip-cliente");
                    }
                    tooltip.removeClass(" active");
                }
            }
        }
        cont = 0;
    }
    return bandera;
}

// valida que el cliente tenga plantas.
function validarPedido() {
    var pedido = JSON.parse(localStorage.getItem("pedido"));
    for (let i = 0; i < pedido.pedidoCliente.length; i++) {
        if (pedido.pedidoCliente[i].pedidoClientePlanta.length == 0) {
            return false;
        }
    }
    return true;
}


function botonAgregarToggle(bandera){ //pasar true para bloquear, false para desbloquear.
    var btn = document.getElementById('insertar');
    var color = "#28a745";
    if(bandera){
        color = "gray";
       
    }
    btn.disabled = bandera;
    btn.style.background = color;
}

/** Metodos para eliminar un pedido */

function levantarEliminar(parametro) {
   // desactivarEliminarBtn();
    $('#confirmar').modal('show');
    //document.getElementById("confirmar").style.display = "block";
    confirmarEliminar(parametro);
}


function confirmarEliminar(parametro) {
    var confirm = document.getElementById("confirm");
    var cancel = document.getElementById("cancel");
    confirm.addEventListener("click", function () {
        eliminar(parametro);
    });
    cancel.addEventListener("click", cancelar);
}


function eliminar(idPedido) {
    var ajax = new nuevoAjax();
    ajax.open("POST", "../business/PedidoAction.php", true);
    ajax.onreadystatechange = function () {
        if (ajax.readyState === 4) {
            console.log(ajax.responseText);
            if (ajax.responseText == 1) {
                operacionExitosa();
            } else {
                operacionFallida();
            }
            cerrarConfirmar();
            desplegarDatos();
        }
    };
    ajax.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    ajax.send("delete=1&idPedido=" + idPedido);
}

function cerrarConfirmar() {
    //document.getElementById("confirmar").style.display = "none";
    //activarEliminarBtn();
    $('#confirmar').modal('hide');
}

function cancelar() {
    document.getElementById("confirmar").innerHTML = document.getElementById("confirmar").innerHTML;
    cerrarConfirmar();
}


/* Metodo para actualizar el pedido */

function cerrarModalPedido() {
    reset();
    /* Primero necesito recorrer los tabs y quitarlos, excepto el 1 */
    // El primero no tiene el mismo id, por lo tanto no es necesario eliminarlo.
    var a = document.getElementsByClassName("nav-link");
    // Utilizamos un for inverso porque es como eliminar de una lista, si elimino el primero nodo incorrectamente
    // pierdo referencia del otro y eos ocurria aca.
    /* entonces es mejor eliminar de atras hacia delante */
    for (let i = a.length-1; i > 0; i--) {
        $('#li'+(i+1)).remove();
        $('#tab'+(i+1)).remove();
    }
    cerrarModalModificar();
}

function cerrarModalModificar() {
    reset();
    var a = document.getElementsByClassName("modificar");
    for (let i = a.length-1; i >= 0; i--) {
        $('#nli'+(i+1)).remove();
        $('#ntab'+(i+1)).remove();
    }
}

function desactivarBotonesActualizar(boton) {
    boton.disabled = true;
}

function activarBotonesActualizar() {
    $('.btn-actualizar-table').each(function(i) {
        $(this).attr('disabled', false);
    });
}

function desactivarEliminarBtn() {
    $('#Eliminar').each(function(i) {
        $(this).attr('disabled', true);
    });
}

function activarEliminarBtn() {
    $('#Eliminar').each(function(i) {
        $(this).attr('disabled', false);
    });
}
// levanta el modal actualizar y lo carga.
function levantarActualizar(parametro) {
    desactivarBotonesActualizar(parametro);
    cerrarModalPedido();
    cerrarModalModificar();
    var idPedido = $(parametro).parents("tr").find("td").eq(0).html();
    var fechaPedido = $(parametro).parents("tr").find("td").eq(1).html();
   // document.getElementsByClassName("tabPanel")[3].innerHTML = ring;
    var ajax = new nuevoAjax();
    ajax.open("POST", "../business/PedidoAction.php", true);
    ajax.onreadystatechange = function () {
        if (ajax.readyState === 4) {
            var JsonData = (JSON.parse(ajax.responseText));
            pedido.id = idPedido;
            pedido.fechaPedido = fechaPedido;
            pedido.pedidoCliente = JsonData;
            localStorage.setItem("pedido", JSON.stringify(pedido));
            llenarFormModificar();
        }
    };
    ajax.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    ajax.send("getDataUp=1&idpedido=" + idPedido);
}

function showLi() {
 var pedido = JSON.parse(localStorage.getItem("pedido"));

    for(let i = 0; i<pedido.pedidoCliente.length; i++) {
        // Agregamos el LI de modificar -> [1] [2] [3]
        addLiModificar();
    }
}

function llenarFormModificar() {
   //console.log('Cargando datos...');
    let promise  = new Promise((resolve, reject) => {
        setTimeout(() => {
            showLi();
            resolve(1);
        }, 250);
    });

    //console.log('Datos cargados...');
    promise.then((success)=>{
        if (success) {
            
            $(".loader").attr('style', 'opacity:.8');
            $('#modalModificar').modal('show');
            $('#modalModificar').on('shown.bs.modal', function () {
                setTimeout(() => {
                    cargarModalModificar();
                    pintarTablaModificar();
                }, 500);
                $(".loader").fadeOut(1000);
                activarBotonesActualizar();
            });
            $('#nli1 a').click();
        }
    });    
   
}

function cargarModalModificar () {
    /* La idea es recorrer el pedido cliente e ir creando los modales. */
    var pedido = JSON.parse(localStorage.getItem("pedido"));
    for (let i = 0; i < pedido.pedidoCliente.length; i++) {
        document.getElementsByClassName('nCliente')[i].value = pedido.pedidoCliente[i].idCliente;
        document.getElementsByClassName('nFecha')[i].value = pedido.fechaPedido;
    }
}


function setPanelModificar(panelId, nuevoRegistro) {
    var tab = panelId.split('ntab');
    //console.log(tab[1]);
    var panel = document.getElementById(panelId);
    var ajax = nuevoAjax();
    ajax.open("POST", "../business/PedidoAction.php", true);
    ajax.onreadystatechange = function () {
        if (ajax.readyState === 4) {
            panel.innerHTML = ajax.responseText;
            botonUpdateToggle(true);
            setTimeout(() => {
                if (nuevoRegistro) { // si es un registro nuevo se agrega al pedido, sino no.
                    addClient(tab[1], false);
                    asignarFechaNewPanel(tab[1]-1, false);
                }
                validarClientes(false);
                validarUpdate(); 
            }, 1000);
        }
    };
    ajax.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    ajax.send("forma=" + 1 + "&tab=" + tab[1]);
}

function addLiModificar() {
    var nbrLiElem = ($('ul#nmyTab li').length) - 1; // Count how many <li> there are (minus 1 because one <li> is the "Add Tab" button)
   //con el pedido
   var pedido = JSON.parse(localStorage.getItem("pedido"));
   if(pedido.pedidoCliente.length === 1) {
        $('ul#nmyTab li:last-child').before('<li id="nli' + (nbrLiElem + 1) + '"><a class="nav-link modificar" href="#ntab' + (nbrLiElem + 1) + '" role="tab" data-toggle="tab"><i class="fa fa-user mr-1"></i> ' + (nbrLiElem + 1) + '</a>');
   } else {
        $('ul#nmyTab li:last-child').before('<li id="nli' + (nbrLiElem + 1) + '"><a class="nav-link modificar" href="#ntab' + (nbrLiElem + 1) + '" role="tab" data-toggle="tab"><i class="fa fa-user mr-1"></i> ' + (nbrLiElem + 1) + ' <button type="button" class="btn btn-delete-pedido" onclick="removeTabModificar(' + (nbrLiElem + 1) + ');"><i class="fas fa-times-circle"></i></button></a>');
   }

    addPanelTabModificar((nbrLiElem+1), false);
    nbrLiElem = nbrLiElem + 1; // 1 more element in the tab system
//validarInsert();
}

function addPanelTabModificar(tabRef, nuevoRegistro) {
    /** Para hacer un panel necesito la referencia del tab que cree 
     * Por eso, creo un nuevo tabPanel+tabCreado
     * y así aprovechamos la utilidad del boostrap y jquery para mostrar los paneles. */
    $('#nTab-content').append("<div class='tab-pane fade' id='ntab" +tabRef+ "' role='tabpanel'>");
    //document.getElementById("tab-content").innerHTML += "<div class='tab-pane fade' id='tab" +tabRef+ "' role='tabpanel'>";
    setPanelModificar('ntab'+tabRef, nuevoRegistro);
}

function pintarTablaModificar() {
    var pedido = JSON.parse(localStorage.getItem("pedido"));
    var tbody = null;
    var array = null;
    for (let i = 0; i < pedido.pedidoCliente.length; i++) {
        tbody = document.getElementsByClassName("nTbody")[i]
        tbody.innerHTML = "";
        array =  pedido.pedidoCliente[i].pedidoClientePlanta;
        for (let j = 0; j < array.length; j++) {
            tbody.innerHTML += "<tr><td>" + array[j].nombrePlanta + "</td><td>" + array[j].cantidad + "</td><td><button class='boton boton-secundario' onclick='quitPlant(" + array[j].idPlanta + "," + i + ",false)'><i class='fa fa-minus' aria-hidden='true'></i></button></td><tr>";
        }
    }
}

/** Pinta una nueva planta en la tabla indicada */
function addPlantToTable(i) {
    array = JSON.parse(localStorage.getItem("pedido")).pedidoCliente[i].pedidoClientePlanta;
    var tbody = document.getElementsByClassName("nTbody")[i];
    tbody.innerHTML = "";
    for (let j = 0; j < array.length; j++) {
        tbody.innerHTML += "<tr><td>" + array[j].nombrePlanta + "</td><td>" + array[j].cantidad + "</td><td><button class='boton boton-secundario' onclick='quitPlant(" + array[j].idPlanta + "," + i + ",false)'><i class='fa fa-minus' aria-hidden='true'></i></button></td><tr>";
    }
}

function validarUpdate() {
    try {
        var fecha = document.getElementsByClassName('nFecha')[0];
        var bandera = true;
        bandera = bandera && !validarCampoVacio(fecha.value);
        bandera = bandera && validarClientes(false);
        bandera = bandera && validarPedido();
        botonUpdateToggle(!bandera);
    } catch(error) {
        console.log('Problemas al cargar...');
    }
}


function addPlantUpdate(i) {
    var idPlanta = document.getElementsByClassName("nIdPlanta")[i-1];
    var cantidad = document.getElementsByClassName("nCantidad")[i-1];
    var pedido = localStorage.getItem("pedido");
    pedido = JSON.parse(pedido);
    planta.idPlanta = idPlanta.value;
    planta.idClientePedido = pedido.pedidoCliente[i-1].idPedidoCliente;
    planta.nombrePlanta = idPlanta.options[idPlanta.selectedIndex].text;
    planta.cantidad = cantidad.value;
    arrayPush(pedido, planta, i-1, false);
    cantidad.value = "";
    addPlantToTable(i-1);
    validarUpdate();
    validarAddPlantN(i);
}

function validarAddPlantN(i) {
    var cantidad = document.getElementsByClassName("nCantidad")[i-1];
    var flag;
    if (validarNumero(cantidad.value) && cantidad.value > 0) {
        cantidad.style.borderBottom = "1px solid green";
        flag = true;
    } else {
        cantidad.style.borderBottom = "1px solid red";
        flag = false;
    }
    adminBtnAdd(!flag, i-1);
}

function changeFechaPedidoUpdate(fecha) {
    var fechas = document.getElementsByClassName('nFecha');
    for (let i = 0; i < fechas.length; i++) {
        fechas[i].value = fecha.value;
    }
    validarUpdate();
}

function changeClientUpdate(i){
    var idCliente = document.getElementsByClassName("nCliente")[i-1].value;
    var pedido = JSON.parse(localStorage.getItem("pedido"));
    pedido.pedidoCliente[i-1].idCliente = idCliente;
    localStorage.setItem("pedido", JSON.stringify(pedido));
    console.log(pedido);
    validarClientes(false);
    validarUpdate();
}

function botonUpdateToggle(bandera){ //pasar true para bloquear, false para desbloquear.
    var btn = document.getElementById('modificar');
    var color = "#007bff";
    if(bandera){
        color = "gray";
       
    }
    btn.disabled = bandera;
    btn.style.background = color;
}

function actualizar() {
    var pedido = localStorage.getItem("pedido");
    var fechaPedido = document.getElementsByClassName("nFecha")[0].value;
    pedido = JSON.parse(pedido);
    pedido.fechaPedido = fechaPedido;
    console.log(pedido);
    localStorage.setItem("pedido", JSON.stringify(pedido));
    var pedido = localStorage.getItem("pedido");
    
    var ajax = new nuevoAjax();
    ajax.open("POST", "../business/PedidoAction.php", true);
    ajax.onreadystatechange = function () {
        if (ajax.readyState === 4) {
            $('#modalModificar').modal('hide');
            if (ajax.responseText == 0) {
                operacionFallida();
            } else {
                operacionExitosa();
            }
            localStorage.clear();
            desplegarDatos();
        }
    }
    ajax.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    ajax.send("update=1&pedido=" + pedido);
}


/** Para el PDF del pedido **/

function cargarPDF(idPedido) {
    window.location.href = "../Reportes/reportePedidoPDF.php?idpedido="+idPedido;
}

