
//--------------------------------------------

function desplegarDatos() {
  var capa = document.getElementById("tab");
  var buscar = document.getElementById("buscar").value;
  var ajax = new nuevoAjax();
  capa.innerHTML = loading;
  ajax.open("POST", "../business/SuministroController.php", true);
  ajax.onreadystatechange = function() {
    if (ajax.readyState === 4) {
      capa.innerHTML = ajax.responseText;
      /** Cuando obtenemos la tabla inmediatamente la hacemos datatable */
      $("#tabla_suministros").DataTable({
        paging: true,
        lengthChange: false,
        searching: true,
        ordering: true,
        info: false,
        autoWidth: true,
        lengthMenu: [[5]],
        language: idioma,
        columnDefs: [ {
          "targets": 'no-sort',
          "orderable": false,
      } ]
      });
      $(".tool").tooltip({
        tooltipClass: 'mytooltip'
      });
      closeTool();
    }
  };
  ajax.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  ajax.send("buscar=" + buscar);
}

function limpiar() {
  let nombre = document.getElementById("nombre");
  let tooltipNombre = document.getElementById("tooltip-nombre");
  limpiarInput(nombre);
  asignarClaseExito(nombre, tooltipNombre);

  let cantidad = document.getElementById("cantidad");
  let tooltipCantidad = document.getElementById("tooltip-cantidad");
  limpiarInput(cantidad);
  asignarClaseExito(cantidad, tooltipCantidad);

  $('#modalAgregar').modal('hide'); 
  //document.getElementById("form").style.display = "none";
}


function limpiarActualizar() {
  let Nombre = document.getElementById("nNombre");
  let tooltipNombre = document.getElementById("tooltip-nNombre");
  limpiarInput(Nombre);
  asignarClaseExito(Nombre, tooltipNombre);

  let nCantidad = document.getElementById("nCantidad");
  let tooltipNCantidad = document.getElementById("tooltip-nCantidad");
  limpiarInput(nCantidad);
  asignarClaseExito(nCantidad, tooltipNCantidad);

  $('#modalModificar').modal('hide'); 
  //document.getElementById("form-actualizar").style.display = "none";
}

function limpiarInput(input) {
  input.value = "";
}

function insertar() {
  var nombre = document.getElementById("nombre").value;
  var cantidad = document.getElementById("cantidad").value;
  var ajax = new nuevoAjax();
  ajax.open("POST", "../business/SuministroAction.php", true);
  ajax.onreadystatechange = function() {
    $('#modalAgregar').modal('hide'); 
    if (ajax.readyState === 4) {
      if (ajax.responseText == "1") {
        operacionExitosa();
      } else {
        operacionFallida();
      }
      desplegarDatos();
    }
  };
  ajax.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  ajax.send(
    "accion=Insertar&nombre=" +
      nombre +
      "&cantidad=" +
      cantidad
  );
}
//--------------------------------------------
function eliminar(idSuministro) {
  var ajax = new nuevoAjax();
  ajax.open("POST", "../business/SuministroAction.php", true);
  ajax.onreadystatechange = function() {
    $('#confirmar').modal('hide'); 
    if (ajax.readyState === 4) {
      if (ajax.responseText == "1") {
        operacionExitosa();
      } else {
        operacionFallida();
      }
      cerrarConfirmar();
      desplegarDatos();
    }
  };
  ajax.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  ajax.send("accion=Eliminar&idSuministro=" + idSuministro);
}

function actualizar() {
  var idSuministro = document.getElementById("nIdSuministro").value;
  var nombre = document.getElementById("nNombre").value;
  var cantidad = document.getElementById("nCantidad").value;
  var ajax = nuevoAjax();
  ajax.open("POST", "../business/SuministroAction.php", true);
  ajax.onreadystatechange = function() {
    $('#modalModificar').modal('hide'); 
    if (ajax.readyState === 4) {
      if (ajax.responseText == "1") {
        operacionExitosa();
      } else {
        operacionFallida();
      }
      cerrarConfirmar();
      desplegarDatos();
    }
  };
  ajax.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  ajax.send(
    "accion=Actualizar&idSuministro=" +
      idSuministro +
      "&nombre=" +
      nombre +
      "&cantidad=" +
      cantidad
  );
}
//--------------------------------------------

///////////////////////////// CONFIRMACIONES ////////////////////////////////////




function cerrarConfirmar() {
  //document.getElementById("confirmar").style.display = "none";
  $('#confirmar').modal('hide'); 
}

function cancelar() {
  document.getElementById("confirmar").innerHTML = document.getElementById(
    "confirmar"
  ).innerHTML;
  cerrarConfirmar();
}

function confirmarActualizar(parametro) {
  var confirm = document.getElementById("confirm");
  var cancel = document.getElementById("cancel");
  document.getElementById("legend").innerHTML = "Desea confirmar los cambios?";
  confirm.addEventListener("click", function() {
    actualizar(parametro);
  });
  cancel.addEventListener("click", cancelar);
}

function levantarActualizar(parametro) {
  //document.getElementById("form-actualizar").style.display = "block";
  row = parametro.parentNode.parentNode;
  document.getElementById("nIdSuministro").value = row.childNodes[0].value;
  document.getElementById("nNombre").value =
  $(parametro).parents("tr").find("td").eq(1).html();
  document.getElementById("nCantidad").value =
  $(parametro).parents("tr").find("td").eq(2).html();
  $('#modalModificar').modal('show'); 
}

function confirmarEliminar(parametro) {
  var confirm = document.getElementById("confirm");
  var cancel = document.getElementById("cancel");
  //document.getElementById("legend").innerHTML = "Desea eliminar este registro?";
  confirm.addEventListener("click", function() {
    eliminar(parametro);
  });
  cancel.addEventListener("click", cancelar);
}

function levantarEliminar(parametro) {
  $('#confirmar').modal('show'); 
  //  document.getElementById("confirmar").style.display = "block";
  confirmarEliminar(parametro);
}


/*--------------VALIDACIONES NUEVAS DE SUMINISTRO---------------*/

function validarAgregarSuministro(e) {
  var inputName = e.name;
  var valor = e.value;

  if (inputName === 'nombre') { 
    let tooltip = document.getElementById("tooltip-nombre"); 
    if(valor.length == 0) 
        asignarClaseError(e, tooltip, "Por favor completar este campo.");
    else if (validarNombre(valor)) 
        asignarClaseExito(e, tooltip);
    else 
        asignarClaseError(e, tooltip,"Solamente son permitidos los carácteres de [A-Z/a-z]");

    }else if(inputName === 'cantidad') {
      let tooltip = document.getElementById("tooltip-cantidad");  
      if(valor.length == 0) 
          asignarClaseError(e, tooltip, "Por favor completar este campo.");
      else if (validarNumero(valor)) 
          asignarClaseExito(e, tooltip);
      else 
      asignarClaseError(e,tooltip, "Solamente son permitidos números mayores a 0.");
   
    }
  gestorBtnAgregar();
}

function gestorBtnAgregar() {
  var nombre = document.getElementById("nombre").value;
  var cantidad = document.getElementById("cantidad").value;
  var bandera = true;

  bandera = bandera && (validarNombre(nombre) && !validarCampoVacio(nombre));
  bandera = bandera && (validarNumero(cantidad) && !validarCampoVacio(cantidad));
  administrarBtnInsertar(!bandera);
  
}

/* asignacion  de clases para los errores en formularios */

function asignarClaseError(input, tooltip, textoValidacion) {
  input.className = "input-error";
  tooltip.setAttribute("data-tooltip", textoValidacion);
  tooltip.className += " active";
}

/* Asignacion de la clase normal(sin errores) */

function asignarClaseExito(input, tooltip) {
  input.className = "";
  tooltip.className = "tooltip-text";
}

function validarActualizarSuministro(e) {
  var inputName = e.name;
  var valor = e.value;

  if (inputName === 'nNombre') { 
    let tooltip = document.getElementById("tooltip-nNombre"); 
    if(valor.length == 0) 
        asignarClaseError(e, tooltip, "Por favor completar este campo.");
    else if (validarNombre(valor)) 
        asignarClaseExito(e, tooltip);
    else 
        asignarClaseError(e, tooltip,"Solamente son permitidos los carácteres de [A-Z/a-z]");

    }else if(inputName === 'nCantidad') {
      let tooltip = document.getElementById("tooltip-nCantidad");  
      if(valor.length == 0) 
          asignarClaseError(e, tooltip, "Por favor completar este campo.");
      else if (validarNumero(valor)) 
          asignarClaseExito(e, tooltip);
      else 
      asignarClaseError(e,tooltip, "Solamente son permitidos números mayores a 0.");
   
    }
  gestorBtnActualizar();
}

function gestorBtnActualizar() {
  var nombre = document.getElementById("nNombre").value;
  var cantidad = document.getElementById("nCantidad").value;
  var bandera = true;

  bandera = bandera && (validarNombre(nombre) && !validarCampoVacio(nombre));
  bandera = bandera && (validarNumero(cantidad) && !validarCampoVacio(cantidad));
  administrarBtnActualizar(!bandera);
  
}


function mostrarModalAgregar(){
  limpiar();
  administrarBtnInsertar(true);
  $('#modalAgregar').modal('show'); 
  $('#modalAgregar').on('shown.bs.modal', function () {
    $('#nombre').trigger('focus');
  });
}



///////////////////////MANUAL DE USUARIO////////////////////////////////////


function manualAgregar() {
  var enjoyhint_instance = new EnjoyHint({});
  var enjoyhint_script_steps = [
    {
      'click #agregar': '¡Bienvenido al sistema de ayuda! presione el botón + para agregar una nuevo suministro.',
      'skipButton': { className: "mySkip", text: "Saltar" }
    },
    {
      'timeout': 500,
      'type #nombre': 'Ingrese el nombre del suministro usando caracteres alfabéticos.',
      'skipButton': { className: "mySkip", text: "Saltar" },
      'nextButton': { className: "mySkip", text: "Siguiente" },
      'showNext': true
    },
    {
      'type #cantidad': 'Ingrese la cantidad del suministro usando caracteres numéricos.',
      'skipButton': { className: "mySkip", text: "Saltar" },
      'nextButton': { className: "mySkip", text: "Siguiente" },
      'showNext': true
    },
    {
      'click #insert': 'Presione el botón Agregar para guardar el suministro.',
      'skipButton': { className: "mySkip", text: "Saltar" }
    }

  ];
  enjoyhint_instance.set(enjoyhint_script_steps);
  enjoyhint_instance.run();
}


function manualActualizar() {
  var enjoyhint_instance = new EnjoyHint({});
  var enjoyhint_script_steps = [
    {
      'click #Actualizar': '¡Bienvenido al sistema de ayuda! presione el botón lápiz para editar un suministro.',
      'skipButton': { className: "mySkip", text: "Saltar" }
    },
    {
      'timeout': 500,
      'type #nNombre': 'Edite el nombre del suministro usando caracteres alfabéticos.',
      'skipButton': { className: "mySkip", text: "Saltar" },
      'nextButton': { className: "mySkip", text: "Siguiente" },
      'showNext': true
    },
    {
      'type #nCantidad': 'Edite la cantidad del suministro usando caracteres numéricos.',
      'skipButton': { className: "mySkip", text: "Saltar" },
      'nextButton': { className: "mySkip", text: "Siguiente" },
      'showNext': true
    },
    {
      'click #update': 'Presione el botón Confirmar para guardar el suministro.',
      'skipButton': { className: "mySkip", text: "Saltar" }
    }

  ];
  enjoyhint_instance.set(enjoyhint_script_steps);
  enjoyhint_instance.run();
}


function manualEliminar() {
  var enjoyhint_instance = new EnjoyHint({});
  var enjoyhint_script_steps = [
    {
      'click #Eliminar': '!Bienvenido al sistema de ayuda¡ presione el botón basurero para eliminar un suministro.',
      'skipButton': { className: "mySkip", text: "Saltar" }
    },
    {
      'timeout': 500,
      'click #confirm': 'Presione el botón Confirmar para eliminar el suministro seleccionado.',
      'skipButton': { className: "mySkip", text: "Saltar" }
    }

  ];
  enjoyhint_instance.set(enjoyhint_script_steps);
  enjoyhint_instance.run();
}


