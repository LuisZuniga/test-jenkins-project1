/*let idioma = {
    sProcessing: "Procesando...",
    sLengthMenu: "Mostrar _MENU_ registros",
    sZeroRecords: "No se encontraron resultados.",
    sEmptyTable: "Químicos no encontrados.",
    sInfo:
      "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
    sInfoEmpty: "Mostrando registros del 0 al 0 de un total de 0 registros",
    sInfoFiltered: "(filtrado de un total de _MAX_ registros)",
    sInfoPostFix: "",
    sSearch: "Buscar:",
    sUrl: "",
    sInfoThousands: ",",
    sLoadingRecords: "Cargando...",
    oPaginate: {
      sFirst: "Primero",
      sLast: "Ultimo",
      sNext: "Siguiente",
      sPrevious: "Anterior"
    },
    oAria: {
      sSortAscending: ": Activar para ordenar la columna de manera ascendente",
      sSortDescending: ": Activar para ordenar la columna de manera descendente"
    },
    buttons: {
      copyTitle: "Informacion copiada",
      copyKeys: "Use your keyboard or menu to select the copy command",
      copySuccess: {
        _: "%d filas copiadas al portapapeles",
        "1": "1 fila copiada al portapapeles"
      },
  
      pageLength: {
        _: "Mostrar %d filas",
        "-1": "Mostrar Todo"
      }
    }
  };

function nuevoAjax(){
    var xmlhttp = false;
    try{
        xmlhttp = new XMLHttpRequest();
    }catch(E){
        xmlhttp = false;
    }
    return xmlhttp;
}*/



function desplegarDatos(){
    var capa = document.getElementById("tab");
    /*var buscar = document.getElementById("buscar").value;*/
    var ajax = new nuevoAjax();
    //capa.innerHTML = "<h1 id='loading' data-text='Cargando...'>Cargando...</h1>";
    capa.innerHTML = loading;
    ajax.open("POST","../business/QuimicoController.php",true);
    ajax.onreadystatechange=function(){
        if(ajax.readyState===4){
            capa.innerHTML=ajax.responseText;
            $("#tabla_quimicos").DataTable({
                paging: true,
                lengthChange: false,
                searching: true,
                ordering: true,
                info: false,
                autoWidth: true,
                lengthMenu: [[5]],
                language: idioma,
                columnDefs: [ {
                    "targets": 'no-sort',
                    "orderable": false,
                } ]
            });
            $('.tool').tooltip({
                tooltipClass: "mytooltip",
            });
            closeTool();
        }
    }
    ajax.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    ajax.send();
}

function limpiar(){ 
    let nombreQuimico = document.getElementById("nombre");
    let tooltipNombreQuimico = document.getElementById("tooltip-nombreQuimico");
    limpiarInput(nombreQuimico);
    asignarClaseExito(nombreQuimico, tooltipNombreQuimico);
    
    let cantidad = document.getElementById("cantidad");
    let tooltipCantidad = document.getElementById("tooltip-cantidad");
    limpiarInput(cantidad);
    asignarClaseExito(cantidad, tooltipCantidad);
    
    let precio = document.getElementById("precio");
    let tooltipPrecio = document.getElementById("tooltip-precio");
    limpiarInput(precio);
    asignarClaseExito(precio, tooltipPrecio);

    //document.getElementById("form").style.display = "none";
    $('#modalAgregar').modal('hide');
}

function limpiarActualizar(){ 
    let nNombreQuimico = document.getElementById("nNombre");
    let tooltipnNombreQuimico = document.getElementById("tooltip-nNombreQuimico");
    limpiarInput(nNombreQuimico);
    asignarClaseExito(nNombreQuimico, tooltipnNombreQuimico);
    
    let nCantidad = document.getElementById("nCantidad");
    let tooltipNCantidad = document.getElementById("tooltip-nCantidad");
    limpiarInput(nCantidad);
    asignarClaseExito(nCantidad, tooltipNCantidad);
    
    let nPrecio = document.getElementById("nPrecio");
    let tooltipNPrecio = document.getElementById("tooltip-nPrecio");
    limpiarInput(nPrecio);
    asignarClaseExito(nPrecio, tooltipNPrecio);

    //document.getElementById("form-actualizar").style.display = "none";
    $('#modalModificar').modal('hide');
}

function limpiarInput(input) {
    input.value = "";
}

function insertar(){
    var nombre = document.getElementById("nombre").value;
    var cantidad = document.getElementById("cantidad").value;
    var precio = document.getElementById("precio").value;
    var idTipoQuimico = document.getElementById("tipoQuimico").value;
    var toxicidad = document.getElementById("toxicidad").value;
    var ajax = new nuevoAjax();
    ajax.open("POST","../business/QuimicoAction.php",true);
    ajax.onreadystatechange=function(){
        $('#modalAgregar').modal('hide');
        if(ajax.readyState===4){
            if(ajax.responseText == "1"){
                operacionExitosa();
            }else{
                operacionFallida();
            }
            desplegarDatos();
        }
    }
    ajax.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    ajax.send("create=1&nombre="+nombre+"&cantidad="+cantidad+"&precio="+precio+"&idTipoQuimico="+idTipoQuimico+"&toxicidad="+toxicidad);
}

function actualizar(){
    var idQuimico = document.getElementById("nIdQuimico").value;
    var nombre = document.getElementById("nNombre").value;
    var cantidad = document.getElementById("nCantidad").value;
    var precio = document.getElementById("nPrecio").value;
    var toxicidad = document.getElementById("nToxicidad").value;
    var tipoQuimico = document.getElementById("nTipoQuimico").value;
    var ajax = new nuevoAjax();
    ajax.open("POST","../business/QuimicoAction.php",true);
    ajax.onreadystatechange=function(){
        $('#modalModificar').modal('hide');
        if(ajax.readyState===4){
            if(ajax.responseText == "1"){
                operacionExitosa();
            }else{
                operacionFallida();
            }
            desplegarDatos();
            /*limpiar();*/
        }
    }
    ajax.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    ajax.send("update=1&idQuimico="+idQuimico+"&nombre="+nombre+"&cantidad="+cantidad+"&precio="+precio+"&idTipoQuimico="+tipoQuimico+"&toxicidad="+toxicidad);
}

function eliminar(idQuimico){
    var ajax = new nuevoAjax();
    ajax.open("POST","../business/QuimicoAction.php",true);
    ajax.onreadystatechange=function(){
        $('#confimar').modal('hide');
        if(ajax.readyState===4){
            if(ajax.responseText == "1"){
                operacionExitosa();
            }else{
                operacionFallida();
            }
            cerrarConfirmar();
            desplegarDatos();
        }
    }
    ajax.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    ajax.send("delete=1&idQuimico="+idQuimico);
}

/////////////////////// CONFIRMACIONES ////////////////////////////////////////

function cerrarConfirmar(){
    //document.getElementById("confirmar").style.display = "none";
    $('#confirmar').modal('hide');
}

function cancelar(){
    document.getElementById("confirmar").innerHTML = document.getElementById("confirmar").innerHTML;
    cerrarConfirmar();
}

function levantarActualizar(parametro){
    //document.getElementById("form-actualizar").style.display = "block";
    document.getElementById("nIdQuimico").value = $(parametro).parents("tr").find("td").eq(0).html();
    document.getElementById("nNombre").value = $(parametro).parents("tr").find("td").eq(1).html();
    document.getElementById("nTipoQuimico").value = $(parametro).parents("tr").find("td").eq(2).html();
    document.getElementById("nToxicidad").value = $(parametro).parents("tr").find("td").eq(4).find("input").eq(0).val();
    document.getElementById("nCantidad").value = $(parametro).parents("tr").find("td").eq(5).html();
    document.getElementById("nPrecio").value = $(parametro).parents("tr").find("td").eq(6).html();
    $('#modalModificar').modal('show');
}

function confirmarEliminar(parametro){
    var confirm = document.getElementById("confirm");
    var cancel = document.getElementById("cancel");
    //document.getElementById("legend").innerHTML = "Desea eliminar este registro?";
    confirm.addEventListener("click",function(event){eliminar(parametro)});
    cancel.addEventListener("click",cancelar);
}

function levantarEliminar(parametro){
    $('#confirmar').modal('show');
    //document.getElementById("confirmar").style.display = "block";
    confirmarEliminar(parametro);
}


/*--------------VALIDACIONES NUEVAS DE QUIMICO---------------*/

function validarAgregarQuimico(e) {
    var valor = e.value;
    var inputName = e.name;

    if (inputName === 'nombre') { // nombre del quimico
        let tooltip = document.getElementById("tooltip-nombreQuimico"); // Declaramos el tooltip de nombre de quimico

        if(valor.length == 0) 
            asignarClaseError(e, tooltip, "Por favor completar este campo.");
        else if (validarNombre(valor)) 
            asignarClaseExito(e, tooltip);
        else 
            asignarClaseError(e, tooltip, "Solamente son permitidos los carácteres de [A-Z/a-z]");
    
    } else if(inputName === 'cantidad') {
        let tooltip = document.getElementById("tooltip-cantidad");  // Declaramos el tooltip de la cantidad de quimicos
        if(valor.length == 0) 
            asignarClaseError(e, tooltip, "Por favor completar este campo.");
        else if (validarNumero(valor)) 
            asignarClaseExito(e, tooltip);
        else 
            asignarClaseError(e, tooltip, "Solamente son permitidos números mayores a 0.");

    } else if (inputName === 'precio') {
        let tooltip = document.getElementById("tooltip-precio");    // Declaramos el tooltip del precio de los quimicos.
        if(valor.length == 0) 
            asignarClaseError(e, tooltip, "Por favor completar este campo.");    
        else if (validarNumero(valor))
            asignarClaseExito(e, tooltip);
        else
            asignarClaseError(e, tooltip, "Solamente son permitidos números mayores a 0.");            
    } else if (inputName === 'toxicidad') {
        let tooltip = document.getElementById("tooltip-nToxicidad");  // Declaramos el tooltip de la toxicidad
        if (valor.length === 0)
            asignarClaseError(e, tooltip, "Por favor completar este campo.");    
        else
            asignarClaseExito(e, tooltip);
    } else if (inputName === 'tipoQuimico') {
        let tooltip = document.getElementById("tooltip-tipoQuimico");   // Declaramos el tooltip del tipo de quimico.
        if (valor.length === 0)
            asignarClaseError(e, tooltip, "Por favor completar este campo.");    
        else
            asignarClaseExito(e, tooltip);
    }
    gestorBtnAgregar();
}

function gestorBtnAgregar() {
    var nombre = document.getElementById("nombre").value;
    var cantidad = document.getElementById("cantidad").value;
    var precio = document.getElementById("precio").value;
    var tipoQuimico = document.getElementById("tipoQuimico").value;
    var toxicidad = document.getElementById("toxicidad").value;
    var bandera = true;

    bandera = bandera && (validarNombre(nombre) && !validarCampoVacio(nombre));
    bandera = bandera && (validarNumero(cantidad) && !validarCampoVacio(cantidad));
    bandera = bandera && (validarNumero(precio) && !validarCampoVacio(precio));
    bandera = bandera && !validarCampoVacio(toxicidad);
    bandera = bandera && !validarCampoVacio(tipoQuimico);
    administrarBtnInsertar(!bandera);
}



function validarActualizarQuimico(e) {
    var valor = e.value;
    var inputName = e.name;
    
    if (inputName === 'nNombre') { // nombre del quimico
        let tooltip = document.getElementById("tooltip-nNombreQuimico"); // Declaramos el tooltip de nombre de quimico

        if(valor.length == 0) 
            asignarClaseError(e, tooltip, "Por favor completar este campo.");
        else if (validarNombre(valor)) 
            asignarClaseExito(e, tooltip);
        else 
            asignarClaseError(e, tooltip, "Solamente son permitidos los carácteres de [A-Z/a-z]");
    
    } else if(inputName === 'nCantidad') {
        let tooltip = document.getElementById("tooltip-nCantidad");  // Declaramos el tooltip de la cantidad de quimicos
        if(valor.length == 0) 
            asignarClaseError(e, tooltip, "Por favor completar este campo.");
        else if (validarNumero(valor)) 
            asignarClaseExito(e, tooltip);
        else 
            asignarClaseError(e, tooltip, "Solamente son permitidos números mayores a 0.");

    } else if (inputName === 'nPrecio') {
        let tooltip = document.getElementById("tooltip-nPrecio");    // Declaramos el tooltip del precio de los quimicos.
        if(valor.length == 0) 
            asignarClaseError(e, tooltip, "Por favor completar este campo.");    
        else if (validarNumero(valor))
            asignarClaseExito(e, tooltip);
        else
            asignarClaseError(e, tooltip, "Solamente son permitidos números mayores a 0.");            
    } else if (inputName === 'nToxicidad') {
        let tooltip = document.getElementById("tooltip-nToxicidad");  // Declaramos el tooltip de la toxicidad
        if (valor.length === 0)
            asignarClaseError(e, tooltip, "Por favor completar este campo.");    
        else
            asignarClaseExito(e, tooltip);
    } else if (inputName === 'nTipoQuimico') {
        let tooltip = document.getElementById("tooltip-nTipoQuimico");   // Declaramos el tooltip del tipo de quimico.
        if (valor.length === 0)
            asignarClaseError(e, tooltip, "Por favor completar este campo.");    
        else
            asignarClaseExito(e, tooltip);
    }
    gestorBtnActualizar();
}

function gestorBtnActualizar() {
    var nombre = document.getElementById("nNombre").value;
    var cantidad = document.getElementById("nCantidad").value;
    var precio = document.getElementById("nPrecio").value;
    var toxicidad = document.getElementById("nToxicidad").value;
    var tipoQuimico = document.getElementById("nTipoQuimico").value;
    var bandera = true;

    bandera = bandera && (validarNombre(nombre) && !validarCampoVacio(nombre));
    bandera = bandera && (validarNumero(cantidad) && !validarCampoVacio(cantidad));
    bandera = bandera && (validarNumero(precio) && !validarCampoVacio(precio));
    bandera = bandera &&  !validarCampoVacio(toxicidad);
    bandera = bandera &&  !validarCampoVacio(tipoQuimico);
    administrarBtnActualizar(!bandera);
}

/* asignacion  de clases para los errores en formularios */

function asignarClaseError(input, tooltip, textoValidacion) {
    input.className = "input-error";
    tooltip.setAttribute("data-tooltip", textoValidacion);
    tooltip.className += " active";
  }
  
  /* Asignacion de la clase normal(sin errores) */
  
  function asignarClaseExito(input, tooltip) {
    input.className = "";
    tooltip.className = "tooltip-text";
  }
  

  /* Levantar formulario de agregar colab */
function mostrarModalAgregar(){
    limpiar();
    administrarBtnInsertar(true);
    $('#modalAgregar').modal('show'); 
    $('#modalAgregar').on('shown.bs.modal', function () {
      $('#nombre').trigger('focus');
    });
}