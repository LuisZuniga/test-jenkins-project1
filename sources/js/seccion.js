/*let idioma = {
    sProcessing: "Procesando...",
    sLengthMenu: "Mostrar _MENU_ registros",
    sZeroRecords: "No se encontraron resultados.",
    sEmptyTable: "Secciones no encontradas.",
    sInfo:
      "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
    sInfoEmpty: "Mostrando registros del 0 al 0 de un total de 0 registros",
    sInfoFiltered: "(filtrado de un total de _MAX_ registros)",
    sInfoPostFix: "",
    sSearch: "Buscar:",
    sUrl: "",
    sInfoThousands: ",",
    sLoadingRecords: "Cargando...",
    oPaginate: {
      sFirst: "Primero",
      sLast: "Ultimo",
      sNext: "Siguiente",
      sPrevious: "Anterior"
    },
    oAria: {
      sSortAscending: ": Activar para ordenar la columna de manera ascendente",
      sSortDescending: ": Activar para ordenar la columna de manera descendente"
    },
    buttons: {
      copyTitle: "Informacion copiada",
      copyKeys: "Use your keyboard or menu to select the copy command",
      copySuccess: {
        _: "%d filas copiadas al portapapeles",
        "1": "1 fila copiada al portapapeles"
      },
  
      pageLength: {
        _: "Mostrar %d filas",
        "-1": "Mostrar Todo"
      }
    }
  };

function nuevoAjax(){
    var xmlhttp = false;
    try{
        xmlhttp = new XMLHttpRequest();
    }catch(E){
        xmlhttp = false;
    }
    return xmlhttp;
}*/

function desplegarDatos(){
    var capa = document.getElementById("tab");
    var lote = localStorage.getItem("lote");
    var seccion =   localStorage.getItem("seccion");
    var ajax = new nuevoAjax();
    capa.innerHTML = loading;
    ajax.open("POST","../business/SeccionController2.php",true);
    ajax.onreadystatechange=function(){
        if(ajax.readyState===4){
            capa.innerHTML=ajax.responseText;
            $("#tabla_secciones").DataTable({
                paging: true,
                lengthChange: false,
                searching: true,
                ordering: true,
                info: false,
                autoWidth: true,
                lengthMenu: [[5]],
                language: idioma,
                columnDefs: [{
                    "targets": 'no-sort',
                    "orderable": false,
                }]
            });
            $(".tool").tooltip({
                tooltipClass: "mytooltip",
            });
            closeTool();
        }
    }
    ajax.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    ajax.send("lote="+lote+"&seccion="+seccion);
}

function limpiar(){ 
    let planta = document.getElementById("planta");
    let tooltipPlanta = document.getElementById("tooltip-planta");
    limpiarInput(planta);
    asignarClaseExito(planta, tooltipPlanta);
    
    let cantidad = document.getElementById("cantidad");
    let tooltipCantidad = document.getElementById("tooltip-cantidad");
    limpiarInput(cantidad);
    asignarClaseExito(cantidad, tooltipCantidad);
    
    let fechaSiembra = document.getElementById("fechaSiembra");
    let tooltipFechaSiembra = document.getElementById("tooltip-fechaSiembra");
    limpiarInput(fechaSiembra);
    asignarClaseExito(fechaSiembra, tooltipFechaSiembra);

    let fechaExtraccion = document.getElementById("fechaExtraccion");
    let tooltipFechaExtraccion = document.getElementById("tooltip-fechaExtraccion");
    limpiarInput(fechaExtraccion);
    asignarClaseExito(fechaExtraccion, tooltipFechaExtraccion);

    $('#modalAgregar').modal('hide');
//    document.getElementById("form").style.display = "none";
}

function limpiarActualizar(){ 
    let nPlanta = document.getElementById("nPlanta");
    let tooltipNPlanta = document.getElementById("tooltip-nPlanta");
    limpiarInput(nPlanta);
    asignarClaseExito(nPlanta, tooltipNPlanta);
    
    let nCantidad = document.getElementById("nCantidad");
    let tooltipNCantidad = document.getElementById("tooltip-nCantidad");
    limpiarInput(nCantidad);
    asignarClaseExito(nCantidad, tooltipNCantidad);
    
    let nFechaSiembra = document.getElementById("nFechaSiembra");
    let tooltipnFechaSiembra = document.getElementById("tooltip-nFechaSiembra");
    limpiarInput(nFechaSiembra);
    asignarClaseExito(nFechaSiembra, tooltipnFechaSiembra);

    let nFechaExtraccion = document.getElementById("nFechaExtraccion");
    let tooltipnFechaExtraccion = document.getElementById("tooltip-nFechaExtraccion");
    limpiarInput(nFechaExtraccion);
    asignarClaseExito(nFechaExtraccion, tooltipnFechaExtraccion);
    $('#modalModificar').modal('hide');
//    document.getElementById("form-actualizar").style.display = "none";
}

function limpiarInput(input) {
    input.value = "";
}

function insertar(){
    var seccion = document.getElementById("seccion").value;
    var lote = document.getElementById("lote").value;
    var planta = document.getElementById("planta").value;
    var cantidad = document.getElementById("cantidad").value;
    var fechaSiembra = document.getElementById("fechaSiembra").value;
    var fechaExtraccion = document.getElementById("fechaExtraccion").value;
    var ajax = new nuevoAjax();
    ajax.open("POST","../business/SeccionAction.php",true);
    ajax.onreadystatechange=function(){
        $('#modalAgregar').modal('hide');
        if(ajax.readyState===4){
            if(ajax.responseText == "1"){
                operacionExitosa();
            }else{
                operacionFallida();
            }
            desplegarDatos();
        }
    }
    ajax.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    ajax.send("Insertar=1&seccion="+seccion+"&lote="+lote+"&planta="+planta+"&cantidad="+cantidad+"&fechaSiembra="+fechaSiembra+"&fechaExtraccion="+fechaExtraccion);
}

function actualizar(){
    var seccion = document.getElementById("nSeccion").value;
    var lote = document.getElementById("nLote").value;
    var cPlanta = document.getElementById("cPlanta").value;
    var planta = document.getElementById("nPlanta").value;
    var cantidad = document.getElementById("nCantidad").value;
    var fechaSiembra = document.getElementById("nFechaSiembra").value;
    var fechaExtraccion = document.getElementById("nFechaExtraccion").value;
    var ajax = nuevoAjax();
    ajax.open("POST","../business/SeccionAction.php",true);
    ajax.onreadystatechange=function(){
        $('#modalModificar').modal('hide');
        if(ajax.readyState===4){
            if(ajax.responseText == "1"){
                operacionExitosa();
            }else{
                operacionFallida();
            }
            cerrarConfirmar();
            desplegarDatos();
        }
    }
    ajax.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    ajax.send("Actualizar=1&seccion="+seccion+"&lote="+lote+"&cPlanta="+cPlanta+"&planta="+planta+"&cantidad="+cantidad+"&fechaSiembra="+fechaSiembra+"&fechaExtraccion="+fechaExtraccion);
}

function eliminar(input){
    var planta =  $(input).parents("tr").find("td").eq(0).html();       
    var numeroseccion =  $(input).parents("tr").find("td").eq(1).html();
    var numerolote = $(input).parents("tr").find("td").eq(2).html();
    var ajax = new nuevoAjax();
    ajax.open("POST","../business/SeccionAction.php",true);
    ajax.onreadystatechange=function(){
        $('#confirmar').modal('hide');
        if(ajax.readyState===4){
            if(ajax.responseText == "1"){
                operacionExitosa();
            }else{
                operacionFallida();
            }
            cerrarConfirmar();
            desplegarDatos();
        }
    }
    ajax.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    ajax.send("Eliminar=1&numeroseccion="+numeroseccion+"&numerolote="+numerolote+"&planta="+planta);
}



function getPlantasPorLote(i){
    let cardLote = document.querySelectorAll('.card-body .card-text')[i];
    let cardBG = document.querySelectorAll('.card-footer .card-body')[i];
    i++;
    var ajax = new nuevoAjax();
    ajax.open("POST","../business/SeccionAction.php",true);
    ajax.onreadystatechange=function(){
        if(ajax.readyState===4){
            cardLote.innerHTML = `Total de plantas: ${ajax.responseText}`;
            cardBG.style.background = switchColor(i);
        }
    }
    ajax.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    ajax.send("total=1&idLote="+i);
}

function switchColor(i){
    let hex = '#C7EFCF';
    if(i==1){
        hex = '#F0B67F';
    }else if(i==2){
        hex = '#D6D1B1';
    }
    return hex;
}

window.onload = () =>{
    for(let i=0;i<3;i++){
        getPlantasPorLote(i);
    }
}

///////////////////////////// CONFIRMACIONES ////////////////////////////////////

function cerrarConfirmar(){
    $('#confirmar').modal('hide');
    //document.getElementById("confirmar").style.display = "none";
}

function cancelar(){
    document.getElementById("confirmar").innerHTML = document.getElementById("confirmar").innerHTML;
    cerrarConfirmar();
}

function levantarActualizar(parametro){
    //document.getElementById("form-actualizar").style.display = "block";
    document.getElementById("nPlanta").value = $(parametro).parents("tr").find("td").eq(0).html();
    document.getElementById("cPlanta").value = $(parametro).parents("tr").find("td").eq(0).html();
    document.getElementById("nSeccion").value = $(parametro).parents("tr").find("td").eq(1).html();
    document.getElementById("nLote").value = $(parametro).parents("tr").find("td").eq(2).html()
    document.getElementById("nCantidad").value = $(parametro).parents("tr").find("td").eq(4).html();
    document.getElementById("nFechaSiembra").value = $(parametro).parents("tr").find("td").eq(5).html();
    document.getElementById("nFechaExtraccion").value = $(parametro).parents("tr").find("td").eq(6).html();
    $('#modalModificar').modal('show');
}

function confirmarEliminar(parametro){
    var confirm = document.getElementById("confirm");
    var cancel = document.getElementById("cancel");
    //document.getElementById("legend").innerHTML = "Desea eliminar este registro?";
    confirm.addEventListener("click",function(){eliminar(parametro)});
    cancel.addEventListener("click",cancelar);
}

function levantarEliminar(parametro){
    $('#confirmar').modal('show');
    //document.getElementById("confirmar").style.display = "block";
    confirmarEliminar(parametro);
}



/** 
 * Validaciones del modal
 */

 // Valida agregar una planta a una seccion en especifico
 function validarAgregarPlantaSeccion(e) {
    var inputName = e.name;
    var valor = e.value;
    var fechaIncio = $('#fechaSiembra').val();
    var fechaFinal = $('#fechaExtraccion').val();

    if (inputName === 'planta') { 
        let tooltip = document.getElementById("tooltip-planta"); 

        if(valor.length == 0) 
            asignarClaseError(e, tooltip, "Por favor completar este campo.");
        else if (validarNumero(valor)) 
            asignarClaseExito(e, tooltip);
        else 
            asignarClaseError(e, tooltip, "Debe seleccionar una planta");
    
    } else if(inputName === 'cantidad') {
        let tooltip = document.getElementById("tooltip-cantidad");  
        if(valor.length == 0) 
            asignarClaseError(e, tooltip, "Por favor completar este campo.");
        else if (validarNumero(valor)) 
            asignarClaseExito(e, tooltip);
        else 
            asignarClaseError(e, tooltip, "Solamente son permitidos números mayores a 0.");

    }else if(inputName === 'fechaSiembra') {
        let tooltip = document.getElementById("tooltip-fechaSiembra");  
        if (validarFecha(valor)){ 
            if(validarFechaCosechaAgregar(fechaIncio,fechaFinal)){
                asignarClaseExito(e, tooltip); 
            }        
        }else{
            asignarClaseError(e, tooltip, "Solamente son permitidas las fechas con el año igual o un año mayor al año actual.");
        } 
           
    }else if(inputName === 'fechaExtraccion') {
        let tooltip = document.getElementById("tooltip-fechaExtraccion");  
        if (validarFecha(valor)){ 
            if(validarFechaCosechaAgregar(fechaIncio,fechaFinal)){
                asignarClaseExito(e, tooltip); 
            }
        }else{
            asignarClaseError(e, tooltip, "Solamente son permitidas las fechas con el año igual o un año mayor al año actual.");
        } 
    }

    gestorBtnAgregarPlantaSeccion();
 }  


 function gestorBtnAgregarPlantaSeccion() {
    var planta = document.getElementById("planta").value;
    var cantidad = document.getElementById("cantidad").value;
    var fechaSiembra = document.getElementById("fechaSiembra").value;
    var fechaExtraccion = document.getElementById("fechaExtraccion").value;
    var bandera = true;
    

    bandera = bandera && (validarNumero(planta) && !validarCampoVacio(planta));
    bandera = bandera && (validarNumero(cantidad) && !validarCampoVacio(cantidad));
    bandera = bandera && (!validarCampoVacio(fechaSiembra) && (validarFecha(fechaSiembra)));
    bandera = bandera && (!validarCampoVacio(fechaExtraccion) && (validarFecha(fechaExtraccion)));
    bandera = bandera && (validarFechaCosechaAgregar(fechaSiembra,fechaExtraccion));
    administrarBtnInsertar(!bandera);
 }


 function validarActualizarPlantaSeccion(e) {
    var inputName = e.name;
    var valor = e.value;
    var fechaIncio = $('#nFechaSiembra').val();
    var fechaFinal = $('#nFechaSiembra').val();

    if (inputName === 'nPlanta') { 
        let tooltip = document.getElementById("tooltip-nPlanta");

        if(valor.length == 0) 
            asignarClaseError(e, tooltip, "Por favor completar este campo.");
        else if (validarNumero(valor)) 
            asignarClaseExito(e, tooltip);
        else 
        asignarClaseError(e, tooltip, "Debe seleccionar una planta");
    
    } else if(inputName === 'nCantidad') {
        let tooltip = document.getElementById("tooltip-nCantidad");  
        if(valor.length == 0) 
            asignarClaseError(e, tooltip, "Por favor completar este campo.");
        else if (validarNumero(valor)) 
            asignarClaseExito(e, tooltip);
        else 
            asignarClaseError(e, tooltip, "Solamente son permitidos números mayores a 0.");

    }else if(inputName === 'nFechaSiembra') {
        let tooltip = document.getElementById("tooltip-nFechaSiembra");  
        if (validarFecha(valor)){ 
            if(validarFechaCosechaActualizar(fechaIncio,fechaFinal)){
                asignarClaseExito(e, tooltip); 
            }        
        }else{
            asignarClaseError(e, tooltip, "Solamente son permitidas las fechas con el año igual o un año mayor al año actual.");
        } 
    }else if(inputName === 'nFechaExtraccion') {
        let tooltip = document.getElementById("tooltip-nFechaExtraccion");  
        if (validarFecha(valor)){ 
            if(validarFechaCosechaActualizar(fechaIncio,fechaFinal)){
                asignarClaseExito(e, tooltip); 
            }        
        }else{
            asignarClaseError(e, tooltip, "Solamente son permitidas las fechas con el año igual o un año mayor al año actual.");
        } 
    }
    gestorBtnActualizarPlantaSeccion();
 }

 function gestorBtnActualizarPlantaSeccion() {
    var planta = document.getElementById("nPlanta").value;
    var cantidad = document.getElementById("nCantidad").value;
    var fechaSiembra = document.getElementById("nFechaSiembra").value;
    var fechaExtraccion = document.getElementById("nFechaExtraccion").value;
    var bandera = true;
    

    bandera = bandera && (validarNumero(planta) && !validarCampoVacio(planta));
    bandera = bandera && (validarNumero(cantidad) && !validarCampoVacio(cantidad));
    bandera = bandera && (!validarCampoVacio(fechaSiembra) && (validarFecha(fechaSiembra)));
    bandera = bandera && (!validarCampoVacio(fechaExtraccion) && (validarFecha(fechaExtraccion))); 
    bandera = bandera && (validarFechaCosechaActualizar(fechaSiembra,fechaExtraccion));
    administrarBtnActualizar(!bandera);
 }

 function Fumigacion(){
    location.href = "../view/fumigacionView.php";
  }
  
  function verMapa(){
   location.href = "../view/mapeoView.php";
  }

  function Acodo(){
    location.href = "../view/acodoView.php";
   }


   /* asignacion  de clases para los errores en formularios */

function asignarClaseError(input, tooltip, textoValidacion) {
    input.className = "input-error";
    tooltip.setAttribute("data-tooltip", textoValidacion);
    tooltip.className += " active";
  }
  
  /* Asignacion de la clase normal(sin errores) */

function asignarClaseExito(input, tooltip) {
    input.className = "";
    tooltip.className = "tooltip-text";
}

/* Levantar el modal de agregar */
function mostrarModalAgregar(){
    limpiar();
    administrarBtnInsertar(true);
    $('#modalAgregar').modal('show'); 
    $('#modalAgregar').on('shown.bs.modal', function () {
        $('#planta').trigger('focus');
    });
}

  