function desplegarDatos() {  
    var capa = document.getElementById("tab");
    var buscar = document.getElementById("buscar").value;
    var lote = localStorage.getItem("lote");
    var seccion =   localStorage.getItem("seccion");
    //window.alert(lote+seccion);
    var ajax = new nuevoAjax();
    capa.innerHTML = loading;
    ajax.open("POST", "../business/AcodoController.php", true);
    ajax.onreadystatechange = function() {
      if (ajax.readyState === 4) {
        capa.innerHTML = ajax.responseText;
        $("#tabla_acodos").DataTable({
          paging: true,
          lengthChange: false,
          searching: true,
          ordering: true,
          info: false,
          autoWidth: true,
          lengthMenu: [[5]],
          language: idioma,
          columnDefs: [{
            "targets": 'no-sort',
            "orderable": false,
          }]
        });
        $(".tool").tooltip({
          tooltipClass: "mytooltip",
        });
        closeTool();
      }
    };
    ajax.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    ajax.send("buscar=" + buscar +"&lote="+lote+"&seccion="+seccion);
  }
  
  function limpiar() {
    let planta = document.getElementById("planta");
    let tooltipPlanta = document.getElementById("tooltip-planta");
    limpiarInput(planta);
    asignarClaseExito(planta, tooltipPlanta);
  
    let fecha = document.getElementById("fecha");
    let tooltipFecha = document.getElementById("tooltip-fecha");
    limpiarInput(fecha);
    asignarClaseExito(fecha, tooltipFecha);
  
  
    let cantidad = document.getElementById("cantidad");
    let tooltipCantidad = document.getElementById("tooltip-cantidad");
    limpiarInput(cantidad);
    asignarClaseExito(cantidad, tooltipCantidad);

    $('#modalAgregar').modal('hide'); 
    //document.getElementById("form").style.display = "none";
  }
  
  
  function limpiarActualizar() {
    let nPlanta = document.getElementById("nPlanta");
    let tooltipnPlanta = document.getElementById("tooltip-nPlanta");
    limpiarInput(nPlanta);
    asignarClaseExito(nPlanta, tooltipnPlanta);
  
    let fecha = document.getElementById("fecha");
    let tooltipFecha = document.getElementById("tooltip-fecha");
    limpiarInput(fecha);
    asignarClaseExito(fecha, tooltipFecha);
  
  
    let cantidad = document.getElementById("cantidad");
    let tooltipCantidad = document.getElementById("tooltip-cantidad");
    limpiarInput(cantidad);
    asignarClaseExito(cantidad, tooltipCantidad);
  
    $('#modalModificar').modal('hide'); 
    //document.getElementById("form-actualizar").style.display = "none";
  }
  
  function limpiarInput(input) {
    input.value = "";
  }
  
  function insertar() {
    var seccion = document.getElementById("seccion").value;
    var lote = document.getElementById("lote").value;
    var planta = document.getElementById("planta").value;
    var cantidad = document.getElementById("cantidad").value;
    var fecha = document.getElementById("fecha").value;
    var ajax = new nuevoAjax();
    ajax.open("POST", "../business/AcodoAction.php", true);
    ajax.onreadystatechange = function() {
      $('#modalAgregar').modal('hide'); 
      if(ajax.readyState===4){
        if(ajax.responseText == "1"){
            operacionExitosa();
        }else{
            operacionFallida();
        }
        desplegarDatos();
      }
    };
    ajax.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  
  
    ajax.send(
        "create=1&idSeccion=" + 
        seccion +
        "&idLote=" + 
        lote +
        "&planta=" +
        planta +
        "&cantidad=" +
        cantidad +
        "&fecha=" +
        fecha 
    );
  }
  
  function actualizar() {
    /*var seccion = document.getElementById("nSeccion").value;
    var lote = document.getElementById("nLote").value;*/
    var acodo = document.getElementById("nAcodo").value;
    var planta = document.getElementById("nPlanta").value;
    var cantidad = document.getElementById("nCantidad").value;
    var fecha = document.getElementById("nFecha").value;
    var ajax = new nuevoAjax();
    ajax.open("POST", "../business/AcodoAction.php", true);
    ajax.onreadystatechange = function() {
      $('#modalModificar').modal('hide'); 
      if (ajax.readyState === 4) {
        if (ajax.responseText == "1") {
          operacionExitosa();
        } else {
          operacionFallida();
        }
        desplegarDatos();
      }
    };
    ajax.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  
  
    ajax.send(
        "update=1&planta=" +
        planta +
        "&cantidad=" +
        cantidad +
        "&fecha=" +
        fecha +
        "&idAcodo=" + 
        acodo
    );
  }
  
  function eliminar(idAcodo) {
    var ajax = new nuevoAjax();
    ajax.open("POST","../business/AcodoAction.php",true);
    ajax.onreadystatechange=function(){
      $('#confirmar').modal('hide'); 
        if(ajax.readyState===4){
            if(ajax.responseText == "1"){
                operacionExitosa();
            }else{
                operacionFallida();
            }
            cerrarConfirmar();
            desplegarDatos();
        }
    }
    ajax.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    ajax.send("delete=1&idAcodo="+idAcodo);
  }
  
  ///////////////////////////// CONFIRMACIONES ////////////////////////////////////
  
  
  function confirmarEliminar(parametro){
    var confirm = document.getElementById("confirm");
    var cancel = document.getElementById("cancel");
    //document.getElementById("legend").innerHTML = "Desea eliminar este registro?";
    confirm.addEventListener("click",function(){eliminar(parametro)});
    cancel.addEventListener("click",cancelar);
  }
  
  function levantarEliminar(parametro){
    $('#confirmar').modal('show'); 
    //document.getElementById("confirmar").style.display = "block";
    confirmarEliminar(parametro);
  }
  
  function cerrarConfirmar() {
    $('#confirmar').modal('hide'); 
    //document.getElementById("confirmar").style.display = "none";
  }
  
  function cancelar() {
    document.getElementById("confirmar").innerHTML = document.getElementById(
      "confirmar"
    ).innerHTML;
    cerrarConfirmar();
  }
  
  function levantarActualizar(parametro) {
      
    //var row = parametro.parentNode.parentNode;
    //document.getElementById("form-actualizar").style.display = "block";

    document.getElementById("nAcodo").value = 
    $(parametro).parents("tr").find("td").eq(0).html();

    document.getElementById("nPlanta").value =
    $(parametro).parents("tr").find("td").eq(1).html();

    document.getElementById("nSeccion").value = 
    $(parametro).parents("tr").find("td").eq(2).html();

    document.getElementById("nLote").value = 
    $(parametro).parents("tr").find("td").eq(3).html();

    document.getElementById("nFecha").value =
    $(parametro).parents("tr").find("td").eq(5).html();

    document.getElementById("nCantidad").value =
    $(parametro).parents("tr").find("td").eq(6).html();

    $('#modalModificar').modal('show'); 
  }
  
  /* asignacion  de clases para los errores en formularios */

function asignarClaseError(input, tooltip, textoValidacion) {
  input.className = "input-error";
  tooltip.setAttribute("data-tooltip", textoValidacion);
  tooltip.className += " active";
}

/* Asignacion de la clase normal(sin errores) */

function asignarClaseExito(input, tooltip) {
  input.className = "";
  tooltip.className = "tooltip-text";
}

  
  
  /*--------------VALIDACIONES FUMIGACIÓN---------------*/
  
  function validarAgregarAcodo(e) {
    var inputName = e.name;
    var valor = e.value;
  
    if (inputName === 'planta') { 
      let tooltip = document.getElementById("tooltip-planta"); 
  
      if(valor.length == 0) 
          asignarClaseError(e, tooltip, "Por favor completar este campo.");
      else if (validarNumero(valor)) 
          asignarClaseExito(e, tooltip);
      else 
          asignarClaseError(e, tooltip,"Solamente son permitidos números mayores a 0.");
  
      }else if(inputName === 'cantidad') {
          let tooltip = document.getElementById("tooltip-cantidad");  
          if(valor.length == 0) 
              asignarClaseError(e, tooltip, "Por favor completar este campo.");
          else if (validarNumero(valor)) 
              asignarClaseExito(e, tooltip);
          else 
          asignarClaseError(e, tooltip,"Solamente son permitidos números mayores a 0.");
            
      }else if(inputName === 'fecha') {
          let tooltip = document.getElementById("tooltip-fecha");  
          if (validarFecha(valor))
                  asignarClaseExito(e, tooltip); 
          else
              asignarClaseError(e, tooltip, "Solamente son permitidas las fechas con el año igual o un año mayor al año actual.");
      }
    gestorBtnAgregar();
  }
  
  function validarActualizarAcodo(e) {
    var inputName = e.name;
    var valor = e.value;
  

    if (inputName === 'nPlanta') { 
      let tooltip = document.getElementById("tooltip-nPlanta"); 
  
      if(valor.length == 0) 
          asignarClaseError(e, tooltip, "Por favor completar este campo.");
      else if (validarNumero(valor)) 
          asignarClaseExito(e, tooltip);
      else 
          asignarClaseError(e, tooltip,"Solamente son permitidos números mayores a 0.");
  
      }else if(inputName === 'nCantidad') {
          let tooltip = document.getElementById("tooltip-nCantidad");  
          if(valor.length == 0) 
              asignarClaseError(e, tooltip, "Por favor completar este campo.");
          else if (validarNumero(valor)) 
              asignarClaseExito(e, tooltip);
          else 
              asignarClaseError(e, tooltip,"Solamente son permitidos números mayores a 0.");
            
      }else if(inputName === 'nFecha') {
          let tooltip = document.getElementById("tooltip-nFecha");  
          if (validarFecha(valor))
                  asignarClaseExito(e, tooltip); 
          else
              asignarClaseError(e, tooltip, "Solamente son permitidas las fechas con el año igual o un año mayor al año actual.");
      }
    gestorBtnActualizar();
  }
  
  /** Apagar o encender el btn de agregar  */
  
  function gestorBtnAgregar() {
    var cantidad = document.getElementById("cantidad").value;
    var planta = document.getElementById("planta").value;
    var fecha = document.getElementById("fecha").value;
    var bandera = true;
    /** 1 - 0 -> 0
     *  1 - 1 -> 1
     *  0 - 1 -> 0
     *  0 - 0 -> 0
     */
    bandera = bandera && (validarNumero(cantidad) && !validarCampoVacio(cantidad));
    //console.error(bandera);
    bandera = bandera && !validarCampoVacio(fecha) && validarFecha(fecha);
    bandera = bandera && (validarNumero(planta) && !validarCampoVacio(planta));
    administrarBtnInsertar(!bandera);
    //console.log(bandera);
  }
  
  function gestorBtnActualizar() {
    var cantidad = document.getElementById("nCantidad").value;
    var fecha = document.getElementById("nFecha").value;
    var planta = document.getElementById("nPlanta").value;
    var bandera = true;
    /** 1 - 0 -> 0
     *  1 - 1 -> 1
     *  0 - 1 -> 0
     *  0 - 0 -> 0
     */
    bandera = bandera && (validarNumero(cantidad) && !validarCampoVacio(cantidad));
    //console.error(bandera);
    bandera = bandera && !validarCampoVacio(fecha) && validarFecha(fecha);
    bandera = bandera && (validarNumero(planta) && !validarCampoVacio(planta));
    administrarBtnActualizar(!bandera);
    //console.log(bandera);
  }
  
  function verMapa(){
    location.href = "../view/mapeoView.php";
  }

  function mostrarModalAgregar(){
    limpiar();
    administrarBtnInsertar(true);
    $('#modalAgregar').modal('show'); 
    $('#modalAgregar').on('shown.bs.modal', function () {
      $('#planta').trigger('focus');
    });
  }