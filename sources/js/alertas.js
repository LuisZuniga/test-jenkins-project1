/* ajax */
function nuevoAjax() {
    var xmlhttp = false;
    try {
      xmlhttp = new XMLHttpRequest();
    } catch (E) {
      xmlhttp = false;
    }
    return xmlhttp;
}



function comandosToastr() {

    toastr.options = {
    "closeButton": false,
    "debug": false,
    "newestOnTop": false,
    "progressBar": true,
    "positionClass": "toast-bottom-right",
    "preventDuplicates": false,
    "onclick": null,
    "showDuration": "300",
    "hideDuration": "1000",
    "timeOut": "5000",
    "extendedTimeOut": "1000",
    "showEasing": "swing",
    "hideEasing": "linear",
    "showMethod": "fadeIn",
    "hideMethod": "fadeOut"
    }
}

function operacionExitosa(){
    var ajax = new nuevoAjax();
    ajax.open("POST", "../business/LoginAction.php", true);
    ajax.onreadystatechange = function() {
      if (ajax.readyState === 4) {
         if (ajax.responseText === '1') {
            comandosToastr();
            Command: toastr['success']('Se ha realizado correctamente.', 'Operación exitosa!');
         } else {
            swal(
                'Operación exitosa!',
                'Se ha realizado correctamente.',
                'success'
            )
         }
      }
    };
    ajax.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    ajax.send("getNotificationType=1");
}

function operacionFallida(){
    var ajax = new nuevoAjax();
    ajax.open("POST", "../business/LoginAction.php", true);
    ajax.onreadystatechange = function() {
      if (ajax.readyState === 4) {
         if (ajax.responseText === '1') {
            comandosToastr();
            Command: toastr['error']('No se ha podido realizar la operación', 'Operación fallida!');
         } else {
            swal(
                'Operación fallida!',
                'No se ha podido realizar la operación.',
                'error'
            )
         }
      }
    };
    ajax.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    ajax.send("getNotificationType=1");
}

// Contrasena actual incorrecta
function currentPassIncorrect(){
    var ajax = new nuevoAjax();
    ajax.open("POST", "../business/LoginAction.php", true);
    ajax.onreadystatechange = function() {
      if (ajax.readyState === 4) {
         if (ajax.responseText === '1') {
            comandosToastr();
            Command: toastr['warning']('La contraseña actual no es correcta.', 'Operación fallida!');
         } else {
            swal(
                'Operación fallida!',
                'La contraseña actual no es correcta.',
                'warning'
            )
         }
      }
    };
    ajax.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    ajax.send("getNotificationType=1");
}

function cantidadNoAceptada() {
    var ajax = new nuevoAjax();
    ajax.open("POST", "../business/LoginAction.php", true);
    ajax.onreadystatechange = function() {
      if (ajax.readyState === 4) {
         if (ajax.responseText === '1') {
            comandosToastr();
            Command: toastr['warning']('La dosis ingresada es mayor que la del stock.', 'Verificar cantidad que desea aplicar.');
         } else {
            swal(
                'Verificar cantidad que desea aplicar.',
                'La dosis ingresada es mayor que la del stock.',
                'warning'
            )
         }
      }
    };
    ajax.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    ajax.send("getNotificationType=1");
}

function probandoNotificacion(){
    console.log(Notification.permission);
    if (("Notificacion" in window)) {

        alert("Su navegador no soporta notifaciones.");

    } else if(Notification.permission === "granted"){
        alert("Esto quiere decir que si tenemos permisos");
        var notifacion = new Notification("Hola mundo.");

    }else if (Notification.permission !== "denied") {

        Notification.requestPermission(function(permission){
            if (Notification.permission === "granted") {

                var notifacion = new Notification("Hola mundo 2.0");

            }
        });
    }
}

function probandoNoficacionPush(){
    if (Push.Permission.GRANTED) {
        Push.config({ serviceWorker: '../sources/js/serviceWorker.min.js'});
        Push.Permission.request();
        Push.create("Registro", {
            body: "No se ha podido ingresar correctamente.",
            icon: '../sources/images/error.png',
            timeout: 4000,
            onClick: function () {
                window.focus();
                this.close();
            }
        });
    } else {
        alert("Aqui va el sweet alert");
    }
}