function validarTexto(input){
    if (/^([0-9])*$/.test(input))
        return false;
    else
        return true;   
}

function validarFecha(input){
    var fecha = new Date();
    var anoActual = fecha.getFullYear();
    var anoFecha = input.split("-",1);
    if (anoFecha > anoActual){
        if(anoFecha-anoActual==1){
            return true;
        }
    }else if ( anoFecha == anoActual){
        return true;
    }else{
        return false;
    }  
}

function validarFechaCosechaAgregar(inicio,fin){
    let tooltip = document.getElementById("tooltip-fechaExtraccion");  
    if(!validarCampoVacio(inicio) && !validarCampoVacio(fin)){
        if (inicio>=fin){       
            asignarClaseError($('#fechaExtraccion'), tooltip, "La fecha de extracción no puede ser menor o igual a la fecha siembra");
            return false;
        }else{
            asignarClaseExito($('#fechaExtraccion'), tooltip);
            return true;
        }
    }   
}

function validarFechaCosechaActualizar(inicio,fin){
    let tooltip = document.getElementById("tooltip-nFechaExtraccion");  
    if(!validarCampoVacio(inicio) && !validarCampoVacio(fin)){
        if (inicio>=fin){       
            asignarClaseError($('#nFechaExtraccion'), tooltip, "La fecha de extracción no puede ser menor o igual a la fecha siembra");
            return false;
        }else{
            asignarClaseExito($('#nFechaExtraccion'), tooltip);
            return true;
        }
    }   
}

function validarNombre(nombre){
    if (/^([aA-zZ, ])*$/.test(nombre))
        return true;
    else
        return false;
}

function validarCampoVacio(campo) {
    if (campo.length > 0)
        return false; // No esta vacio
    else 
        return true; // Si esta vacio
}

function validarCedula(cedula){
    if ((cedula.length>=9 && cedula.length <= 16) && ((/^([0-9])*$/.test(cedula))))
        return true;
    else
        return false;    
}

function validarNumero(numero){
    if (/^([0-9])*$/.test(numero) && numero != "" && numero >0 && numero < 999999999)
        return true;
    else
        return false;
}

function validarTelefono(telefono) {
    if (/^([0-9])*$/.test(telefono) && telefono !== "" && telefono.toString().length === 8  )
        return true;
    else 
        return false;
}

function validarDecimal(input){
    if (/^([0-9,.])*$/.test(input) && input != "" && input >=0 && input < 999999999)
        return true;
    else
        return false;
    
}

function validarEspacios(input) {
    var espacios = /\s/; // Exp regular para no permitir espacios en un texto
    if (espacios.test(input))
        return true;
    else 
        return false;
}

function validarTamanoTexto(texto, longitudDebida) {
    if (texto.length <= longitudDebida) 
        return true;
    else   
        return false;
}

function validarEmail(email) {
    var emailRegex = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/;
    //Se muestra un texto a modo de ejemplo, luego va a ser un icono
    if (emailRegex.test(email)) 
      return true;
     else 
        return false;
}

function validarPassword(pass) {
    ///^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[A-Za-z\d$@$!%*?&]{8,15}[^'\s]/
    ///^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&])([A-Za-z\d$@$!%*?&]|[^ ]){8,15}$/
    var passRegex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{8,16}$/;
    if (passRegex.test(pass)) 
        return true;
     else 
        return false;        
}

function administrarBtnInsertar(bandera){ //pasar true para bloquear, false para desbloquear.
    var btn = document.getElementById("insert");
    var color = "#28a745";
    if(bandera){
        color = "gray";
       
    }
    console.log("Desactivando boton"+bandera);
    btn.disabled = bandera;
    btn.style.backgroundColor = color;
}

function administrarBtnActualizar(bandera){ //pasar true para bloquear, false para desbloquear.
    var btn = document.getElementById("update");
    var color = "#007bff";
    if(bandera){
        color = "gray";
    }
    btn.disabled = bandera;
    btn.style.backgroundColor = color;
}

function validarFormInsertarCompleto(){
    var array  = document.getElementsByClassName("entrada");
    for(var i=0;i<array.length;i++){
        if(array[i].childNodes[0].childNodes[0].value == ""){
            document.getElementById("mensaje").style.display ="block";
            return false;
        }
    }
    document.getElementById("mensaje").style.display ="none";
    return true;
}


function validarFormInsertar(){
    if(validarFormInsertarCompleto()){
        var array  = document.getElementsByClassName("entrada");
        var bandera = true;
        var tipoAtributo;
        var claseAttr;
        for(var i=0;i<array.length;i++){
            tipoAtributo = array[i].childNodes[0].childNodes[0].getAttribute("type");
            if(tipoAtributo == "text"){
                claseAttr = array[i].childNodes[0].childNodes[0].getAttribute("class"); 
                if(claseAttr == "cedula"){
                    bandera = bandera && validarCedula(array[i].childNodes[0]);
                }else if(claseAttr == "nombre"){
                    bandera = bandera && validarNombre(array[i].childNodes[0]);
                }else{
                    bandera = bandera && validarTexto(array[i].childNodes[0]);
                }
            }else if(tipoAtributo == "number"){
                claseAttr = array[i].childNodes[0].childNodes[0].getAttribute("class"); 
                if(claseAttr == "decimal"){
                    bandera = bandera && validarDecimal(array[i].childNodes[0]);
                }else{
                    bandera = bandera && validarNumero(array[i].childNodes[0]);
                }
            }else if(tipoAtributo == "tel"){
                bandera = bandera && validarTelefono(array[i].childNodes[0]);
            }
        }
        administrarBtnInsertar(!bandera);
        return bandera; 
    }else{
        administrarBtnInsertar(true);
        return false;
    }
}

function validarFormActualizarCompleto(){
    var array  = document.getElementsByClassName("nuevo");
    for(var i=0;i<array.length;i++){
        if(array[i].childNodes[0].childNodes[0].value == ""){
            document.getElementById("mensaje").style.display ="block";
            return false;
        }
    }
    document.getElementById("mensaje").style.display ="none";
    return true;
}

function validarFormActualizar(){
    if(validarFormActualizarCompleto()){
        var array  = document.getElementsByClassName("nuevo");
        var bandera = true;
        var tipoAtributo;
        var claseAttr;
        for(var i=0;i<array.length;i++){
            tipoAtributo = array[i].childNodes[0].childNodes[0].getAttribute("type");
            if(tipoAtributo == "text"){
                claseAttr = array[i].childNodes[0].childNodes[0].getAttribute("class");
                if(claseAttr == "cedula"){
                    bandera = bandera && validarCedula(array[i].childNodes[0]);
                }else if(claseAttr == "nombre"){
                    bandera = bandera && validarNombre(array[i].childNodes[0]);
                }else{
                    bandera = bandera && validarTexto(array[i].childNodes[0]);
                }
            }else if(tipoAtributo == "number"){
                claseAttr = array[i].childNodes[0].childNodes[0].getAttribute("class"); 
                if(claseAttr == "decimal"){
                    bandera = bandera && validarDecimal(array[i].childNodes[0]);
                }else{
                    bandera = bandera && validarNumero(array[i].childNodes[0]);
                }
            }else if(tipoAtributo == "tel"){
                bandera = bandera && validarTelefono(array[i].childNodes[0]);
            }
        }
        administrarBtnActualizar(!bandera);
        return bandera; 
    }else{
        administrarBtnActualizar(true);
        return false;
    }
}

/*
function levantarForm(){
    administrarBtnInsertar(true);
    jQuery.noConflict(); 
    $('#modalAgregar').modal('show'); 
}*/