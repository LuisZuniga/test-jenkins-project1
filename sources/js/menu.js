function displayMenu(){
    var menu = document.getElementById("menu");
    if (menu.classList == "active") {
        menu.classList.remove('active');
    } else {
        menu.classList.add('active');
    }
}

function dropDown(etiquetaA){
    if (etiquetaA.name == "quimico") {
        document.getElementById("carett").classList.remove('fa-caret-down');
        document.getElementById("carett").classList.add('fa-caret-up');
    } else if(etiquetaA.name == "ajustes") {
        document.getElementById("caret").classList.remove('fa-caret-down');
        document.getElementById("caret").classList.add('fa-caret-up');
    }      
}

function dropUp(etiquetaA) {
    if (etiquetaA.name == "quimico") {
        document.getElementById("carett").classList.add('fa-caret-down');
        document.getElementById("carett").classList.remove('fa-caret-up');
    } else if(etiquetaA.name == "ajustes") {
        document.getElementById("caret").classList.add('fa-caret-down');
        document.getElementById("caret").classList.remove('fa-caret-up');
    }
}