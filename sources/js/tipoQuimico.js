/*let idioma = {
  sProcessing: "Procesando...",
  sLengthMenu: "Mostrar _MENU_ registros",
  sZeroRecords: "No se encontraron tipos de químicos.",
  sEmptyTable: "Suministros no encontrados.",
  sInfo:
    "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
  sInfoEmpty: "Mostrando registros del 0 al 0 de un total de 0 registros",
  sInfoFiltered: "(filtrado de un total de _MAX_ registros)",
  sInfoPostFix: "",
  sSearch: "Buscar:",
  sUrl: "",
  sInfoThousands: ",",
  sLoadingRecords: "Cargando...",
  oPaginate: {
    sFirst: "Primero",
    sLast: "Ultimo",
    sNext: "Siguiente",
    sPrevious: "Anterior"
  },
  oAria: {
    sSortAscending: ": Activar para ordenar la columna de manera ascendente",
    sSortDescending: ": Activar para ordenar la columna de manera descendente"
  },
  buttons: {
    copyTitle: "Informacion copiada",
    copyKeys: "Use your keyboard or menu to select the copy command",
    copySuccess: {
      _: "%d filas copiadas al portapapeles",
      "1": "1 fila copiada al portapapeles"
    },

    pageLength: {
      _: "Mostrar %d filas",
      "-1": "Mostrar Todo"
    }
  }
};
*/

function nuevoAjax() {
  var xmlhttp = false;
  try {
    xmlhttp = new XMLHttpRequest();
  } catch (E) {
    xmlhttp = false;
  }
  return xmlhttp;
}

//--------------------------------------------

function desplegarDatos(){
    var capa = document.getElementById("tab");
    /*var buscar = document.getElementById("buscar").value;*/
    var ajax = new nuevoAjax();
    //capa.innerHTML = "<h1 id='loading' data-text='Cargando...'>Cargando...</h1>";
    capa.innerHTML = loading;
    ajax.open("POST","../business/TipoQuimicoController.php",true);
    ajax.onreadystatechange=function(){
        if(ajax.readyState===4){
            capa.innerHTML=ajax.responseText;
            $("#tabla_tipoQuimicos").DataTable({
                paging: true,
                lengthChange: false,
                searching: true,
                ordering: true,
                info: false,
                autoWidth: true,
                lengthMenu: [[5]],
                language: idioma,
                columnDefs: [ {
                    "targets": 'no-sort',
                    "orderable": false,
                } ]
            });
            $('.tool').tooltip({
                tooltipClass: "mytooltip",
            });
            closeTool();
        }
    }
    ajax.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    ajax.send();
}

  
  function limpiar() {    
    var nombre = document.getElementById("nombre");
    var tooltipNombre = document.getElementById("tooltip-nombre");
    asignarClaseExito(nombre, tooltipNombre);
    limpiarInput(nombre);

    //document.getElementById("form").style.display = "none";
    //document.getElementById("form-actualizar").style.display = "none";
    $('#modalAgregar').modal('hide');
    $('#modalModificar').modal('hide');
  }
  
  function insertar() {
    var nombre = document.getElementById("nombre").value;
    var ajax = new nuevoAjax();
    ajax.open("POST", "../business/TipoQuimicoAction.php", true);
    ajax.onreadystatechange = function() {
      $('#modalAgregar').modal('hide');
      if (ajax.readyState === 4) {
        if (ajax.responseText == "1") {
          operacionExitosa();
        } else {
          operacionFallida();
        }
        desplegarDatos();
      }
    };
    ajax.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    ajax.send(
      "insert=1&nombre=" +
        nombre 
    );
  }
  
  //--------------------------------------------
  
  ///////////////////////////// CONFIRMACIONES ////////////////////////////////////
  
  function cerrarConfirmar() {
    //document.getElementById("confirmar").style.display = "none";
    $('#confirmar').modal('hide');
  }
  
  function cancelar() {
    document.getElementById("confirmar").innerHTML = document.getElementById(
      "confirmar"
    ).innerHTML;
    cerrarConfirmar();
  }
  
  
  /*--------------VALIDACIONES NUEVAS DE Tipo Químico---------------*/
  
  function validarAgregarTipoQuimico(e) {
    var inputName = e.name;
    var valor = e.value;
    if (inputName === 'nombre') {
      var tooltip = document.getElementById("tooltip-nombre");
      if (!validarCampoVacio(valor)) {
        if (validarNombre(valor)) {
          asignarClaseExito(e, tooltip);
        } else {
          asignarClaseError(e, tooltip, "Solamente son permitidos los carácteres de [A-Z/a-z]");
        }   
      } else {
        asignarClaseError(e, tooltip, "No se permiten campos vacíos.");
      }       
    }
    gestorBtnAgregar();
  }
  
  function gestorBtnAgregar() {
    var nombre = document.getElementById("nombre").value;
    var bandera = true;
  
    bandera = bandera && (validarNombre(nombre) && !validarCampoVacio(nombre));
    administrarBtnInsertar(!bandera);
  }
  
  /** PARTE DE MODIFICAR TIPO DE QUIMICO **/

  // Levantamos el formulario de actualizar, y obtenemos los parametros de la tabla para mostrarlos 
  // en el form.
  
  function levantarActualizar(parametro){
    //document.getElementById("form-actualizar").style.display = "block";
    document.getElementById("nIdTipoQuimico").value = $(parametro).parents("tr").find("td").eq(0).html();
    document.getElementById("nNombre").value = $(parametro).parents("tr").find("td").eq(1).html();
    $('#modalModificar').modal('show');
}

// Validar actualizar
function validarActualizarTipoQuimico(input) {
  var valor = input.value;
  var name = input.name;
  if (name === "nNombre") {
    var tooltip = document.getElementById("tooltip-nNombre");
    if (!validarCampoVacio(valor)) {
      if (validarNombre(valor)) {
        // Asignamos clase de exito
        asignarClaseExito(input, tooltip);
      } else {
        // Asignamos clase de error.
        asignarClaseError(input, tooltip, "Solamente son permitidos los carácteres de [A-Z/a-z]");
      }
    } else {
      asignarClaseError(input, tooltip, "No se permiten campos vacíos.");
    }
    
  }
  gestorBtnActualizar();
}
  
// Gestor del boton de actualizar.
function gestorBtnActualizar() {
  var nombre = document.getElementById("nNombre").value;
  var bandera = true;

  bandera = bandera && (validarNombre(nombre) && !validarCampoVacio(nombre));
  administrarBtnActualizar(!bandera);
}

// Metodo para actualizar un tipo de quimico

function actualizarTipoQuimico() {
  var nombre = document.getElementById("nNombre").value;
  var idTipoQuimico = document.getElementById("nIdTipoQuimico").value;
  var ajax = new nuevoAjax();
    ajax.open("POST", "../business/TipoQuimicoAction.php", true);
    ajax.onreadystatechange = function() {
      $('#modalModificar').modal('hide');
      if (ajax.readyState === 4) {
        if (ajax.responseText == "1") {
          operacionExitosa();
        } else {
          operacionFallida();
        }
        desplegarDatos();
      }
    };
    ajax.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    ajax.send("update=1&nombre=" + nombre + "&idtipoquimico=" + idTipoQuimico);
}


/** PARTE DE ELIMINAR TIPO DE QUIMICO */

// Mensaje mostrado al usuario para confirmar la eliminacion del tipo de quimico
function confirmarEliminar(parametro){
  var confirm = document.getElementById("confirm");
  var cancel = document.getElementById("cancel");
  //document.getElementById("legend").innerHTML = "Desea eliminar este registro?";
  confirm.addEventListener("click",function(event){eliminarTipoQuimico(parametro)});
  cancel.addEventListener("click",cancelar);
}

// Levanta el mensaje en la pantalla
function levantarEliminar(parametro){
  $('#confirmar').modal('show');
  //document.getElementById("confirmar").style.display = "block";
  confirmarEliminar(parametro);
}

// Metodo que elimina el tipo de quimico
function eliminarTipoQuimico(idTipoQuimico){
  var ajax = new nuevoAjax();
  ajax.open("POST","../business/TipoQuimicoAction.php",true);
  ajax.onreadystatechange=function(){
      $('#confirmar').modal('hide');
      if(ajax.readyState===4){
          if(ajax.responseText == "1"){
              operacionExitosa();
          }else{
              operacionFallida();
          }
          cerrarConfirmar();
          desplegarDatos();
      }
  }
  ajax.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  ajax.send("delete=1&idtipoquimico="+idTipoQuimico);
}

/** ASIGNACIONES DE CLASES PARA LOS ERRORES Y TOOLTIPS */


// asignacion  de clases para los errores en formularios 
function asignarClaseError(input, tooltip, textoValidacion) {
  input.className = "input-error";
  tooltip.setAttribute("data-tooltip", textoValidacion);
  tooltip.className += " active";
}

// Asignacion de la clase normal(sin errores) 
function asignarClaseExito(input, tooltip) {
  input.className = "";
  tooltip.className = "tooltip-text";
}
// Metodo para limpiar los inputs
function limpiarInput(input) {
  input.value = "";
}

  /* Levantar formulario de agregar colab */
function mostrarModalAgregar(){
  limpiar();
  administrarBtnInsertar(true);
  $('#modalAgregar').modal('show'); 
  $('#modalAgregar').on('shown.bs.modal', function () {
    $('#cedula').trigger('focus');
  });
}