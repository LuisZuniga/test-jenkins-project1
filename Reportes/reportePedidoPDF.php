<?php


// Include the main TCPDF library (search for installation path).
require_once('../LibPDF/tcpdf/tcpdf.php');
include '../business/PedidoBusiness.php';

if (isset($_GET['idpedido'])) {
	$idPedido = $_GET['idpedido'];
	$pedidoBusiness = new PedidoBusiness();
	$pedido = $pedidoBusiness->getDatosPedido($idPedido);
	$totalPlantas = 0;
	// create new PDF document
	$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Nicola Asuni');
$pdf->SetTitle('Reporte de pedido');
$pdf->SetSubject('PDF de pedido');
$pdf->SetKeywords('Ornexp, PDF, example, test, guide');

// set default header data
$pdf->SetHeaderData('tcpdf_logo.jpg', PDF_HEADER_LOGO_WIDTH, ' Sistema de plantas ornamentales', PDF_HEADER_STRING, array(0,64,255), array(0,64,128));
$pdf->setFooterData(array(0,64,0), array(0,64,128));

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
	require_once(dirname(__FILE__).'/lang/eng.php');
	$pdf->setLanguageArray($l);
}

// ---------------------------------------------------------

// set default font subsetting mode
$pdf->setFontSubsetting(true);

// Set font
// dejavusans is a UTF-8 Unicode font, if you only need to
// print standard ASCII chars, you can use core fonts like
// helvetica or times to reduce file size.
$pdf->SetFont('dejavusans', '', 14, '', true);

// Add a page
// This method has several options, check the source code documentation for more information.
$pdf->AddPage();

// set text shadow effect
$pdf->setTextShadow(array('enabled'=>true, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal'));

// Set some content to print
$html = '<style>'.file_get_contents('../sources/css/reportePedidoPdf.css').'</style>
		<div class="row">
			<label>Fecha pedido: '. $pedido->getFechaEnvio() .' </label><br>
			<label>Número de pedido: '. $pedido->getIdPedido() .'</label><br>
		</div>';

 /* Recorremos los pedidos clientes. */

 foreach($pedido->getPedidoCliente() as $pedidoCliente) {
	$html .= '<div class="titulos"><br><label>Cliente: '.$pedidoCliente->getNombreCliente(). ' </label><br>';
	$html .= '<label>Cédula: '.$pedidoCliente->getCedulaCliente(). ' </label><br></div>';
	$html .= '
	<table cellspacing="0" cellpadding="1" border="0.5">
		<tr>
			<td class="">Nombre de la planta</td>
			<td class="">Cantidad de plantas</td>
			<td class="">Precio unitario</td>
			<td class="">Total</td>
		</tr>';
	
	foreach($pedidoCliente->getPedidoClientePlanta() as $pedidoCP) {
		$html .= '<tr>
		
					<td>'. $pedidoCP->getNombrePlanta() .'</td>
					<td>'. $pedidoCP->getCantidad().'</td>
					<td>$'. $pedidoCP->getPrecio(). '</td>
					<td>$'. $pedidoCP->getCantidad() * $pedidoCP->getPrecio() .'</td>
				</tr>';
		$totalPlantas += $pedidoCP->getCantidad();
	}

	$html .= '</table><br><br>';
}



$html .= 	'<table cellspacing="0" cellpadding="1" border="0.5">

				<tr>	
					<td><br><br> Cantidad total de plantas:</td>
					<td align="center"><br><br><label> ' . $totalPlantas . '</label><br></td>
				</tr>	
				<tr>
					<td class=""><br><br><label> Monto total: </label></td>
					<td align="center"><br><br><label> $'.$pedido->getMontoTotal().' </label><br></td>
				</tr>
				
			</table>';
// Print text using writeHTMLCell()
$pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);

// ---------------------------------------------------------

// Close and output PDF document
// This method has several options, check the source code documentation for more information.
$pdf->Output('pedido_'.$pedido->getIdPedido().'.pdf', 'I');

//============================================================+
// END OF FILE
//============================================================+
}