const input = {};

input.randomText = ()=>{
    const posible = 'abcdefghijklmnopqrstuvwxyz';
    let random = '';
    for(let i=0;i<8;i++){
        random += posible.charAt(Math.floor(Math.random()*posible.length));
    }
    return random;
}

input.randomNumber = (size)=>{
    const posible = '0123456789';
    let random = '';
    for(let i=0;i<size;i++){
        random += posible.charAt(Math.floor(Math.random()*posible.length));
    }
    return random;
}

input.randomDecimal = (size)=>{
    const posible = '0123456789';
    let random = '';
    for(let i=0;i<size;i++){
        random += posible.charAt(Math.floor(Math.random()*posible.length));
    }
    random += ('.'+posible.charAt(Math.floor(Math.random()*posible.length)));
    return random;
}



module.exports = input;