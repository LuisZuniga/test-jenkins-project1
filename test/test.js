const Nightmare = require('nightmare')
const input = require('./helpers/input')

describe('Testing plant module', function() {
  this.timeout('30s')

  let nightmare = null
  beforeEach(() => {
    nightmare = new Nightmare({show: true})
  })

  //Int159//A

  describe('Login and insert', () => {
    it('Inserting success', done => {
      nightmare.goto('http://localhost/repositorio/test-jenkins-project1/view/login.php')
        .type('#username', 'admin')
        .type('#password', 'admin')
        .click('#login')
        .wait(500)
        .goto('http://localhost/repositorio/test-jenkins-project1/view/plantaView.php')
        .wait(1500)
        .click('#agregar')
        .wait(1000)
        .type('#nombreComun', input.randomText())
        .wait(500)
        .type('#nombreCientifico', input.randomText())
        .wait(500)
        .type('#precioUnitario', input.randomDecimal(2))
        .wait(500)
        .click('#insert')
        .wait(1500)
        .end()
        .then(function (result) { done() })
        .catch(done)
    })
  })

  describe('Login and update', () => {
    it('Updating success', done => {
      nightmare.goto('http://localhost/repositorio/test-jenkins-project1/view/login.php')
        .type('#username', 'admin')
        .type('#password', 'admin')
        .click('#login')
        .wait(500)
        .goto('http://localhost/repositorio/test-jenkins-project1/view/plantaView.php')
        .wait(1500)
        .click('th')
        .wait(1000)
        .click('#Actualizar')
        .wait(500)
        .evaluate(function(){
          document.getElementById('nNombreComun').value = '';
          document.getElementById('nNombreCientifico').value = '';
          document.getElementById('nPrecioUnitario').value = '';
        })
        .type('#nNombreComun', input.randomText())
        .wait(500)
        .type('#nNombreCientifico', input.randomText())
        .wait(500)
        .type('#nPrecioUnitario', input.randomDecimal(1))
        .click('#update')
        .wait(1500)
        .end()
        .then(function (result) { done() })
        .catch(done)
    })
  })

  describe('Login and delete', () => {
    it('Deleting success', done => {
      nightmare.goto('http://localhost/repositorio/test-jenkins-project1/view/login.php')
        .type('#username', 'admin')
        .type('#password', 'admin')
        .click('#login')
        .wait(500)
        .goto('http://localhost/repositorio/test-jenkins-project1/view/plantaView.php')
        .wait(1500)
        .click('th')
        .wait(500)
        .click('#Eliminar')
        .wait(1000)
        .click('#confirm')
        .wait(1500)
        .end()
        .then(function (result) { done() })
        .catch(done)
    })
  })

})