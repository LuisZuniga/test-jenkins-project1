-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 10-04-2019 a las 02:37:00
-- Versión del servidor: 10.1.30-MariaDB
-- Versión de PHP: 7.1.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `dbornexp`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbacodo`
--

CREATE TABLE `tbacodo` (
  `idacodo` int(11) NOT NULL,
  `idlote` int(11) NOT NULL,
  `idseccion` int(11) NOT NULL,
  `idplanta` int(11) NOT NULL,
  `cantidad` int(11) NOT NULL,
  `fecha` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tbacodo`
--

INSERT INTO `tbacodo` (`idacodo`, `idlote`, `idseccion`, `idplanta`, `cantidad`, `fecha`) VALUES
(3, 1, 3, 11, 9000, '2019-04-21'),
(15, 1, 3, 9, 7000, '2019-04-28'),
(18, 1, 3, 10, 9000, '2019-05-05');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbcliente`
--

CREATE TABLE `tbcliente` (
  `cedula` varchar(20) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `apellido1` varchar(50) NOT NULL,
  `apellido2` varchar(50) NOT NULL,
  `direccion` varchar(100) NOT NULL,
  `telefono` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tbcliente`
--

INSERT INTO `tbcliente` (`cedula`, `nombre`, `apellido1`, `apellido2`, `direccion`, `telefono`) VALUES
('106090135', 'Maria', 'Salazar', 'Leiva', 'Guapiles', 87401521),
('702450245', 'Tomas', 'Torres', 'Castillo', 'Guadalupe', 87452151),
('702630248', 'Fernando', 'zuniga ', 'Salazar', 'Cariari', 87401521);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbcolaborador`
--

CREATE TABLE `tbcolaborador` (
  `cedula` int(20) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `apellido1` varchar(100) NOT NULL,
  `apellido2` varchar(100) NOT NULL,
  `direccion` varchar(100) NOT NULL,
  `telefono` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tbcolaborador`
--

INSERT INTO `tbcolaborador` (`cedula`, `nombre`, `apellido1`, `apellido2`, `direccion`, `telefono`) VALUES
(702630248, 'Fernando', 'Salazar', 'Zuniga', 'Cariari', 87405623);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbfumigacion`
--

CREATE TABLE `tbfumigacion` (
  `idfumigacion` int(11) NOT NULL,
  `idseccion` int(11) NOT NULL,
  `idlote` int(11) NOT NULL,
  `idquimico` int(11) NOT NULL,
  `idcolaborador` int(20) NOT NULL,
  `dosis` int(11) NOT NULL,
  `fechafumigacion` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tbfumigacion`
--

INSERT INTO `tbfumigacion` (`idfumigacion`, `idseccion`, `idlote`, `idquimico`, `idcolaborador`, `dosis`, `fechafumigacion`) VALUES
(2, 3, 1, 2, 702630248, 12, '2019-03-31'),
(3, 3, 1, 3, 702630248, 700, '2019-03-24');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tblote`
--

CREATE TABLE `tblote` (
  `idlote` int(11) NOT NULL,
  `numerolote` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tblote`
--

INSERT INTO `tblote` (`idlote`, `numerolote`) VALUES
(1, 1),
(2, 2),
(3, 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbpedido`
--

CREATE TABLE `tbpedido` (
  `idpedido` int(11) NOT NULL,
  `montototal` float NOT NULL,
  `fechaenvio` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tbpedido`
--

INSERT INTO `tbpedido` (`idpedido`, `montototal`, `fechaenvio`) VALUES
(1, 12, '2019-04-28'),
(33, 231, '2019-04-09');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbpedidocliente`
--

CREATE TABLE `tbpedidocliente` (
  `idpedidocliente` int(11) NOT NULL,
  `idpedido` int(11) NOT NULL,
  `idcliente` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tbpedidocliente`
--

INSERT INTO `tbpedidocliente` (`idpedidocliente`, `idpedido`, `idcliente`) VALUES
(1, 1, '106090135'),
(95, 33, '106090135'),
(102, 33, '702450245'),
(103, 33, '702630248');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbpedidoclienteplanta`
--

CREATE TABLE `tbpedidoclienteplanta` (
  `idpedidoclienteplanta` int(11) NOT NULL,
  `idplanta` int(11) NOT NULL,
  `idpedidocliente` int(11) NOT NULL,
  `cantidad` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tbpedidoclienteplanta`
--

INSERT INTO `tbpedidoclienteplanta` (`idpedidoclienteplanta`, `idplanta`, `idpedidocliente`, `cantidad`) VALUES
(1, 9, 1, 7),
(124, 9, 95, 1),
(133, 9, 102, 111),
(138, 9, 103, 15),
(139, 10, 103, 15);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbplanta`
--

CREATE TABLE `tbplanta` (
  `idplanta` int(11) NOT NULL,
  `nombrecomun` varchar(50) NOT NULL,
  `nombrecientifico` varchar(50) NOT NULL,
  `cantidad` int(11) NOT NULL,
  `preciounitario` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tbplanta`
--

INSERT INTO `tbplanta` (`idplanta`, `nombrecomun`, `nombrecientifico`, `cantidad`, `preciounitario`) VALUES
(9, 'aztec grass', 'LIRIOPE MUSCARI', 0, 1.7),
(10, 'mini mondo', 'OPHIOPOGON PLANISCARPUS', 0, 0.99),
(11, 'bolero tricor', 'CORDYLINE TERMINALES', 8000, 0.98);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbplantaseccion`
--

CREATE TABLE `tbplantaseccion` (
  `idplanta` int(11) NOT NULL,
  `numeroseccion` int(11) NOT NULL,
  `numerolote` int(11) NOT NULL,
  `cantidadplantas` int(11) NOT NULL,
  `fechasiembra` date NOT NULL,
  `fechaextraccion` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tbplantaseccion`
--

INSERT INTO `tbplantaseccion` (`idplanta`, `numeroseccion`, `numerolote`, `cantidadplantas`, `fechasiembra`, `fechaextraccion`) VALUES
(11, 3, 1, 8000, '2019-03-30', '2019-03-31');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbquimico`
--

CREATE TABLE `tbquimico` (
  `idquimico` int(11) NOT NULL,
  `nombre` varchar(35) NOT NULL,
  `cantidad` int(11) NOT NULL,
  `precio` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tbquimico`
--

INSERT INTO `tbquimico` (`idquimico`, `nombre`, `cantidad`, `precio`) VALUES
(2, 'Cal', 38, 13),
(3, 'Gramoson', 100, 1500);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbseccion`
--

CREATE TABLE `tbseccion` (
  `numeroseccion` int(11) NOT NULL,
  `numerolote` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tbseccion`
--

INSERT INTO `tbseccion` (`numeroseccion`, `numerolote`) VALUES
(1, 1),
(1, 2),
(1, 3),
(2, 1),
(2, 2),
(2, 3),
(3, 1),
(3, 2),
(3, 3),
(4, 1),
(4, 2),
(4, 3),
(5, 1),
(5, 2),
(5, 3),
(6, 1),
(6, 2),
(6, 3),
(7, 1),
(7, 2),
(7, 3),
(8, 1),
(8, 2),
(8, 3),
(9, 1),
(9, 2),
(9, 3),
(10, 1),
(10, 2),
(10, 3),
(11, 1),
(11, 2),
(11, 3),
(12, 1),
(12, 2),
(12, 3),
(13, 1),
(13, 2),
(13, 3),
(14, 1),
(14, 2),
(14, 3),
(15, 1),
(15, 3),
(16, 1),
(16, 3),
(17, 1),
(17, 3),
(18, 1),
(18, 3),
(19, 1),
(19, 3),
(20, 1),
(20, 3),
(21, 1),
(21, 3),
(22, 1),
(22, 3),
(23, 1),
(24, 1),
(25, 1),
(26, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbsuministro`
--

CREATE TABLE `tbsuministro` (
  `idsuministro` int(11) NOT NULL,
  `tiposuministro` int(11) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `cantidad` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tbsuministro`
--

INSERT INTO `tbsuministro` (`idsuministro`, `tiposuministro`, `nombre`, `cantidad`) VALUES
(1, 1, 'Tomas', '125');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbtiposuministro`
--

CREATE TABLE `tbtiposuministro` (
  `idtiposuministro` int(11) NOT NULL,
  `nombre` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tbtiposuministro`
--

INSERT INTO `tbtiposuministro` (`idtiposuministro`, `nombre`) VALUES
(1, 'Empacadora');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbusuario`
--

CREATE TABLE `tbusuario` (
  `username` varchar(16) NOT NULL,
  `password` varchar(16) NOT NULL,
  `role` varchar(16) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tbusuario`
--

INSERT INTO `tbusuario` (`username`, `password`, `role`) VALUES
('admin', 'admin', 'admin');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `tbacodo`
--
ALTER TABLE `tbacodo`
  ADD PRIMARY KEY (`idacodo`),
  ADD UNIQUE KEY `idplanta` (`idplanta`),
  ADD KEY `tbacodo_ibfk_1` (`idlote`),
  ADD KEY `tbacodo_ibfk_3` (`idseccion`);

--
-- Indices de la tabla `tbcliente`
--
ALTER TABLE `tbcliente`
  ADD PRIMARY KEY (`cedula`);

--
-- Indices de la tabla `tbcolaborador`
--
ALTER TABLE `tbcolaborador`
  ADD PRIMARY KEY (`cedula`);

--
-- Indices de la tabla `tbfumigacion`
--
ALTER TABLE `tbfumigacion`
  ADD PRIMARY KEY (`idfumigacion`),
  ADD KEY `idcolaborador` (`idcolaborador`),
  ADD KEY `idlote` (`idlote`),
  ADD KEY `idseccion` (`idseccion`),
  ADD KEY `idquimico` (`idquimico`);

--
-- Indices de la tabla `tblote`
--
ALTER TABLE `tblote`
  ADD PRIMARY KEY (`idlote`);

--
-- Indices de la tabla `tbpedido`
--
ALTER TABLE `tbpedido`
  ADD PRIMARY KEY (`idpedido`);

--
-- Indices de la tabla `tbpedidocliente`
--
ALTER TABLE `tbpedidocliente`
  ADD PRIMARY KEY (`idpedidocliente`),
  ADD KEY `tbpedidocliente_ibfk_1` (`idpedido`),
  ADD KEY `tbpedidocliente_ibfk_2` (`idcliente`);

--
-- Indices de la tabla `tbpedidoclienteplanta`
--
ALTER TABLE `tbpedidoclienteplanta`
  ADD PRIMARY KEY (`idpedidoclienteplanta`),
  ADD KEY `tbpedidoclienteplanta_ibfk_1` (`idplanta`),
  ADD KEY `tbpedidoclienteplanta_ibfk_2` (`idpedidocliente`);

--
-- Indices de la tabla `tbplanta`
--
ALTER TABLE `tbplanta`
  ADD PRIMARY KEY (`idplanta`),
  ADD UNIQUE KEY `nombreCientifico` (`nombrecientifico`);

--
-- Indices de la tabla `tbplantaseccion`
--
ALTER TABLE `tbplantaseccion`
  ADD PRIMARY KEY (`idplanta`,`numeroseccion`,`numerolote`),
  ADD KEY `idseccion` (`numeroseccion`),
  ADD KEY `idlote` (`numerolote`);

--
-- Indices de la tabla `tbquimico`
--
ALTER TABLE `tbquimico`
  ADD PRIMARY KEY (`idquimico`),
  ADD UNIQUE KEY `nombre` (`nombre`);

--
-- Indices de la tabla `tbseccion`
--
ALTER TABLE `tbseccion`
  ADD PRIMARY KEY (`numeroseccion`,`numerolote`),
  ADD KEY `numerolote` (`numerolote`);

--
-- Indices de la tabla `tbsuministro`
--
ALTER TABLE `tbsuministro`
  ADD PRIMARY KEY (`idsuministro`),
  ADD UNIQUE KEY `nombre` (`nombre`),
  ADD KEY `tipoSuministro` (`tiposuministro`);

--
-- Indices de la tabla `tbtiposuministro`
--
ALTER TABLE `tbtiposuministro`
  ADD PRIMARY KEY (`idtiposuministro`);

--
-- Indices de la tabla `tbusuario`
--
ALTER TABLE `tbusuario`
  ADD PRIMARY KEY (`username`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `tbacodo`
--
ALTER TABLE `tbacodo`
  MODIFY `idacodo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT de la tabla `tbfumigacion`
--
ALTER TABLE `tbfumigacion`
  MODIFY `idfumigacion` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `tblote`
--
ALTER TABLE `tblote`
  MODIFY `idlote` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `tbpedido`
--
ALTER TABLE `tbpedido`
  MODIFY `idpedido` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT de la tabla `tbpedidocliente`
--
ALTER TABLE `tbpedidocliente`
  MODIFY `idpedidocliente` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=104;

--
-- AUTO_INCREMENT de la tabla `tbpedidoclienteplanta`
--
ALTER TABLE `tbpedidoclienteplanta`
  MODIFY `idpedidoclienteplanta` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=140;

--
-- AUTO_INCREMENT de la tabla `tbplanta`
--
ALTER TABLE `tbplanta`
  MODIFY `idplanta` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT de la tabla `tbquimico`
--
ALTER TABLE `tbquimico`
  MODIFY `idquimico` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `tbsuministro`
--
ALTER TABLE `tbsuministro`
  MODIFY `idsuministro` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `tbtiposuministro`
--
ALTER TABLE `tbtiposuministro`
  MODIFY `idtiposuministro` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `tbacodo`
--
ALTER TABLE `tbacodo`
  ADD CONSTRAINT `tbacodo_ibfk_1` FOREIGN KEY (`idlote`) REFERENCES `tblote` (`idlote`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tbacodo_ibfk_2` FOREIGN KEY (`idplanta`) REFERENCES `tbplanta` (`idplanta`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tbacodo_ibfk_3` FOREIGN KEY (`idseccion`) REFERENCES `tbseccion` (`numeroseccion`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `tbfumigacion`
--
ALTER TABLE `tbfumigacion`
  ADD CONSTRAINT `tbfumigacion_ibfk_1` FOREIGN KEY (`idcolaborador`) REFERENCES `tbcolaborador` (`cedula`),
  ADD CONSTRAINT `tbfumigacion_ibfk_2` FOREIGN KEY (`idlote`) REFERENCES `tblote` (`idlote`),
  ADD CONSTRAINT `tbfumigacion_ibfk_3` FOREIGN KEY (`idseccion`) REFERENCES `tbseccion` (`numeroseccion`),
  ADD CONSTRAINT `tbfumigacion_ibfk_4` FOREIGN KEY (`idquimico`) REFERENCES `tbquimico` (`idquimico`);

--
-- Filtros para la tabla `tbpedidocliente`
--
ALTER TABLE `tbpedidocliente`
  ADD CONSTRAINT `tbpedidocliente_ibfk_1` FOREIGN KEY (`idpedido`) REFERENCES `tbpedido` (`idpedido`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tbpedidocliente_ibfk_2` FOREIGN KEY (`idcliente`) REFERENCES `tbcliente` (`cedula`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `tbpedidoclienteplanta`
--
ALTER TABLE `tbpedidoclienteplanta`
  ADD CONSTRAINT `tbpedidoclienteplanta_ibfk_1` FOREIGN KEY (`idplanta`) REFERENCES `tbplanta` (`idplanta`) ON DELETE CASCADE,
  ADD CONSTRAINT `tbpedidoclienteplanta_ibfk_2` FOREIGN KEY (`idpedidocliente`) REFERENCES `tbpedidocliente` (`idpedidocliente`) ON DELETE CASCADE;

--
-- Filtros para la tabla `tbplantaseccion`
--
ALTER TABLE `tbplantaseccion`
  ADD CONSTRAINT `idlote` FOREIGN KEY (`numerolote`) REFERENCES `tbseccion` (`numerolote`),
  ADD CONSTRAINT `idplanta` FOREIGN KEY (`idplanta`) REFERENCES `tbplanta` (`idplanta`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `idseccion` FOREIGN KEY (`numeroseccion`) REFERENCES `tbseccion` (`numeroseccion`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `tbseccion`
--
ALTER TABLE `tbseccion`
  ADD CONSTRAINT `tbseccion_ibfk_1` FOREIGN KEY (`numerolote`) REFERENCES `tblote` (`idlote`);

--
-- Filtros para la tabla `tbsuministro`
--
ALTER TABLE `tbsuministro`
  ADD CONSTRAINT `tbsuministro_ibfk_1` FOREIGN KEY (`tiposuministro`) REFERENCES `tbtiposuministro` (`idtiposuministro`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
