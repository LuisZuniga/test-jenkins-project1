// limpia localStorage al cargar la pagina.
window.onload = function () {
    localStorage.clear();
};

// OBJETOS JS

var planta = {
    idPedidoClientePlanta: 0,
    idPlanta: 0,
    idClientePedido: 0,
    nombrePlanta: "",
    cantidad: 0,
};
var cliente = {
    idPedidoCliente: 0,
    idPedido: 0,
    idCliente: 0,
    pedidoClientePlanta: []
};

var pedido = {
    id: 0,
    fechaPedido: "",
    pedidoCliente: []
};


// CRUD.


function desplegarDatos() {
    var capa = document.getElementById("tab");
    var buscar = document.getElementById("buscar").value;
    var ajax = new nuevoAjax();
    capa.innerHTML = loading;
    ajax.open("POST", "../business/PedidoController.php", true);
    ajax.onreadystatechange = function () {
        if (ajax.readyState === 4) {
            capa.innerHTML = ajax.responseText;
            $("#tabla_pedidos").DataTable({
                paging: true,
                lengthChange: false,
                searching: true,
                ordering: true,
                info: false,
                autoWidth: true,
                lengthMenu: [[5]],
                language: idioma,
                columnDefs: [{
                    "targets": 'no-sort',
                    "orderable": false,
                }]
            });
            $(".tool").tooltip({
                tooltipClass: "mytooltip",
            });
            closeTool();
            pedido.id = 0;
            pedido.fechaPedido = "";
            pedido.pedidoCliente = [];
        }
    };
    ajax.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    ajax.send("buscar=" + buscar);
}


function insertar() {
    var pedido = localStorage.getItem("pedido");
    var fechaPedido = document.getElementById("fechaPedido").value;
    pedido = JSON.parse(pedido);
    pedido.fechaPedido = fechaPedido;
    localStorage.setItem("pedido", JSON.stringify(pedido));
    var pedido = localStorage.getItem("pedido");
    var ajax = new nuevoAjax();
    ajax.open("POST", "../business/PedidoAction.php", true);
    ajax.onreadystatechange = function () {
        if (ajax.readyState === 4) {
            if (ajax.responseText == 0) {
                operacionFallida();
            } else {
                operacionExitosa();
                console.log(ajax.responseText);
            }
            localStorage.clear();
            desplegarDatos();
        }
    }
    ajax.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    ajax.send("create=1&pedido=" + pedido);
}


function eliminar(idPedido) {
    var ajax = new nuevoAjax();
    ajax.open("POST", "../business/PedidoAction.php", true);
    ajax.onreadystatechange = function () {
        if (ajax.readyState === 4) {
            console.log(ajax.responseText);
            if (ajax.responseText == 1) {
                operacionExitosa();
            } else {
                operacionFallida();
            }
            cerrarConfirmar();
            desplegarDatos();
        }
    };
    ajax.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    ajax.send("delete=1&idPedido=" + idPedido);
}

function actualizar() {
    var pedido = localStorage.getItem("pedido");
    var fechaPedido = document.getElementById("nFechaPedido").value;
    pedido = JSON.parse(pedido);
    pedido.fechaPedido = fechaPedido;
    localStorage.setItem("pedido", JSON.stringify(pedido));
    var pedido = localStorage.getItem("pedido");
    var ajax = new nuevoAjax();
    ajax.open("POST", "../business/PedidoAction.php", true);
    ajax.onreadystatechange = function () {
        if (ajax.readyState === 4) {
            if (ajax.responseText == 0) {
                operacionFallida();
            } else {
                operacionExitosa();
            }
            localStorage.clear();
            desplegarDatos();
        }
    }
    ajax.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    ajax.send("update=1&pedido=" + pedido);
}


// FORMS Y CONFIRMACIONES.

// al levantar el form insertar mete un pedido a localStorage.
function levantarForm() {
    localStorage.setItem("pedido", JSON.stringify(pedido));
    
    startPanel();
    setPanel(0);
    showPanel(0);
    document.getElementsByClassName("tabPanel")[0].innerHTML = ring;
    
    document.getElementById("form").style.display = "block";
}

// levanta el modal actualizar y lo carga.
function levantarActualizar(parametro) {
    var idPedido = $(parametro).parents("tr").find("td").eq(0).html();
    var fechaPedido = $(parametro).parents("tr").find("td").eq(1).html();
    document.getElementsByClassName("tabPanel")[3].innerHTML = ring;
    var ajax = new nuevoAjax();
    ajax.open("POST", "../business/PedidoAction.php", true);
    ajax.onreadystatechange = function () {
        if (ajax.readyState === 4) {
            var JsonData = (JSON.parse(ajax.responseText));
            pedido.id = idPedido;
            pedido.fechaPedido = fechaPedido;
            pedido.pedidoCliente = JsonData;
            localStorage.setItem("pedido", JSON.stringify(pedido));
            for (let i = 0; i < 3; i++) {
                if (i < pedido.pedidoCliente.length) {
                    // funcion como parametro de otra funcion o callback.
                    displayPanel(setData, i);
                } else {
                    displayPanel(function (i) {

                    }, i);
                    displayTab(i);
                }
            }
            document.getElementById("nFechaPedido").value = fechaPedido;
            document.getElementById("form-actualizar").style.display = "block";
            showPanel(3);
        }
    };
    ajax.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    ajax.send("getDataUp=1&idpedido=" + idPedido);
}



function levantarEliminar(parametro) {
    $('#confirmar').modal('show');
    //document.getElementById("confirmar").style.display = "block";
    confirmarEliminar(parametro);
}

function limpiar() {
    localStorage.removeItem("pedido");
    $('#modalAgregar').modal('hide');
    $('#modalModificar').modal('hide');
    /*document.getElementById("form").style.display = "none";
    document.getElementById("form-actualizar").style.display = "none";*/
    reset();
}

function reset() {
    var panels = document.getElementsByClassName("tabPanel");
    var tabs = document.getElementsByClassName("tab");
    for (let i = 0; i < panels.length; i++) {
        panels[i].style.display = "block";
        tabs[i].style.display = "block";
        if (i == 0 || i == 3) {
            panels[i].innerHTML = ring;
        } else {
            panels[i].innerHTML = "";
        }
    }
    localStorage.clear();
}

function confirmarEliminar(parametro) {
    var confirm = document.getElementById("confirm");
    var cancel = document.getElementById("cancel");
    confirm.addEventListener("click", function () {
        eliminar(parametro);
    });
    cancel.addEventListener("click", cancelar);
}

function cerrarConfirmar() {
    //document.getElementById("confirmar").style.display = "none";
    $('#confirmar').modal('hide');
}

function cancelar() {
    document.getElementById("confirmar").innerHTML = document.getElementById("confirmar").innerHTML;
    cerrarConfirmar();
}


// TABS.

// esta funcion realiza la logica del desplazamiento de los paneles, recorre la lista de paneles
// de html y a todos los oculta(display none) y cambia los bordes.
// y luego muestra el panel seleccionado.
function showPanel(panelIndex) {
    alert('Mostrando paneles');
    var tabButtons = document.getElementsByClassName("tab");
    var tabPanels = document.getElementsByClassName("tabPanel");
    Object.keys(tabButtons).forEach(function (element) {
        //tabButtons[element].className = "btn-tab-desactive";
        
        tabButtons[element].style.border = "transparent";
        tabButtons[element].style.borderBottom = "0.2px solid #28a745";
        tabButtons[element].style.backgroundColor = "";
    });
    tabButtons[panelIndex].style.backgroundColor = "#ffffff";
    tabButtons[panelIndex].style.border = "0.2px solid #28a745";
    tabButtons[panelIndex].style.borderBottom = "0.2px solid transparent";

    Object.keys(tabPanels).forEach(function (element) {
        tabPanels[element].style.display = "none";
    });

    tabPanels[panelIndex].style.display = "block";
    //validarInsert();
}

// carga un panel con un formulario respectivo.
function setPanel(n) {
    var panels = document.getElementsByClassName("tabPanel");
    var ajax = nuevoAjax();
    ajax.open("POST", "../business/PedidoAction.php", true);
    ajax.onreadystatechange = function () {
        if (ajax.readyState === 4) {
            panels[n].innerHTML = ajax.responseText;
            addClient(n);
            validarAddPlant(n);
            validarInsert();
        }
    };
    ajax.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    ajax.send("form=" + 1 + "&tab=" + n);
}

// oculta todos los tabs.
function startPanel() {
    var a = document.getElementsByClassName("tab");
    for (let i = 1; i < a.length; i++) {
        a[i].style.display = "none";
    }
}

// mustra el siguiente tab disponible cuando se toca el +.
function addPanel() {
    var a = document.getElementsByClassName("tab");
    for (let i = 1; i < a.length; i++) {
        if (a[i].style.display == "none") {
            setPanel(i);
            a[i].style.display = "block";
            i = a.length;
        }
    }
    validarInsert();
}

// de igual manera con el form actualizar.
function addPanelN() {
    var a = document.getElementsByClassName("tab");
    for (let i = 3; i < a.length; i++) {
        if (a[i].style.display == "none") {
            displayPanel(setData, i - 3);
            addClientN(i - 3);
            validarUpdate();
            a[i].style.display = "block";
            i = a.length;
        }
    }
}

// cierra un panel especifico boton x.
function closePanel(doc, i) {
    let pedidoLenngth = JSON.parse(localStorage.getItem('pedido')).pedidoCliente.length;
    let panel = document.getElementsByClassName("tabPanel");
    if (i == 1 && pedidoLenngth == 3) {
        document.getElementsByClassName("tab")[i + 1].style.display = "none";
        panel[i + 1].innerHTML = "";
        quitClient(i);
        pintar(i);
        let pedido = JSON.parse(localStorage.getItem('pedido'));
        document.getElementsByClassName('cliente')[i].value = pedido.pedidoCliente[i].idCliente;
        localStorage.setItem('pedido',JSON.stringify(pedido));
    } else {
        doc.parentNode.style.display = "none";
        quitClient(i);
    }
    validarInsert();
    showPanel(0);
}


function closePanelN(doc, i) {
    let pedidoLenngth = JSON.parse(localStorage.getItem('pedido')).pedidoCliente.length;
    if (i == 1 && pedidoLenngth == 3) {
        document.getElementsByClassName("tab")[i + 4].style.display = "none";
        displayPanel(setData, i);
    } else {
        doc.parentNode.style.display = "none";
    }
    quitClient(i);
    validarUpdate();
    showPanel(3);
}

// carga un panel nuevo con los datos de la base de datos.
// callback es una funcion dentro de otra para evitar la asincronia.
function displayPanel(callback, tab) {
    var index = (tab + 3);
    var panels = document.getElementsByClassName("tabPanel");
    var ajax = new nuevoAjax();
    ajax.open("POST", "../business/PedidoAction.php", true);
    ajax.onreadystatechange = function () {
        if (ajax.readyState === 4 && ajax.status === 200) {
            panels[index].innerHTML = ajax.responseText;
            setTimeout(function () {
                callback(index);
                //validarAddPlantN(tab);
            }, 200);
        }
    };
    ajax.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    ajax.send("forma=1&tab=" + tab);
}

// oculta tabs.
function displayTab(i) {
    var x = document.getElementsByClassName("tab");
    x[i + 3].style.display = "none";
}

// carga la informacion de cada cliente en el form actualizar.
function setData(i) {
    i -= 3;
    var pedido = JSON.parse(localStorage.getItem("pedido"));
    document.getElementsByClassName("nCliente")[i].value = pedido.pedidoCliente[i].idCliente;
    pintarN(i);
    validarAddPlantN(i);
}

// Manejo de localStorage INSERTAR.
// es el manejo de arrays internos y  mini cruds.

function addClient(i) {
    var idCliente = document.getElementsByClassName("cliente")[i].value;
    var pedido = localStorage.getItem("pedido");
    pedido = JSON.parse(pedido);
    var nCliente = cliente;
    nCliente.idCliente = idCliente;
    pedido.pedidoCliente.push(nCliente);
    localStorage.setItem("pedido", JSON.stringify(pedido));
}

function changeClient(i) {
    var idCliente = document.getElementsByClassName("cliente")[i].value;
    var pedido = JSON.parse(localStorage.getItem("pedido"));
    pedido.pedidoCliente[i].idCliente = idCliente;
    localStorage.setItem("pedido", JSON.stringify(pedido));
    validarInsert();
}

function quitClient(i) {
    var pedido = localStorage.getItem("pedido");
    pedido = JSON.parse(pedido);
    var arrayC = [];
    for (let j = 0; j < pedido.pedidoCliente.length; j++) {
        if (j != i) {
            arrayC.push(pedido.pedidoCliente[j]);
        }
    }
    pedido.pedidoCliente = arrayC;
    localStorage.setItem("pedido", JSON.stringify(pedido));
}

function addPlant(i) {
    var idPlanta = document.getElementsByClassName("idPlanta")[i];
    var cantidad = document.getElementsByClassName("cantidad")[i];
    var pedido = localStorage.getItem("pedido");
    pedido = JSON.parse(pedido);
    planta.idPlanta = idPlanta.value;
    planta.nombrePlanta = idPlanta.options[idPlanta.selectedIndex].text;
    planta.cantidad = cantidad.value;
    arrayPush(pedido, planta, i);
    cantidad.value = "";
    pintar(i);
    validarInsert();
    validarAddPlant(i);
}

/* Quita una planta (-) */
function quitPlant(id, i, flag) {
    arrayPop(id, i);
    if (flag) {
        validarInsert();
        pintar(i);
    } else {
        validarUpdate();
        pintarN(i);
    }
}

// push y pop para plantas.
function arrayPush(array, objeto, i) {
    var arrayp = array.pedidoCliente[i].pedidoClientePlanta;
    //var tooltip = document.getElementById("tooltip-planta");
    var flag = true;
    for (let j = 0; j < arrayp.length; j++) {
        if (arrayp[j].idPlanta == objeto.idPlanta) {
            flag = false;
        }
    }
    if (flag) {
        arrayp.push(objeto);
        array.pedidoCliente[i].pedidoClientePlanta = arrayp;
        // tooltip.className = "tooltip-text";
    } else {
        // Acá mostramos el tooltip por un par de segundos.
        var tooltip = $(`#${i}`).find("#tooltip-planta");
        tooltip.attr("data-tooltip", "La planta ya ha sido ingresada con este cliente.");
        tooltip.addClass(" active");
        // Desaparecemos el tooltip
        setTimeout(function () {
            tooltip.removeClass(" active");
        }, 3000);
    }
    localStorage.setItem("pedido", JSON.stringify(array));
}

function arrayPop(id, i) {
    var pedido = localStorage.getItem("pedido");
    var pedido = JSON.parse(pedido);
    var arrayTemp = [];
    var pedidoClientePlanta = pedido.pedidoCliente[i].pedidoClientePlanta;
    for (let j = 0; j < pedidoClientePlanta.length; j++) {
        if (pedidoClientePlanta[j].idPlanta != id) {
            arrayTemp.push(pedidoClientePlanta[j]);
        }
    }
    pedido.pedidoCliente[i].pedidoClientePlanta = arrayTemp;
    localStorage.setItem("pedido", JSON.stringify(pedido));
}

/* Pinta la tabla */
function pintar(i) {
    array = JSON.parse(localStorage.getItem("pedido")).pedidoCliente[i].pedidoClientePlanta;
    var tbody = document.getElementsByClassName("tbody")[i];
    tbody.innerHTML = "";
    for (let j = 0; j < array.length; j++) {
        tbody.innerHTML += "<tr><td>" + array[j].nombrePlanta + "</td><td>" + array[j].cantidad + "</td><td><button class='boton boton-secundario tool' onclick='quitPlant(" + array[j].idPlanta + "," + i + ",true)'><i class='fa fa-minus' aria-hidden='true'></i></button></td><tr>";
    }
}

// LocalStorage Actualizar.

function addPlantN(i) {
    var idPlanta = document.getElementsByClassName("nIdPlanta")[i];
    var cantidad = document.getElementsByClassName("nCantidad")[i];
    var pedido = localStorage.getItem("pedido");
    pedido = JSON.parse(pedido);
    planta.idPlanta = idPlanta.value;
    planta.idClientePedido = pedido.pedidoCliente[i].idPedidoCliente;
    planta.nombrePlanta = idPlanta.options[idPlanta.selectedIndex].text;
    planta.cantidad = cantidad.value;
    arrayPush(pedido, planta, i);
    cantidad.value = "";
    pintarN(i);
    validarUpdate();
    validarAddPlantN(i);
}

function addClientN(i) {
    var idCliente = document.getElementsByClassName("nCliente")[i].value;
    var pedido = localStorage.getItem("pedido");
    pedido = JSON.parse(pedido);
    var nCliente = cliente;
    nCliente.idCliente = idCliente;
    nCliente.idPedido = pedido.id;
    pedido.pedidoCliente.push(nCliente);
    localStorage.setItem("pedido", JSON.stringify(pedido));
}

function changeClientN(i) {
    var idCliente = document.getElementsByClassName("nCliente")[i].value;
    var pedido = JSON.parse(localStorage.getItem("pedido"));
    pedido.pedidoCliente[i].idCliente = idCliente;
    localStorage.setItem("pedido", JSON.stringify(pedido));
    validarUpdate();
}

function pintarN(i) {
    array = JSON.parse(localStorage.getItem("pedido")).pedidoCliente[i].pedidoClientePlanta;
    var tbody = document.getElementsByClassName("nTbody")[i];
    tbody.innerHTML = "";
    for (let j = 0; j < array.length; j++) {
        tbody.innerHTML += "<tr><td>" + array[j].nombrePlanta + "</td><td>" + array[j].cantidad + "</td><td><button class='boton boton-secundario' onclick='quitPlant(" + array[j].idPlanta + "," + i + ",false)'><i class='fa fa-minus' aria-hidden='true'></i></button></td><tr>";
    }
}

// Validaciones.

// valida que el cliente tenga plantas.
function validarPedido() {
    var pedido = JSON.parse(localStorage.getItem("pedido"));
    for (let i = 0; i < pedido.pedidoCliente.length; i++) {
        if (pedido.pedidoCliente[i].pedidoClientePlanta.length == 0) {
            return false;
        }
    }
    return true;
}

function validarAddPlant(i) {
    var cantidad = document.getElementsByClassName("cantidad")[i];
    var flag;
    if (validarNumero(cantidad.value) && cantidad.value > 0) {
        cantidad.style.borderBottom = "1px solid green";
        flag = true;
    } else {
        cantidad.style.borderBottom = "1px solid red";
        flag = false;
    }
    adminBtnAdd(!flag, i);
}

function validarAddPlantN(i) {
    var cantidad = document.getElementsByClassName("nCantidad")[i];
    var flag;
    if (validarNumero(cantidad.value) && cantidad.value > 0) {
        cantidad.style.borderBottom = "1px solid green";
        flag = true;
    } else {
        cantidad.style.borderBottom = "1px solid red";
        flag = false;
    }
    adminBtnAdd(!flag, i);
}

function validarClientes(i) {
    var mensaje = document.getElementsByClassName("mensaje")[i];
    var pedido = localStorage.getItem("pedido");
    pedido = JSON.parse(pedido);
    var pedidoCliente = pedido.pedidoCliente;
    var cont = 0;
    for (let i = 0; i < pedidoCliente.length; i++) {
        for (let j = 0; j < pedidoCliente.length; j++) {
            if (pedidoCliente[i].idCliente == pedidoCliente[j].idCliente) {
                cont++;
                if (cont > 1) { // Ya debemos asignar los tooltips
                    var tooltip = $(`#${i}`).find("#tooltip-cliente");
                    var tooltip2 = $(`#${j}`).find("#tooltip-cliente");
                    tooltip.attr("data-tooltip", "El cliente ya está dentro de este pedido.");
                    tooltip2.attr("data-tooltip", "El cliente ya está dentro de este pedido.");
                    tooltip.addClass(" active");
                    tooltip2.addClass(" active");
                } else {
                    var tooltip = $(`#${i}`).find("#tooltip-cliente");
                    var tooltip2 = $(`#${j}`).find("#tooltip-cliente");
                    tooltip.removeClass(" active");
                    tooltip2.removeClass(" active");
                }
            }
        }
        if (cont > 1) {
            //i = pedidoCliente.length;
            return false;
        }
        cont = 0;
    }
    mensaje.innerHTML = "Se deben completar todos los campos";
    return true;
}

function validarInsert() {
    var fecha = document.getElementById("fechaPedido").value;
    var bandera = true;
    bandera = bandera && !validarCampoVacio(fecha);
    bandera = bandera && validarClientes(0);
    bandera = bandera && validarPedido();
    administrarBtnInsertar(!bandera);
}

function validarUpdate() {
    var fecha = document.getElementById("nFechaPedido").value;
    var bandera = true;
    bandera = bandera && !validarCampoVacio(fecha);
    bandera = bandera && validarClientes(1);
    bandera = bandera && validarPedido();
    administrarBtnActualizar(!bandera);
}


// Administracion de botones.

/* Administra el boton + */
function adminBtnAdd(flag, i) {
    var boton = document.getElementsByClassName("add")[i];
    var color = "#007bff";
    if (flag) {
        color = "gray";
    }
    boton.disabled = flag;
    boton.style.backgroundColor = color;
}


/* Asignacion de la clase de error */
function asignarClaseError(input, tooltip, textoValidacion) {
    // if (input != null) {input.className = "input-error";}
    tooltip.setAttribute("data-tooltip", textoValidacion);
    tooltip.className += " tooltip-active";
}

/* Asignacion de la clase normal(sin errores) */

function asignarClaseExito(input, tooltip) {
    input.className = "";
    tooltip.className = "tooltip";
}  

/* Muestra el modal de agregar  */
function mostrarModalAgregar(){
    limpiar();
    administrarBtnInsertar(true);
    $('#modalAgregar').modal('show'); 
    $('#modalAgregar').on('shown.bs.modal', function () {
//      $('#nombreComun').trigger('focus');
        levantarForm();
    });
  }