-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 20-10-2018 a las 02:19:50
-- Versión del servidor: 10.1.34-MariaDB
-- Versión de PHP: 7.2.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `dbornexp`
--

DELIMITER $$
--
-- Procedimientos
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `actualizarSeccion` (IN `codigo` INT, IN `numeroseccion` INT, IN `numerolote` INT)  NO SQL
UPDATE `tbseccion` SET `numeroseccion`=numeroseccion,`numerolote`=numerolote WHERE `idseccion`=codigo$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `actualizarSuministro` (IN `idSuministro` INT, IN `tipoSuministro` INT, IN `nombre` VARCHAR(50), IN `cantidad` VARCHAR(50))  NO SQL
UPDATE `tbsuministro` SET `tipoSuministro`=tipoSuministro,`nombre`=nombre,`cantidad`=cantidad WHERE tbsuministro.idSuministro =idSuministro$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `filtrarportiposuministro` (IN `codigo` INT)  NO SQL
SELECT `idSuministro`, `tipoSuministro`, `nombre`, `cantidad` FROM `tbsuministro` WHERE tbsuministro.tipoSuministro = codigo$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `insertarSeccion` (IN `idseccion` INT, IN `numeroseccion` INT, IN `numerolote` INT)  NO SQL
INSERT INTO `tbseccion`(`idseccion`, `numeroseccion`, `numerolote`) VALUES (idseccion,numeroseccion,numerolote)$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `insertarSuministro` (IN `idSuministro` INT, IN `tipoSuministro` INT, IN `nombre` VARCHAR(50), IN `cantidad` VARCHAR(50))  NO SQL
INSERT INTO `tbsuministro`(`idSuministro`, `tipoSuministro`, `nombre`, `cantidad`) VALUES (idSuministro,tipoSuministro,nombre,cantidad)$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbcliente`
--

CREATE TABLE `tbcliente` (
  `cedula` varchar(20) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `apellido1` varchar(50) NOT NULL,
  `apellido2` varchar(50) NOT NULL,
  `direccion` varchar(100) NOT NULL,
  `telefono` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tbcliente`
--

INSERT INTO `tbcliente` (`cedula`, `nombre`, `apellido1`, `apellido2`, `direccion`, `telefono`) VALUES
('4545454', 'Fernando', 'Torres', 'Tres', 'cari', 874),
('52827457154', 'Christopher Jose', 'Zuniga', 'Salazar', 'Cariari', 4844564),
('7024801245', 'Tomas', 'Torres', 'Hurtado', 'Guapiles', 87404151),
('702630248', 'Luis Fernando', 'Zuñiga', 'Zuñiga', 'La Victoria, Horquetas, Heredia', 86984086);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbcolaboradores`
--

CREATE TABLE `tbcolaboradores` (
  `cedula` int(20) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `apellido1` varchar(100) NOT NULL,
  `apellido2` varchar(100) NOT NULL,
  `direccion` varchar(100) NOT NULL,
  `telefono` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tbcolaboradores`
--

INSERT INTO `tbcolaboradores` (`cedula`, `nombre`, `apellido1`, `apellido2`, `direccion`, `telefono`) VALUES
(1231235, 'd', 'asd', 'asd', 'asd', 13),
(1231241, 'Luis', 'Zuñiga', 'Zuñiga', 'La Victoria, Horquetas, Heredia', 123123123),
(8405450, 'ad', 'asd', 'asd', 'asd', 5454),
(106090135, 'Fernando', 'Torres', 'Murillo', 'Cariari, guapiles', 87405612),
(701630482, 'Fernando', 'Z', 'Salazar', 'La Victoria, Horquetas, Heredia', 86984086),
(702450234, 'Maria', 'Salazar', 'Leiva', 'Cariari, Pococi', 87405612),
(702450236, 'Tomas', 'Torres', 'Tontera', 'Guapiles', 87405623),
(702450245, 'Fernando', 'Torres', 'Arronz', 'ad', 87012451),
(702560248, 'Fernando', 'Gaitan', 'Torres', 'Guapiles', 87405623),
(702630247, 'Torres', 'Mora ', 'Tonas', 'La Victoria, Horquetas, Heredia', 87402623),
(704520124, 'Reinaldo', 'Torres', 'Marcoele', 'Cariari', 87404512),
(875454545, 'asda', 'da', 'da', 'da', 123123),
(2147483647, 'asd', 'asd', 'asd', 'asd', 123);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tblote`
--

CREATE TABLE `tblote` (
  `idlote` int(11) NOT NULL,
  `numerolote` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tblote`
--

INSERT INTO `tblote` (`idlote`, `numerolote`) VALUES
(1, 1),
(2, 2),
(3, 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbplanta`
--

CREATE TABLE `tbplanta` (
  `idPlanta` int(11) NOT NULL,
  `nombreComun` varchar(50) NOT NULL,
  `nombreCientifico` varchar(50) NOT NULL,
  `cantidad` int(11) NOT NULL,
  `precioUnitario` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tbplanta`
--

INSERT INTO `tbplanta` (`idPlanta`, `nombreComun`, `nombreCientifico`, `cantidad`, `precioUnitario`) VALUES
(1, 'Guaria', 'Guarianthe Skinneri', 1233, 20.35),
(2, 'Zamia', 'Zamia Skinneri', 856, 19.75),
(3, 'Dianella', 'Dianella Tasmanica', 324, 20.2),
(4, 'liriio', 'Lirius-gonsales', 60, 5);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbquimico`
--

CREATE TABLE `tbquimico` (
  `idquimico` int(11) NOT NULL,
  `nombre` varchar(35) NOT NULL,
  `cantidad` int(11) NOT NULL,
  `precio` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tbquimico`
--

INSERT INTO `tbquimico` (`idquimico`, `nombre`, `cantidad`, `precio`) VALUES
(3, 'fghjk', 8658578, 895759);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbseccion`
--

CREATE TABLE `tbseccion` (
  `idseccion` int(11) NOT NULL,
  `numeroseccion` int(11) NOT NULL,
  `numerolote` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tbseccion`
--

INSERT INTO `tbseccion` (`idseccion`, `numeroseccion`, `numerolote`) VALUES
(0, 13, 1),
(1, 2, 2),
(2, 1, 2),
(3, 6, 1),
(4, 12, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbsuministro`
--

CREATE TABLE `tbsuministro` (
  `idSuministro` int(11) NOT NULL,
  `tipoSuministro` int(11) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `cantidad` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tbsuministro`
--

INSERT INTO `tbsuministro` (`idSuministro`, `tipoSuministro`, `nombre`, `cantidad`) VALUES
(0, 1, 'Agua', '54'),
(12, 3, 'gasolina', '5 galones'),
(15, 3, 'prueba', '17 kilos');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbtiposuministro`
--

CREATE TABLE `tbtiposuministro` (
  `idtiposuministro` int(11) NOT NULL,
  `nombre` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tbtiposuministro`
--

INSERT INTO `tbtiposuministro` (`idtiposuministro`, `nombre`) VALUES
(1, 'Finca'),
(2, 'Empacadora'),
(3, 'Riego'),
(4, 'prueba');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `tbcliente`
--
ALTER TABLE `tbcliente`
  ADD PRIMARY KEY (`cedula`);

--
-- Indices de la tabla `tbcolaboradores`
--
ALTER TABLE `tbcolaboradores`
  ADD PRIMARY KEY (`cedula`);

--
-- Indices de la tabla `tblote`
--
ALTER TABLE `tblote`
  ADD PRIMARY KEY (`idlote`);

--
-- Indices de la tabla `tbplanta`
--
ALTER TABLE `tbplanta`
  ADD PRIMARY KEY (`idPlanta`);

--
-- Indices de la tabla `tbquimico`
--
ALTER TABLE `tbquimico`
  ADD PRIMARY KEY (`idquimico`);

--
-- Indices de la tabla `tbseccion`
--
ALTER TABLE `tbseccion`
  ADD PRIMARY KEY (`idseccion`),
  ADD KEY `numerolote` (`numerolote`);

--
-- Indices de la tabla `tbsuministro`
--
ALTER TABLE `tbsuministro`
  ADD PRIMARY KEY (`idSuministro`),
  ADD KEY `tipoSuministro` (`tipoSuministro`);

--
-- Indices de la tabla `tbtiposuministro`
--
ALTER TABLE `tbtiposuministro`
  ADD PRIMARY KEY (`idtiposuministro`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `tblote`
--
ALTER TABLE `tblote`
  MODIFY `idlote` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `tbquimico`
--
ALTER TABLE `tbquimico`
  MODIFY `idquimico` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `tbseccion`
--
ALTER TABLE `tbseccion`
  MODIFY `idseccion` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `tbsuministro`
--
ALTER TABLE `tbsuministro`
  MODIFY `idSuministro` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT de la tabla `tbtiposuministro`
--
ALTER TABLE `tbtiposuministro`
  MODIFY `idtiposuministro` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `tbseccion`
--
ALTER TABLE `tbseccion`
  ADD CONSTRAINT `tbseccion_ibfk_1` FOREIGN KEY (`numerolote`) REFERENCES `tblote` (`idlote`);

--
-- Filtros para la tabla `tbsuministro`
--
ALTER TABLE `tbsuministro`
  ADD CONSTRAINT `tbsuministro_ibfk_1` FOREIGN KEY (`tipoSuministro`) REFERENCES `tbtiposuministro` (`idtiposuministro`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
