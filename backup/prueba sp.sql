
/*Instrucciones:
 1- Copiar todo el archivo.
 2- descomentar la sentencia USE <nombre de la base de datos a la que va a ingresar los SP>
 3- Ejecutar dicho query.*/



/*INICIO*/

-- Selecciona la base de datos(descomentar)

-- USE dbornexp;
-- USE unagrupo3;

/*Elimina todos los sp*/

--  DELETE FROM mysql.proc WHERE db = 'dbornexp' AND type = 'PROCEDURE';

/*Crea todos los sp*/


DELIMITER $$
CREATE PROCEDURE `sp_agregar_clientepedido`(IN `idpedido` INT(11), IN `idcliente` VARCHAR(20))
    NO SQL
INSERT INTO tbpedidocliente VALUES (null,idpedido,idcliente)$$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE `sp_agregar_plantapedido`(IN `idplanta` INT(11), IN `idpedidocliente` INT(11), IN `cantidad` INT(11))
    NO SQL
INSERT INTO tbpedidoclienteplanta VALUES (null,idplanta,idpedidocliente,cantidad)$$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE `sp_eliminar_pedido`(IN `idpedido` INT)
    NO SQL
DELETE FROM tbpedido WHERE tbpedido.idpedido = idpedido$$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE `sp_cambiar_pswd`(IN `id` INT, IN `actual` VARCHAR(16), IN `nueva` VARCHAR(16))
    NO SQL
BEGIN
SET @pswdok := (SELECT tbusuario.pass = actual FROM tbusuario LIMIT 1);
SET @idok := (SELECT tbusuario.idusuario = id FROM tbusuario LIMIT 1);
IF(@pswdok AND @idok) THEN
UPDATE tbusuario SET tbusuario.pass = nueva WHERE tbusuario.idusuario =  id;
SET @res = 1;
ELSEIF (NOT @idok) THEN
SET @res = 0;
ELSE
SET @res = 2;
END IF;
SELECT @res;
END$$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE `sp_eliminar_colaborador`(IN `ced` INT)
    NO SQL
UPDATE tbcolaborador SET tbcolaborador.estado = 0 WHERE tbcolaborador.cedula = ced$$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE `sp_eliminar_plantaseccion`(IN `nlote` INT(11), IN `nseccion` INT(11), IN `nplanta` INT(11))
    NO SQL
BEGIN
DELETE FROM tbplantaseccion WHERE tbplantaseccion.numerolote = nlote AND tbplantaseccion.numeroseccion = nseccion AND tbplantaseccion.idplanta = nplanta;
SET @cantidad := (SELECT tbplantaseccion.cantidadplantas FROM tbplantaseccion WHERE tbplantaseccion.numerolote = nlote AND tbplantaseccion.numeroseccion = nseccion AND tbplantaseccion.idplanta = nplanta);
UPDATE tbplanta SET tbplanta.cantidad = (tbplanta.cantidad - @cantidad);
END$$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE `sp_eliminar_fumigacion`(IN `idfumigacion` INT)
    NO SQL
DELETE FROM tbfumigacion WHERE tbfumigacion.idfumigacion = idfumigacion$$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE `sp_eliminar_acodo`(IN `idacodo` INT)
    NO SQL
DELETE FROM tbacodo WHERE tbacodo.idacodo = idacodo$$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE `sp_eliminar_cliente`(IN `id` INT(11))
    NO SQL
BEGIN
SET @cont := (SELECT COUNT(*) FROM tbpedidocliente WHERE tbpedidocliente.idcliente = id);
IF(@cont = 0) THEN 
DELETE FROM tbcliente WHERE tbcliente.idcliente = id;
ELSE
UPDATE tbcliente SET tbcliente.estado = 0 WHERE tbcliente.idcliente = id;
END IF;
END$$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE `sp_eliminar_planta`(IN `id` INT(11))
    NO SQL
BEGIN
SET @contps := (SELECT COUNT(*) FROM tbplantaseccion WHERE tbplantaseccion.idplanta = id);
SET @contpp := (SELECT COUNT(*) FROM tbpedidoclienteplanta WHERE tbpedidoclienteplanta.idplanta = id);
IF(@contps = 0 AND @contpp = 0) THEN
DELETE FROM tbplanta WHERE idplanta = id;
ELSE
UPDATE tbplanta SET tbplanta.estado = 0 WHERE tbplanta.idplanta = id;
END IF;
END$$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE `sp_eliminar_quimico`(IN `id` INT(11))
    NO SQL
UPDATE tbquimico SET tbquimico.estado = 0 WHERE tbquimico.idquimico = id$$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE `sp_eliminar_suministro`(IN `id` INT(11))
    NO SQL
DELETE FROM tbsuministro WHERE idsuministro=id$$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE `sp_eliminar_tipoquimico`(IN `idtipoquimico` INT(11))
    NO SQL
BEGIN
SET @cont := (SELECT COUNT(*) FROM tbquimico WHERE tbquimico.idtipoquimico = idtipoquimico);
IF(@cont = 0) THEN
DELETE FROM tbtipoquimico WHERE tbtipoquimico.idtipoquimico = idtipoquimico;
ELSE
UPDATE tbtipoquimico SET tbtipoquimico.estado = 0 WHERE tbtipoquimico.idtipoquimico = idtipoquimico;
END IF;
END$$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE `sp_get_cantidad_plantas`(IN `id` INT(11))
    NO SQL
SELECT cantidad FROM tbplanta WHERE idplanta=id$$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE `sp_get_cantidad_quimico`(IN `id` INT(11))
    NO SQL
SELECT cantidad FROM tbquimico WHERE idquimico = id$$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE `sp_get_cosecha`(IN `fechaactual` DATE)
    NO SQL
SELECT * FROM tbplantaseccion WHERE (SELECT DATEDIFF(fechaextraccion,fechaactual) < 10) AND (SELECT DATEDIFF(fechaextraccion,fechaactual) >= 0)$$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE `sp_get_nombre_comun`(IN `id` INT(11))
    NO SQL
SELECT nombrecomun FROM tbplanta WHERE idplanta = id$$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE `sp_get_plantasloteseccion`(IN `nplanta` INT(11), IN `nseccion` INT(11), IN `nlote` INT(11))
    NO SQL
SELECT cantidadplantas FROM tbplantaseccion WHERE idplanta= nplanta AND numeroseccion= nseccion AND numerolote= nlote$$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE `sp_insertar_acodo`(IN `idlote` INT, IN `idseccion` INT, IN `idplanta` INT, IN `cantidad` INT, IN `fecha` DATE)
    NO SQL
INSERT INTO tbacodo VALUES(null, idlote, idseccion, idplanta, cantidad, fecha)$$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE `sp_insertar_cliente`(IN `ced` VARCHAR(20), IN `nomb` VARCHAR(50), IN `ape1` VARCHAR(50), IN `ape2` VARCHAR(50), IN `direc` VARCHAR(100), IN `tel` INT(11))
    NO SQL
BEGIN
SET @idcliente := (SELECT tbcliente.idcliente FROM tbcliente WHERE tbcliente.cedula = ced);
SET @contpc := (SELECT COUNT(*) FROM tbpedidocliente WHERE tbpedidocliente.idcliente = @idcliente);
SET @estado := (SELECT tbcliente.estado FROM tbcliente WHERE tbcliente.idcliente = @idcliente);
SET @contc := (SELECT COUNT(*) FROM tbcliente WHERE tbcliente.idcliente = @idcliente);
IF(@contpc = 0 AND @contc = 0) THEN
INSERT INTO tbcliente VALUES (null,ced,nomb,ape1,ape2,direc,tel,1);
ELSEIF(@contc = 1 AND @estado = 1) THEN
INSERT INTO tbcliente VALUES (null,ced,nomb,ape1,ape2,direc,tel,1);
ELSE
UPDATE tbcliente SET tbcliente.estado = 1, tbcliente.nombre = nomb, tbcliente.apellido1 = ape1, tbcliente.apellido2 = ape2, tbcliente.direccion = direc, tbcliente.telefono = tel WHERE tbcliente.idcliente = @idcliente;
END IF;
END$$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE `sp_insertar_colaborador`(IN `ced` INT(20), IN `nomb` VARCHAR(100), IN `ape1` VARCHAR(100), IN `ape2` VARCHAR(100), IN `direc` VARCHAR(100), IN `tel` INT(11))
    NO SQL
BEGIN
SET @cont := (SELECT COUNT(*) FROM tbcolaborador WHERE ced = tbcolaborador.cedula && tbcolaborador.estado = 0);
SET @temp := (SELECT COUNT(*) FROM tbcolaborador WHERE ced = tbcolaborador.cedula && tbcolaborador.estado = 1);

IF(@cont = 1 && @temp = 0) THEN
UPDATE tbcolaborador SET tbcolaborador.estado = 1, tbcolaborador.cedula = ced, tbcolaborador.nombre = nomb, tbcolaborador.apellido1 = ape1, tbcolaborador.apellido2 = ape2, tbcolaborador.direccion = direc, tbcolaborador.telefono = tel WHERE tbcolaborador.cedula = ced;
ELSE
INSERT INTO tbcolaborador VALUES (null,ced,nomb,ape1,ape2,direc,tel,1);
END IF;
END$$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE `sp_insertar_fumigacion`(IN `idseccion` INT, IN `idlote` INT, IN `idquimico` INT, IN `idcolaborador` INT, IN `dosis` INT, IN `fechafumigacion` DATE)
    NO SQL
INSERT INTO tbfumigacion VALUES(null, idseccion, idlote, idquimico, idcolaborador, dosis, fechafumigacion)$$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE `sp_insertar_pedido`(IN `montototal` FLOAT, IN `fechaenvio` DATE)
    NO SQL
INSERT INTO tbpedido VALUES(null,montototal, fechaenvio)$$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE `sp_insertar_pedidocliente`(IN `idpedido` INT, IN `idcliente` INT)
    NO SQL
INSERT INTO tbpedidocliente VALUES(null, idpedido, idcliente)$$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE `sp_insertar_pedidoclienteplanta`(IN `idplanta` INT, IN `idpedidocliente` INT, IN `cantidad` INT)
    NO SQL
INSERT INTO tbpedidoclienteplanta VALUES(null, idplanta, idpedidocliente, cantidad)$$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE `sp_insertar_planta`(IN `nombrecomun` VARCHAR(50), IN `nombrecientifico` VARCHAR(50), IN `cantidad` INT(11), IN `precio` FLOAT)
    NO SQL
BEGIN
SET @idplanta := (SELECT tbplanta.idplanta FROM tbplanta WHERE tbplanta.nombrecomun = nombrecomun);
SET @estado := (SELECT tbplanta.estado FROM tbplanta WHERE tbplanta.idplanta = @idplanta);
SET @contp := (SELECT COUNT(*) FROM tbplanta WHERE tbplanta.idplanta = @idplanta);
SET @contps := (SELECT COUNT(*) FROM tbplantaseccion WHERE tbplantaseccion.idplanta = @idplanta);
SET @contpp := (SELECT COUNT(*) FROM tbpedidoclienteplanta WHERE tbpedidoclienteplanta.idplanta = @idplanta);
IF(@contps = 0 AND @contpp = 0 AND @contp = 0) THEN
INSERT INTO tbplanta VALUES (null,nombrecomun,nombrecientifico,cantidad,precio,1);
ELSEIF(@contp = 1 AND @estado = 1) THEN
INSERT INTO tbplanta VALUES
(null,nombrecomun,nombrecientifico,cantidad,precio,1);
ELSE
UPDATE tbplanta SET tbplanta.nombrecientifico = nombrecientifico, tbplanta.cantidad = cantidad, tbplanta.preciounitario = precio, tbplanta.estado = 1 WHERE tbplanta.idplanta = @idplanta;
END IF;
END$$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE `sp_insertar_plantaseccion`(IN `idplanta` INT(11), IN `idseccion` INT(11), IN `idlote` INT(11), IN `cantidad` INT(11), IN `fechasiembra` DATE, IN `fechaextraccion` DATE)
    NO SQL
BEGIN
 INSERT INTO tbplantaseccion VALUES(idplanta,idseccion,idlote,cantidad,fechasiembra,fechaextraccion);
SET @cantidad := (SELECT tbplanta.cantidad FROM tbplanta WHERE tbplanta.idplanta = idplanta);
SET @cantidad = (cantidad+@cantidad);
UPDATE tbplanta SET tbplanta.cantidad = @cantidad WHERE tbplanta.idplanta = idplanta;
END$$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE `sp_insertar_quimico`(IN `nombre` VARCHAR(35), IN `idtipoquimico` INT(11), IN `toxicidad` VARCHAR(50), IN `cantidad` INT(11), IN `precio` INT(11))
    NO SQL
BEGIN
SET @cont := (SELECT COUNT(*) FROM tbquimico WHERE nombre = tbquimico.nombre && tbquimico.estado = 0);
SET @temp := (SELECT COUNT(*) FROM tbquimico WHERE nombre = tbquimico.nombre && tbquimico.estado = 1);

IF(@cont = 1 && @temp = 0) THEN
UPDATE tbquimico SET tbquimico.estado = 1, tbquimico.nombre = nombre, tbquimico.idtipoquimico = idtipoquimico, tbquimico.toxicidad = toxicidad, tbquimico.cantidad = cantidad, tbquimico.precio = precio WHERE tbquimico.nombre = nombre;
ELSE
INSERT INTO tbquimico VALUES (null,nombre,idtipoquimico,toxicidad,cantidad,precio,1);
END IF;
END$$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE `sp_insertar_suministro`(IN `nomb` VARCHAR(50), IN `cant` VARCHAR(50))
    NO SQL
INSERT INTO tbsuministro VALUES (null,'Empacadora',nomb,cant)$$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE `sp_insertar_tipoquimico`(IN `nombre` VARCHAR(30))
    NO SQL
BEGIN
SET @cont := (SELECT COUNT(*) FROM tbtipoquimico WHERE tbtipoquimico.nombre = nombre);
SET @estado := (SELECT tbtipoquimico.estado FROM tbtipoquimico WHERE tbtipoquimico.nombre = nombre);
IF(@cont = 0 OR @estado = 1) THEN
INSERT INTO tbtipoquimico VALUES(null,nombre,1);
ELSE
UPDATE tbtipoquimico SET tbtipoquimico.estado = 1 WHERE tbtipoquimico.nombre = nombre;
END IF;
END$$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE `sp_login`(IN `username` VARCHAR(16))
    NO SQL
SELECT * FROM tbusuario WHERE tbusuario.username = username$$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE `sp_modificar_acodo`(IN `idacodo` INT, IN `idplanta` INT, IN `cantidad` INT, IN `fecha` DATE)
    NO SQL
UPDATE tbacodo SET tbacodo.idplanta = idplanta, tbacodo.cantidad = cantidad, tbacodo.fecha = fecha WHERE tbacodo.idacodo = idacodo$$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE `sp_modificar_cantidad_quimico`(IN `idquimico` INT, IN `cantidad` INT)
    NO SQL
UPDATE tbquimico SET tbquimico.cantidad = cantidad WHERE tbquimico.idquimico = idquimico$$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE `sp_modificar_cliente`(IN `idcliente` INT(11), IN `ced` VARCHAR(20), IN `nomb` VARCHAR(50), IN `ape1` VARCHAR(50), IN `ape2` VARCHAR(50), IN `direc` VARCHAR(100), IN `tel` INT(11))
    NO SQL
UPDATE tbcliente SET cedula=ced,nombre=nomb,apellido1=ape1,apellido2=ape2,direccion=direc,telefono=tel WHERE tbcliente.idcliente = idcliente$$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE `sp_modificar_colaborador`(IN `ced` INT(20), IN `nomb` VARCHAR(100), IN `ape1` VARCHAR(100), IN `ape2` VARCHAR(100), IN `direc` VARCHAR(100), IN `tel` INT(11), IN `idcolaborador` INT)
    NO SQL
UPDATE tbcolaborador SET 
cedula=ced, nombre=nomb,apellido1=ape1,apellido2=ape2,direccion=direc,telefono=tel WHERE tbcolaborador.idcolaborador = idcolaborador$$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE `sp_modificar_email`(IN `email` VARCHAR(50), IN `idusuario` INT(11))
UPDATE tbusuario SET tbusuario.email = email WHERE tbusuario.idusuario = idusuario$$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE `sp_modificar_fumigacion`(IN `idfumigacion` INT, IN `dosis` INT, IN `idquimico` INT, IN `idcolaborador` INT, IN `fechafumigacion` DATE)
    NO SQL
UPDATE tbfumigacion SET tbfumigacion.dosis = dosis , tbfumigacion.idquimico = idquimico, tbfumigacion.idcolaborador = idcolaborador, tbfumigacion.fechafumigacion = fechafumigacion WHERE tbfumigacion.idfumigacion = idfumigacion$$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE `sp_modificar_idcliente`(IN `idpedidocliente` INT(11), IN `idcliente` INT(11))
    NO SQL
UPDATE tbpedidocliente SET tbpedidocliente.idcliente = idcliente WHERE tbpedidocliente.idpedidocliente = idpedidocliente$$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE `sp_modificar_notificacion`(IN `notificacion` BOOLEAN, IN `idusuario` INT(11))
    NO SQL
UPDATE tbusuario SET tbusuario.notificacion = notificacion WHERE tbusuario.idusuario = idusuario$$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE `sp_modificar_pedido`(IN `idpedido` INT, IN `montototal` DOUBLE, IN `fechaenvio` DATE)
    NO SQL
UPDATE tbpedido SET tbpedido.montototal = montototal, tbpedido.fechaenvio = fechaenvio WHERE tbpedido.idpedido = idpedido$$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE `sp_modificar_planta`(IN `id` INT(11), IN `ncomun` VARCHAR(50), IN `ncientifico` VARCHAR(50), IN `can` INT(11), IN `precio` DOUBLE)
    NO SQL
UPDATE tbplanta SET nombrecomun = ncomun,nombrecientifico = ncientifico,cantidad = can, preciounitario = precio WHERE idplanta = id$$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE `sp_modificar_plantaseccion`(IN `ncantidad` INT(11), IN `nfsiembra` DATE, IN `nfextraccion` DATE, IN `cplanta` INT(11), IN `nplanta` INT(11), IN `nseccion` INT(11), IN `nlote` INT(11))
    NO SQL
BEGIN
UPDATE tbplantaseccion SET cantidadplantas=ncantidad, fechasiembra=nfsiembra, fechaextraccion=nfextraccion WHERE idplanta=cplanta AND numeroseccion=nseccion AND numerolote=nlote;
IF(cplanta != nplanta) THEN
UPDATE tbplantaseccion SET idplanta = nplanta WHERE idplanta = cplanta;
UPDATE tbplanta SET tbplanta.cantidad = (tbplanta.cantidad - ncantidad) WHERE tbplanta.idplanta = cplanta;
UPDATE tbplanta SET tbplanta.cantidad = (tbplanta.cantidad + ncantidad) WHERE tbplanta.idplanta = nplanta;
END IF;
END$$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE `sp_modificar_quimico`(IN `idquimico` INT(11), IN `nombre` VARCHAR(35), IN `idtipquim` INT(11), IN `toxicidad` VARCHAR(50), IN `cantidad` INT(11), IN `precio` INT(11))
    NO SQL
UPDATE tbquimico SET tbquimico.nombre = nombre, tbquimico.idtipoquimico = idtipoquimico, tbquimico.toxicidad = toxicidad, tbquimico.cantidad = cantidad, tbquimico.precio = precio WHERE tbquimico.idquimico = idquimico$$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE `sp_modificar_suministro`(IN `id` INT(11), IN `tipo` INT(11), IN `nomb` VARCHAR(50), IN `cant` VARCHAR(50))
    NO SQL
UPDATE tbsuministro SET tiposuministro = tipo,nombre = nomb,cantidad = cant WHERE idsuministro = id$$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE `sp_modificar_tipoquimico`(IN `nombre` VARCHAR(30), IN `idtipoquimico` INT(11))
    NO SQL
UPDATE tbtipoquimico SET tbtipoquimico.nombre = nombre WHERE tbtipoquimico.idtipoquimico = idtipoquimico$$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE `sp_modificar_username`(IN `username` VARCHAR(16), IN `idusuario` INT(11))
    NO SQL
UPDATE tbusuario SET tbusuario.username = username WHERE tbusuario.idusuario = idusuario$$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE `sp_mostrar_acodo`(IN `idlote` INT(11), IN `idseccion` INT)
    NO SQL
SELECT tba.idacodo, tba.idlote, tba.idseccion, tba.idplanta, tba.fecha, tba.cantidad, tbp.nombrecomun FROM tbacodo AS tba INNER JOIN tbplanta AS tbp ON tba.idplanta = tbp.idplanta WHERE tba.idlote = idlote AND tba.idseccion = idseccion$$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE `sp_mostrar_clientes`()
    NO SQL
SELECT * FROM tbcliente WHERE tbcliente.estado = 1$$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE `sp_mostrar_colaboradores`()
    NO SQL
SELECT * FROM tbcolaborador WHERE tbcolaborador.estado = 1$$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE `sp_mostrar_fumigacion`(IN `idseccion` INT, IN `idlote` INT)
    NO SQL
SELECT tf.idfumigacion, tf.idseccion, tf.idlote, tf.idquimico, tq.nombre, tf.idcolaborador, tc.nombre, tf.dosis, tf.fechafumigacion FROM tbfumigacion AS tf INNER JOIN tbquimico AS tq ON tf.idquimico = tq.idquimico INNER JOIN tbcolaborador AS tc ON tf.idcolaborador = tc.idcolaborador$$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE `sp_mostrar_pedidos`()
    NO SQL
SELECT * FROM tbpedido$$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE `sp_mostrar_pedidosrecientes`(IN `fechaactual` DATE)
    NO SQL
SELECT * FROM tbpedido WHERE(SELECT DATEDIFF(tbpedido.fechaenvio,fechaactual) > 0) ORDER BY tbpedido.fechaenvio ASC$$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE `sp_mostrar_plantas`()
    NO SQL
SELECT * FROM tbplanta WHERE tbplanta.estado = 1$$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE `sp_mostrar_plantaseccion`(IN `seccion` INT(11), IN `lote` INT(11))
    NO SQL
SELECT t1.idplanta, t1.numeroseccion, t1.numerolote, t1.cantidadplantas, t1.fechasiembra, t1.fechaextraccion FROM tbplantaseccion AS t1 INNER JOIN tbplanta AS t2 ON t1.idplanta = t2.idplanta WHERE t1.numeroseccion = seccion AND t1.numerolote = lote ORDER BY t2.nombrecomun$$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE `sp_mostrar_quimicos`()
    NO SQL
SELECT tbq.idquimico, tbq.nombre, tbq.idtipoquimico, tbt.nombre, tbq.toxicidad, tbq.cantidad, tbq.precio  FROM tbquimico AS tbq INNER JOIN tbtipoquimico AS tbt ON tbq.idtipoquimico = tbt.idtipoquimico WHERE tbq.estado = 1$$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE `sp_mostrar_quimicos_agotados`()
    NO SQL
SELECT * FROM `tbquimico` WHERE cantidad <= 10 ORDER BY cantidad ASC$$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE `sp_mostrar_suministros`()
    NO SQL
SELECT * FROM tbsuministro$$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE `sp_mostrar_suministros_agotados`()
    NO SQL
SELECT * FROM tbsuministro WHERE cantidad <= 10 ORDER BY cantidad ASC$$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE `sp_mostrar_tipoquimico`()
    NO SQL
SELECT * FROM tbtipoquimico WHERE tbtipoquimico.estado = 1$$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE `sp_mostrar_tiposuministro`()
    NO SQL
SELECT * FROM tbtiposuministro$$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE `sp_mostrar_usuario`(IN `idusuario` INT)
    NO SQL
SELECT * FROM tbusuario WHERE tbusuario.idusuario = idusuario$$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE `sp_quitar_clientepedido`(IN `idpedido` INT,IN `idpedidocliente` INT)
    NO SQL
DELETE FROM tbpedidocliente WHERE tbpedidocliente.idpedido = idpedido AND tbpedidocliente.idpedidocliente = idpedidocliente$$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE `sp_quitar_plantapedido`(IN `idpedidocliente` INT, IN `idpedidoclienteplanta` INT, IN `idplanta` INT)
    NO SQL
DELETE FROM tbpedidoclienteplanta WHERE tbpedidoclienteplanta.idpedidocliente = idpedidocliente AND tbpedidoclienteplanta.idpedidoclienteplanta = idpedidoclienteplanta AND tbpedidoclienteplanta.idplanta = idplanta$$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE `sp_set_cantidad_plantas`(IN `id` INT(11), IN `can` INT(11))
    NO SQL
UPDATE tbplanta SET cantidad = can WHERE idplanta = id$$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE `sp_get_plantas_por_lote`(IN `idlote` INT)
    NO SQL
SELECT tbplantaseccion.cantidadplantas FROM tbplantaseccion WHERE tbplantaseccion.numerolote = idlote$$
DELIMITER ;


DELIMITER $$
CREATE PROCEDURE `sp_get_pass`(IN `mail` VARCHAR(50))
    NO SQL
BEGIN
SET @validmail := (SELECT tbusuario.email = mail FROM tbusuario WHERE 1);
IF(@validmail) THEN
SELECT tbusuario.pass as pass FROM tbusuario WHERE 1;
ELSE
SELECT '' as pass;
END IF;
END$$
DELIMITER ;



-- Nuevos.


DELIMITER $$
CREATE PROCEDURE `sp_get_datoscliente_pc`(IN `pedido` INT(11))
    NO SQL
BEGIN
SELECT tbpedidocliente.idpedidocliente, tbcliente.nombre, tbcliente.cedula FROM tbpedidocliente INNER JOIN tbcliente ON tbpedidocliente.idcliente = tbcliente.idcliente  WHERE tbpedidocliente.idpedido = pedido;
END$$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE `sp_get_datosplanta_pcp`(IN `cliente` INT(11))
    NO SQL
BEGIN
SELECT tbplanta.nombrecomun, tbplanta.preciounitario, tbpedidoclienteplanta.cantidad FROM tbplanta INNER JOIN tbpedidoclienteplanta ON tbplanta.idplanta = tbpedidoclienteplanta.idplanta WHERE tbpedidoclienteplanta.idpedidocliente = cliente;
END$$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE `sp_mostrar_pedidoclienteplanta`(IN `pedidocliente` INT)
    NO SQL
BEGIN
SELECT tbpcp.idpedidoclienteplanta, tbpcp.idplanta, tbpcp.idpedidocliente, tbpcp.cantidad, tbp.nombrecomun FROM tbpedidoclienteplanta as tbpcp INNER JOIN tbplanta tbp ON tbpcp.idplanta = tbp.idplanta WHERE tbpcp.idpedidocliente = pedidocliente;
END$$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE `sp_mostrar_pedidocliente`(IN `id` INT)
    NO SQL
BEGIN
SELECT * FROM tbpedidocliente WHERE idpedido =  id;
END$$
DELIMITER ;

-- FIN