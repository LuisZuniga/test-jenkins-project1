-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 11-11-2018 a las 08:45:16
-- Versión del servidor: 10.1.30-MariaDB
-- Versión de PHP: 7.1.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `dbornexp`
--

DELIMITER $$
--
-- Procedimientos
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `actualizarSeccion` (IN `codigo` INT, IN `numeroseccion` INT, IN `numerolote` INT)  NO SQL
UPDATE `tbseccion` SET `numeroseccion`=numeroseccion,`numerolote`=numerolote WHERE `idseccion`=codigo$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `actualizarSuministro` (IN `idSuministro` INT, IN `tipoSuministro` INT, IN `nombre` VARCHAR(50), IN `cantidad` VARCHAR(50))  NO SQL
UPDATE `tbsuministro` SET `tipoSuministro`=tipoSuministro,`nombre`=nombre,`cantidad`=cantidad WHERE tbsuministro.idSuministro =idSuministro$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `filtrarportiposuministro` (IN `codigo` INT)  NO SQL
SELECT `idSuministro`, `tipoSuministro`, `nombre`, `cantidad` FROM `tbsuministro` WHERE tbsuministro.tipoSuministro = codigo$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `insertarSeccion` (IN `idseccion` INT, IN `numeroseccion` INT, IN `numerolote` INT)  NO SQL
INSERT INTO `tbseccion`(`idseccion`, `numeroseccion`, `numerolote`) VALUES (idseccion,numeroseccion,numerolote)$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `insertarSuministro` (IN `idSuministro` INT, IN `tipoSuministro` INT, IN `nombre` VARCHAR(50), IN `cantidad` VARCHAR(50))  NO SQL
INSERT INTO `tbsuministro`(`idSuministro`, `tipoSuministro`, `nombre`, `cantidad`) VALUES (idSuministro,tipoSuministro,nombre,cantidad)$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbcliente`
--

CREATE TABLE `tbcliente` (
  `cedula` varchar(20) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `apellido1` varchar(50) NOT NULL,
  `apellido2` varchar(50) NOT NULL,
  `direccion` varchar(100) NOT NULL,
  `telefono` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tbcliente`
--

INSERT INTO `tbcliente` (`cedula`, `nombre`, `apellido1`, `apellido2`, `direccion`, `telefono`) VALUES
('115470662', 'Yeudi', 'Carazo', 'Prendas', 'La rambla', 71941273),
('52827457154', 'Christopher Jose', 'Zuniga', 'Salazar', 'Cariari', 48445641),
('7024801245', 'Tomas', 'Torres', 'Hurtado', 'Guapiles', 12345678);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbcolaborador`
--

CREATE TABLE `tbcolaborador` (
  `cedula` int(20) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `apellido1` varchar(100) NOT NULL,
  `apellido2` varchar(100) NOT NULL,
  `direccion` varchar(100) NOT NULL,
  `telefono` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tbcolaborador`
--

INSERT INTO `tbcolaborador` (`cedula`, `nombre`, `apellido1`, `apellido2`, `direccion`, `telefono`) VALUES
(115470662, 'Yeudi', 'Carazo', 'Prendas', 'La Rambla', 71941273),
(702560248, 'Fernando', 'Herrera', 'Torres', 'Guapiles', 87405624),
(702630247, 'Daniela', 'Mora ', 'Torres', 'La Victoria, Horquetas, Heredia', 87402623);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tblote`
--

CREATE TABLE `tblote` (
  `idlote` int(11) NOT NULL,
  `numerolote` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tblote`
--

INSERT INTO `tblote` (`idlote`, `numerolote`) VALUES
(1, 1),
(2, 2),
(3, 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbplanta`
--

CREATE TABLE `tbplanta` (
  `idPlanta` int(11) NOT NULL,
  `nombreComun` varchar(50) NOT NULL,
  `nombreCientifico` varchar(50) NOT NULL,
  `cantidad` int(11) NOT NULL,
  `precioUnitario` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tbplanta`
--

INSERT INTO `tbplanta` (`idPlanta`, `nombreComun`, `nombreCientifico`, `cantidad`, `precioUnitario`) VALUES
(1, 'Guaria', 'Guarianthe Skinneri', 1232, 20.35),
(2, 'Zamia', 'Zamia Skinneri', 870, 19.75),
(3, 'Dianella', 'Dianella Tasmanica', 324, 20.9);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbplantaseccion`
--

CREATE TABLE `tbplantaseccion` (
  `idplanta` int(11) NOT NULL,
  `numeroseccion` int(11) NOT NULL,
  `numerolote` int(11) NOT NULL,
  `cantidadplantas` int(11) NOT NULL,
  `fechasiembra` date NOT NULL,
  `fechaextraccion` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tbplantaseccion`
--

INSERT INTO `tbplantaseccion` (`idplanta`, `numeroseccion`, `numerolote`, `cantidadplantas`, `fechasiembra`, `fechaextraccion`) VALUES
(1, 3, 1, 900, '2018-02-02', '2018-12-02'),
(2, 3, 2, 1211, '2018-02-13', '2018-11-01'),
(3, 2, 1, 1200, '2018-01-15', '2018-10-30'),
(3, 3, 1, 300, '2018-01-15', '2018-10-30');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbquimico`
--

CREATE TABLE `tbquimico` (
  `idquimico` int(11) NOT NULL,
  `nombre` varchar(35) NOT NULL,
  `cantidad` int(11) NOT NULL,
  `precio` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tbquimico`
--

INSERT INTO `tbquimico` (`idquimico`, `nombre`, `cantidad`, `precio`) VALUES
(112, 'Cal', 2, 7800),
(113, 'Herbicida', 9, 3400),
(114, 'a', 1, 1),
(115, 'b', 1, 1),
(116, 'c', 3, 3),
(117, 'd', 1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbseccion`
--

CREATE TABLE `tbseccion` (
  `numeroseccion` int(11) NOT NULL,
  `numerolote` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tbseccion`
--

INSERT INTO `tbseccion` (`numeroseccion`, `numerolote`) VALUES
(2, 1),
(3, 1),
(4, 1),
(4, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbsuministro`
--

CREATE TABLE `tbsuministro` (
  `idSuministro` int(11) NOT NULL,
  `tipoSuministro` int(11) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `cantidad` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tbsuministro`
--

INSERT INTO `tbsuministro` (`idSuministro`, `tipoSuministro`, `nombre`, `cantidad`) VALUES
(1, 1, 'Rastrillo', '5'),
(3, 2, 'Pala', '3');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbtiposuministro`
--

CREATE TABLE `tbtiposuministro` (
  `idtiposuministro` int(11) NOT NULL,
  `nombre` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tbtiposuministro`
--

INSERT INTO `tbtiposuministro` (`idtiposuministro`, `nombre`) VALUES
(1, 'Finca'),
(2, 'Empacadora'),
(3, 'Riego'),
(4, 'prueba');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbusuario`
--

CREATE TABLE `tbusuario` (
  `username` varchar(16) NOT NULL,
  `password` varchar(16) NOT NULL,
  `role` varchar(16) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tbusuario`
--

INSERT INTO `tbusuario` (`username`, `password`, `role`) VALUES
('admin', 'admin', 'admin');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `tbcliente`
--
ALTER TABLE `tbcliente`
  ADD PRIMARY KEY (`cedula`);

--
-- Indices de la tabla `tbcolaborador`
--
ALTER TABLE `tbcolaborador`
  ADD PRIMARY KEY (`cedula`);

--
-- Indices de la tabla `tblote`
--
ALTER TABLE `tblote`
  ADD PRIMARY KEY (`idlote`);

--
-- Indices de la tabla `tbplanta`
--
ALTER TABLE `tbplanta`
  ADD PRIMARY KEY (`idPlanta`),
  ADD UNIQUE KEY `nombreCientifico` (`nombreCientifico`);

--
-- Indices de la tabla `tbplantaseccion`
--
ALTER TABLE `tbplantaseccion`
  ADD PRIMARY KEY (`idplanta`,`numeroseccion`,`numerolote`),
  ADD KEY `idseccion` (`numeroseccion`),
  ADD KEY `idlote` (`numerolote`);

--
-- Indices de la tabla `tbquimico`
--
ALTER TABLE `tbquimico`
  ADD PRIMARY KEY (`idquimico`),
  ADD UNIQUE KEY `nombre` (`nombre`);

--
-- Indices de la tabla `tbseccion`
--
ALTER TABLE `tbseccion`
  ADD PRIMARY KEY (`numeroseccion`,`numerolote`),
  ADD KEY `numerolote` (`numerolote`);

--
-- Indices de la tabla `tbsuministro`
--
ALTER TABLE `tbsuministro`
  ADD PRIMARY KEY (`idSuministro`),
  ADD UNIQUE KEY `nombre` (`nombre`),
  ADD KEY `tipoSuministro` (`tipoSuministro`);

--
-- Indices de la tabla `tbtiposuministro`
--
ALTER TABLE `tbtiposuministro`
  ADD PRIMARY KEY (`idtiposuministro`);

--
-- Indices de la tabla `tbusuario`
--
ALTER TABLE `tbusuario`
  ADD PRIMARY KEY (`username`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `tblote`
--
ALTER TABLE `tblote`
  MODIFY `idlote` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `tbplanta`
--
ALTER TABLE `tbplanta`
  MODIFY `idPlanta` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `tbquimico`
--
ALTER TABLE `tbquimico`
  MODIFY `idquimico` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=118;

--
-- AUTO_INCREMENT de la tabla `tbsuministro`
--
ALTER TABLE `tbsuministro`
  MODIFY `idSuministro` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `tbtiposuministro`
--
ALTER TABLE `tbtiposuministro`
  MODIFY `idtiposuministro` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `tbplantaseccion`
--
ALTER TABLE `tbplantaseccion`
  ADD CONSTRAINT `idlote` FOREIGN KEY (`numerolote`) REFERENCES `tbseccion` (`numerolote`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `idplanta` FOREIGN KEY (`idplanta`) REFERENCES `tbplanta` (`idPlanta`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `idseccion` FOREIGN KEY (`numeroseccion`) REFERENCES `tbseccion` (`numeroseccion`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `tbseccion`
--
ALTER TABLE `tbseccion`
  ADD CONSTRAINT `tbseccion_ibfk_1` FOREIGN KEY (`numerolote`) REFERENCES `tblote` (`idlote`);

--
-- Filtros para la tabla `tbsuministro`
--
ALTER TABLE `tbsuministro`
  ADD CONSTRAINT `tbsuministro_ibfk_1` FOREIGN KEY (`tipoSuministro`) REFERENCES `tbtiposuministro` (`idtiposuministro`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
