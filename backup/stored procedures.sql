
/*Instrucciones:
 1- Copiar todo el archivo.
 2- descomentar la sentencia USE <nombre de la base de datos a la que va a ingresar los SP>
 3- Ejecutar dicho query.*/



/*INICIO*/

-- Selecciona la base de datos(descomentar)

-- USE dbornexp;
-- USE unagrupo3;

/*Elimina todos los sp*/

--  DELETE FROM mysql.proc WHERE db = 'dbornexp' AND type = 'PROCEDURE';

/*Crea todos los sp*/
 
CREATE PROCEDURE `sp_eliminar_colaborador`(IN `ced` INT) NO SQL DELETE FROM tbcolaborador WHERE cedula = ced;
CREATE PROCEDURE `sp_agregar_clientepedido`(IN `idpedido` INT(11), IN `idcliente` VARCHAR(20)) NO SQL INSERT INTO tbpedidocliente VALUES (null,idpedido,idcliente); 
CREATE PROCEDURE `sp_agregar_plantapedido`(IN `idplanta` INT(11), IN `idpedidocliente` INT(11), IN `cantidad` INT(11)) NO SQL INSERT INTO tbpedidoclienteplanta VALUES (null,idplanta,idpedidocliente,cantidad);
CREATE PROCEDURE `sp_eliminar_acodo`(IN `idacodo` INT) NO SQL DELETE FROM tbacodo WHERE tbacodo.idacodo = idacodo;
CREATE PROCEDURE `sp_eliminar_fumigacion`(IN `idfumigacion` INT) NO SQL DELETE FROM tbfumigacion WHERE tbfumigacion.idfumigacion = idfumigacion;
CREATE PROCEDURE `sp_eliminar_cliente`(IN `ced` INT(11)) NO SQL DELETE FROM tbcliente WHERE cedula = ced;
CREATE PROCEDURE `sp_eliminar_pedido`(IN `idpedido` INT) NO SQL DELETE FROM tbpedido WHERE tbpedido.idpedido = idpedido;
CREATE PROCEDURE `sp_eliminar_planta`(IN `id` INT(11)) NO SQL DELETE FROM tbplanta WHERE idplanta = id;
CREATE PROCEDURE `sp_eliminar_plantaseccion`(IN `nlote` INT(11), IN `nseccion` INT(11), IN `nplanta` INT(11)) NO SQL DELETE FROM tbplantaseccion WHERE tbplantaseccion.numerolote = nlote AND tbplantaseccion.numeroseccion = nseccion AND tbplantaseccion.idplanta = nplanta;
CREATE PROCEDURE `sp_eliminar_quimico`(IN `id` INT(11)) NO SQL DELETE FROM tbquimico WHERE idquimico=id;
CREATE PROCEDURE `sp_eliminar_suministro`(IN `id` INT(11)) NO SQL DELETE FROM tbsuministro WHERE idsuministro=id;
CREATE PROCEDURE `sp_get_cantidad_plantas`(IN `id` INT(11)) NO SQL SELECT cantidad FROM tbplanta WHERE idplanta=id;
CREATE PROCEDURE `sp_get_cantidad_quimico`(IN `id` INT(11)) NO SQL SELECT cantidad FROM tbquimico WHERE idquimico = id;
CREATE PROCEDURE `sp_get_cosecha`(IN `fechaactual` DATE) NO SQL SELECT * FROM tbplantaseccion WHERE (SELECT DATEDIFF(fechaextraccion,fechaactual) < 10) AND (SELECT DATEDIFF(fechaextraccion,fechaactual) >= 0);
CREATE PROCEDURE `sp_get_nombre_comun`(IN `id` INT(11)) NO SQL SELECT nombrecomun FROM tbplanta WHERE idplanta = id;
CREATE PROCEDURE `sp_get_plantasloteseccion`(IN `nplanta` INT(11), IN `nseccion` INT(11), IN `nlote` INT(11)) NO SQL SELECT cantidadplantas FROM tbplantaseccion WHERE idplanta= nplanta AND numeroseccion= nseccion AND numerolote= nlote;
CREATE PROCEDURE `sp_insertar_acodo`(IN `idlote` INT, IN `idseccion` INT, IN `idplanta` INT, IN `cantidad` INT, IN `fecha` DATE) NO SQL INSERT INTO tbacodo VALUES(null, idlote, idseccion, idplanta, cantidad, fecha);
CREATE PROCEDURE `sp_insertar_cliente`(IN `ced` VARCHAR(20), IN `nomb` VARCHAR(50), IN `ape1` VARCHAR(50), IN `ape2` VARCHAR(50), IN `direc` VARCHAR(100), IN `tel` INT(11)) NO SQL INSERT INTO tbcliente VALUES (ced,nomb,ape1,ape2,direc,tel);
CREATE PROCEDURE `sp_insertar_colaborador`(IN `ced` INT(20), IN `nomb` VARCHAR(100), IN `ape1` VARCHAR(100), IN `ape2` VARCHAR(100), IN `direc` VARCHAR(100), IN `tel` INT(11)) NO SQL INSERT INTO tbcolaborador VALUES (ced,nomb,ape1,ape2,direc,tel);
CREATE PROCEDURE `sp_insertar_fumigacion`(IN `idseccion` INT, IN `idlote` INT, IN `idquimico` INT, IN `idcolaborador` INT, IN `dosis` INT, IN `fechafumigacion` DATE) NO SQL INSERT INTO tbfumigacion VALUES(null, idseccion, idlote, idquimico, idcolaborador, dosis, fechafumigacion);
CREATE PROCEDURE `sp_insertar_pedido`(IN `montototal` INT, IN `fechaenvio` DATE) NO SQL INSERT INTO tbpedido VALUES(null,montototal, fechaenvio);
CREATE PROCEDURE `sp_insertar_pedidocliente`(IN `idpedido` INT, IN `idcliente` INT) NO SQL INSERT INTO tbpedidocliente VALUES(null, idpedido, idcliente);
CREATE PROCEDURE `sp_insertar_pedidoclienteplanta`(IN `idplanta` INT, IN `idpedidocliente` INT, IN `cantidad` INT) NO SQL INSERT INTO tbpedidoclienteplanta VALUES(null, idplanta, idpedidocliente, cantidad);
CREATE PROCEDURE `sp_insertar_planta`(IN `nombrecomun` VARCHAR(50), IN `nombrecientifico` VARCHAR(50), IN `cantidad` INT(11), IN `precio` FLOAT) NO SQL INSERT INTO tbplanta VALUES (null,nombrecomun,nombrecientifico,cantidad,precio);
CREATE PROCEDURE `sp_insertar_plantaseccion`(IN `idplanta` INT(11), IN `idseccion` INT(11), IN `idlote` INT(11), IN `cantidad` INT(11), IN `fechasiembra` DATE, IN `fechaextraccion` DATE) NO SQL INSERT INTO tbplantaseccion VALUES(idplanta,idseccion,idlote,cantidad,fechasiembra,fechaextraccion);
CREATE PROCEDURE `sp_insertar_quimico`(IN `nombre` VARCHAR(35), IN `idtipoquimico` INT(11), IN `toxicidad` VARCHAR(50), IN `cantidad` INT(11), IN `precio` INT(11)) NO SQL INSERT INTO tbquimico VALUES (null,nombre,idtipoquimico,toxicidad,cantidad,precio);
CREATE PROCEDURE `sp_insertar_suministro`(IN `tipo` INT(11), IN `nomb` VARCHAR(50), IN `cant` VARCHAR(50)) NO SQL INSERT INTO tbsuministro VALUES (null,tipo,nomb,cant);
CREATE PROCEDURE `sp_insertar_tipoquimico`(IN `nombre` VARCHAR(30)) NO SQL INSERT INTO tbtipoquimico VALUES(null,nombre);
CREATE PROCEDURE `sp_modificar_acodo`(IN `idacodo` INT, IN `idplanta` INT, IN `cantidad` INT, IN `fecha` DATE) NO SQL UPDATE tbacodo SET tbacodo.idplanta = idplanta, tbacodo.cantidad = cantidad, tbacodo.fecha = fecha WHERE tbacodo.idacodo = idacodo;
CREATE PROCEDURE `sp_modificar_cantidad_quimico`(IN `idquimico` INT, IN `cantidad` INT) NO SQL UPDATE tbquimico SET tbquimico.cantidad = cantidad WHERE tbquimico.idquimico = idquimico;
CREATE PROCEDURE `sp_modificar_cliente`(IN `ced` VARCHAR(20), IN `nomb` VARCHAR(50), IN `ape1` VARCHAR(50), IN `ape2` VARCHAR(50), IN `direc` VARCHAR(100), IN `tel` INT(11)) NO SQL UPDATE tbcliente SET nombre=nomb,apellido1=ape1,apellido2=ape2,direccion=direc,telefono=tel WHERE cedula=ced;
CREATE PROCEDURE `sp_modificar_colaborador`(IN `ced` INT(20), IN `nomb` VARCHAR(100), IN `ape1` VARCHAR(100), IN `ape2` VARCHAR(100), IN `direc` VARCHAR(100), IN `tel` INT(11)) NO SQL UPDATE tbcolaborador SET nombre=nomb,apellido1=ape1,apellido2=ape2,direccion=direc,telefono=tel WHERE cedula=ced;
CREATE PROCEDURE `sp_modificar_fumigacion`(IN `idfumigacion` INT, IN `dosis` INT, IN `idquimico` INT, IN `idcolaborador` INT, IN `fechafumigacion` DATE) NO SQL UPDATE tbfumigacion SET tbfumigacion.dosis = dosis , tbfumigacion.idquimico = idquimico, tbfumigacion.idcolaborador = idcolaborador, tbfumigacion.fechafumigacion = fechafumigacion WHERE tbfumigacion.idfumigacion = idfumigacion; 
CREATE PROCEDURE `sp_modificar_pedido`(IN `idpedido` INT, IN `montototal` INT, IN `fechaenvio` DATE) NO SQL UPDATE tbpedido SET tbpedido.montototal = montototal, tbpedido.fechaenvio = fechaenvio WHERE tbpedido.idpedido = idpedido; 
CREATE PROCEDURE `sp_modificar_planta`(IN `id` INT(11), IN `ncomun` VARCHAR(50), IN `ncientifico` VARCHAR(50), IN `can` INT(11), IN `precio` DOUBLE) NO SQL UPDATE tbplanta SET nombrecomun = ncomun,nombrecientifico = ncientifico,cantidad = can, preciounitario = precio WHERE idplanta = id;
CREATE PROCEDURE `sp_modificar_plantaseccion`(IN `ncantidad` INT(11), IN `nfsiembra` DATE, IN `nfextraccion` DATE, IN `nplanta` INT(11), IN `nseccion` INT(11), IN `nlote` INT(11)) NO SQL UPDATE tbplantaseccion SET cantidadplantas=ncantidad, fechasiembra=nfsiembra, fechaextraccion=nfextraccion WHERE idplanta=nplanta AND numeroseccion=nseccion AND numerolote=nlote;
CREATE PROCEDURE `sp_modificar_quimico`(IN `idquimico` INT(11), IN `nombre` VARCHAR(35), IN `idtipquim` INT(11), IN `toxicidad` VARCHAR(50), IN `cantidad` INT(11), IN `precio` INT(11)) NO SQL UPDATE tbquimico SET tbquimico.nombre = nombre, tbquimico.idtipoquimico = idtipoquimico, tbquimico.toxicidad = toxicidad, tbquimico.cantidad = cantidad, tbquimico.precio = precio WHERE tbquimico.idquimico = idquimico;
CREATE PROCEDURE `sp_modificar_suministro`(IN `id` INT(11), IN `tipo` INT(11), IN `nomb` VARCHAR(50), IN `cant` VARCHAR(50)) NO SQL UPDATE tbsuministro SET tiposuministro = tipo,nombre = nomb,cantidad = cant WHERE idsuministro = id;
CREATE PROCEDURE `sp_mostrar_clientes`() NO SQL SELECT * FROM tbcliente;
CREATE PROCEDURE `sp_mostrar_colaboradores`() NO SQL SELECT * FROM tbcolaborador;
CREATE PROCEDURE `sp_mostrar_fumigacion`(IN `idseccion` INT, IN `idlote` INT) NO SQL SELECT tf.idfumigacion, tf.idseccion, tf.idlote, tf.idquimico, tq.nombre, tf.idcolaborador, tc.nombre, tf.dosis, tf.fechafumigacion FROM tbfumigacion AS tf INNER JOIN tbquimico AS tq ON tf.idquimico = tq.idquimico INNER JOIN tbcolaborador AS tc ON tf.idcolaborador = tc.cedula;
CREATE PROCEDURE `sp_mostrar_plantas`() NO SQL SELECT * FROM tbplanta;
CREATE PROCEDURE `sp_mostrar_plantaseccion`(IN `seccion` INT(11), IN `lote` INT(11)) NO SQL SELECT t1.idplanta, t1.numeroseccion, t1.numerolote, t1.cantidadplantas, t1.fechasiembra, t1.fechaextraccion FROM tbplantaseccion AS t1 INNER JOIN tbplanta AS t2 ON t1.idplanta = t2.idplanta WHERE t1.numeroseccion = seccion AND t1.numerolote = lote ORDER BY t2.nombrecomun;
CREATE PROCEDURE `sp_mostrar_quimicos`() NO SQL SELECT tbq.idquimico, tbq.nombre, tbq.idtipoquimico, tbt.nombre, tbq.toxicidad, tbq.cantidad, tbq.precio  FROM tbquimico AS tbq INNER JOIN tbtipoquimico AS tbt ON tbq.idtipoquimico = tbt.idtipoquimico;
CREATE PROCEDURE `sp_mostrar_quimicos_agotados`() NO SQL SELECT * FROM `tbquimico` WHERE cantidad <= 10 ORDER BY cantidad ASC;
CREATE PROCEDURE `sp_mostrar_suministros`() NO SQL SELECT * FROM tbsuministro;
CREATE PROCEDURE `sp_mostrar_tipoquimico`() NO SQL SELECT * FROM tbtipoquimico;
CREATE PROCEDURE `sp_mostrar_tiposuministro`() NO SQL SELECT * FROM tbtiposuministro;
CREATE PROCEDURE `sp_quitar_clientepedido`(IN `idpedido` INT,IN `idpedidocliente` INT) NO SQL DELETE FROM tbpedidocliente WHERE tbpedidocliente.idpedido = idpedido AND tbpedidocliente.idpedidocliente = idpedidocliente;
CREATE PROCEDURE `sp_quitar_plantapedido`(IN `idpedidocliente` INT, IN `idpedidoclienteplanta` INT, IN `idplanta` INT) NO SQL DELETE FROM tbpedidoclienteplanta WHERE tbpedidoclienteplanta.idpedidocliente = idpedidocliente AND tbpedidoclienteplanta.idpedidoclienteplanta = idpedidoclienteplanta AND tbpedidoclienteplanta.idplanta = idplanta;
CREATE PROCEDURE `sp_set_cantidad_plantas`(IN `id` INT(11), IN `can` INT(11)) NO SQL UPDATE tbplanta SET cantidad = can WHERE idplanta = id;
CREATE PROCEDURE `sp_mostrar_usuario`(IN `idusuario` INT) NO SQL SELECT * FROM tbusuario WHERE tbusuario.idusuario = idusuario;
CREATE PROCEDURE `sp_mostrar_suministros_agotados`() NO SQL SELECT * FROM tbsuministro WHERE cantidad <= 10 ORDER BY cantidad ASC;
CREATE PROCEDURE `sp_mostrar_acodo`(IN `idlote` INT(11), IN `idseccion` INT) NO SQL SELECT tba.idacodo, tba.idlote, tba.idseccion, tba.idplanta, tba.fecha, tba.cantidad, tbp.nombrecomun FROM tbacodo AS tba INNER JOIN tbplanta AS tbp ON tba.idplanta = tbp.idplanta WHERE tba.idlote = idlote AND tba.idseccion = idseccion;
CREATE PROCEDURE `sp_login`(IN `username` VARCHAR(16)) NO SQL SELECT * FROM tbusuario WHERE tbusuario.username = username;
CREATE PROCEDURE `sp_eliminar_tipoquimico`(IN `idtipoquimico` INT(11)) NO SQL DELETE FROM tbtipoquimico WHERE tbtipoquimico.idtipoquimico = idtipoquimico;
CREATE PROCEDURE `sp_modificar_tipoquimico`(IN `nombre` VARCHAR(30), IN `idtipoquimico` INT(11)) NO SQL UPDATE tbtipoquimico SET tbtipoquimico.nombre = nombre WHERE tbtipoquimico.idtipoquimico = idtipoquimico;
CREATE PROCEDURE `sp_modificar_username`(IN `username` VARCHAR(16), IN `idusuario` INT(11)) NO SQL UPDATE tbusuario SET tbusuario.username = username WHERE tbusuario.idusuario = idusuario;
CREATE PROCEDURE `sp_modificar_email`(IN `email` VARCHAR(50), IN `idusuario` INT(11)) UPDATE tbusuario SET tbusuario.email = email WHERE tbusuario.idusuario = idusuario;
CREATE PROCEDURE `sp_modificar_notificacion`(IN `notificacion` BOOLEAN, IN `idusuario` INT(11)) NO SQL UPDATE tbusuario SET tbusuario.notificacion = notificacion WHERE tbusuario.idusuario = idusuario;

CREATE PROCEDURE `sp_mostrar_pedidos`() NO SQL SELECT * FROM tbpedido;
CREATE PROCEDURE `sp_mostrar_pedidosrecientes`(IN `fechaactual` DATE) NO SQL SELECT * FROM tbpedido WHERE(SELECT DATEDIFF(tbpedido.fechaenvio,fechaactual) > 0) ORDER BY tbpedido.fechaenvio ASC;


-- Esta consulta tiene que ser tal cual esta.



DELIMITER $$
CREATE PROCEDURE `sp_cambiar_pswd`(IN `id` INT, IN `actual` VARCHAR(16), IN `nueva` VARCHAR(16))
    NO SQL
BEGIN
SET @pswdok := (SELECT tbusuario.pass = actual FROM tbusuario LIMIT 1);
SET @idok := (SELECT tbusuario.idusuario = id FROM tbusuario LIMIT 1);
IF(@pswdok AND @idok) THEN
UPDATE tbusuario SET tbusuario.pass = nueva WHERE tbusuario.idusuario =  id;
SET @res = 1;
ELSEIF (NOT @idok) THEN
SET @res = 0;
ELSE
SET @res = 2;
END IF;
SELECT @res;
END$$
DELIMITER ;

-- FIN.