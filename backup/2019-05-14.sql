-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 15-05-2019 a las 03:15:51
-- Versión del servidor: 10.1.30-MariaDB
-- Versión de PHP: 7.1.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `dbornexp`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbacodo`
--

CREATE TABLE `tbacodo` (
  `idacodo` int(11) NOT NULL,
  `idlote` int(11) NOT NULL,
  `idseccion` int(11) NOT NULL,
  `idplanta` int(11) NOT NULL,
  `cantidad` int(11) NOT NULL,
  `fecha` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tbacodo`
--

INSERT INTO `tbacodo` (`idacodo`, `idlote`, `idseccion`, `idplanta`, `cantidad`, `fecha`) VALUES
(3, 1, 3, 11, 9000, '2019-04-21'),
(15, 1, 3, 9, 7000, '2019-04-28');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbcliente`
--

CREATE TABLE `tbcliente` (
  `idcliente` int(11) NOT NULL,
  `cedula` varchar(20) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `apellido1` varchar(50) NOT NULL,
  `apellido2` varchar(50) NOT NULL,
  `direccion` varchar(100) NOT NULL,
  `telefono` int(11) NOT NULL,
  `estado` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tbcliente`
--

INSERT INTO `tbcliente` (`idcliente`, `cedula`, `nombre`, `apellido1`, `apellido2`, `direccion`, `telefono`, `estado`) VALUES
(1, '106090135', 'Maria', 'Salazar', 'Leiva', 'Guapiles', 87401521, 1),
(2, '702450245', 'Tomas', 'Torres', 'Castillo', 'Guadalupe', 87452151, 1),
(3, '702630248', 'Fernando', 'zuniga ', 'Salazar', '<button>HOLA</button>', 87401521, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbcolaborador`
--

CREATE TABLE `tbcolaborador` (
  `idcolaborador` int(11) NOT NULL,
  `cedula` int(20) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `apellido1` varchar(100) NOT NULL,
  `apellido2` varchar(100) NOT NULL,
  `direccion` varchar(100) NOT NULL,
  `telefono` int(11) NOT NULL,
  `estado` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tbcolaborador`
--

INSERT INTO `tbcolaborador` (`idcolaborador`, `cedula`, `nombre`, `apellido1`, `apellido2`, `direccion`, `telefono`, `estado`) VALUES
(1, 504170602, 'Christopher', '', '', 'Virgen, Heredia', 84562600, 1),
(4, 2147483647, 'Francisco', 'Porras', '', 'Virgen, Heredia', 84562600, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbfumigacion`
--

CREATE TABLE `tbfumigacion` (
  `idfumigacion` int(11) NOT NULL,
  `idseccion` int(11) NOT NULL,
  `idlote` int(11) NOT NULL,
  `idquimico` int(11) NOT NULL,
  `idcolaborador` int(20) NOT NULL,
  `dosis` int(11) NOT NULL,
  `fechafumigacion` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tbfumigacion`
--

INSERT INTO `tbfumigacion` (`idfumigacion`, `idseccion`, `idlote`, `idquimico`, `idcolaborador`, `dosis`, `fechafumigacion`) VALUES
(3, 3, 1, 2, 4, 1, '2019-05-26'),
(4, 3, 1, 3, 1, 2, '2019-05-25'),
(7, 3, 1, 2, 4, 2, '2019-06-02'),
(8, 21, 3, 4, 1, 1, '2019-05-15');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tblote`
--

CREATE TABLE `tblote` (
  `idlote` int(11) NOT NULL,
  `numerolote` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tblote`
--

INSERT INTO `tblote` (`idlote`, `numerolote`) VALUES
(1, 1),
(2, 2),
(3, 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbpedido`
--

CREATE TABLE `tbpedido` (
  `idpedido` int(11) NOT NULL,
  `montototal` float NOT NULL,
  `fechaenvio` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbpedidocliente`
--

CREATE TABLE `tbpedidocliente` (
  `idpedidocliente` int(11) NOT NULL,
  `idpedido` int(11) NOT NULL,
  `idcliente` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbpedidoclienteplanta`
--

CREATE TABLE `tbpedidoclienteplanta` (
  `idpedidoclienteplanta` int(11) NOT NULL,
  `idplanta` int(11) NOT NULL,
  `idpedidocliente` int(11) NOT NULL,
  `cantidad` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbplanta`
--

CREATE TABLE `tbplanta` (
  `idplanta` int(11) NOT NULL,
  `nombrecomun` varchar(50) NOT NULL,
  `nombrecientifico` varchar(50) NOT NULL,
  `cantidad` int(11) NOT NULL,
  `preciounitario` float NOT NULL,
  `estado` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tbplanta`
--

INSERT INTO `tbplanta` (`idplanta`, `nombrecomun`, `nombrecientifico`, `cantidad`, `preciounitario`, `estado`) VALUES
(9, 'aztec grass', 'LIRIOPE MUSCARI', 100, 1.7, 1),
(10, 'mini mondo', 'OPHIOPOGON PLANISCARPUS', 100, 0.99, 1),
(11, 'bolero tricor', 'CORDYLINE TERMINALES', 0, 0.98, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbplantaseccion`
--

CREATE TABLE `tbplantaseccion` (
  `idplanta` int(11) NOT NULL,
  `numeroseccion` int(11) NOT NULL,
  `numerolote` int(11) NOT NULL,
  `cantidadplantas` int(11) NOT NULL,
  `fechasiembra` date NOT NULL,
  `fechaextraccion` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tbplantaseccion`
--

INSERT INTO `tbplantaseccion` (`idplanta`, `numeroseccion`, `numerolote`, `cantidadplantas`, `fechasiembra`, `fechaextraccion`) VALUES
(10, 21, 3, 15, '2019-05-13', '2019-05-31'),
(10, 23, 1, 100, '2019-05-14', '2019-05-15');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbquimico`
--

CREATE TABLE `tbquimico` (
  `idquimico` int(11) NOT NULL,
  `nombre` varchar(35) NOT NULL,
  `idtipoquimico` int(11) NOT NULL,
  `toxicidad` varchar(50) NOT NULL,
  `cantidad` int(11) NOT NULL,
  `precio` int(11) NOT NULL,
  `estado` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tbquimico`
--

INSERT INTO `tbquimico` (`idquimico`, `nombre`, `idtipoquimico`, `toxicidad`, `cantidad`, `precio`, `estado`) VALUES
(2, 'Cal', 3, '#ff0000', 37, 13, 1),
(3, 'Gramoson', 2, '#ffffff', 98, 1500, 1),
(4, 'X', 1, '#ff0000', 0, 1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbseccion`
--

CREATE TABLE `tbseccion` (
  `numeroseccion` int(11) NOT NULL,
  `numerolote` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tbseccion`
--

INSERT INTO `tbseccion` (`numeroseccion`, `numerolote`) VALUES
(1, 1),
(1, 2),
(1, 3),
(2, 1),
(2, 2),
(2, 3),
(3, 1),
(3, 2),
(3, 3),
(4, 1),
(4, 2),
(4, 3),
(5, 1),
(5, 2),
(5, 3),
(6, 1),
(6, 2),
(6, 3),
(7, 1),
(7, 2),
(7, 3),
(8, 1),
(8, 2),
(8, 3),
(9, 1),
(9, 2),
(9, 3),
(10, 1),
(10, 2),
(10, 3),
(11, 1),
(11, 2),
(11, 3),
(12, 1),
(12, 2),
(12, 3),
(13, 1),
(13, 2),
(13, 3),
(14, 1),
(14, 2),
(14, 3),
(15, 1),
(15, 3),
(16, 1),
(16, 3),
(17, 1),
(17, 3),
(18, 1),
(18, 3),
(19, 1),
(19, 3),
(20, 1),
(20, 3),
(21, 1),
(21, 3),
(22, 1),
(22, 3),
(23, 1),
(24, 1),
(25, 1),
(26, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbsuministro`
--

CREATE TABLE `tbsuministro` (
  `idsuministro` int(11) NOT NULL,
  `tiposuministro` varchar(25) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `cantidad` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tbsuministro`
--

INSERT INTO `tbsuministro` (`idsuministro`, `tiposuministro`, `nombre`, `cantidad`) VALUES
(9, 'Empacadora', 'Rastrillo', '4'),
(10, 'Empacadora', 'Guantes', '2'),
(11, 'Empacadora', 'Pala', '6'),
(12, 'Empacadora', 'Cajas', '12'),
(13, 'Empacadora', 'a', '1');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbtipoquimico`
--

CREATE TABLE `tbtipoquimico` (
  `idtipoquimico` int(11) NOT NULL,
  `nombre` varchar(30) NOT NULL,
  `estado` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tbtipoquimico`
--

INSERT INTO `tbtipoquimico` (`idtipoquimico`, `nombre`, `estado`) VALUES
(1, 'Bactericida', 1),
(2, 'Herbicida', 1),
(3, 'Fungicida', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbusuario`
--

CREATE TABLE `tbusuario` (
  `idusuario` int(11) NOT NULL,
  `username` varchar(16) NOT NULL,
  `pass` varchar(16) NOT NULL,
  `role` varchar(16) NOT NULL,
  `email` varchar(50) NOT NULL,
  `notificacion` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tbusuario`
--

INSERT INTO `tbusuario` (`idusuario`, `username`, `pass`, `role`, `email`, `notificacion`) VALUES
(1, 'admin', 'admin', 'admin', 'yeucprendas@gmail.es', 1);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `tbacodo`
--
ALTER TABLE `tbacodo`
  ADD PRIMARY KEY (`idacodo`),
  ADD UNIQUE KEY `idplanta` (`idplanta`),
  ADD KEY `tbacodo_ibfk_1` (`idlote`),
  ADD KEY `tbacodo_ibfk_3` (`idseccion`);

--
-- Indices de la tabla `tbcliente`
--
ALTER TABLE `tbcliente`
  ADD PRIMARY KEY (`idcliente`),
  ADD UNIQUE KEY `cedula` (`cedula`);

--
-- Indices de la tabla `tbcolaborador`
--
ALTER TABLE `tbcolaborador`
  ADD PRIMARY KEY (`idcolaborador`),
  ADD UNIQUE KEY `cedula` (`cedula`);

--
-- Indices de la tabla `tbfumigacion`
--
ALTER TABLE `tbfumigacion`
  ADD PRIMARY KEY (`idfumigacion`),
  ADD KEY `idlote` (`idlote`),
  ADD KEY `idseccion` (`idseccion`),
  ADD KEY `idquimico` (`idquimico`),
  ADD KEY `tbcolaborador` (`idcolaborador`) USING BTREE;

--
-- Indices de la tabla `tblote`
--
ALTER TABLE `tblote`
  ADD PRIMARY KEY (`idlote`);

--
-- Indices de la tabla `tbpedido`
--
ALTER TABLE `tbpedido`
  ADD PRIMARY KEY (`idpedido`);

--
-- Indices de la tabla `tbpedidocliente`
--
ALTER TABLE `tbpedidocliente`
  ADD PRIMARY KEY (`idpedidocliente`),
  ADD KEY `tbpedidocliente_ibfk_1` (`idpedido`),
  ADD KEY `cliente` (`idcliente`);

--
-- Indices de la tabla `tbpedidoclienteplanta`
--
ALTER TABLE `tbpedidoclienteplanta`
  ADD PRIMARY KEY (`idpedidoclienteplanta`),
  ADD KEY `tbpedidoclienteplanta_ibfk_1` (`idplanta`),
  ADD KEY `tbpedidoclienteplanta_ibfk_2` (`idpedidocliente`);

--
-- Indices de la tabla `tbplanta`
--
ALTER TABLE `tbplanta`
  ADD PRIMARY KEY (`idplanta`),
  ADD UNIQUE KEY `nombrecomun` (`nombrecomun`);

--
-- Indices de la tabla `tbplantaseccion`
--
ALTER TABLE `tbplantaseccion`
  ADD PRIMARY KEY (`idplanta`,`numeroseccion`,`numerolote`),
  ADD KEY `idlote` (`numerolote`),
  ADD KEY `idseccion` (`numeroseccion`);

--
-- Indices de la tabla `tbquimico`
--
ALTER TABLE `tbquimico`
  ADD PRIMARY KEY (`idquimico`),
  ADD UNIQUE KEY `nombre` (`nombre`),
  ADD KEY `tipoquimico` (`idtipoquimico`);

--
-- Indices de la tabla `tbseccion`
--
ALTER TABLE `tbseccion`
  ADD PRIMARY KEY (`numeroseccion`,`numerolote`),
  ADD KEY `numerolote` (`numerolote`);

--
-- Indices de la tabla `tbsuministro`
--
ALTER TABLE `tbsuministro`
  ADD PRIMARY KEY (`idsuministro`),
  ADD UNIQUE KEY `nombre` (`nombre`);

--
-- Indices de la tabla `tbtipoquimico`
--
ALTER TABLE `tbtipoquimico`
  ADD PRIMARY KEY (`idtipoquimico`),
  ADD UNIQUE KEY `nombre` (`nombre`);

--
-- Indices de la tabla `tbusuario`
--
ALTER TABLE `tbusuario`
  ADD PRIMARY KEY (`idusuario`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `tbacodo`
--
ALTER TABLE `tbacodo`
  MODIFY `idacodo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT de la tabla `tbcliente`
--
ALTER TABLE `tbcliente`
  MODIFY `idcliente` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `tbcolaborador`
--
ALTER TABLE `tbcolaborador`
  MODIFY `idcolaborador` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `tbfumigacion`
--
ALTER TABLE `tbfumigacion`
  MODIFY `idfumigacion` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `tblote`
--
ALTER TABLE `tblote`
  MODIFY `idlote` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `tbpedido`
--
ALTER TABLE `tbpedido`
  MODIFY `idpedido` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `tbpedidocliente`
--
ALTER TABLE `tbpedidocliente`
  MODIFY `idpedidocliente` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `tbpedidoclienteplanta`
--
ALTER TABLE `tbpedidoclienteplanta`
  MODIFY `idpedidoclienteplanta` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `tbplanta`
--
ALTER TABLE `tbplanta`
  MODIFY `idplanta` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT de la tabla `tbquimico`
--
ALTER TABLE `tbquimico`
  MODIFY `idquimico` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `tbsuministro`
--
ALTER TABLE `tbsuministro`
  MODIFY `idsuministro` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT de la tabla `tbtipoquimico`
--
ALTER TABLE `tbtipoquimico`
  MODIFY `idtipoquimico` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `tbacodo`
--
ALTER TABLE `tbacodo`
  ADD CONSTRAINT `tbacodo_ibfk_1` FOREIGN KEY (`idlote`) REFERENCES `tblote` (`idlote`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tbacodo_ibfk_2` FOREIGN KEY (`idplanta`) REFERENCES `tbplanta` (`idplanta`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tbacodo_ibfk_3` FOREIGN KEY (`idseccion`) REFERENCES `tbseccion` (`numeroseccion`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `tbfumigacion`
--
ALTER TABLE `tbfumigacion`
  ADD CONSTRAINT `tbcolaborador_ibfk_1` FOREIGN KEY (`idcolaborador`) REFERENCES `tbcolaborador` (`idcolaborador`),
  ADD CONSTRAINT `tbfumigacion_ibfk_2` FOREIGN KEY (`idlote`) REFERENCES `tblote` (`idlote`),
  ADD CONSTRAINT `tbfumigacion_ibfk_3` FOREIGN KEY (`idseccion`) REFERENCES `tbseccion` (`numeroseccion`),
  ADD CONSTRAINT `tbfumigacion_ibfk_4` FOREIGN KEY (`idquimico`) REFERENCES `tbquimico` (`idquimico`);

--
-- Filtros para la tabla `tbpedidocliente`
--
ALTER TABLE `tbpedidocliente`
  ADD CONSTRAINT `cliente` FOREIGN KEY (`idcliente`) REFERENCES `tbcliente` (`idcliente`),
  ADD CONSTRAINT `pedido` FOREIGN KEY (`idpedido`) REFERENCES `tbpedido` (`idpedido`) ON DELETE CASCADE;

--
-- Filtros para la tabla `tbpedidoclienteplanta`
--
ALTER TABLE `tbpedidoclienteplanta`
  ADD CONSTRAINT `tbpedidoclienteplanta_ibfk_1` FOREIGN KEY (`idplanta`) REFERENCES `tbplanta` (`idplanta`) ON DELETE CASCADE,
  ADD CONSTRAINT `tbpedidoclienteplanta_ibfk_2` FOREIGN KEY (`idpedidocliente`) REFERENCES `tbpedidocliente` (`idpedidocliente`) ON DELETE CASCADE;

--
-- Filtros para la tabla `tbplantaseccion`
--
ALTER TABLE `tbplantaseccion`
  ADD CONSTRAINT `idlote` FOREIGN KEY (`numerolote`) REFERENCES `tblote` (`idlote`),
  ADD CONSTRAINT `idplanta` FOREIGN KEY (`idplanta`) REFERENCES `tbplanta` (`idplanta`),
  ADD CONSTRAINT `idseccion` FOREIGN KEY (`numeroseccion`) REFERENCES `tbseccion` (`numeroseccion`);

--
-- Filtros para la tabla `tbquimico`
--
ALTER TABLE `tbquimico`
  ADD CONSTRAINT `tipoquimico` FOREIGN KEY (`idtipoquimico`) REFERENCES `tbtipoquimico` (`idtipoquimico`);

--
-- Filtros para la tabla `tbseccion`
--
ALTER TABLE `tbseccion`
  ADD CONSTRAINT `tbseccion_ibfk_1` FOREIGN KEY (`numerolote`) REFERENCES `tblote` (`idlote`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
