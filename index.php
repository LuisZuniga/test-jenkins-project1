<?php
    session_start();
    if(!isset($_SESSION['role'])){
        header("location:./view/login.php");
    }
?>

<!DOCTYPE html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Pagina principal</title>
    <link rel="stylesheet" href="./sources/css/bootstrap/bootstrap.min.css">
    <link rel="shortcut icon" href="./sources/images/Ornexp.png" type="image/x-icon">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="./sources/css/menu.css">
    <!--link rel="stylesheet" href="./sources/css/crud.css"-->
    <link rel="stylesheet" href="./sources/css/index.css">
    <link rel="stylesheet" href="./sources/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="./sources/css/all.css">
    <!--link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous"-->
    <script src="./sources/js/jquery-3.3.1.min.js" ></script>
    <script src="./sources/js/jquery.dataTables.min.js" ></script>
    <script src="./sources/js/menu.js" ></script>
    <script src="./sources/js/push.min.js" ></script>
    <script src="./sources/js/alertas.js" ></script>
    <script src="./sources/js/index.js" ></script>

</head>
    

<body>
    <header>
        <div class="logo"><img src="./sources/images/Ornexp.png" alt="ornexpsa"></div>
        <nav id="menu">
            <ul id="ul">
                <li class="menu-item active">
                    <a href="./index.php" class="menu-btn"><i class="fas fa-home"></i> Inicio</a>
                </li>
                <li class="menu-item">
                    <a href="./view/mapeoView.php" class="menu-btn"><i class="fas fa-map-marked-alt"></i> Mapeo</a>
                </li>
                <li class="menu-item">
                    <a href="./view/pedidoView.php" class="menu-btn"><i class="fas fa-luggage-cart"></i> Pedidos</a>
                </li>
                <li class="menu-item">
                    <a href="./view/plantaView.php" class="menu-btn"><i class="fas fa-seedling"></i> Plantas</a>
                </li>
                <li class="menu-item" id='quimico'>
                    <a href="#quimico" class="menu-btn" onmouseover="dropDown(this)" onmouseleave = "dropUp(this)" name="quimico"><i class="fas fa-flask"></i> Químicos <i id="carett" class="fa fa-caret-down" aria-hidden="true"></i></a>
                    <div class="sub-menu" >
                        <a href="./view/quimicoView.php">Químicos</a>
                        <a href="./view/tipoQuimicoView.php">Tipo de químicos</a>
                    </div>
                </li>
                <li class="menu-item">
                    <a href="./view/suministroView.php" class="menu-btn"><i class="fas fa-boxes"></i> Suministros</a>
                </li>
                <li class="menu-item">
                    <a href="./view/clienteView.php" class="menu-btn"><i class="fas fa-user-tie"></i> Clientes</a>
                </li>
                <li class="menu-item">
                    <a href="./view/colaboradorView.php" class="menu-btn"><i class="fas fa-people-carry"></i> Colaboradores</a>
                </li>
                <li class="menu-item" id="ajustes">
                    <a href="#ajustes" class="menu-btn ajustes" onmouseover="dropDown(this)" onmouseleave = "dropUp(this)" name="ajustes"><i class="fas fa-cog"></i> Ajustes <i id="caret" class="fa fa-caret-down" aria-hidden="true"></i></a>
                    <div class="sub-menu" >
                        <a href="./view/gestionarCuentaView.php"><i class="fas fa-users-cog"></i> Gestionar cuenta</a>
                        <a href="./view/logout.php"><i class="fas fa-power-off"></i> Cerrar sessión</a>
                    </div>
                </li>
            </ul>
        </nav>
        <div class="toggle">
            <i class="fas fa-bars menu" onclick=displayMenu()></i>
        </div>
    </header>
    <div class="wrapper">
        <section class="top-container">
            <div id="secciones-container" class="table-container"></div>
        </section>
        
        <section class="section-orders">
                <div id="pedidos-container" class="table-container"></div>
        </section>

        <section class="boxes">
            <div  class="box">
                <div id="quimicos-container" class="table-container"></div>
            </div>
            <div class="box">
                <div id="suministros-container" class="table-container"></div>
            </div>
            <!--div class="box">
                <i class="fas fa-tint fa-1x"></i>
                <h3>Riego</h3>
            </div-->
        </section>
        
        <footer>
            <p>Ornexp Derechos Reservados &copy; 2018-2019</p>
        </footer>
    </div>
</body>
</html>
