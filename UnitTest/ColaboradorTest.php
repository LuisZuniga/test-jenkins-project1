<?php

    declare(strict_types=1);

    use PHPUnit\Framework\TestCase;
    use ColaboradorData;
    require_once 'domain/Colaborador.php';
    final class ColaboradorTest extends TestCase{

        function testInsertarColaborador(){		
			$data = new ColaboradorData();
			$Colaborador = new Colaborador('404170602','Chris','Prendas','Chacon','Puerto Viejo','845626000');	
			$this->assertTrue($data->insertarTBColaborador($Colaborador));
        }

        function testBuscarColaboradorInsertado(){
            $data = new ColaboradorData();
            $id = $data->getLastId();
			$Colaborador = new Colaborador($id,'404170602','Chris','Prendas','Chacon','Puerto Viejo','845626000');	
			$this->assertEquals($Colaborador,$data->buscarColaborador("404170602"));		
        }
        
        function testActualizarColaborador(){		
			$data = new ColaboradorData();
            $id = $data->getLastId();
			$Colaborador = new Colaborador($id,'404170602','Christopher','Prendas','Chacon','Puerto Viejo','845626000');		
			$this->assertTrue($data->actualizarTBColaborador($Colaborador));
        }
        
        function testBuscarColaboradorActualizado(){
			$data = new ColaboradorData();
            $id = $data->getLastId();
			$Colaborador = new Colaborador($id,'404170602','Christopher','Prendas','Chacon','Puerto Viejo','845626000');	
			$this->assertEquals($Colaborador,$data->buscarColaborador("404170602"));		
        }

        function testEliminarColaborador(){		
			$data = new ColaboradorData();
			$cedula = "404170602";	
			$this->assertTrue($data->eliminarTBColaborador($cedula));
        }
        
        function testBuscarColaboradorEliminado(){	
			$data = new ColaboradorData();
			$this->assertEquals(null,$data->buscarColaborador("404170602"));		
        }
        
    }
