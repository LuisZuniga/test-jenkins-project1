<?php

    declare(strict_types=1);

    use PHPUnit\Framework\TestCase;
    use PlantaData;
    require_once 'domain/Planta.php';
    final class PlantaTest extends TestCase{

        function testInsertarPlanta(){		
			$data = new PlantaData();
			$planta = new Planta('0','planta','plantita','0','20.25');	
			$this->assertTrue($data->insertarTBPlanta($planta));
        }

        function testBuscarPlantaInsertada(){	
			$data = new PlantaData();	
			$buscar = "planta";
            $id = $data->getLastId();
			$planta = new Planta($id,'planta','plantita','0','20.25');
			$this->assertEquals($planta,$data->buscarPlanta($buscar));		
        }

        /*function testActualizarPlanta(){		
			$data = new PlantaData();
            $id = $data->getLastId();
			$planta = new Planta($id,'orquidea','Orchidaceace','0','20.25');	
			$this->assertTrue($data->actualizarTBPlanta($planta));
        }

        function testBuscarPlantaActualizada(){	
			$data = new PlantaData();	
			$buscar = "orquidea";
            $id = $data->getLastId();
			$planta = new Planta($id,'orquidea','Orchidaceace','0','20.25');
			$this->assertEquals($planta,$data->buscarPlanta($buscar));		
        }

        function testEliminarPlanta(){		
			$data = new PlantaData();
            $id = $data->getLastId();	
			$this->assertTrue($data->eliminarTBPlanta($id));
        }


        function testBuscarPlantaEliminada(){	
			$data = new PlantaData();	
			$buscar = "orquidea";
			$this->assertEquals(null,$data->buscarPlanta($buscar));	
        }*/

    }