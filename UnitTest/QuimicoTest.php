<?php

    declare(strict_types=1);

    use PHPUnit\Framework\TestCase;
    use QuimicoData;
    require_once 'domain/Quimico.php';

    final class QuimicoTest extends TestCase{

        public function testInsertTBQuimico(){
            $quimicoData = new QuimicoData();
            $idtipo = $quimicoData->getLastIdTQ();
	        $quimico = new Quimico(0,'prueba2',$idtipo,'#ffffff', 15, 200);
            $this->assertTrue($quimicoData->insertarTBQuimico($quimico));
        }

        function testBuscarQuimicoInsertado(){	
			$data = new QuimicoData();	
			$buscar = "prueba2";
            $id = $data->getLastId();
            $idtipo = $data->getLastIdTQ();
	        $Quimico = new Quimico($id,'prueba2',$idtipo,'#ffffff', 15, 200);
			$this->assertEquals($Quimico,$data->buscarTBQuimico($buscar));		
        }
        
        function testActualizarQuimico(){		
            $data = new QuimicoData();
            $idtipo = $data->getLastIdTQ();
	        $Quimico = new Quimico(0,'temp',$idtipo,'#000000', 16, 200);	
			$this->assertTrue($data->actualizarTBQuimico($Quimico));
        }
        
        function testBuscarQuimicoActualizado(){	
			$data = new QuimicoData();	
			$buscar = "temp";
            $id = $data->getLastId();
            $idtipo = $data->getLastIdTQ();
	        $Quimico = new Quimico($id,'temp',$idtipo,'#000000', 16, 200);
			$this->assertEquals($Quimico,$data->buscarTBQuimico($buscar));		
        }

        function testEliminarQuimico(){		
            $data = new QuimicoData();
            $id = $data->getLastId();	
			$this->assertTrue($data->eliminarTBQuimico($id));
        }

        function testBuscarQuimicoEliminado(){	
			$data = new QuimicoData();	
			$buscar = "temp";
			$this->assertEquals(null,$data->buscarTBQuimico($buscar));		
        }
    }
