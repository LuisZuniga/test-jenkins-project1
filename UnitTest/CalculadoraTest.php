<?php

    declare(strict_types=1);

    use PHPUnit\Framework\TestCase;
    use Calculadora;

    final class CalculadoraTest extends TestCase{

        public function testSuma(){
            $calc = new Calculadora();
            $this->assertEquals(
                9,$calc->sumar(5,4)
            );
        }

    }