<?php

    declare(strict_types=1);

    use PHPUnit\Framework\TestCase;
    use ClienteData;
    require_once 'domain/Cliente.php';
    final class QuimicoTest extends TestCase{

        /*function testInsertarCliente(){		
			$data = new ClienteData();
			$cliente = new Cliente(0,'202345679','Ruben','Porras','Sanchez','Rio Frio','63230013');	
			$this->assertTrue($data->insertarTBCliente($cliente));
        }

        function testBuscarClienteInsertado(){
            $data = new ClienteData();
            $id = $data->getLastId();
			$cliente = new Cliente($id,'202345679','Ruben','Porras','Sanchez','Rio Frio','63230013');
			$this->assertEquals($cliente,$data->buscarTBCliente("202345679"));		
        }*/
        
        function testActualizarCliente(){		
			$data = new ClienteData();
            $id = $data->getLastId();
			$cliente = new Cliente($id,'202345670','Ruchi','Porras','Sanchez','Rio Frio','63230013');	
			$this->assertTrue($data->actualizarTBCliente($cliente));
        }
        
        function testBuscarClienteActualizado(){
			$data = new ClienteData();
            $id = $data->getLastId();
			$cliente = new Cliente($id,'202345670','Ruchi','Porras','Sanchez','Rio Frio','63230013');
			$this->assertEquals($cliente,$data->buscarTBCliente("202345670"));		
        }

        function testEliminarCliente(){		
			$data = new ClienteData();
            $id = $data->getLastId();
			$this->assertTrue($data->eliminarTBCliente($id));
        }
        
        function testBuscarQuimicoEliminado(){	
			$data = new ClienteData();
			$this->assertEquals(null,$data->buscarTBCliente("202345670"));		
        }
        
    }