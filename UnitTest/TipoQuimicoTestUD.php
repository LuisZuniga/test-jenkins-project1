<?php

    declare(strict_types=1);

    use PHPUnit\Framework\TestCase;
    use TipoQuimicoData;
    require_once 'domain/TipoQumico.php';
    final class TipoQuimicoTest extends TestCase{

        /*function testInsertarTipoQuimico(){		
			$data = new TipoQuimicoData();
			$TipoQuimico = new TipoQuimico(0,'Algicida');	
			$this->assertTrue($data->insertarTBTipoQuimico($TipoQuimico));
        }

        function testBuscarTipoQuimicoInsertado(){
            $data = new TipoQuimicoData();
            $id = $data->getLastId();
			$TipoQuimico = new TipoQuimico($id,'Algicida');	
			$this->assertEquals($TipoQuimico,$data->buscarTipoQuimico($id));		
        }*/
        
        function testActualizarTipoQuimico(){		
			$data = new TipoQuimicoData();
            $id = $data->getLastId();
			$TipoQuimico = new TipoQuimico($id,'AlgicidaSecond');		
			$this->assertTrue($data->actualizarTBTipoQuimico($TipoQuimico));
        }
        
        function testBuscarTipoQuimicoActualizado(){
			$data = new TipoQuimicoData();
            $id = $data->getLastId();
			$TipoQuimico = new TipoQuimico($id,'AlgicidaSecond');	
			$this->assertEquals($TipoQuimico,$data->buscarTipoQuimico($id));		
        }

        function testEliminarTipoQuimico(){		
			$data = new TipoQuimicoData();
            $id = $data->getLastId();	
			$this->assertTrue($data->eliminarTBTipoQuimico($id));
        }
        
        function testBuscarTipoQuimicoEliminado(){	
			$data = new TipoQuimicoData();
            $id = $data->getLastId();	
			$this->assertEquals(null,$data->buscarTipoQuimico($id));		
        }
        
    }
