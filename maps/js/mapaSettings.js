function nuevoAjax() {
  var xmlhttp = false;
  try {
    xmlhttp = new XMLHttpRequest();
  } catch (E) {
    xmlhttp = false;
  }
  return xmlhttp;
}

/*
var cantPlantas =  getCantPlantas();

function getCantPlantas() {
  var c;
  var ajax = new nuevoAjax();
  ajax.open("POST", "../business/PlantaAction.php", true);
  ajax.onreadystatechange = function() {
    if (ajax.readyState === 4) {
      c = ajax.responseText;
      console.log(parseInt(ajax.responseText));
    }
  };
  ajax.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  ajax.send("total=1");
  return 0;
}*/


var map = AmCharts.makeChart( "mapdiv", {
    "type": "map",
    "dataProvider": {
      "map": "mapa",  
      "areas": [ 
        //----------------LOTE#1-------------------
        {
          id: "LOTE-1-CASA",
          color: "#F0B67F",
          
        }, {
          id: "LOTE-1-EMPACADORA",
          color: "#F0B67F"
        }, {
          id: "LOTE-1-SECCION-1",
          color: "#F0B67F"
        }, {
          id: "LOTE-1-SECCION-2",
          color: "#F0B67F"
        }, {
          id: "LOTE-1-SECCION-3",
          color: "#F0B67F"
        } , {
          id: "LOTE-1-SECCION-4",
          color: "#F0B67F"
        } , {
          id: "LOTE-1-SECCION-5",
          color: "#F0B67F"
        } , {
          id: "LOTE-1-SECCION-6",
          color: "#F0B67F"
        } ,{
          id: "LOTE-1-SECCION-7",
          color: "#F0B67F"
        } , {
          id: "LOTE-1-SECCION-8",
          color: "#F0B67F"
        } , {
          id: "LOTE-1-SECCION-9",
          color: "#F0B67F"
        } , {
          id: "LOTE-1-SECCION-10",
          color: "#F0B67F"
        } , {
          id: "LOTE-1-SECCION-11",
          color: "#F0B67F"
        } , {
          id: "LOTE-1-SECCION-12",
          color: "#F0B67F"
        } , {
          id: "LOTE-1-SECCION-13",
          color: "#F0B67F"
        } , {
          id: "LOTE-1-SECCION-14",
          color: "#F0B67F"
        } , {
          id: "LOTE-1-SECCION-15",
          color: "#F0B67F"
        }  , {
          id: "LOTE-1-SECCION-16",
          color: "#F0B67F"
        } , {
          id: "LOTE-1-SECCION-17",
          color: "#F0B67F"
        } , {
          id: "LOTE-1-SECCION-18",
          color: "#F0B67F"
        } , {
          id: "LOTE-1-SECCION-19",
          color: "#F0B67F"
        } , {
          id: "LOTE-1-SECCION-20",
          color: "#F0B67F"
        } , {
          id: "LOTE-1-SECCION-21",
          color: "#F0B67F"
        } , {
          id: "LOTE-1-SECCION-22",
          color: "#F0B67F"
        } , {
          id: "LOTE-1-SECCION-23",
          color: "#F0B67F"
        } , {
          id: "LOTE-1-SECCION-24",
          color: "#F0B67F"
        } , {
          id: "LOTE-1-SECCION-25",
          color: "#F0B67F"
        } 
        , {
          id: "LOTE-1-SECCION-26",
          color: "#F0B67F"
        },
      //----------------LOTE#2--------------------       
        {
          id: "LOTE-2-SECCION-1",
          color: "#D6D1B1"
        }, {
          id: "LOTE-2-SECCION-2",
          color: "#D6D1B1"
        }, {
          id: "LOTE-2-SECCION-3",
          color: "#D6D1B1"
        } , {
          id: "LOTE-2-SECCION-4",
          color: "#D6D1B1"
        } , {
          id: "LOTE-2-SECCION-5",
          color: "#D6D1B1"
        } , {
          id: "LOTE-2-SECCION-6",
          color: "#D6D1B1"
        } ,{
          id: "LOTE-2-SECCION-7",
          color: "#D6D1B1"
        } , {
          id: "LOTE-2-SECCION-8",
          color: "#D6D1B1"
        } , {
          id: "LOTE-2-SECCION-9",
          color: "#D6D1B1"
        } , {
          id: "LOTE-2-SECCION-10",
          color: "#D6D1B1"
        } , {
          id: "LOTE-2-SECCION-11",
          color: "#D6D1B1"
        } , {
          id: "LOTE-2-SECCION-12",
          color: "#D6D1B1"
        } , {
          id: "LOTE-2-SECCION-13",
          color: "#D6D1B1"
        } , {
          id: "LOTE-2-SECCION-14",
          color: "#D6D1B1"
        } ,
    //----------------LOTE#3--------------------       
        {
          id: "LOTE-3-SECCION-1",
          color: "#C7EFCF"
        }, {
          id: "LOTE-3-SECCION-2",
          color: "#C7EFCF"
        }, {
          id: "LOTE-3-SECCION-3",
          color: "#C7EFCF"
        } , {
          id: "LOTE-3-SECCION-4",
          color: "#C7EFCF"
        } , {
          id: "LOTE-3-SECCION-5",
          color: "#C7EFCF"
        } , {
          id: "LOTE-3-SECCION-6",
          color: "#C7EFCF"
        } ,{
          id: "LOTE-3-SECCION-7",
          color: "#C7EFCF"
        } , {
          id: "LOTE-3-SECCION-8",
          color: "#C7EFCF"
        } , {
          id: "LOTE-3-SECCION-9",
          color: "#C7EFCF"
        } , {
          id: "LOTE-3-SECCION-10",
          color: "#C7EFCF"
        } , {
          id: "LOTE-3-SECCION-11",
          color: "#C7EFCF"
        } , {
          id: "LOTE-3-SECCION-12",
          color: "#C7EFCF"
        } , {
          id: "LOTE-3-SECCION-13",
          color: "#C7EFCF"
        } , {
          id: "LOTE-3-SECCION-14",
          color: "#C7EFCF"
        } , {
          id: "LOTE-3-SECCION-15",
          color: "#C7EFCF"
        } , {
          id: "LOTE-3-SECCION-16",
          color: "#C7EFCF"
        } , {
          id: "LOTE-3-SECCION-17",
          color: "#C7EFCF"
        } , {
          id: "LOTE-3-SECCION-18",
          color: "#C7EFCF"
        } , {
          id: "LOTE-3-SECCION-19",
          color: "#C7EFCF"
        } , {
          id: "LOTE-3-SECCION-20",
          color: "#C7EFCF"
        } , {
          id: "LOTE-3-SECCION-21",
          color: "#C7EFCF"
        } , {
          id: "LOTE-3-SECCION-22",
          color: "#C7EFCF"
        } 
      ]
      
    },
      "areasSettings": {
        autoZoom: true,
        selectedColor: "#990000"          
      }/*,
             
      "legend": {
        width: 180,
        marginRight: 27,
        marginLeft: 27,
        equalWidths: false,
        backgroundAlpha: 0.5,
        backgroundColor: "#FFFFFF",
        borderColor: "#ffffff",
        borderAlpha: 1,
        top: 300,
        right: 700,
        maxColumns: 1,
        equalWidths: true,
        horizontalGap: 10,
        data: [ {
          title: "LOTE#1 - 26 Secciones",
          color: "#F0B67F"
        }, {
          title: "LOTE#2 - 14 Secciones",
          color: "#D6D1B1"
        }, {
          title: "LOTE#3 - 22 Secciones",
          color: "#C7EFCF"
        },  {
          title: "# Plantas -"+ cantPlantas +" plantas",
          color: "#C7EFCF"
        } ]
      }*/
  }); 
  map.addListener("init", function() {
    setTimeout( function() {
      map.dataProvider.images = [];
      for ( x in map.dataProvider.areas ) {
        var area = map.dataProvider.areas[x];
        var image = new AmCharts.MapImage();
        if(area.title=="LOTE-1/SECCION-2"){
          //alert("entro"+map.getAreaCenterLatitude(area));
          image.latitude = map.getAreaCenterLatitude(area)-0.04;
          image.longitude = map.getAreaCenterLongitude(area);
        }else if (area.title=="LOTE-1/SECCION-7"){
          image.latitude = map.getAreaCenterLatitude(area);
          image.longitude = map.getAreaCenterLongitude(area)-0.04;               
        }else if (area.title=="LOTE-1/SECCION-9"){
          image.latitude = map.getAreaCenterLatitude(area);
          image.longitude = map.getAreaCenterLongitude(area)-0.06;        
        }else if (area.title=="LOTE-1/SECCION-10"){
          image.latitude = map.getAreaCenterLatitude(area);
          image.longitude = map.getAreaCenterLongitude(area)-0.06;        
        }else if (area.title=="LOTE-1/SECCION-11"){
          image.latitude = map.getAreaCenterLatitude(area);
          image.longitude = map.getAreaCenterLongitude(area)-0.06;        
        }else if (area.title=="LOTE-1/SECCION-12"){
          image.latitude = map.getAreaCenterLatitude(area);
          image.longitude = map.getAreaCenterLongitude(area)-0.06;        
        }else if (area.title=="LOTE-1/SECCION-13"){
          image.latitude = map.getAreaCenterLatitude(area);
          image.longitude = map.getAreaCenterLongitude(area)-0.06;        
        }else if (area.title=="LOTE-1/SECCION-14"){
          image.latitude = map.getAreaCenterLatitude(area);
          image.longitude = map.getAreaCenterLongitude(area)-0.06;        
        }else if (area.title=="LOTE-1/SECCION-15"){
          image.latitude = map.getAreaCenterLatitude(area);
          image.longitude = map.getAreaCenterLongitude(area)-0.06;        
        }else if (area.title=="LOTE-1/SECCION-16"){
          image.latitude = map.getAreaCenterLatitude(area);
          image.longitude = map.getAreaCenterLongitude(area)-0.06;        
        }else if (area.title=="LOTE-1/SECCION-17"){
          image.latitude = map.getAreaCenterLatitude(area);
          image.longitude = map.getAreaCenterLongitude(area)-0.06;        
        }else if (area.title=="LOTE-1/SECCION-18"){
          image.latitude = map.getAreaCenterLatitude(area);
          image.longitude = map.getAreaCenterLongitude(area)-0.06;        
        }else if (area.title=="LOTE-1/SECCION-19"){
          image.latitude = map.getAreaCenterLatitude(area);
          image.longitude = map.getAreaCenterLongitude(area)-0.06;        
        }else if (area.title=="LOTE-1/SECCION-20"){
          image.latitude = map.getAreaCenterLatitude(area);
          image.longitude = map.getAreaCenterLongitude(area)-0.06;        
        }else if (area.title=="LOTE-1/SECCION-21"){
          image.latitude = map.getAreaCenterLatitude(area);
          image.longitude = map.getAreaCenterLongitude(area)-0.06;        
        }else if (area.title=="LOTE-1/SECCION-24"){
          image.latitude = map.getAreaCenterLatitude(area);
          image.longitude = map.getAreaCenterLongitude(area)-0.09;        
        }else if (area.title=="LOTE-1/SECCION-25"){
          image.latitude = map.getAreaCenterLatitude(area);
          image.longitude = map.getAreaCenterLongitude(area)-0.06;        
        }else if (area.title=="LOTE-2/SECCION-3"){
          image.latitude = map.getAreaCenterLatitude(area);
          image.longitude = map.getAreaCenterLongitude(area)-0.06;        
        }else if (area.title=="LOTE-2/SECCION-4"){
          image.latitude = map.getAreaCenterLatitude(area);
          image.longitude = map.getAreaCenterLongitude(area)-0.06;        
        }else if (area.title=="LOTE-2/SECCION-5"){
          image.latitude = map.getAreaCenterLatitude(area);
          image.longitude = map.getAreaCenterLongitude(area)-0.06;        
        }else if (area.title=="LOTE-2/SECCION-6"){
          image.latitude = map.getAreaCenterLatitude(area);
          image.longitude = map.getAreaCenterLongitude(area)-0.06;        
        }else if (area.title=="LOTE-2/SECCION-7"){
          image.latitude = map.getAreaCenterLatitude(area);
          image.longitude = map.getAreaCenterLongitude(area)+0.02;        
        }else if (area.title=="LOTE-2/SECCION-10"){
          image.latitude = map.getAreaCenterLatitude(area);
          image.longitude = map.getAreaCenterLongitude(area)-0.099;        
        }else if (area.title=="LOTE-2/SECCION-11"){
          image.latitude = map.getAreaCenterLatitude(area)-0.09;
          image.longitude = map.getAreaCenterLongitude(area)-0.09;        
        }else if (area.title=="LOTE-2/SECCION-12"){
          image.latitude = map.getAreaCenterLatitude(area);
          image.longitude = map.getAreaCenterLongitude(area)-0.09;        
        }else if (area.title=="LOTE-2/SECCION-13"){
          image.latitude = map.getAreaCenterLatitude(area)+0.09;
          image.longitude = map.getAreaCenterLongitude(area);        
        }else if (area.title=="LOTE-2/SECCION-14"){
          image.latitude = map.getAreaCenterLatitude(area)+0.06;
          image.longitude = map.getAreaCenterLongitude(area);             
        }else if (area.title=="LOTE-3/SECCION-1"){
          image.latitude = map.getAreaCenterLatitude(area);
          image.longitude = map.getAreaCenterLongitude(area)-0.04;        
        }else if (area.title=="LOTE-3/SECCION-2"){
          image.latitude = map.getAreaCenterLatitude(area);
          image.longitude = map.getAreaCenterLongitude(area)-0.04;        
        }else if (area.title=="LOTE-3/SECCION-3"){
          image.latitude = map.getAreaCenterLatitude(area);
          image.longitude = map.getAreaCenterLongitude(area)-0.04;        
        }else if (area.title=="LOTE-3/SECCION-4"){
          image.latitude = map.getAreaCenterLatitude(area);
          image.longitude = map.getAreaCenterLongitude(area)-0.04;        
        }else if (area.title=="LOTE-3/SECCION-5"){
          image.latitude = map.getAreaCenterLatitude(area);
          image.longitude = map.getAreaCenterLongitude(area)-0.04;        
        }else if (area.title=="LOTE-3/SECCION-6"){
          image.latitude = map.getAreaCenterLatitude(area);
          image.longitude = map.getAreaCenterLongitude(area)-0.06;        
        }else if (area.title=="LOTE-3/SECCION-7"){
          image.latitude = map.getAreaCenterLatitude(area);
          image.longitude = map.getAreaCenterLongitude(area)-0.05;        
        }else if (area.title=="LOTE-3/SECCION-8"){
          image.latitude = map.getAreaCenterLatitude(area);
          image.longitude = map.getAreaCenterLongitude(area)-0.05;        
        }else if (area.title=="LOTE-3/SECCION-9"){
          image.latitude = map.getAreaCenterLatitude(area);
          image.longitude = map.getAreaCenterLongitude(area)-0.05;        
        }else if (area.title=="LOTE-3/SECCION-10"){
          image.latitude = map.getAreaCenterLatitude(area);
          image.longitude = map.getAreaCenterLongitude(area)-0.07;        
        }else if (area.title=="LOTE-3/SECCION-11"){
          image.latitude = map.getAreaCenterLatitude(area);
          image.longitude = map.getAreaCenterLongitude(area)-0.2;        
        }else if (area.title=="LOTE-3/SECCION-12"){
          image.latitude = map.getAreaCenterLatitude(area);
          image.longitude = map.getAreaCenterLongitude(area)-0.09;        
        }else if (area.title=="LOTE-3/SECCION-13"){
          image.latitude = map.getAreaCenterLatitude(area);
          image.longitude = map.getAreaCenterLongitude(area)-0.09;        
        }else if (area.title=="LOTE-3/SECCION-14"){
          image.latitude = map.getAreaCenterLatitude(area);
          image.longitude = map.getAreaCenterLongitude(area)-0.09;        
        }else if (area.title=="LOTE-3/SECCION-15"){
          image.latitude = map.getAreaCenterLatitude(area);
          image.longitude = map.getAreaCenterLongitude(area)-0.09;        
        }else if (area.title=="LOTE-3/SECCION-16"){
          image.latitude = map.getAreaCenterLatitude(area);
          image.longitude = map.getAreaCenterLongitude(area)-0.09;        
        }else if (area.title=="LOTE-3/SECCION-17"){
          image.latitude = map.getAreaCenterLatitude(area);
          image.longitude = map.getAreaCenterLongitude(area)-0.09;        
        }else if (area.title=="LOTE-3/SECCION-18"){
          image.latitude = map.getAreaCenterLatitude(area);
          image.longitude = map.getAreaCenterLongitude(area)-0.09;        
        }else if (area.title=="LOTE-3/SECCION-19"){
          image.latitude = map.getAreaCenterLatitude(area);
          image.longitude = map.getAreaCenterLongitude(area)-0.09;        
        }else if (area.title=="LOTE-3/SECCION-20"){
          image.latitude = map.getAreaCenterLatitude(area);
          image.longitude = map.getAreaCenterLongitude(area)-0.09;        
        }else{
          image.latitude = map.getAreaCenterLatitude(area);
          image.longitude = map.getAreaCenterLongitude(area);
        }
        image.label = area.title.substr(15);
        image.linkToObject = area;
        map.dataProvider.images.push( image );
      }
      map.validateData();
    }, 100 )
  } );